/**
 * ID : School.RoomAp
 * require : School.Model.School
 * require : School.View.Level
 */
Modus.School.RoomApp = Backbone.View.extend(
{
    tagName:  "div",
    className : "row",
    template: _.template(document.getElementById('tmpl-admin-school').innerHTML),
    School : null,

    initialize: function(param) {
        var that = this;
        that.School = new Modus.School.Model.School();
        that.School.url = Modus.uriRoot+'/schools/'+param.schoolId;
        that.$el.html('<center style="margin-top: 10%;"><h1><i class="fa fa-circle-o-notch fa-spin"></i> Cargando...</h1></center>');
        that.loadSchool();
    },

    loadSchool : function()
    {
        var that = this;
        that.$('.fa.fa-refresh').addClass('fa-spinner');
        that.School.fetch(
                {
                    success:
                        $.proxy(that.render,that),
                    error : function(model,xhr)
                    {
                        var msg = Modus.Helper.getMsgXhr(xhr,"No se pudo cargar información del colegio");
                        that.$el.html('<div style="margin-top:10%;color:red"><h2 class="text-center"><i class="fa fa-times-circle"></i> Ocurrio un error<br/>'+msg+'<br/><br/><a class="reload"><i class="fa fa-refresh"></i> Reintentar</a></h2></div>')
                    }
                }
        );
    },

    render: function() 
    {
        if(this.School && this.School.get('name'))
        {

            this.$el.html($(this.template({school:this.School.toJSON()})));
            this.alert = $("#alert-rooms");
            this.Levels = this.School.getLevels();
            this.levels = this.$('.levels-list');
            this.addAll();
        }
        return this;
    },

    events: {
        "click .reload" : "loadSchool",
    },

    showMsgXhr : function(xhr, msg)
    {
        var msgs = Modus.Helper.getMsgXhr(xhr, msg);
        this.showMsg(msgs, "danger");
    },

    showMsg : function(msj, css)
    {
        if(this.alert)
        {
            this.alert.removeClass(function (index, css) {return (css.match (/\balert-\S+/g) || []).join(' ');});
            this.alert.addClass("alert-" + (css || 'warning'));
            this.alert.html(msj);
            this.alert.show();
        }
        else
        {
            alert(msj);
        }
    },

    hideMsg : function()
    {
        this.alert.hide();
    },

    addOne: function(level) {
        var view = new Modus.School.View.Level({model: level,school_id:this.School.id,AppRooms:this});
        this.levels.append(view.render().el);
    },
    addAll: function() {
        if(this.Levels)
        {
            this.Levels.each(this.addOne, this);

            if(this.Levels.length == 1)
            {
                this.$('.col-sm-6').addClass('col-sm-8 col-sm-offset-2').removeClass('col-sm-6');
            }
        }
    }

});