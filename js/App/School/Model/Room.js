/**
 * ID : School.Model.Room
 * require : School.Model.Grade
 * require : School.Model.School
 */
Modus.School.Model.Room = Backbone.Model.extend(
{
    initialize: function(attrs, opts){
        this.set('id',attrs.id);
        this.set('name',attrs.name);
        
        if(attrs.grade)
            this.set('grade',new Modus.School.Model.Grade(attrs.grade));
        if(attrs.school)
            this.set('school', new Modus.School.Model.School(attrs.school));
        if(attrs.teams && Modus.ProjectAdmin && Modus.ProjectAdmin.Model.TeamList)
            this.set('teams', new Modus.ProjectAdmin.Model.TeamList(attrs.teams));
        if(attrs.instructors && Modus.Instructor && Modus.Instructor.Model.InstructorList)
            this.set('instructors', new Modus.Instructor.Model.InstructorList(attrs.instructors));
    },
    parse : function(response, options){
        var resp = response.success||response;
        return resp;
    }
});