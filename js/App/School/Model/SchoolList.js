/**
 * ID : School.Model.SchoolList
 * require : School.Model.School
 */
Modus.School.Model.SchoolList = Backbone.Collection.extend({
  model : Modus.School.Model.School,
  url: Modus.uriRoot+'/schools',
  parse : function(response, options){
        return response.success?response.success:response;
    }
});