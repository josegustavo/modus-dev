/**
 * ID : School.Model.Grade
 * require : School.Model.RoomList
 */
Modus.School.Model.Grade = Backbone.Model.extend(
{
    initialize: function(attrs, opts){
        attrs.rooms && this.set('rooms', new Modus.School.Model.RoomList(attrs.rooms) );
        //attrs.level && this.set('level', new Model.Level(attrs.level));
    },
    getRooms : function(){
        return this.get('rooms');
    },
    getRoom : function(idRoom){
        return this.getRooms().get(idRoom);
    }
    
});