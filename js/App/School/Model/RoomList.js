/**
 * ID : School.Model.RoomList
 * require : School.Model.Room
 */
Modus.School.Model.RoomList = Backbone.Collection.extend(
{
    model : Modus.School.Model.Room,
    url : Modus.uriRoot+'/rooms',
    comparator: function(c){
            return c.get('name');
    },    
    parse : function(response, options){
        return response.success?response.success:response;
    },
    getFirstTeam : function()
    {
        var resp = null;
        this.each(function(room){
            if(room.get('teams')){
                room.get('teams').each(function(team){
                    if(team.id>0 && !resp) resp = team;
                },this);
            }
        },this);
        return resp;
    }
});