/**
 * ID : School.View.Creator
 */
Modus.School.View.Creator = Backbone.View.extend(
{
    tagName :  "tr",
    className : "",
    template : _.template(document.getElementById('tmpl-admin-school-creator').innerHTML),
    events : {
      "click .toggle"   : "toggleDone",
      "click a.delete"  : "clear",
    },

    initialize : function(params)
    {
        this.AppSchoolCreators = params.AppSchoolCreators;
        this.listenTo(this.model, 'change', this.render);
        this.listenTo(this.model, 'destroy', this.remove);
    },

    render : function() 
    {
        this.$el.html(this.template(this.model.toJSON()));
        return this;
    },

    clear: function()
    {
        if(confirm("¿Est\u00e1 seguro que desea eliminar al creador?"))
        {
            var that = this;
            this.model.destroy({wait: true, 
                success : function()
                {
                    that.trigger("remove");
                },
                error : function(model,response){
                    var resp = response.responseJSON;
                    alert(resp.error?resp.error:'No se pudo eliminar.');
            }});
        }
    }
});