/**
 * ID : School.View.Room
 */
Modus.School.View.Room = Backbone.View.extend(
{
    tagName:  "tr",
    className : "input-room input-group-sm",
    template: _.template(document.getElementById('tmpl-admin-school-level-grade-room').innerHTML),

    events: 
    {
      "click .toggle"   : "toggleDone",
      "dblclick .view"  : "edit",
      "click .action-edit"  : "edit",
      "click .action-delete"  : "clear",
      "keypress .edit"  : "updateOnEnter",
      "blur .edit"      : "close"
    },

    initialize: function(param)
    {
        this.schoolId = param.schoolId || 0;
        this.levelId = param.levelId || 0;
        this.gradeId = param.gradeId || 0;
        this.AppRooms = param.AppRooms;
        this.listenTo(this.model, 'change', this.render);
        this.listenTo(this.model, 'destroy', this.remove);
    },

    render: function()
    {
        var toTempl = this.model.toJSON();
        toTempl.schoolId = this.schoolId;
        toTempl.levelId = this.levelId;
        toTempl.gradeId = this.gradeId;
        this.$el.html(this.template(toTempl));
        this.input = this.$('.edit');
        return this;
    },

    edit: function() 
    {
      this.$el.addClass("editing");
      this.lastinput = this.input.val();
      this.input.focus();
    },

    close: function(e) 
    {
        var that = this;
        var value = that.input.val().toUpperCase();
        if(that.$el.hasClass('editing'))
        {
            if(value==that.lastinput.toUpperCase())
            {
                that.$el.removeClass("editing");
            }
            else
            {
                if (!value) 
                {
                  that.clear();
                }
                else 
                {
                    that.model.save({name: value},
                        {
                            wait: true,
                            success : function()
                            {
                                that.AppRooms.showMsg("Se modificó la sección \"" + that.lastinput +"\" a \""+value+"\".","success");
                                that.$el.removeClass("editing");
                                that.lastinput = value;
                            },
                            error : function(model, xhr)
                            {
                                that.input.val(that.lastinput);
                                that.$el.removeClass("editing");
                                that.AppRooms.showMsgXhr(xhr, "No se pudo modificar la sección.");
                            }
                        }
                    );
                }
            }
        }
        return false;
    },

    updateOnEnter: function(e) {
      if (e.keyCode == 13)
      {
          this.input.trigger("blur");
          return false;
      }
    },

    clear: function() {
        var that = this;
        var count_instructors = that.model.get('count_instructors');
        var count_students = that.model.get('count_students');
        if(count_instructors>0 || count_students>0)
        {
            that.AppRooms.showMsg("Esta sección no se puede eliminar, debido a que tiene " 
                    + ((count_instructors>0)?(count_instructors + " docente" + ((count_instructors==1)?"":"s")):"")
                    + ((count_instructors>0&&count_students>0)?" y ":"")
                    + ((count_students>0)?(count_students + " alumno"+ ((count_students==1)?"":"s")):""),"danger");
        }
        else
        {
            var name = that.model.get('name');
            that.model.destroy(
                {
                    wait: true, 
                    success : function(model,xhr)
                    {
                        that.AppRooms.showMsg("Se eliminó la sección \"" + name +"\".","success");
                    },
                    error : function(model,response)
                    {
                        that.input.val(that.lastinput);
                        that.AppRooms.showMsgXhr(response, "No se pudo eliminar la sección");
                    }
                }
            );
        }   
    }
});