/**
 * ID : Year.Model.YearList
 * require : Year.Model.Year
 */
Modus.Year.Model.YearList = Backbone.Collection.extend(
{
    url : Modus.uriRoot + '/years',
    model : Modus.Year.Model.Year,
    parse : function(response, options)
    {
        return response.success?response.success:response;
    }
});