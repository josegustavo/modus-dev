/**
 * ID Boot
 * require GeneralWorkspace
 */

(function()
{
    "use strict";

    $(document).on("click", "a[modus-link]", function(event)
    {
        if (!event.altKey && !event.ctrlKey && !event.metaKey && !event.shiftKey) {
            if(this.hasAttribute('modus-link') && !this.hasAttribute('target')) {
                event.preventDefault();
                var url = this.getAttribute('href').replace(Modus.uriRoot, "");
                if(url=='/')url='/?';//fix complete slash
                Modus.GeneralRouter.navigate(url, { trigger: true });
            }
        }
    });
    $(document).on("click", "a[modus-link-back]", function(event)
    {
        history.back();
    });
    
    $( document ).ajaxStart(function() {
        NProgress.inc();
    });
    $( document ).ajaxStop(function() {
        NProgress.done();
    });
    $('.year-select').on("click", function()
    {
        var btn = $(this);
        var id = btn.attr('y-id');
        var url = document.URL;
        window.location.href = Modus.uriRoot + '/years/' + id + '?url=' + url;
    })
    
    $('body').tooltip({
        selector: 'a[rel="tooltip"], [data-toggle="tooltip"]'
    });
    
    Modus.Cache = Modus.Cache || {};
    
    
    Backbone.emulateHTTP = true;Backbone.emulateJSON = true;
    
    Modus.GeneralRouter = new Modus.GeneralWorkspace();
    
    Backbone.history.start({pushState: true, root: Modus.uriRoot});
})();