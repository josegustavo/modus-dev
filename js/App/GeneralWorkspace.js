Modus.GeneralWorkspace = Backbone.Router.extend(
{
    routes: {
        //General
        "" : "home",
        "/" : "home",
        "home" : "home",
        "home/" : "home",
        "home/:page" : "home",
        "home/:page/:school_id/:school" : "home",
        "home/:page/:school_id/:school/:grade_id/:grade" : "home",
        "home/:page/:school_id/:school/:grade_id/:grade/:search" : "home",
        "user" : "adminUser",
        "user/:role/:page" : "adminUser",
        "user/:role/:page/:search" : "adminUser",
        //Proyectos
        "project" : "indexProject",
        "project/" : "indexProject",
        "project/add" : "addProject",
        "project/add/:tab" : "addProject",
        "project/edit/:id/:slugProject" : "editProject",
        "project/edit/:id/:slugProject/:tab" : "editProject",
        "project/view/:id/:slug": "viewResumeProject",
        "project/view/:id/:slug/logs": "viewLogProject",
        "project/view/:id/:slug/team/:nameRoom/:ordTeam": "viewResumeProject",
        "project/view/:id/:slug/team/:nameRoom/:ordTeam/:namePhase": "viewResumeProject",
        "project/view/:id/:slug/dashboard/:nameRoom/:pageDash": "viewDashboardProject",
        "project/view/:id/:slug/marks/:nameRoom": "viewMarksProject",
        //Años
        "year" : "indexYear",
        //Colegios
        "school" : "indexSchool",
        "school/:id/:option" : "indexSchool",
        //Coordinadores
        "coordinator" : "indexCoordinator",
        "coordinator/:coordinatorId" : "indexCoordinator",
        //Creadores
        "creator" : "indexCreator",
        "creator/:coordinatorId" : "indexCreator",
        //Estudiantes
        "student" : "indexStudent",
        "student/:schoolId" : "indexStudent",
        "student/:schoolId/:levelId/:gradeId" : "indexStudent",
        "student/:schoolId/:levelId/:gradeId/:roomId" : "indexStudent",
        //Profesores
        "instructor" : "indexInstructor",
        "instructor/:schoolId" : "indexInstructor",
        "instructor/:schoolId/:levelId/:gradeId" : "indexInstructor",
        "instructor/:schoolId/:levelId/:gradeId/:roomId" : "indexInstructor",
        //Estrategias
        "strategy" : "indexStrategy",
        "strategy/:action" : "indexStrategy",
        "strategy/:action/:strategyId" : "indexStrategy",
        //Herramientas
        "tool" : "indexTool",
        "tool/:action" : "indexTool",
        "tool/:action/:toolId" : "indexTool",
        //Fases
        "phase" : "indexPhase",
        "phase/:view" : "indexPhase",
        "phase/:add" : "indexPhase",
        "phase/:edit/:toolId" : "indexPhase"
    },
    adminUser : function(role, page, search)
    {
        var appUsers = new Modus.User.App({role:role,page:page,search:search});
        $(".main-container").html(appUsers.render().el);
        $('body').scrollTop(0);
    },

    home : function(page, school_id, school, grade_id, grade, search)
    {
        var index = new Modus.Index.App(
                {
                    page : page, 
                    school_id : school_id,
                    school : school,
                    grade_id : grade_id,
                    grade : grade,
                    search:search
                }
        );
        $(".main-container").html(index.render().el);
        $('body').scrollTop(0);
    },
    viewResumeProject : function( id, slug,  nameRoom, ordTeam, namePhase)
    {
        var options = {type: "resume", id : id, slug : slug, nameRoom : nameRoom, ordTeam : ordTeam, namePhase : namePhase};
        this.viewProject(options);
    },
    viewDashboardProject : function(id, slug,  nameRoom, pageDash)
    {
        var page = (pageDash=='general')?'general':'progress';
        var options = {type: "dashboard", id : id, slug : slug, nameRoom : nameRoom, pageDash : page};
        this.viewProject(options);
    },
    viewMarksProject : function(id, slug,  nameRoom)
    {
        var options = {type: "marks", id : id, slug : slug, nameRoom : nameRoom}
        this.viewProject(options);
    },
    viewLogProject : function(id, slug)
    {
        var options = {type: "logs", id : id, slug : slug}
        this.viewProject(options);
    },
    viewProject : function(options)
    {
        var projectAppView =  new Modus.Project.App(options);
        $(".main-container").html(projectAppView.render().el);
        $('body').scrollTop(0);
    },
    addProject : function(tab){
        document.title = ('Modus: Nuevo Proyecto');
        var addProject = new Modus.ProjectAdmin.App({isEdit:false,tab:tab});
        $(".main-container").html(addProject.render().el);
        $('body').scrollTop(0);
    },
    editProject : function(id, slug,tab)
    {
        document.title = ('Modus: Editar Proyecto');
        var addProject = new Modus.ProjectAdmin.App({isEdit:true, id:id ,slug:slug, tab:tab });
        $(".main-container").html(addProject.render().el);
        $('body').scrollTop(0);
    },
    indexYear : function()
    {
        document.title = ('Modus: Periodos');
        var appYears = new Modus.Year.App();
        $(".main-container").html(appYears.el);
        $('body').scrollTop(0);
    },
    indexSchool : function(school_id, option)
    {
        document.title = ('Modus: Colegios');
        var appSchools;
        if(school_id>0)
        {
            if(option=="coordinators")
            {
                appSchools = new Modus.School.CoordinatorApp({schoolId:school_id});
            }
            else if(option=="creators")
            {
                appSchools = new Modus.School.CreatorApp({schoolId:school_id});
            }
            else
            {
                appSchools = new Modus.School.RoomApp({schoolId:school_id});
            }
        }else{
            appSchools = new Modus.School.App();
        }        
        $(".main-container").html(appSchools.render().el);
        $('body').scrollTop(0);
    },
    indexStrategy : function(action,id){
        document.title = ('Modus: Estrategias');
        var appStrategy = new Modus.Strategy.App({tab:action,strategyId:id});
        $(".main-container").html(appStrategy.render().el);
        $('body').scrollTop(0);
    },
    indexTool : function(action,id){
        document.title = ('Modus: Herramientas');
        var appTool = new Modus.Tool.App({tab:action,toolId:id});
        $(".main-container").html(appTool.render().el);
        $('body').scrollTop(0);
    },
    indexPhase : function(action,id){
        document.title = ('Modus: Fases');
        var appPhase = new Modus.Phase.App({tab:action,phaseId:id});
        $(".main-container").html(appPhase.render().el);
        $('body').scrollTop(0);
    },
    indexCoordinator : function(coordinatorId){
        document.title = ('Modus: Coordinadores');
        var appCoordinator;
        if(coordinatorId>0)
        {
            appCoordinator = new Modus.Coordinator.SchoolApp({coordinatorId: coordinatorId});
        }
        else
        {
            appCoordinator = new Modus.Coordinator.App();
        }
        $(".main-container").html(appCoordinator.el);
        $('body').scrollTop(0);
    },
    indexCreator : function(creatorId){
        document.title = ('Modus: Creadores');
        var appCreator;
        if(creatorId>0)
        {
            appCreator = new Modus.Creator.SchoolApp({creatorId: creatorId});
        }
        else
        {
            appCreator = new Modus.Creator.App();
        }
        $(".main-container").html(appCreator.el);
        $('body').scrollTop(0);
    },
    indexStudent : function(school_id,level_id,grade_id,room_id){
        document.title = ('Modus: Alumnos');
        var appStudent = new Modus.Student.App({schoolId:school_id,levelId:level_id,gradeId:grade_id,roomId:room_id});
        $(".main-container").html(appStudent.render().el);
        $('body').scrollTop(0);
    },
    indexInstructor : function(school_id,level_id,grade_id,room_id){
        document.title = ('Modus: Profesores');
        var appInstructor = new Modus.Instructor.App({schoolId:school_id,levelId:level_id,gradeId:grade_id,roomId:room_id});
        $(".main-container").html(appInstructor.render().el);
        $('body').scrollTop(0);
    }
});