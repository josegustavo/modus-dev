/**
 * ID : Student.Model.Student
 */
Modus.Student.Model.Student = Backbone.Model.extend(
{
    initialize : function(attrs,opts)
    {
        if(attrs && attrs.name && attrs.lastname)
        {
            var full_name = attrs.name + ' ' + attrs.lastname;
            this.set('full_name',full_name);
        }
        this.set('isSelected',false);
    },
    parse : function(response, options)
    {
        return response.success?response.success:response;
    },
    defaults : {
            isSelected : false
    },
    select : function(){
        this.set('isSelected',true);  
    },
    noSelect : function(){
        this.set('isSelected',false);
    },
    isSelected : function(){
        return this.get('isSelected');
    },
    hasOtherChanged : function()
    {
        return this.get("isChanged");
    }
});