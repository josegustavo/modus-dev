/**
 * ID : User.App
 * require : User.Model.User
 * require : User.Controller.PageableUsers
 * require : Common.Modus.GeneralRouter
 */
Modus.User.App = Backbone.View.extend(
{
    tagName:  "div",
    className : "row",
    template: _.template(document.getElementById('tmpl-admin-users').innerHTML),

    initialize: function(params)
    {
        document.title = ('Modus: Administrar usuarios');
        var that = this;
        this.role = params.role || 'all';
        this.page = params.page || 1;
        this.search = params.search || "";

        this.pageableUsers = new Modus.User.Controller.PageableUsers();

        this.pageableUsers.on("sync",function()
        {
            if(that.pageableUsers.length === 0)
            {
                that.$('.backgrid tbody').html('<tr class="error"><td colspan="7" style="display:table-cell"><center style="margin-top:10%;margin-bottom:10%;color:red"><h4><i class="fa fa-exclamation-triangle"></i> No se encontraron resultados</h4></center></td></tr>');
            }
            else
            {
                that.$('.backgrid tbody .error').remove();
            }
        });

        this.pageableGrid = new Backgrid.Grid({
            columns : [{
                name: "id",
                label: "ID",
                editable: false,
                cell: "string"
                }, {
                name: "username",
                label: "Usuario",
                editable: false,
                cell: "string"
                }, {
                name: "firstname",
                label: "Nombre",
                editable: false,
                cell: "string"
                }, {
                name: "lastname",
                label: "Apellido",
                editable: false,
                cell: "string"
                }, {
                name: "email",
                label: "Email",
                editable: false,
                cell: "string"
                }, {
                name: "role",
                label: "Rol",
                cell: Backgrid.StringCell.extend({
                    render: function() {
                        var role = this.model.get('role');
                        var text = '';
                        switch(role)
                        {
                            case 'student': text = "Alumno"; break;
                            case 'instructor': text = "Docente"; break;
                            case 'creator': text = "Creador"; break;
                            case 'coordinator': text = "Coordinador"; break;
                            case 'admin': text = "Administrador"; break;
                        }
                        this.$el.text(text);
                        return this;
                    }
                }),
                editable: false,
                sortable: false,
                }, {
                name: "infoHTML",
                label: "",
                cell: Backgrid.StringCell.extend({
                    render: function() {
                        var active = this.model.get('active');
                        if(active)
                        {
                            this.$el.html("");
                            var role = this.model.get('role');
                            if( (role == 'student' || role == 'instructor') && this.model.get('info') && this.model.get('link'))
                            {
                                var url = '', link = this.model.get('link');
                                if(role == 'student')
                                {
                                    url = Modus.uriRoot + '/student/' + link.join('/');
                                }else if(role == 'instructor')
                                {
                                    url = Modus.uriRoot + '/instructor/' + link.join('/');
                                }
                                this.$el.html('<a modus-link href="'+url+'"><i class="fa fa-info-circle" title="'+this.model.get('info')+'"></i></a>&nbsp;');
                            }
                            else if(role == 'admin' || role == 'creator' || role == 'coordinator')
                            {
                                if(this.model.get('info'))
                                {
                                    var url = '';
                                    if(role == 'coordinator')
                                    {
                                        url = Modus.uriRoot + '/coordinator/' + this.model.id;
                                    }else if(role == 'creator')
                                    {
                                        url = Modus.uriRoot + '/creator/' + this.model.id;
                                    }
                                    this.$el.html('<a modus-link href="'+url+'"><i class="fa fa-info-circle" title="'+this.model.get('info')+'"></i></a>&nbsp;');
                                }
                            }
                            this.$el.append('<a class="edit-user" userid="'+this.model.id+'"><i class="fa fa-pencil" title="Editar usuario"></i></a>&nbsp;<a class="delete-user" userid="'+this.model.id+'"><i class="fa fa-times" title="Eliminar usuario"></i></a>');
                        }
                        else
                        {
                            this.$el.html('<a class="enable-user" userid="'+this.model.id+'"><i class="fa fa-check" title="Habilitar usuario"></i></a>');
                        }
                        return this;
                    }
                }),
                editable: false,
                }],
            collection: this.pageableUsers
          });

        this.paginator = new Backgrid.Extension.Paginator({
            collection: this.pageableUsers
        });

        this.paginator.delegateEvents({"click a": function(e){
            var $el = $(e.currentTarget), col = this.collection;
            if (!$el.hasClass("active") && !$el.hasClass("disabled"))
            {
                that.page = that.pageableUsers.state.currentPage;
                that.virtualRoute();
            }
            return this;
        }});
    },

    loadPageableUrl : function()
    {
        var that = this;
        that.pageableUsers.queryParams.q = this.search;
        that.pageableUsers.queryParams.role = this.role;
        that.pageableUsers.state.currentPage = parseInt(this.page);
        that.$('.backgrid tbody').html('<tr><td colspan="7" style="display:table-cell"><center style="margin-top:10%;margin-bottom:10%"><h4><i class="fa fa-circle-o-notch fa-spin"></i> Cargando...</h4></center></td></tr>');
        this.virtualRoute();

        this.pageableUsers.fetch({reset:true,
            error : function(xhr)
            {
                var msg = Modus.Helper.getMsgXhr(xhr,"Error al cargar usuarios.");
                that.$('.backgrid tbody').html('<tr><td colspan="7" style="display:table-cell"><center style="margin-top:10%;margin-bottom:10%;color:red"><h4><i class="fa fa-times-circle"></i> '+msg+'<br/><br/><a class="btn btn-sm btn-default reload">Reintentar</a></h4></center></td></tr>');
            }
        });

    },

    render: function()
    {
        this.$el.html(this.template());
        var usersList = this.$(".users-list");
        usersList.append(this.pageableGrid.render().el);
        var filter = $((_.template($('#tmpl-admin-users-search').html()))());
        usersList.before(filter);

        this.searchInput = filter.find('#search-input');
        this.searchInput.val(this.search);

        this.roleInput = filter.find('#role-input');
        this.showRoleInput();

        var pagination = $('<div>').addClass('backgrid-paginator').html(this.paginator.render().el)
        usersList.after(pagination);

        this.loadPageableUrl();

        return this;
    },

    events:
    {
      "click .delete-user" : "deleteUser",
      "click .enable-user" : "enableUser",
      "click .add-user" : "showAddUser",
      "click .edit-user" : "showEditUser",
      "click .btn-save" : "saveUser",
      "click .reload" : "loadPageableUrl",
      "keypress #search-input" : "onKeyPressSearchInput",
      "click .form-search>.clear" : "clearSearchInput",
      "click .role-list li" : "selectRole"
    },

    showRoleInput : function()
    {
        var text = 'Todos';
        var role = this.role;
        switch(role)
        {
            case 'student': text = "Alumno"; break;
            case 'instructor': text = "Docente"; break;
            case 'creator': text = "Creador"; break;
            case 'coordinator': text = "Coordinador"; break;
            case 'admin': text = "Administrador"; break;
        }
        this.roleInput.text(text);
    },

    onKeyPressSearchInput : function(e)
    {
        if (e.keyCode && (e.keyCode != 13)) return;
        e.preventDefault();
        var val = this.searchInput.val();
        this.search = val;
        this.page = 1;
        this.loadPageableUrl();
    },

    clearSearchInput : function(e)
    {
        if(this.searchInput.val().length>0)
        {
            this.searchInput.val("");
            this.onKeyPressSearchInput(e);
        }
        else
        {
            this.searchInput.focus();
        }
    },

    selectRole : function(e)
    {
        if(e.currentTarget.attributes.role)
        {
            this.role = e.currentTarget.attributes.role.value;
        }
        else
        {
            this.role = "all";
        }
        this.page = 1;
        this.showRoleInput();
        this.loadPageableUrl();
    },

    showAddUser : function()
    {
        this.userEditing = null;
        this.modal = this.$('#myModal')
        this.modal.find("#myModalLabel").text("Agregar usuario");
        this.modal.find('#form-new-user').trigger("reset");
        this.modal.modal();
    },

    showEditUser : function(e)
    {
        if(e && e.currentTarget && e.currentTarget.attributes && e.currentTarget.attributes.userid)
        {
            var userid = e.currentTarget.attributes.userid.value;

            var user = this.pageableUsers.get(userid), role = user.get('role');

            this.userEditing = userid;
            this.modal = this.$('#myModal')
            this.modal.find("#myModalLabel").text("Editar usuario");
            this.modal.find('#form-new-user').trigger("reset");

            this.modal.find('#newUser').val(user.get('username'));
            this.modal.find('#newName').val(user.get('firstname'));
            this.modal.find('#newLastname').val(user.get('lastname'));
            this.modal.find('#newEmail').val(user.get('email'));
            this.modal.find('#newPassword').val('');
            if(role != 'admin' && role != 'coordinator' && role != 'creator')
            {
                this.modal.find('.user-type').hide();
            }
            else
            {
                this.modal.find('.user-type').show();
                if(role == 'admin')
                    this.modal.find('#optionAdmin').click();
                else if(role == 'creator')
                    this.modal.find('#optionCreator').click();
                else if(role == 'coordinator')
                    this.modal.find('#optionCoordinator').click();
            }
            this.modal.modal();
        }
        else
        {
            alert("No se conoce el id");
        }
    },

    deleteUser : function(e)
    {
        if(e && e.currentTarget && e.currentTarget.attributes && e.currentTarget.attributes.userid && confirm("\u00BFEst\u00e1 seguro que desea eliminar este usuario?"))
        {
            var that = this;
            var $el = $(e.currentTarget);
            var html = $el.html();
            $el.html('<i class="fa fa-spin fa-spinner" title="Eliminando..."></i>');
            var userid = e.currentTarget.attributes.userid.value;
            var user = this.pageableUsers.get(userid);
            user && user.destroy({wait:true, 
                error:function(model, xhr, options)
                {
                    $el.html(html);
                    if(confirm("Este usuario no se puede eliminar\n\n\u00BFDesea deshabilitarlo?"))
                    {
                        that.disableUser(e)
                    }
                    else
                    {
                        var msg = Modus.Helper.getMsgXhr(xhr, "Error:\nOcurrió un error desconocido, intente nuevamente o recargue la página\n si no se resuelve contacte al administrador.");
                    }
                }
            });
        }
    },

    disableUser : function(e)
    {
        var $el = $(e.currentTarget);
        var html = $el.html();
        $el.html('<i class="fa fa-spin fa-spinner" title="Deshabilitando..."></i>');
        var userid = e.currentTarget.attributes.userid.value;
        var user = this.pageableUsers.get(userid);
        var url = Modus.uriRoot+ '/users/disable/' + userid;
        $.ajax({
            url : url,
            type : 'DELETE',
            success : function()
            {
                user.set({active : false});
                user.trigger("change:infoHTML");
            },
            error : function()
            {
                alert("Ocurrió un error desconocido, recargue la página y vuelva a intentarlo.");
                $el.html(html);
            }
        });
    },

    enableUser : function(e)
    {
        var that = this;
        var $el = $(e.currentTarget);
        var html = $el.html();
        $el.html('<i class="fa fa-spin fa-spinner" title="Habilitando..."></i>');
        var userid = e.currentTarget.attributes.userid.value;
        var user = this.pageableUsers.get(userid);
        var url = Modus.uriRoot+ '/users/enable/' + userid;
        $.ajax({
            url : url,
            type : 'PUT',
            success : function()
            {
                user.set({active : true});
                user.trigger("change:infoHTML");
            },
            error : function()
            {
                alert("Ocurrió un error desconocido, recargue la página y vuelva a intentarlo.");
                $el.html(html);
            }
        });
    },

    saveUser : function(e)
    {
        var element = $(e.currentTarget);
        var unindexed_array = this.$('#form-new-user').serializeArray();
        var indexed_array = {};

        $.map(unindexed_array, function(n, i){
            indexed_array[n['name']] = n['value'];
        });
        var newuser, that = this;
        if(this.userEditing > 0)
        {
            newuser = this.pageableUsers.get(this.userEditing);
            newuser.url = Modus.uriRoot + '/users/' + this.userEditing;
        }
        else
        {
            newuser = new Modus.User.Model.User();
            newuser.url = Modus.uriRoot + '/users';
        }
        var original_html = element.html();
        element.addClass('disabled').html('<i class="fa fa-spinner fa-spin"></i> Guardando...');
        var role = newuser.get('role');
        if(role && role != 'admin' && role != 'coordinator' && role != 'creator')
        {
            delete(indexed_array['role']);
        }
        newuser.save(indexed_array, {
            wait: true,
            success : function()
            {
                that.pageableUsers.add(newuser);
                element.removeClass('disabled').html(original_html);
                if(that.modal) that.modal.modal('hide');
            },
            error : function(model, xhr, options)
            {
                if(xhr && xhr.responseJSON && xhr.responseJSON.error){
                        var error = xhr.responseJSON.error;
                        if(_.isObject(error) || _.isArray(error)){
                            _.each(error,function(err){
                                alert(err);
                            });
                        }
                        else
                            alert(error);
                }
                else
                {
                    alert("Error:\nOcurrió un error desconocido, intente nuevamente o recargue la página\n si no se resuelve contacte al administrador.")
                }
                element.removeClass('disabled').html(original_html);
            }
        });
    },

    virtualRoute : function()
    {
        var url = '/user/'+this.role+'/'+this.page+(this.search?('/'+this.search):"");
        Modus.GeneralRouter.navigate(url, { trigger: false, replace:true });
    }
});