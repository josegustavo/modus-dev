/**
 * ID : User.Model.User
 */
Modus.User.Model.User = Backbone.Model.extend(
{
    initialize : function(attrs,opts)
    {
        if(attrs && attrs.name && attrs.lastname)
        {
            var full_name = attrs.name + ' ' + attrs.lastname;
            this.set('full_name',full_name);
            
        }
    },    
    parse : function(response, options){
        return response.success?response.success:response;
    },
    getFullName : function(){
        return this.get('name') + ' ' + this.get('lastname');
    }
});