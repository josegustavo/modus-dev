/**
 * ID : Strategy.View.Edit
 * require Strategy.Model.Strategy
 */
Modus.Strategy.View.Edit = Backbone.View.extend(
{
        tagName:  "div",
        className : "tab-pane active",
        template: _.template(document.getElementById('tmpl-strategies-add').innerHTML),

        initialize : function(params)
        {
            this.AppStrategy = params.AppStrategy;
            if(!this.model)
            {
                this.model = new Modus.Strategy.Model.Strategy;
                this.model.url = Modus.uriRoot+'/strategies/'+params.strategyId;
                this.model.fetch();
            }
            else
            {
                this.model.url = Modus.uriRoot+'/strategies/'+this.model.id;
            }
        },

        render : function()
        {
            this.$el.html($(this.template({tab:this.tab})));

            this.title = this.$('#strategy-title');
            this.image = this.$('#strategy-image');
            Modus.Helper.activeImageSelector(this.image);
            this.description = this.$('#strategy-description');

            Modus.Helper.activateSummerNote(this.description);

            if(this.model && this.model.id)
            {
                this.bind();
            }else
            {
                this.model.once("sync",this.bind,this);
            }
            return this;
        },

        events: {
            "click .btn-save"   : "save",
            "click .btn-cancel"  : "cancel",
        },

        bind : function()
        {
            var that = this;
            that.title.val(that.model.get('name'));
            if(that.model.get('image_url'))
            {
                that.image.css('background',"url('"+ that.model.get('image_url') +"') no-repeat center center");
                that.image.css({width:100,height:100});
                that.image.attr("image_id", that.model.get('image_id'));
                that.image.find('.status').addClass("hidden").html("");
            }
            else
            {
                that.image.find('.status').removeClass("hidden");
            }
            that.description.code(that.model.get('description'));
        },    

        save : function()
        {
            var that = this;
            that.model.save(
                {
                    name        : that.title.val(),
                    image_id       : that.image.attr('image_id'),
                    description : that.description.code()
                },
                {
                    wait: true,
                    success : function(e)
                    {
                        that.trigger("save", that.model);
                    },
                    error : function(model, xhr)
                    {
                        that.AppStrategy.showMsgXhr(xhr, "No se pudo guardar la estrategia");
                    }
                }
            );
        },
        
        cancel: function(e) {
            this.trigger("cancel",this.model);
        }

    });
