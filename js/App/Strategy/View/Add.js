/**
 * ID : Strategy.View.Add
 * require Strategy.Model.Strategy
 */
Modus.Strategy.View.Add = Backbone.View.extend(
{
        tagName:  "div",
        className : "tab-pane active",
        template: _.template(document.getElementById('tmpl-strategies-add').innerHTML),

        initialize : function(params)
        {
            this.AppStrategy = params.AppStrategy;
            this.model = new Modus.Strategy.Model.Strategy;
            this.model.url = Modus.uriRoot+'/strategies/';
        },

        render : function()
        {
            this.$el.html(this.template({tab:this.tab}));

            this.title = this.$('#strategy-title');
            this.image = this.$('#strategy-image');
            Modus.Helper.activeImageSelector(this.image);
            this.image_status = this.$('#strategy-image-status');
            this.description = this.$('#strategy-description');
                        
            Modus.Helper.activateSummerNote(this.description);
            this.image_status.removeClass("hidden");
            return this;
        },

        events: {
            "click .btn-save"   : "save",
            "click .btn-cancel"  : "cancel",
            "click #strategy-image" : "selectImage",
        },

        save : function(){
            var that = this;
            if(that.title.val().length==0)
            {
                that.title.focus();
                return;
            }
            this.model.save(
                {
                    name        : that.title.val(),
                    image_id    : that.image.attr('image_id'),
                    description : that.description.code()
                },
                {
                    wait: true,
                    success : function(e)
                    {
                        that.trigger("save",that.model);
                    },
                    error : function(model, xhr)
                    {
                        that.AppStrategy.showMsgXhr(xhr, "No se pudo guardar la estrategia");
                    }
                });
        },
        
        cancel: function(e) {
            this.trigger("cancel",this.model);
        }

    });