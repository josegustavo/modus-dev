/**
 * ID : ProjectAdmin.Model.ProjectEdit
 */
Modus.ProjectAdmin.Model.ProjectEdit = Backbone.Model.extend(
{
    url : Modus.uriRoot+'/projects',
    initialize : function(attrs,opts)
    {
        //attrs && attrs.phases && this.set('phases',new Model.PhaseList(attrs.phases));
        //attrs && attrs.rooms && this.set('rooms',new Model.RoomList(attrs.rooms));
        //attrs && attrs.creator && this.set('creator',new Model.User(attrs.creator));
        //attrs && attrs.grade && this.set('grade',new Model.Grade(attrs.grade));
        //attrs && attrs.school && this.set('school',new Model.School(attrs.school));
        //attrs && attrs.my_team && this.set('my_team',new Model.Team(attrs.my_team));
        //attrs && attrs.managers && this.set('managers',new Model.InstructorList(attrs.managers));
        //attrs && attrs.type && this.set('type',new Model.Type(attrs.type));
    },
    parse : function(response, options){
            var resp = response.success||response;
            resp.phases && (resp.phases = new Modus.ProjectAdmin.Model.PhaseListEdit(resp.phases));
            //resp.rooms && (resp.rooms = new Model.RoomList(resp.rooms));
            //resp.creator && (resp.creator = new Model.User(resp.creator));
            //resp.grade && (resp.grade = new Model.Grade(resp.grade));
            //resp.school && (resp.school = new Model.School(resp.school));
            //resp.my_team && (resp.my_team = new Model.Team(resp.my_team) );
            //resp.managers && (resp.managers = new Model.InstructorList(resp.managers) );
            //resp.type && (resp.type = new Model.Type(resp.type) );
            return resp;
    },
    
    getPhases : function()
    {
        return this.get("phases");
    },
});