/**
 * ID : ProjectAdmin.Model.Team
 */
Modus.ProjectAdmin.Model.Team = Backbone.Model.extend(
{
    initialize : function(attrs, opts)
    {
        attrs && attrs.students && Modus.Student && Modus.Student.Model.StudentList && this.set("students", new Modus.Student.Model.StudentList(attrs.students));
        attrs && attrs.room && Modus.School && Modus.School.Model.Room && this.set("room", new Modus.School.Model.Room(attrs.room));
    },
    parse : function(response, options)
    {
        return response.success?response.success:response;
    }
});