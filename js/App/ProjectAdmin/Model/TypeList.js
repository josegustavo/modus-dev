/**
 * ID : Project.Model.TypeList
 * require : Project.Model.Type
 */
 Modus.ProjectAdmin.Model.TypeList = Backbone.Collection.extend(
{
    model :  Modus.ProjectAdmin.Model.Type,
    parse : function(response, options)
    {
        return response.success?response.success:response;
    }
});