/**
 * ID : ProjectAdmin.Model.TeamList
 */
Modus.ProjectAdmin.Model.TeamList = Backbone.Collection.extend(
{
    model : Modus.ProjectAdmin.Model.Team,
    parse : function(response, options)
    {
        return response.success?response.success:response;
    },
    comparator: function(c)
    {
        return  parseInt(c.get('ord'));
    }
});
