/**
 * ID : ProjectAdmin.Model.PhaseListEdit
 */
Modus.ProjectAdmin.Model.PhaseListEdit = Backbone.Collection.extend(
{
    url : Modus.uriRoot+'/phases',
    model : Modus.ProjectAdmin.Model.PhaseEdit,
    parse : function(response, options)
    {
            return response.success||response;
    }
});