/**
 * ID : ProjectAdmin.App
 */
Modus.ProjectAdmin.App = Backbone.View.extend(
{
    tagName:  "div",
    className : "",
    template : _.template($('#tmpl-projects-app').html()),

    initialize : function(params)
    {
        this.tab = params.tab||'project';
        this.model = new Modus.ProjectAdmin.Model.ProjectEdit();
        if(params.id>0)
        {
            this.model.id = params.id;
            this.model.url = Modus.uriRoot + '/projects/' + this.model.id;
        }
        else
        {
            var phases = new Modus.ProjectAdmin.Model.PhaseList();
            phases.url = Modus.uriRoot + '/phases/basic'
            this.model.set("phases",phases);
            phases.fetch();
        }

        this.model.on('sync',this.afterSave, this);
    },

    render : function()
    {
        var addContainer = $(this.template()); 
        this.$el.html(addContainer);

        this.tabLinkProject                 = this.$("#project-tab");
        this.tabLinkProject.hide();
        this.tabLinkPhasesAndAssignements   = this.$("#phases-and-assignments-tab");
        this.tabLinkPhasesAndAssignements.hide();
        this.tabLinkAssignTeams            = this.$("#assign-teams-tab");
        if(this.model.id)
        {
            this.tabLinkAssignTeams.show();
        }
        else
        {
            this.tabLinkAssignTeams.hide();
        }

        this.tabProject                 = this.$("#project");
        this.tabPhasesAndAssignements   = this.$("#phases-and-assignments");
        this.tabAssignTeams            = this.$("#assign-teams");

        if(this.model.id)
        {
            this.model.fetch({
                success : $.proxy(this.goTo,this),
                error : $.proxy(this.errorLoadProject,this)
            });
        }
        else
        {
            this.goTo();
        }

        return this;
    },

    errorLoadProject : function(model, xhr)
    {
        var msg = Modus.Helper.getMsgXhr(xhr, "Ocurrió un error al cargar el proyecto.");
        this.$el.html('<div style="color:red;margin-top:10%;"><h2 class="text-center"><i class="fa fa-times-circle"></i> Ocurrio un error<br>'+msg+'<br/><br/><a modus-link href="/"><i class="fa fa-chevron-circle-left"></i> Volver</a></h2></div>')
    },

    goTo : function()
    {
        this.$('.loading').remove();
        if(this.model.get('only_assign_teams') === true)
        {
            this.tabLinkProject.remove();
            this.tabLinkPhasesAndAssignements.remove();
            this.goToAssignTeams();
        }
        else
        {
            this.tabLinkProject.show();
            this.tabLinkPhasesAndAssignements.show();
            if(this.tab == 'phases-and-assignments')
            {
                this.goToPhasesAndAssignements();
            }
            else if(this.tab == 'assign-teams')
            {
                this.goToAssignTeams();
            }
            else
            {
                this.goToProject();
            }
        }
    },

    events : 
    {
        "click #project-tab" : "goToProject",
        "click #phases-and-assignments-tab" : "goToPhasesAndAssignements",
        "click #assign-teams-tab" : "goToAssignTeams",
    },

    goToProject : function()
    {
        this.virtualRoute("");

        if(!this.modelAddView)
        {
            this.modelAddView = new Modus.ProjectAdmin.View.ProjectAdd({
                model : this.model
            });
            this.modelAddView.on('next', this.goToPhasesAndAssignements, this);
            this.modelAddView.on('save', this.save, this);
            this.tabProject.html(this.modelAddView.render().el);
        }
    },

    goToPhasesAndAssignements : function()
    {
        this.virtualRoute("phases-and-assignments");

        if(!this.PhasesEdit)
        {
            this.PhasesEdit = new Modus.ProjectAdmin.View.PhasesEdit({
                model : this.model
            });
            this.PhasesEdit.on('next', this.goToAssignTeams, this);
            this.PhasesEdit.on('save', this.save, this);
            this.tabPhasesAndAssignements.html(this.PhasesEdit.render().el);
        }
    },

    goToAssignTeams : function()
    {
        if(this.model.id)
        {
            this.virtualRoute("assign-teams");

            if(this.AssignTeamsView)this.AssignTeamsView.remove()

            this.AssignTeamsView = new Modus.ProjectAdmin.View.AssignTeams({
                model : this.model
            });
            this.AssignTeamsView.on('save', this.save, this);
            this.AssignTeamsView.on('saveEnd', this.saveEnd, this);
            this.tabAssignTeams.html(this.AssignTeamsView.render().el);


        }
        return false;
    },

    virtualRoute : function(tab)
    {
        this.tabLinkProject.hasClass('active') && this.tabLinkProject.removeClass('active');
        this.tabProject.hasClass('active') && this.tabProject.removeClass('active');

        this.tabLinkPhasesAndAssignements.hasClass('active') && this.tabLinkPhasesAndAssignements.removeClass('active');
        this.tabPhasesAndAssignements.hasClass('active') && this.tabPhasesAndAssignements.removeClass('active');

        this.tabLinkAssignTeams.hasClass('active') && this.tabLinkAssignTeams.removeClass('active');
        this.tabAssignTeams.hasClass('active') && this.tabAssignTeams.removeClass('active');

        this.tab = tab;
        if(this.tab == 'phases-and-assignments')
        {
            this.tabLinkPhasesAndAssignements.hasClass('active') || this.tabLinkPhasesAndAssignements.addClass('active');
            this.tabPhasesAndAssignements.hasClass('active') || this.tabPhasesAndAssignements.addClass('active');
        }
        else if(this.tab == 'assign-teams')
        {
            this.tabLinkAssignTeams.hasClass('active') || this.tabLinkAssignTeams.addClass('active');
            this.tabAssignTeams.hasClass('active') || this.tabAssignTeams.addClass('active');
        }
        else
        {
            this.tabLinkProject.hasClass('active') || this.tabLinkProject.addClass('active');
            this.tabProject.hasClass('active') || this.tabProject.addClass('active');
        }     

        var url = '/project/'+(!this.model.id?'add':('edit/'+this.model.id+'/'+this.model.get('slug')))+(tab?'/'+tab:'');
         Modus.GeneralRouter.navigate(url, { trigger: false,replace:true });
    },

    save : function(e,callback)
    {
        var that = this;
        var element = $(e.currentTarget);
        var original = element.html();
        element.html('<i class="fa fa-spinner fa-spin"></i> Guardando... ');

        this.model.save({},{
            success : function(model)
            {
                e && e.currentTarget && $(e.currentTarget).html('Guardado <i class="fa fa-check"></i>')
                && ( setTimeout(function(){$(e.currentTarget).html(original); callback && callback.call();},1000) || (callback && callback.call()) );
                var url = '/project/'+(!model.id?'add':('edit/'+model.id+'/'+model.get('slug')))+(that.tab?'/'+that.tab:'');
                 Modus.GeneralRouter.navigate(url, { trigger: false,replace:true });
                that.model.url = Modus.uriRoot + '/projects/' + that.model.id;
            },
            error : function(model, xhr, options)
            {
                e && e.currentTarget && $(e.currentTarget).html('Error <i class="fa fa-times"></i>')
                if(xhr && xhr.responseJSON && xhr.responseJSON.error){
                    var error = xhr.responseJSON.error;
                    if(_.isArray(error)){
                        _.each(error,function(err){
                            alert(err);
                        });
                    }else
                        alert(error);

                }else{
                    alert("Error:\nOcurrió un error desconocido, intente nuevamente o recargue la página\n si no se resuelve contacte al administrador.")
                }
                $(e.currentTarget).html(original);
            }
        });

        return;
    },

    saveEnd : function(e)
    {
        this.save(e,function(){
             Modus.GeneralRouter.navigate("/", { trigger: true,replace:false });
        });
    },

    afterSave : function()
    {
        var phases_ids = this.model.get("phases_ids");
        if(phases_ids)
        {
            var phases = this.model.get("phases");
            phases.each(function(phase)
            {
                var find = _.findWhere(phases_ids,{ord : phase.get("ord")});
                if(find)
                {
                    phase.set("id",find.id);
                }
            },this);
            this.model.unset("phases_ids");
        }
    },

});