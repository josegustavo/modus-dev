/**
 * ID : Project.View.Teams
 */
 Modus.ProjectAdmin.View.Teams = Backbone.View.extend(
{
    tagName :  "div",
    className : "teams-zone",
    template : _.template($('#tmpl-projects-assign-teams-cont').html()),

    initialize : function(params)
    {
        this.projectId = params.projectId || 0;
        this.room = params.room;

        this.teams = this.room.get('teams');
        this.teams.url = Modus.uriRoot + '/projects/' + this.projectId + '/teams'
        this.teams.on('add', this.addTeam, this);
    },

    render : function()
    {
        this.$el.html($(this.template()));

        this.buttonZone = this.$('#teams-zone');

        this.addAll();
        return this;
    },

    events :
    {
        "click .action-add-team" : "addNewTeam"
    },

    addNewTeam : function()
    {
        var that = this;
        var circle = "fa-plus-circle";
        var spin = "fa-spinner fa-spin";
        var button = that.$('.action-add-team');
        var icon = button.find('.fa');
        button.addClass('disabled');
        icon.removeClass(circle).addClass(spin);
        var end = function(){
            icon.removeClass(spin).addClass(circle);
            button.removeClass('disabled');
        };
        that.teams.create({
            'project_id' : that.projectId,
            'room_id' : that.room.id,
            'students' : []
        },{wait : true,
        success : function()
        {
            end();
        },
        error : function(xhr)
        {
            var msj = Modus.Helper.getMsgXhr(xhr, "No se pudo crear un nuevo grupo, intente recargar la página");
            alert(msj);
            end();
        }});
    },

    addTeam : function(team)
    {
        var teamView = new Modus.ProjectAdmin.View.Team({model : team});
        if(!this.teamZeroView) this.teamZeroView = teamView;
        this.teamViewList = this.teamViewList || [];
        this.teamViewList[teamView.model.id] = teamView;
        teamView.on("removeStudent", this.removeStudent,this);
        teamView.on("receiveStudent", this.receiveStudent,this);
        teamView.on("destroyTeam", this.destroyTeam,this);
        this.buttonZone.append(teamView.render().el);
    },

    addAll : function()
    {
        this.teams.each(this.addTeam,this);
    },

    removeStudent : function(team, studentId)
    {
        this.toSend = {
            team : team, student : team.get('students').get(studentId)
        };
    },

    receiveStudent : function(team, studentId, nosubmit)
    {

        var that = this;
        if(this.toSend)
        {

            var go = true;
            var student = this.toSend.student;
            var teamFrom = this.toSend.team;
            var teamTo = team;
            this.toSend = null;

            if(!student) return;

            var answer_count = student.get('answer_count');
            (answer_count > 0) && (go = confirm("- Al cambiar de grupo se eliminará"+(answer_count>1?"n":"")+" "+answer_count+" respuesta"+(answer_count>1?"s":"")+",\n\n\u00BFEstá seguro de que quiere cambiar de grupo a este alumno?"));

            if(go && teamFrom.get('has_marks') > 0 && teamTo.get('has_marks') > 0 )
            {
                alert("No se puede reasignar al alumno, ya que el grupo al cual pertenece, ya tiene calificación");
                go = false;
            }
            if(go && teamTo.get('has_marks') > 0)
            {
                alert("No se puede reasignar al alumno, ya que el grupo al cual se intenta cambiar, ya tiene calificación");
                go = false;
            }


            if(go === true)
            {
                teamTo.get('students').add(student);
                teamFrom.get('students').remove(student);

                if(!nosubmit)
                {
                    var teamToView = that.teamViewList[teamTo.id];
                    var studentView = teamToView.$("[student-id='"+student.id+"']");
                    var content = studentView.html();
                    studentView.css('background-color', 'gray');
                    studentView.html('<i class="fa fa-spinner fa-spin"></i> ' + content);
                    $.post( Modus.uriRoot+"/projects/"+this.projectId+"/teams/assign", { team_from:teamFrom.id, team_to: teamTo.id, student: student.id } )
                    .success(function(){
                        if(teamTo.id==0)
                        {
                            student.unset('answer_count');
                        }
                        else
                        {
                            student.set('answer_count',0);
                        }
                    })
                    .fail(function(xhr)
                    {
                        var msj = Modus.Helper.getMsgXhr(xhr, "No se pudo asignar al alumno, recargue la página y vuelva a intentarlo.");
                        alert(msj);
                        that.removeStudent(teamTo,student.id);
                        that.receiveStudent(teamFrom, student.id, true);
                    })
                    .always(function()
                    {
                        that.teamViewList[teamTo.id].reRender();
                        that.teamViewList[teamFrom.id].reRender();
                    });
                }
                else
                {
                    if(teamTo.id>=0 && that.teamViewList && that.teamViewList[teamTo.id])
                    {
                        that.teamViewList[teamTo.id].reRender();

                    }
                    if(teamFrom.id>=0 && that.teamViewList && that.teamViewList[teamFrom.id])
                    {
                        that.teamViewList[teamFrom.id].reRender();

                    }
                }
            }
            else
            {
                that.teamViewList[teamTo.id].reRender();
                that.teamViewList[teamFrom.id].reRender();
            }
        }
    },

    destroyTeam : function(view, teamFrom,callback)
    {
        var button = view.$('.action-delete-team');
        button.html('<i class="fa fa-spinner fa-spin"></i>').addClass("disabled");
        var end = function()
        {
            button.html("&times;");
            button.removeClass("enabled")
        };
        var students = teamFrom.get('students').clone();
        var teamTo = this.teams.at(0);
        teamFrom.destroy({
            success : $.proxy(function(model, response, options){
                students.each(function(student)
                    {
                        student.unset('answer_count');
                        teamTo.get('students').add(student);
                        teamFrom.get('students').remove(student);

                    }, this);
                    this.teams.remove(teamFrom);
                    this.teamZeroView.reRender();        
                    $.isFunction(callback) && callback();
                    end();
            },this),
            error : function(model, xhr, options)
            {
                var msj = Modus.Helper.getMsgXhr(xhr, 'No se puede eliminar el grupo.');
                alert(msj);
                end();
            },
            wait : true
        });

    }
});