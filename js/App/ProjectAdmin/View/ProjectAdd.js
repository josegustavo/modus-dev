/**
 * ID : Project.View.ProjectAdd
 */
Modus.ProjectAdmin.View.ProjectAdd = Backbone.View.extend(
{
    tagName:  "div",
    template : _.template($('#tmpl-projects-add').html()),
    Types : null,
    initialize : function()
    {
        this.Types = new Modus.ProjectAdmin.Model.TypeList();
        if( this.Types.length <= 0 )
        {
            this.Types.url = Modus.uriRoot+'/projects/types';
            this.Types.fetch();
        }
    },

    render : function()
    {
        this.$el.html($(this.template()));

        this.title          = this.$("#project-title");
        this.rooms       = this.$("#project-room");
        this.startDate      = this.$("#project-start-date");
        this.endDate        = this.$("#project-end-date");
        this.typeInput      = this.$("#project-type");
        this.typeDDM        = this.$("#project-type-ddm");
        this.image          = this.$("#project-image");Modus.Helper.activeImageSelector(this.image);
        this.description    = this.$("#project-description");
        this.saveButton     = this.$(".btn-save");
        this.nextButton     = this.$(".select-next-tab");

        this.schoolInput = this.$("#project-school");
        this.gradeInput = this.$("#project-grade");
        this.roomInput = this.$("#project-room");
        this.instructorInput = this.$("#project-instructor");
        this.schoolDDM = this.$("#project-school-ddm");
        this.gradeDDM = this.$("#project-grade-ddm");
        this.roomDDM = this.$("#project-room-ddm");
        this.instructorDDM = this.$("#project-instructor-ddm");

        this.dom = {
            schoolInput : this.schoolInput,
            gradeInput : this.gradeInput,
            roomInput : this.roomInput,
            instructorInput : this.instructorInput,
            schoolDDM : this.schoolDDM,
            gradeDDM : this.gradeDDM,
            roomDDM : this.roomDDM,
            instructorDDM : this.instructorDDM
        };

        if(!this.model.id)
        {
            this.Selected = new Modus.ProjectAdmin.Model.SelectedRoom();
            this.RoomSelectProject = new Modus.ProjectAdmin.View.RoomSelect(this.Selected,this.dom);
        }
        else
        {
            setTimeout($.proxy(this.fillAll,this),0);
        }


        var config = {language: 'es', pickTime: false};
        this.startDate.parent().datetimepicker(config);
        this.endDate.parent().datetimepicker(config);

        if(this.Types.length>0)
        {
            this.fillTypesDDM();
        }
        else
        {
            this.Types.once('sync',this.fillTypesDDM,this);
        }

        Modus.Helper.activateSummerNote(this.description);

        return this;
    },

    events : 
    {
        "click .btn-save" : "save",
        "click .btn-cancel" : "cancel",
        "click .btn-next-tab" : "next",
    },

    fillAll : function()
    {
        var model = this.model.toJSON();

        this.title.val(this.model.get('title'));

        var school_id = model.school_id?model.school_id:0;
        var level_id = model.level_id?model.level_id:0;
        var grade_id = model.grade_id?model.grade_id:0;
        var rooms_ids = model.rooms_ids?model.rooms_ids:[];
        var instructors_ids = model.instructors_ids?model.instructors_ids:[];
        this.Selected = new Modus.ProjectAdmin.Model.SelectedRoom({ schoolId : school_id, levelId:level_id, gradeId : grade_id,  rooms_ids : rooms_ids, instructors_ids : instructors_ids});
        this.RoomSelectProject = new Modus.ProjectAdmin.View.RoomSelect(this.Selected,this.dom, model.has_teams);

        var sd = new Date(this.model.get('start_date')*1000);
        this.startDate.parent().data('datetimepicker').setLocalDate(sd);

        var ed = new Date(this.model.get('end_date')*1000);
        this.endDate.parent().data('datetimepicker').setLocalDate(ed);

        if(this.model.get('type'))
        {
            var type = this.model.get('type');
            this.typeInput.data('typeId', type.id);
            this.typeInput.text(type.name);
        }         

        if(this.model.get('image'))
        {
            var image = this.model.get('image');
            this.image.css('background',"url('"+ image.cdn_url +"') no-repeat center center");
            var w = image.width || 100;
            var h = image.height || 120;
            var showWidth = Math.round(120*w/h);
            this.image.css({width:showWidth,height:120});
            this.image.attr("image_id", image.id);
            this.image.find('.status').addClass("hidden");
        }


        this.description.code(this.model.get('description'));

        return this;
    },

    fillType : function(type)
    {
        if(type)
        {
            this.typeInput.data('typeId', type.id);
            this.typeInput.text(type.get('name'));
        }
        else
        {
            this.typeInput.removeData('typeId');
            this.typeInput.text('');
        }
    },

    fillTypesDDM : function()
    {
        if(this.Types)
        {
            var types = this.Types;
            var that = this;

            this.typeDDM.empty();

            var li = $('<li/>');
            li.html("<a>Ninguno</a>").on('click', function(){that.fillType(null);});
            this.typeDDM.append(li);

            types.each(function(type)
            {
                var li = $('<li/>');
                var a = $('<a/>').text(type.get("name"));

                li.html(a).on('click', function(){
                    that.fillType(type);
                });
                this.typeDDM.append(li);
            },this);
        }
    },

    save : function(e)
    {
        var error = false;
        this.$('.has-error').removeClass('has-error');

        var title = this.title.val().trim();
        if(!title)
        {
            this.title.attr('placeholder', 'Ingrese un título para el proyecto');
            this.title.parent().parent().addClass('has-error');
            if(!error) this.title.focus();
            error = true;
        }

        var star_date = this.startDate.val();
        if(!star_date)
        {
            this.startDate.attr('placeholder', 'Elegir una fecha de inicio');
            this.startDate.parent().parent().parent().addClass('has-error');
            if(!error) this.startDate.focus();
            error = true;
        }

        var end_date = this.endDate.val();
        if(!end_date)
        {
            this.endDate.attr('placeholder', 'Elegir una fecha de fin');
            this.endDate.parent().parent().parent().addClass('has-error');
            if(!error) this.endDate.focus();
            error = true;
        }

        var description = this.description.code();
        if(!error)
        {
            var set = { 
                title : title,
                instructors_ids : this.Selected.getSelectedInstructors().pluck("id"),
                type_id : this.typeInput.data("typeId"),
                start_date :  star_date,
                end_date : end_date,
                image_id : this.image.attr("image_id") ,
                description : description
            };
            var has_teams = this.model.get('has_teams');
            if(!has_teams)
            {
                set.school_id = this.Selected.get('schoolId');
                set.grade_id = this.Selected.get('gradeId');
                set.rooms_ids = this.Selected.get('rooms_ids');
            }
            this.model.set(set);
            this.trigger('save',e);
        }
    },

    cancel : function(e)
    {
        history.back(-1);
    },

    next : function(e)
    {
        this.save(e);
        this.trigger('next');
    }
});