/**
 * ID : Project.View.RoomSelect
 */
 Modus.ProjectAdmin.View.RoomSelect  = Backbone.View.extend(
{
    initialize : function(selected, dom, no_editable)
    {
        this.editable = !no_editable;
        this.schoolInput = dom.schoolInput;
        this.gradeInput = dom.gradeInput;
        this.roomInput = dom.roomInput;
        this.instructorInput = dom.instructorInput;

        this.schoolDDM = dom.schoolDDM;
        this.gradeDDM = dom.gradeDDM;
        this.roomDDM = dom.roomDDM;
        this.instructorDDM = dom.instructorDDM;

        this.Selected = selected;
        this.Selected.on({changeSelected : $.proxy(this.onChange,this)});

        this.Schools = this.Selected.get('schools');
        if(this.Schools.length<=0)
        {
            this.Schools.url = Modus.uriRoot+'/schools/listAll/1';
            this.Schools.once('sync', this.onSync,this);
            this.Schools.fetch({
                success : $.proxy(this.onSync, this),
                error : $.proxy(this.onSync, this)
            });
        }else{
            this.onSync();
        }
    },

    onChange : function()
    {
        this.fillSchool();
    },

    onSync : function()
    {
        this.fillSchool();
    },

    fillSchool : function()
    {
        var sel = this.Selected;
        var school = sel.getSchool();
        var schools = sel.get('schools');
        var count = schools.length;
        if(count <= 0)
        {
            this.schoolInput.html("<span class='text-danger'><i class='fa fa-times'></i> No se encontraron colegios.</span>");
            this.schoolInput.parent().addClass('disabled');
            this.schoolInput.next().find('button').addClass('disabled');
        }
        else
        {
            this.schoolDDM.empty();
            var li = $('<li/>');
            li.html($('<a/>').text("Ninguno"));
            li.on('click',function()
            {
                sel.setSchool("0");
            });
            this.schoolDDM.append(li);
            schools.each(
                function(s,index){
                    if(!school || s.id != school.id){
                        var li = $('<li/>');
                        li.html($('<a/>').text(s.get('name')));
                        li.on('click',function()
                        {
                            sel.setSchool(s.id);
                        });
                        this.schoolDDM.append(li);
                    }
                },
            this);
            if( school )
            {
                var school_name = school.get('name');
                this.schoolInput.text(school_name);
            }
            else
            {
                this.schoolInput.text("");
            }
        }
        !this.editable && this.showNoSectionEditableInfo(this.schoolInput);
        this.fillGrade();
    },

    fillGrade : function()
    {
        var that = this;
        var sel = that.Selected;
        var school = sel.getSchool();

        if(school)
        {
            var level = sel.getLevel(), grade = sel.getGrade();

            var onlyOneElement = false;
            var levels = school.get('levels');
            if((levels.length === 1) && (levels.at(0).getGrades().length === 1))
                onlyOneElement = true;

            if( onlyOneElement )
            {
                that.gradeInput.next().find('button').addClass('disabled');
                var level_id = levels.at(0).id, grade_id = levels.at(0).getGrades().at(0).id;
                if(!level || !grade)
                {
                    sel.setGrade(level_id, grade_id);
                    return false;
                }
                if(level.id != level_id || grade.id != grade_id)
                {
                    sel.setGrade(level_id, grade_id);
                    return false;                        
                }
            }
            else
            {
                that.gradeInput.next().find('button').removeClass('disabled');
                that.gradeDDM.empty();
                levels.each(function(level){
                    var li = $('<li/>');
                    li.addClass('pull-left').append('<dt><span class="text">'+level.get("name")+'</span></dt>');
                    level.getGrades().each(function(grade){
                            var a = $('<a/>');
                            a.text(grade.get("name")+'\u00B0 Grado').on('click', function()
                            {
                                sel.setGrade(level.id, grade.id);
                            });
                            li.append(a);
                    });
                    that.gradeDDM.append(li);
                });
            }


            if(level && grade)
            {
                that.gradeInput.text(grade.get("name")+'\u00B0 Grado de '+level.get("name"));
            }
            else
            {
                that.gradeInput.text("");
            }
        }
        else
        {
            that.gradeInput.text("");
            that.gradeInput.next().find('button').addClass('disabled');
        }
        !this.editable && that.showNoSectionEditableInfo(that.gradeInput);
        that.fillRoom();
    },

    fillRoom : function()
    {
        var that = this;
        var sel = this.Selected;
        var grade = this.Selected.getGrade();
        var rooms = this.Selected.getRooms();
        this.roomDDM.empty();
        if(grade && rooms && rooms.length)
        {
            var grade_name = grade.get('name');
            this.roomInput.next().find('button').removeClass('disabled');
            var selecteds = sel.getSelectedRooms();
            if(rooms.length===1)
            {
                this.roomInput.next().find('button').addClass('disabled');
            }
            else
            {
                rooms.each(function(room)
                {
                    var li = $('<li/>');
                    var a = $('<a/>').addClass('checkbox');
                    var label = $('<label/>');

                    var isSelected = selecteds.get(room.id)?true:false;
                    var input = $('<input/>').attr({type:'checkbox',checked:isSelected});
                    label.append(input).append(grade_name + "° Sección \"" + room.get("name") + "\"");
                    a.html(label);
                    li.html(a).on('click', $.proxy(function(){
                        if(!isSelected)
                        {
                            this.Selected.addRoom(room);
                        }
                        else
                        {
                            if(this.Selected.getSelectedRooms().length == 1)
                            {
                                alert("Debe seleccionar la menos una sección");
                            }
                            else
                            {
                                this.Selected.removeRoom(room);
                            }
                        }
                        return false;
                    }, this));
                    this.roomDDM.append(li);
                },this);
            }
            if(selecteds.length)
            {
                var names = selecteds.pluck("name");
                this.roomInput.html(names.join(', '));
            }
            else
            {
                this.roomInput.empty();
            }
        }
        else
        {
            this.roomInput.empty();
            this.roomInput.next().find('button').addClass('disabled');
        }
        !this.editable && this.showNoSectionEditableInfo(that.roomInput);
        this.fillInstructors();
    },

    showNoSectionEditableInfo : function(input)
    {
        input.next().attr('title','No se puede editar la sección, ya que el proyecto tiene grupos asignados.').find('button').addClass('disabled');
    },

    fillInstructors : function()
    {
        var that = this;
        var sel = that.Selected;
        var instructors = that.Selected.getInstructors();
        var selectedInstructors = that.Selected.getSelectedInstructors();

        if(sel.getGrade() && sel.getSelectedRooms() && (!instructors || (instructors.length == 0)))
        {
            that.instructorInput.parent().addClass('disabled');
            that.instructorInput.next().find('button').addClass('disabled');
            that.instructorInput.html("<span class='text-danger'><i class='fa fa-times'></i> No se encontraron docentes.</span>");
        }
        else if(instructors)
        {
            that.instructorInput.parent().removeClass('disabled');
            that.instructorInput.next().find('button').removeClass('disabled');
            that.instructorDDM.empty();
            instructors.each(function(instructor)
            {
                var li = $('<li/>');
                var a = $('<a/>').addClass('checkbox');
                var label = $('<label/>');

                var isSelected = selectedInstructors.get(instructor.id)?true:false;
                var input = $('<input/>').attr({type:'checkbox',checked:isSelected});
                label.append(input).append(instructor.get("name"));
                a.html(label).prop('title',instructor.get("name") + " (" + instructor.get("username") + ")");

                li.html(a).on('click', function(){
                    if(isSelected)
                    {
                        that.Selected.removeInstructor(instructor);
                    }
                    else
                    {
                        that.Selected.addInstructor(instructor);
                    }
                    return false;
                });
                that.instructorDDM.append(li);
            });
            if(selectedInstructors && selectedInstructors.length>0)
            {
                var names = selectedInstructors.map(function(i){return i.get('name');});
                that.instructorInput.html(names.join(', '));
            }
            else
            {
                that.instructorInput.empty();
            }
        }
        else
        {
            that.instructorInput.empty();
            that.instructorInput.parent().addClass('disabled');
            that.instructorInput.next().find('button').addClass('disabled');
        }

    },

});