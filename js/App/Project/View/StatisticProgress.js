/**
 * ID : Project.View.StatisticProgress
 */
Modus.Project.View.StatisticProgress = Backbone.View.extend(
{
    tagName         : "div",
    className       : "row",


    initialize : function(params)
    {
        this.template       = _.template($('#tmpl-projects-statistics').html());
        this.templatePhase  = _.template($('#tmpl-projects-statistics-phase').html());

        this.roomId = params.room_id;
        this.project   = params.project;
        this.projectId = params.project.id;

        this.model = new Modus.Project.Model.StatisticList;
        if(this.projectId && this.roomId)
            this.model.url = Modus.uriRoot + '/statistics/'+ this.projectId + '/progress/' + this.roomId;
        this.model.on('add',this.onAdd,this);
        this.model.on('reset',this.onReset,this);
    },

    render : function()
    {
        this.$el.html($(this.template({project : this.project})));
        this.title = this.$('.panel-title');
        this.title.text('Avances por grupo y alumno ');
        this.area = this.$('#statistic-all');
        return this;
    },

    onAdd : function(model)
    {
        if(!this.phases)this.phases=[];
        if(!this.phases[model.id])
        {
            var div = $('<div>').attr('phase-id',model.id);
            div.html($(this.templatePhase({phase : model.toJSON()})))
            if(this.phases.length == 0)
            {
                this.area.html(div);
            }
            else
            {
                this.area.append(div);
            }
            this.phases[model.id] = div;
        }
        else
        {
            this.phases[model.id].html($(this.templatePhase({phase : model.toJSON()})))
        }
    },

    onReset : function()
    {

    },

    changeRoom : function(room)
    {
        this.roomId = room.id;
        this.title.text('Avances por grupo y alumno - Sección ' + room.name );
        this.model.url = Modus.uriRoot + '/statistics/'+ this.projectId + '/progress/' + this.roomId;
        if(this.model.length) this.model.reset();
        this.model.fetch();
    }
});