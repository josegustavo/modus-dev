/**
 * ID : Project.View.Assessment
 */
Modus.Project.View.Assessment = Backbone.View.extend(
{
    tagName         : "div",
    className       : "row",

    initialize : function(params)
    {
        this.templateCont  = _.template($('#tmpl-projects-statistics').html());
        this.template      = _.template($('#tmpl-projects-statistics-assessment').html());

        this.roomId = params.room_id;
        this.project   = params.project;
        this.projectId = params.project.id;

        this.model = new Modus.Project.Model.Statistic();
        if(this.projectId && this.roomId)
        {
            this.model.url = Modus.uriRoot + '/statistics/' + this.projectId + '/assessment/' + this.roomId;
        }
        this.model.on('change',this.onChange,this);
        this.model.on('reset',this.onReset,this);
    },

    render : function()
    {
        this.$el.html($(this.templateCont({project : this.project})));
        this.title = this.$('.panel-title');
        this.title.text('Notas finales y parciales ');
        this.area = this.$('#statistic-all');
        if(this.roomId>0)this.onChange();
        return this;
    },

    onChange : function(model)
    {
        var div = $('<div class="table-responsive">');
        var json = this.model.toJSON();
        json.url_export = Modus.uriRoot + '/statistics/' + this.projectId  + '/exportAssessment/' + this.roomId;
        div.html(this.template(json));
        this.area.html(div);
    },

    onReset : function()
    {

    },

    changeRoom : function(room)
    {
        this.roomId = room.id;
        this.title.text('Notas finales y parciales - Sección ' + room.name);
        this.model.url = Modus.uriRoot + '/statistics/' + this.projectId + '/assessment/' + this.roomId;
        this.model.fetch();
        return this;
    }
});
