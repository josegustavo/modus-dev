/**
 * ID : Project.View.ReAnswer
 * require : Project.Model.AnswerList
 */
Modus.Project.View.ReAnswer = Backbone.View.extend(
{
    tagName : "div",
    className : "",
    template : _.template($('#tmpl-projects-phase-view-reanswer').html()),

    initialize : function(params)
    {
        this.project_id = params.project_id;
        this.phase_id = params.phase_id;
        this.team_id = params.team_id;
        this.answer_id = params.answer_id;
        this.can_remove = params.can_remove;

        this.reanswers = params.reanswers || new Modus.Project.Model.AnswerList();
        this.reanswers.on('add', this.addReAnswer, this);

        this.reAnswersContentView = [];

        this.initializeNewReAnswer();
    },

    initializeNewReAnswer : function()
    {
        this.newReAnswer && this.newReAnswer.clear();
        this.newReAnswer = new Modus.Project.Model.Answer;
        this.newReAnswer.url = Modus.uriRoot + '/projects/view/' + this.project_id + '/phases/' + this.phase_id + '/teams/' + this.team_id + '/answers/' + this.answer_id + '/reanswers';
        this.newReAnswer.set({
            'attachments'   : new Modus.Project.Model.AttachmentList,
            'answer_id'     : this.answer_id
        });
        this.newReAnswer.get('attachments').on('add', this.onAddAttach, this);
        this.newReAnswer.get('attachments').on('reset', this.onResetAttach, this);
    },

    render : function()
    {
        this.$el.html($(this.template()));
        this.postAnswer     = this.$('.form.post-reanswer');
        this.inputAnswer    = this.$('#input-reanswer');
        this.attachBtn      = this.$('.attach-btn-reanswer');
        this.attachValues   = this.$('#attach-values');
        this.btnSumbit      = this.$('#btn-publish');
        this.reAnswersZone  = this.$('#reanswers-zone');

        setTimeout($.proxy(this.addAllReAnswer,this),0);
        return this;
    },

    events : 
    {
        "click .attach-btn-reanswer": "attachInReAnswer",
        "click #btn-publish"        : "submitReAnswer"
    },

    addReAnswer : function(reanswer)
    {
        var reAnswerContent = new Modus.Project.View.ReAnswerContent({model:reanswer, can_remove : this.can_remove, project_id : this.project_id,
                phase_id : this.phase_id,
                team_id : this.team_id,
                answer_id : this.answer_id});
        this.reAnswersContentView.push(reAnswerContent);
        this.reAnswersZone.append(reAnswerContent.render().el);
    },

    addAllReAnswer : function()
    {
        this.reanswers.each(this.addReAnswer, this);
    },

    showReAnswer : function()
    {
        if(this.postAnswer.is(':hidden'))
        {
            this.postAnswer.show();
            this.loadSummernote();
        }
        else
        {
            this.postAnswer.hide();
            this.inputAnswer.destroy();
        }
    },

    loadSummernote : function()
    {
        Modus.Helper.activateSummerNote(this.inputAnswer);
    },

    submitReAnswer : function()
    {
        var reanswer = this.inputAnswer.code();
        if(reanswer && reanswer.length>0)
        {
            this.btnSumbit.html('<i class="fa fa-spinner fa-spin"></i> Enviando... ');
            this.newReAnswer.save({
                reanswer:reanswer
            },{
                wait : true,
                success : $.proxy(this.successSubmitReAnswer,this),
                error : $.proxy(this.errorSubmitReAnswer,this)
            });
        }
    },

    successSubmitReAnswer : function()
    {
        var answer = this.newReAnswer.clone();
        this.reanswers.add(answer);

        this.initializeNewReAnswer();
        this.inputAnswer.code('');
        this.showReAnswer();

        this.attachValues.empty();

        //this.btnSumbit.html('Enviado <i class="fa fa-check"></i>');
        this.btnSumbit.html('Enviar <i class="fa fa-chevron-circle-right"></i>');
    },

    errorSubmitReAnswer : function(model, xhr, options)
    {
        this.btnSumbit.html('Error <i class="fa fa-times"></i>');
        if(xhr && xhr.responseJSON && xhr.responseJSON.error){
            var error = xhr.responseJSON.error;
            if(_.isArray(error)){
                _.each(error,function(err){
                    alert(err);
                });
            }else
            alert(error);
        }else{
            alert("Error:\nOcurrió un error desconocido, intente nuevamente o recargue la página\n si no se resuelve contacte al administrador.")
        }
        this.btnSumbit.html('Enviar <i class="fa fa-chevron-circle-right"></i>');
    },

    attachInReAnswer : function()
    {
        var that = this;
        var percent = false;
        var end = function()
        {
            that.btnSumbit.prop("disabled", false);
            that.attachBtn.prop("disabled", false);
        };
        $('<input/>').attr('type','file').fileupload({
            add: Modus.Helper.fileUploadRestriction,
            url: Modus.uriRoot+'/uploads/file',
            dataType: 'json',
            success: function (data)
            {
                data = data.success;
                that.newReAnswer.get('attachments').add({id: data.id, url:data.full_cdn, original_name:data.original_name});
                that.attachBtn.html('<i class="fa fa-search-plus"></i> Adjuntar archivo');
                end();
            },
            error : function(xhr)
            {
                var msg = Modus.Helper.getMsgXhr(xhr, "No se pudo cargar el archivo.");
                alert(msg);
                that.attachBtn.html('<i class="fa fa-search-plus"></i> Adjuntar archivo');
                end();
            },
            progressall: function (e, data)
            {
                var info = "";
                if(data && data.loaded && data.total) info = Math.round((data.loaded/data.total)*100) + "%";
                if(!percent)
                {
                    that.attachBtn.html('<i class="fa fa-spinner fa-spin"></i> A&ntilde;adiendo... <span class="percent"></span>');
                    percent = that.attachBtn.find('.percent');
                }
                else
                {
                    percent.text(info);
                }
                that.btnSumbit.prop("disabled", true);
                that.attachBtn.prop("disabled", true);
            }
        }).click();
    },

    onAddAttach : function(attach)
    {
        var file = $('<a><iclass="fa fa-paperclip"></i> '+attach.get('original_name')+' <i class="fa fa-times-circle remove-attach" title="Click para quitar este adjunto"></i></a>');
        file.find('.remove-attach').on('click','i.remove-attach',
            $.proxy(function(){
                    attach.destroy();
                    file.remove();
                    return false;
                },this)
        );
        this.attachValues.append(file);
    },

    onResetAttach : function()
    {
        this.attachValues.empty();
    },

    deleteAll : function()
    {
        this.inputAnswer && this.inputAnswer.destroy && this.inputAnswer.destroy();
        _.each(this.reAnswersContentView,function(view){view.remove();});
        return this.remove();
    }
});
