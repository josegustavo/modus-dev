/**
 * ID : Project.View.SideDashboard
 */
Modus.Project.View.SideDashboard = Backbone.View.extend(
{
    tagName : "div",
    className : "panel panel-default side-block",
    template  : _.template($('#tmpl-projects-sidebar-statistics').html()),
    initialize : function(params)
    {
        this.rooms = params.rooms;
        this.options = params.options;
        this.on('selectDashBoard', this.onSelectDashBoard, this);
    },

    render : function()
    {
        this.$el.html(this.template({rooms : this.rooms}));
        return this;
    },

    events :
    {
        "click .gotoTeamBtn" : "eventSelectStatistic",
        "show.bs.collapse #statistic-side-list" : "onCollapse"
    },

    eventSelectStatistic : function(e)
    {
        e.preventDefault();
        var current = $(e.currentTarget),
            room_id = current.attr('room-id'),
            pageDash = current.attr('page-dash');
        if((room_id>0) || (pageDash))
        {
            this.changeRoomId(room_id, pageDash);
        }
    },

    changeRoomId : function(room_id, pageDash)
    {
        var room = _.find(this.rooms, function(room){ return room.id == room_id});
        if(room)
        {
            this.trigger('selectDashBoard', room, pageDash);
        }
    },

    onSelectDashBoard : function(room, pageDash)
    {
        this.$('.gotoTeamBtn').removeClass('active');
        this.$("[room-id='"+room.id+"'][page-dash='"+pageDash+"']").addClass('active');
    },

    unselect : function()
    {
        this.$('.gotoTeamBtn').removeClass('active');
        this.collapse('hide');
    },

    collapse : function(action)
    {
        action = action || 'toggle';
        this.$('.collapse-btn').addClass('collapsed');
        this.$('#statistic-side-list').collapse(action);
    },

    onCollapse : function(e)
    {
        if(e && e.type == "show")
        {
            this.trigger("expand", this);
        }
    }
});