/**
 * ID : Project.View.StatisticGeneral
 */
Modus.Project.View.StatisticGeneral = Backbone.View.extend(
{
    tagName     : "div",
    className   : "row",


    initialize : function(params)
    {
        this.template    = _.template($('#tmpl-projects-statistics').html());

        this.roomId = params.room_id;
        this.project   = params.project;
        this.projectId = params.project.id;

        this.model = new Modus.Project.Model.StatisticList;
        if(this.projectId && this.roomId)
            this.model.url = Modus.uriRoot + '/statistics/'+ this.projectId + '/general/' + this.roomId;
        this.model.on('add',this.onAdd,this);
        this.model.on('reset',this.onReset,this);
    },

    render : function()
    {
        this.$el.html($(this.template({project : this.project})));
        this.title = this.$('.panel-title');
        this.title.text('Estadística general ');
        this.area = this.$('#statistic-all');
        return this;
    },

    optionsChart :
    {
        chart: {
            height: 300,
            type: 'column',
            margin: [ 50, 50, 100, 50]
        },
        title: {
            text: ''
        },
        xAxis: {
            categories: [
                'Sin avance',
                'En borrador',
                'Terminado'
            ],
            labels: {
                rotation: 0,
                align: 'center',
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: ''
            },
            lineColor: '#c0d0e0',
            lineWidth: 1,
            gridLineColor : '#ddd',
            gridLineDashStyle: 'longdash'
        },
        legend: {enabled: false},
        tooltip: {enabled : false},
        series: [{
            name: 'Alumnos',
            color : '#F3EA33',
            data: [0,0,0],
            dataLabels: {
                enabled: true,
                rotation: 0,
                color: 'rgb(25,171,209)',
                align: 'center',
                formatter : function() {
                    return this.y<=0?"ninguno":(this.y + " alumno" + (this.y==1?"":"s"));
                },
                x: 0,
                y: 0,
                style: {
                    fontSize: '15px',
                    fontFamily: 'Oswald',
                }
            }
        }],
        credits: {enabled: false}
    },

    onAdd : function(model)
    {
        if(!this.phases) this.phases = [];
        if(!this.phases[model.id])
        {
            var options = _.clone(this.optionsChart);
            model.set('options',options);
            options.title.text = 'Fase ' + model.get('name');
            options.series[0].data[0] = model.get('none');
            options.series[0].data[1] = model.get('draft');
            options.series[0].data[2] = model.get('ended');
            var div = $('<div>').attr('phase-id',model.id);
            if(this.phases.length == 0)
            {
                this.area.html(div);
            }
            else
            {
                this.area.append(div);
            }
            this.phases[model.id] = div;
            div.highcharts(options);
        }
        else
        {
            this.phases[model.id].highcharts().series[0].data[0].update({y : model.get('none')});
            this.phases[model.id].highcharts().series[0].data[1].update({y : model.get('draft')});
            this.phases[model.id].highcharts().series[0].data[2].update({y : model.get('ended')});
        }
    },

    onReset : function()
    {

    },

    changeRoom : function(room)
    {
        this.roomId = room.id;
        this.title.text('Estadística general - Sección ' + room.name);
        this.model.url = Modus.uriRoot + '/statistics/'+ this.projectId + '/general/' + this.roomId;
        if(this.model.length) this.model.reset();
        this.model.fetch();
    }
});