/**
 * ID : Project.View.QualificationModal
 */
Modus.Project.View.QualificationModal = Backbone.View.extend(
{
    initialize : function(params)
    {
        this.$el            = params.el;
    },

    set : function(params)
    {
        this.rule           = params.rule;
        this.phase_id       = params.phase_id;
        this.team_id       = params.team_id;
        this.score_max      = this.rule.score;

        this.$('.modal-title').text(this.rule.rule);

        this.model  = new Modus.Project.Model.Qualification;
        this.model.set({rule:this.rule.id,phase:this.phase_id,team:this.team_id});
        if(params.id>0) this.model.set({id:params.id,mark:params.mark,comment:params.comment,date:params.date});

        this.btnPost        = this.$('.btn-post-qualification');
        this.textComment    = this.$('.text-comment');
        this.help           = this.$('.help-block');
        this.$el.on('hidden.bs.modal', $.proxy(this.cleanAll, this));

        if(this.model.id>0 && this.model.get('mark')){
            this.fillMark();
            this.fillComment();
            this.help.text('Calificado ' + this.model.get('date') + '.');
        }
        else
        {
            this.help.empty();
        }
        var score_max = this.score_max;
        this.$('.btn-mark').each(function(){
            var me = $(this);
            if(me.attr('mark')>score_max)
                me.hide();
            else
                me.show();
        });
        return this;
    },

    events : 
    {
        "change .text-comment" : "bindComment",
        "click .btn-post-qualification" : "postQualification",
        "click .btn-mark" : "onClickMark"
    },

    show : function()
    {
        this.$el.modal('show');
    },

    onClickMark : function(e)
    {
        var el = $(e.currentTarget), mark = parseInt(el.attr('mark'));
        if(mark>=0){
            this.cleanMark();                
            this.bindMark(mark);
        }
    },

    bindMark : function(mark)
    {
        this.model.set('mark',mark);
        this.$("[mark='"+(mark.toString().length==1?'0'+mark.toString():mark)+"']").addClass(mark==0?'btn-danger':'btn-success');
        this.btnPost.removeClass('disabled');
    },

    bindComment : function()
    {
        var comment = this.textComment.val();
        this.model.set('comment',comment);
    },

    fillMark : function()
    {
        this.bindMark(this.model.get('mark'));
    },

    fillComment : function()
    {
        this.textComment.val(this.model.get('comment'));
    },

    cleanAll : function()
    {
        this.cleanMark();
        this.cleanComment();
    },

    cleanMark : function()
    {
        this.model.unset('mark');
        this.$('.btn-mark').removeClass('btn-danger btn-success');
        this.btnPost.addClass('disabled');
    },

    cleanComment : function()
    {
        this.model.unset('comment');
        this.textComment.val('');
    },

    postQualification : function()
    {
        var that = this;
        this.btnPost.html('<i class="fa fa-spinner fa-spin"></i> Calificando...');
        this.model.url = Modus.uriRoot+"/projects/postMark";
        this.model.save({},{
            wait : true,
            success : function(){
                that.btnPost.html('Calificar');
                that.$el.modal('hide');
                that.trigger('saved');
            },
            error : function(xhr)
            {
                var msg = Modus.Helper.getMsgXhr(xhr, "No se pudo enviar su calificación, vuelva a intentarlo luego.");
                that.btnPost.html('<i class="fa fa-times"></i> Error');
                alert(msg);
                that.btnPost.html('Calificar');
            }
        });
    }
});