/**
 * ID : Project.Model.ProjectList
 * require : Project.Model.Project
 */
Modus.Project.Model.ProjectList = Backbone.Collection.extend(
{
    model : Modus.Project.Model.Project
});