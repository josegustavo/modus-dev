/**
 * ID : Project.Model.PhaseList
 * require : Project.Model.Phase
 */
Modus.Project.Model.PhaseList = Backbone.Collection.extend(
{
    url : Modus.uriRoot+'/phases',
    model : Modus.Project.Model.Phase,
    parse : function(response, options)
    {
            return response.success||response;
    }
});