/**
 * ID : Project.App
 * require : Project.Model.Project
 * require : Project.View.SideAnswers
 * require : Project.View.SideMyTeam
 * require : Project.View.SideDashboard
 * require : Project.View.SideMarks
 * require : Project.View.Resume
 * require : Project.View.StatisticGeneral
 * require : Project.View.StatisticProgress
 * require : Project.View.Assessment
 */
Modus.Project.App  = Backbone.View.extend(
{
    tagName:  "div",
    className : "container row",
    template : _.template(document.getElementById('tmpl-projects-resume').innerHTML),
    
    initialize : function(options)
    {
        document.title = ('Modus: Proyecto');
        this.options = options;
        if(!this.model)
        {
            var that = this;
            this.model = new Modus.Project.Model.Project();
            this.model.url = Modus.uriRoot + "/projects/view/"+ this.options.id;
            this.model.fetch({
                success : function(model){
                    document.title = ('Modus: Log ' + model.get('title'));
                    that.render();
                },
                error : function(model,xhr,opts){
                    var msg = Modus.Helper.getMsgXhr(xhr, "");
                    that.$el.html('<div style="margin:10em;color:red"><h2 class="text-center"><i class="fa fa-times-circle"></i> Ocurrió un error al cargar el proyecto.<br/>'+msg+'</h2></div>');
                }
            });
        }
    },    

    events : 
    {
        "click #collapse-column" : "collapseToogle",
    },

    render : function()
    {
        if(this.model && this.model.id>0)
        {
            var model =  this.model.toJSON();
            this.$el.html(this.template({model : model}));
            this.sidebar = this.$('.sidebar');
            this.generalbar = this.$('.generalbar');

            this.sideAnswersZone    = this.$('#sidebar-answers');
            this.sideMyTeamZone    = this.$('#sidebar-myteam');
            this.sideStatisticsZone = this.$('#sidebar-dashboard');
            this.sideMarksZone      = this.$('#sidebar-marks');

            this.lastSidebarZone = this.sidebar.find('.last-block');

            var exist_teams = 0;
            var rooms = this.model.get('rooms');
            _.each(rooms, function(room)
            {
                if(room.teams) exist_teams += room.teams.length;
            });
            if(exist_teams>0)
            {
                this.addSideAnswers();
                this.addSideStatistics();
            }
            if(exist_teams || this.model.get('my_team'))
            {
                this.addSideMyTeam();
            }
            this.goToByOptions();

            if(!this.SideAnswers && !(this.SideMyTeam && this.SideMyTeam.team)  && !this.SideMarks)
            {
                this.collapseToogle();
                this.sidebar.remove();
            }
            else
            {
                if(this.generalbar.height()<this.sidebar.height())
                {
                    this.generalbar.css('min-height',this.sidebar.height()+315);
                }
                this.sidebar.affix({offset: {top : 315, bottom : 100}});
            }
        }
        else
        {
            this.$el.html('<div style="margin-top:10em"><h2 class="text-center"><i class="fa fa-spinner fa-spin"></i> Cargando proyecto...</h2></div>');
        }
        return this;
    },

    addSideAnswers : function()
    {
        if(this.model.get('rooms'))
        {
            var rooms = this.model.get('rooms');
            this.SideAnswers = new Modus.Project.View.SideAnswers({rooms: rooms, options : this.options});
            this.SideAnswers.on('selectTeam', this.onSelectTeam, this);
            this.SideAnswers.on('expand', this.onExpandOne, this);
            this.sideAnswersZone.html(this.SideAnswers.render().el);
        }
    },

    addSideMyTeam : function(room, team)
    {
        if(!this.SideMyTeam)
        {
            this.SideMyTeam = new Modus.Project.View.SideMyTeam({room : room, team : team});
            this.sideMyTeamZone.html(this.SideMyTeam.render().el);
        }
        else
        {
            this.SideMyTeam.changeTeam(room, team);
        }
    },

    addSideStatistics : function()
    {
        if(this.model.get('is_manager'))
        {
            var exist_teams = 0;
            var rooms = this.model.get('rooms');
            _.each(rooms, function(room)
            {
                if(room.teams) exist_teams += room.teams.length;
            });
            if(exist_teams>0)
            {
                this.SideDashBoard = new Modus.Project.View.SideDashboard({rooms: rooms, options : this.options});
                this.SideDashBoard.on('selectDashBoard', this.onSelectDashBoard, this);
                this.SideDashBoard.on('expand', this.onExpandOne, this);
                this.sideStatisticsZone.html(this.SideDashBoard.render().el);

                this.SideMarks = new Modus.Project.View.SideMarks({rooms: rooms, options : this.options});
                this.SideMarks.on('selectMark', this.onSelectMark, this);
                this.SideMarks.on('expand', this.onExpandOne, this);
                this.sideMarksZone.html(this.SideMarks.render().el);
            }
        }
    },

    onExpandOne : function(who)
    {
        if(who)
        {
            if(this.SideAnswers && who!=this.SideAnswers)
            {
                this.SideAnswers.unselect();
                if(this.SideMyTeam && who!=this.SideMyTeam)this.SideMyTeam.unselect();
            }
            else
            {

            }

            if(this.SideMarks && who!=this.SideMarks)this.SideMarks.unselect();
            if(this.SideDashBoard && who!=this.SideDashBoard)this.SideDashBoard.unselect();

        }
    },

    renderGeneralResume : function()
    {
        if(this.actuallyGeneral && this.actuallyGeneral.cid && this.generalResumeView && this.generalResumeView.cid && (this.actuallyGeneral.cid === this.generalResumeView.cid))
        {

        }
        else
        {
            if(this.generalResumeView)
            {
                this.generalbar.html(this.generalResumeView.render().el);
            }
            else
            {
                this.generalResumeView = new Modus.Project.View.Resume({model:this.model, options : this.options});
                this.generalbar.html(this.generalResumeView.render().el);
                this.scrollTop();
            }
        }

        this.actuallyGeneral = this.generalResumeView;
    },

    renderLogs : function()
    {
        if(this.actuallyGeneral && this.actuallyGeneral.cid && this.generalLogsView && this.generalLogsView.cid && (this.actuallyGeneral.cid === this.generalLogsView.cid))
        {

        }
        else
        {
            if(this.generalLogsView)
            {
                this.generalbar.html(this.generalLogsView.render().el);
            }
            else
            {
                this.generalLogsView = new Modus.Project.View.Log({project : this.model, options : this.options});
                this.generalbar.html(this.generalLogsView.render().el);
                this.scrollTop();
            }
        }
        this.actuallyGeneral = this.generalLogsView;
    },
    
    renderStatisticsView : function()
    {
        if(this.options.pageDash == "general")
        {
            if(this.actuallyGeneral && this.actuallyGeneral.cid && this.generalStatisticsView && this.generalStatisticsView.cid && (this.actuallyGeneral.cid === this.generalStatisticsView.cid)) return;
            if(!this.generalStatisticsView)
            {
                this.generalStatisticsView = new Modus.Project.View.StatisticGeneral({project:this.model});
                this.generalStatisticsView.render();
            }
            this.generalbar.html(this.generalStatisticsView.el);
            $('body').scrollTop(150);
            this.actuallyGeneral = this.generalStatisticsView;
        }
        else if(this.options.pageDash == "progress")
        {
            if(this.actuallyGeneral && this.actuallyGeneral.cid && this.progressStatisticsView && this.progressStatisticsView.cid && (this.actuallyGeneral.cid === this.progressStatisticsView.cid)) return;
            if(!this.progressStatisticsView)
            {
                this.progressStatisticsView = new Modus.Project.View.StatisticProgress({project:this.model});
                this.progressStatisticsView.render();
            }
            this.generalbar.html(this.progressStatisticsView.el);
            $('body').scrollTop(150);
            this.actuallyGeneral = this.progressStatisticsView;
        }
    },

    renderAssessmentView : function()
    {
        if(this.actuallyGeneral && this.actuallyGeneral.cid && this.assessmentView && this.assessmentView.cid && (this.actuallyGeneral.cid === this.assessmentView.cid))
            return;

        if(this.assessmentView)
        {
            this.generalbar.html(this.assessmentView.render().el);
        }
        else
        {
            this.assessmentView = new Modus.Project.View.Assessment({project:this.model,roomId:this.options.roomId});
            this.generalbar.html(this.assessmentView.render().el);
        }
        $('body').scrollTop(150);
        this.actuallyGeneral = this.assessmentView;
    },

    goToByOptions : function()
    {
        var opt = this.options;
        if( opt.type === 'dashboard')//Statistics
        {
            if(opt.roomId>0)
            {
                this.SideDashBoard && this.SideDashBoard.changeRoomId(opt.roomId, opt.pageDash);
            }
            else if(opt.nameRoom && this.model.get('rooms'))
            {
                var name = opt.nameRoom;
                var room = _.find(this.model.get('rooms'), function(room){ return room.name == name});
                opt.idRoom = room.id;
                if(room)
                {
                    this.SideDashBoard && this.SideDashBoard.changeRoomId(opt.idRoom, opt.pageDash);
                }
            }
            else if(this.model.get('rooms'))
            {
                var firstRoom = this.model.get('rooms')[0];
                if(firstRoom)
                {
                    opt.idRoom = firstRoom.id;
                    this.SideDashBoard && this.SideDashBoard.changeRoomId(opt.idRoom, opt.pageDash);
                }
            }
        }
        else if(opt.type === 'marks') //Marks
        {
            if(this.options.roomId>0)
            {
                this.SideMarks && this.SideMarks.changeRoomId(this.options.roomId);
            }
            else if(this.model.get('rooms'))
            {
                var firstRoom = this.model.get('rooms')[0];
                if(firstRoom)
                {
                    opt.idRoom = firstRoom.id;
                    this.SideMarks && this.SideMarks.changeRoomId(opt.idRoom);
                }
            }
        }
        else if(opt.type === 'logs') //Logs
        {
            this.renderLogs();
        }
        else //default team
        {
            this.renderGeneralResume();
            //Select a team if exist 
            //Selected team
            if(opt.nameRoom && opt.ordTeam)
            {
                if(!opt.idTeam)
                {
                    var findIdTeam = 0;
                    var rooms = this.model.get('rooms');

                    _.each(rooms, function(room){
                        if(room.name == opt.nameRoom)
                        {
                            _.each(room.teams, function(team){
                                if((team.ord == opt.ordTeam) && !findIdTeam) findIdTeam = team.id;
                            });
                        }
                    });
                    opt.idTeam = findIdTeam;
                }
            }
            if(opt.idTeam>0)
            {
                if(this.SideAnswers)
                {
                    this.SideAnswers.changeTeamId(opt.idTeam);
                }
                else
                {
                    this.onSelectTeam();
                }
            }//Search my Team
            else if(this.model.get('my_team') && this.model.get('my_team').id)
            {
                opt.idTeam = this.model.get('my_team').id;
                if(this.SideAnswers)
                    this.SideAnswers.changeTeamId(opt.idTeam);
                else
                    this.onSelectTeam();
            }//Search first Team
            else if(this.model.get('rooms'))
            {
                var firstIdTeam = false;
                var rooms = this.model.get('rooms');
                _.each(rooms, function(room){
                    _.each(room.teams, function(team){
                        if(!firstIdTeam) firstIdTeam = team.id;
                    });
                });
                if(firstIdTeam)
                {
                    if(this.SideAnswers)
                        this.SideAnswers.changeTeamId(firstIdTeam);
                    else
                        this.onSelectTeam();
                }
            }
        }
    },

    onSelectTeam : function()
    {
        this.changeSideMyTeam();
        this.renderGeneralResume();
        this.generalResumeView.changeTeam(this.options.idTeam);
        this.SideDashBoard && this.SideDashBoard.unselect();
        this.SideMarks && this.SideMarks.unselect();
        this.virtualRoute();
    },

    changeSideMyTeam : function()
    {
        var that = this;
        var room = null, team = null;
        if(this.model.get('rooms'))
        {
            var rooms = this.model.get('rooms'), i = 0;
            while(!team && i<rooms.length)
            {
                var room = rooms[i];
                var teams = room.teams;
                _.each(teams, function(t){
                    if(!team && t.id == that.options.idTeam){
                        team = t;
                    }
                });
                i++;
            }
        }
        if(!team && that.model.get('my_team'))
        {
            team = that.model.get('my_team');
            room = team.room;
        }
        this.SideMyTeam && this.SideMyTeam.changeTeam(room,team);
        //this.generalbarTitle.html("<h4>Viendo respuestas del "+team.get('name')+" - Sección \"" + room.get('name') + "\"</h4>")
    },

    onSelectDashBoard : function(room, pageDash)
    {
        this.SideMyTeam && this.SideMyTeam.changeTeam();
        this.SideAnswers && this.SideAnswers.unselect();
        this.SideMarks && this.SideMarks.unselect();

        this.options.type = "dashboard";
        this.options.pageDash = pageDash;
        this.options.roomId = room.id;
        this.options.nameRoom = room.name;

        this.renderStatisticsView();

        pageDash == 'general' && this.generalStatisticsView && this.generalStatisticsView.changeRoom(room);
        pageDash == 'progress' && this.progressStatisticsView && this.progressStatisticsView.changeRoom(room);

        this.virtualRoute();
    },

    onSelectMark : function(room)
    {
        this.SideAnswers && this.SideAnswers.unselect();
        this.SideMyTeam && this.SideMyTeam.changeTeam();            
        this.SideDashBoard && this.SideDashBoard.unselect();

        this.options.type = "marks";
        this.options.roomId = room.id;
        this.options.nameRoom = room.name;

        this.renderAssessmentView();

        this.assessmentView && this.assessmentView.changeRoom(room);

        this.virtualRoute();
    },

    collapseToogle : function()
    {
        var c = this.$('#collapse-column');
        if( c.text().indexOf("una")>0 )
        {
            c.html('<i class="fa fa-caret-square-o-left"></i> Mostrar dos columnas');
            this.sidebar.parent().removeClass('col-sm-3 col-md-2');
            //this.sidebar.removeClass('affix affix-top affix-bottom');
            this.sidebar.css({position:'',width:'100%'});
            this.generalbar.removeClass('col-sm-9 col-md-10');
            $(window).off('.affix');
            this.sidebar.removeData('affix').removeClass('affix affix-top affix-bottom')
        }
        else
        { 
            c.html('<i class="fa fa-caret-square-o-left"></i> Mostrar una columna');
            this.sidebar.parent().addClass('col-sm-3 col-md-2');
            //this.sidebar.css({position:'',width:this.sidebar.width()+30});
            this.generalbar.addClass('col-sm-9 col-md-10');
            //$(window).on('.affix');
            //this.sidebar.affix({offset: {top : 300, bottom : 100}}).css("width",this.sidebar.width()+30);
        }
    },
    virtualRoute : function()
    {
        var opt = this.options;
        var last_url = '';
        if(opt.type == 'dashboard')
        {
            last_url = (opt.nameRoom && opt.pageDash)? '/dashboard/' +opt.nameRoom + '/' + opt.pageDash:'';
        }
        else if(opt.type == 'marks')
        {
            last_url = (opt.nameRoom)? '/marks/' +opt.nameRoom:'';
        }
        else if(opt.type == 'logs')
        {
            last_url = '/logs';
        }
        else //team
        {
            last_url = (opt.nameRoom && opt.ordTeam)?('/team/' + opt.nameRoom + '/' + opt.ordTeam + (opt.namePhase?('/'+opt.namePhase):'')):'';
        }
        var url = '/project/view/' + opt.id + '/'  + opt.slug + last_url;
        Modus.GeneralRouter.navigate(url, { trigger: false,replace:true });
    },

    scrollTop : function()
    {
        $('body').scrollTop(150);
    }
});