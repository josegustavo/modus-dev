/**
 * ID : Instructor.Model.InstructorList
 * require : Instructor.Model.Instructor
 */
Modus.Instructor.Model.InstructorList = Backbone.Collection.extend(
{
    model : Modus.Instructor.Model.Instructor,
    url : Modus.uriRoot + '/instructors',
    parse : function(response, options)
    {
        return response.success?response.success:response;
    },
    countNew : function()
    {
        return this.getNew().length;
    },
    getNew : function()
    {
        return this.filter(function(n) { return n.isNew(); });
    },
    countModified : function()
    {
        return this.filter(function(n) { 
            return n.get('isChanged');
        }).length;
    },    
    countSelected : function()
    {
        return this.getSelected().length;
    },
    getSelected : function()
    {
        return this.where({'isSelected':true});
    }
});