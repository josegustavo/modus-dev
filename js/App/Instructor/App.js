/**
 * ID : Instructor.App
 * require : Instructor.Model.Selected
 * require : Instructor.View.Instructor
 * require : Instructor.View.RoomSelect
 */
Modus.Instructor.App = Backbone.View.extend(
{
    tagName : "div",
    className : "row",
    template : _.template(document.getElementById('tmpl-instructors-instructors').innerHTML),
    template_ap : _.template(document.getElementById('tmpl-instructors-subpanel').innerHTML),//Additional panel
    template_instr : _.template(document.getElementById('tmpl-instructors-panel').innerHTML),//Instructors zone

    initialize : function(data)
    {
        this.data = data;
        this.Selected = new Modus.Instructor.Model.Selected();
        this.Selected.on({"changeSelected" : $.proxy(this.onSelected,this)});
    },

    render : function()
    {
        var that = this;
        var container = $(this.template({
                    schoolId:this.schoolId, levelId:this.levelId,
                    gradeId:this.gradeId, roomId: this.roomId
                })
        ); 
        that.$el.html(container);

        that.leftpanel = that.$('.instructor-panel-left');
        that.RoomSelect = new Modus.Instructor.View.RoomSelect(that.Selected);
        that.leftpanel.html(that.RoomSelect.el);
        that.assignForm = that.$('.assign-instructor-form')

        that.initializeTypeHead();

        that.RoomSelect.on("error", function(msg)
        {
            that.$el.html('<div style="margin-top:10%;color:red"><h2 class="text-center"><i class="fa fa-times-circle"></i> Ocurrio un error<br/>'+msg+'</h2></div>')
        });
        var data = that.data;
        that.RoomSelect.on("loadSuccess", function()
        {
            //Add leftpanel
            var leftpanel = that.RoomSelect.$el;
            leftpanel.append(that.template_ap());
            that.leftpanel.html(leftpanel);
            that.Selected.set({
                schoolId : data.schoolId||0,
                levelId : data.levelId||0,
                gradeId : data.gradeId||0,
                roomId : data.roomId||0
            });
            if(data.schoolId && data.levelId && data.gradeId)
            {
                that.onSelected();
            }
        });

        return this;
    },

    initializeTypeHead : function()
    {
        var that = this;
        that.inputAssign = that.$('#assign-instructor-input');
        that.searchList = new Bloodhound({
            datumTokenizer: function(d) { return Bloodhound.tokenizers.whitespace(d.name + ' ' + d.username); },
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            prefetch:  {
              url: Modus.uriRoot + '/users/instructors',
              filter: function(list) {
                return list.success;
              }
            }
        });
        that.searchList.clearPrefetchCache();
        that.searchList.initialize(true);

        that.inputAssign.typeahead({
            hint: true,
            highlight: true,
            minLength: 1
        },
        {
            name: 'instructors',
            displayKey : "name",
            source: that.searchList.ttAdapter(),
            templates: {
                suggestion: _.template('<p><%=name%> (<i><%=username%></i>)</p>')
            }
        });
        that.inputAssign.on("typeahead:selected", function(event, suggest, nameds)
        {
            that.assignInstructor(suggest);
        });
    },

    events : 
    {
        "click .btn-save"   : "saveChangesInstructor",
        "click .btn-delete" : "deleteSelected",
        "click .delete"  : "deleteSelected",
        "click .create-user-instructor" : "createUserInstructor",
        "click .import-from-csv" :"importInstructors",
        "click .retry-load-instructors" : "onSelected"
    },

    assignInstructor : function(instructor)
    {
        var that = this;
        that.showMsgAssign("<i class='fa fa-cog fa-spin'></i> Asignando profesor...","warning");
        var sel = this.Selected;
        var schoolId = sel.get('schoolId'),levelId = sel.get('levelId'),gradeId = sel.get('gradeId'), roomId = sel.get('roomId');
        var instructors;
        var grade = this.Selected.getGrade();
        if(!grade.get('instructors'))
        {
            instructors = new Modus.Instructor.Model.InstructorList();
            instructors.url = Modus.uriRoot+'/instructors/'+ schoolId +'/' + gradeId;
            grade.set('instructors', instructors);
        }
        instructors = grade.get('instructors');

        var rooms = [];
        if(that.Selected.getRoom())
        {
            rooms.push(that.Selected.getRoom().id);
        }
        else
        {
            rooms = grade.get("rooms").map(function(s){return s.id;});
        }
        var success = function()
            {
                that.showMsgAssign("Se asignó correctamente el profesor.","success");
                that.inputAssign.typeahead('val', "");
            };
        var error = function(m, xhr)
            {
                that.inputAssign.typeahead('val', "");
                var msg = Modus.Helper.getMsgXhr(xhr,"Se produjo un error al asignar un Docente, intentelo nuevamente.\n ");
                that.showMsgAssign(msg,"danger");
            };
        if(instructors.get(instructor.id))
        {
            var has_changed = false;
            var rooms_actual = instructors.get(instructor.id).get("rooms");
            _.each(rooms, function(s){if(!_.contains(rooms_actual,s))
                {
                    rooms_actual.push(s);
                    has_changed = true;
                }});
            if(has_changed)
            {
                var instructor = instructors.get(instructor.id);
                instructor.set("rooms",rooms_actual);
                instructor.url =  Modus.uriRoot+'/instructors/'+ schoolId +'/' + gradeId + '/' + instructor.id;
                instructor.save({},
                        {
                            wait : true,
                            success : function()
                            {
                                success();
                                that.fillInstructors();
                            },
                            error : error
                        });
            }
            else
            {
                success();
                that.showMsgAssign("Este profesor ya se encuentra en la lista","warning");
            }
        }
        else
        {

            instructor.rooms = rooms;
            instructors.create(instructor,{ wait: true,success: success, error : error });
        }
    },

    showMsgAssign : function(msj, css, elem)
    {
        elem = elem || this.$('.alertassign');
        elem.removeClass(function (index, css) {return (css.match (/\balert-\S+/g) || []).join(' ');});
        elem.addClass("alert-" + (css || 'warning'));
        elem.html(msj);
        elem.show();
    },

    setStatus : function(msg,hide)
    {
        if(!this.status)this.status=this.$('#who-instructors');
        if(this.status){
            this.status./*show().*/fadeTo(0, 1);
            this.status.html(msg);
            if(hide)
                this.status.delay(5000).fadeTo(2000, 0/*, function(){$(this).hide();}*/);
        }
    },

    onSelected : function()
    {
        var sel = this.Selected;
        var schoolId = sel.get('schoolId'),levelId = sel.get('levelId'),gradeId = sel.get('gradeId'), roomId = sel.get('roomId');

            this.gradeIdActual = gradeId;
            this.schoolIdActual = schoolId;
            if(schoolId>0 && levelId>0 && gradeId>0)
            {
                this.loadInstructors();
            }
            else
            {
                this.$('#panel-instructors').html('<div class="alert alert-info">Para ver los Docentes seleccione un Colegio y Grado.</div>');
                this.$('.assign-instructor-form').addClass('hidden');
                this.setStatus("");
            }
    },

    loadInstructors : function()
    {
        var that = this;
        var sel = this.Selected;
        var s = sel.get('Schools');
        var schoolId = sel.get('schoolId'),gradeId = sel.get('gradeId'), roomId = sel.get('roomId');

        this.$('#panel-instructors').html('<div class="text-center"><i class="fa fa-spinner fa-spin"></i> Cargando...</div>');
        that.$('.assign-instructor-form').addClass('hidden');
        that.setStatus("");

        var grade = this.Selected.getGrade();
        if(!grade.get('instructors'))
        {
            grade.set('instructors', new Modus.Instructor.Model.InstructorList());
            var instructors = grade.get('instructors');
            instructors.url = Modus.uriRoot+'/instructors/'+ schoolId +'/' + gradeId;
            that.fillInstructors();
            that.instructors.html('<tr class="loading"><td colspan="7" style="display:table-cell;background-color:#fff;"><center style="margin-top:10%;margin-bottom:10%"><h4><i class="fa fa-circle-o-notch fa-spin"></i> Cargando...</h4></center></td></tr>');
            instructors.on("add", that.addOne, that);
            instructors.on("remove", that.removeOne, that);
            instructors.fetch({
                success : $.proxy(that.successLoadInstructors, that),
                error : function(model, xhr)
                {
                    var msg = Modus.Helper.getMsgXhr(xhr, "No se pudo cargar la lista de profesores.");
                    that.instructors.html('<tr class="additional-info"><td colspan="5"><div  class="text-center" style="color:red;"><span><i class="fa fa-times-circle"></i> '+msg+'<br/><u style="cursor:pointer;" class="retry-load-instructors">Reintentar</u></span></div></td></tr>');
                    grade.unset('instructors');
                }
            });
        }
        else
        {
            that.fillInstructors();
        }
    },

    successLoadInstructors : function()
    {
        var that = this;
        var grade = this.Selected.getGrade();
        var room_id = 0;
        if(this.Selected.getRoom())
        {
            room_id = this.Selected.get('roomId');
        }
        var exist = false;
        grade.get('instructors').each(function(instructor){
            var ins_rooms = instructor.get("rooms");
            if(room_id <= 0 || _.contains(ins_rooms,room_id))
            { 
                exist = true;
            }
        });
        if(!exist)
        {
            that.instructors.html('<tr class="additional-info"><td colspan="5"><div class="text-center"><span><i class="fa fa-exclamation-circle"></i> No se encontraron docentes asignados.</span></div></td></tr>');
        }
    },

    fillInstructors : function()
    {
        var that = this;
        var sel = this.Selected;
        var roomId = sel.get('roomId');
        var school = sel.getSchool(),level = sel.getLevel(),grade = sel.getGrade(), room = sel.getRoom();
        this.setStatus("Lista de Docentes: <span style='color:gray'>\"" + school.get('name')+"\"  "+ grade.get("name") + "° de "+ level.get('name')+""+((roomId>0)?("  Sección \""+room.get('name')+"\""):"") + "</span>" , false);

        var t = $(that.template_instr()); 
        that.$('#panel-instructors').html(t);
        that.count = t.find('#allcount');
        that.instructors = t.find('#instructos-list');
        that.checkboxAll = t.find('#all-check');
        that.checkboxAll.on("change", function(e){
            var checked = e.currentTarget.checked;
            that.instructors.find('.select-check').each(function()
            {
                if(checked && !this.checked)
                    $(this).click();
                else if(!checked && this.checked)
                    $(this).click();
            });
        });
        that.$('.assign-instructor-form').removeClass('hidden');
        var grade = this.Selected.getGrade();
        if(grade.get('instructors').length == 0 && this.instructors)
        {
            that.instructors.html('<tr class="additional-info hidden"><td colspan="5"><div class="text-center"><span><i class="fa fa-exclamation-circle"></i> No se encontraron docentes asignados.</span></div></td></tr>');
        }
        else
        {
           grade.get('instructors').each($.proxy(that.addOne,that));
           this.successLoadInstructors();
        }
    },

    addOne : function(instructor) 
    {
        var that = this;
        if(that.instructors.find(".loading").length)
        {
            that.instructors.empty();
        }
        var room_id = 0;
        if(this.Selected.getRoom())
        {
            room_id = this.Selected.get('roomId');
        }
        var ins_rooms = instructor.get("rooms");
        if(room_id == 0 || _.contains(ins_rooms,room_id))
        {
            this.instructors.find('.additional-info').addClass("hidden");
            var sel = this.Selected;
            var grade = sel.getGrade();
            var view = new Modus.Instructor.View.Instructor({model: instructor,rooms:grade.getRooms()});
            view.on("changed", this.changedOne, this);
            view.on("editUserInstructor", this.editUserInstructor, this);
            view.on("onPreRemove", this.onPreRemoveInstructor, this);
            this.instructors.append(view.render().el);
        }
    },
    
    onPreRemoveInstructor : function()
    {
        this.showMsgAssign("<i class='fa fa-cog fa-spin'></i> Quitando profesor...","warning");
    },
    
    removeOne : function()
    {
        this.showMsgAssign("Se quitó correctamente el profesor de la lista.","success");
        this.successLoadInstructors();
    },

    changedOne : function(view)
    {
        var room_id = 0;
        if(this.Selected.getRoom())
        {
            room_id = this.Selected.get('roomId');
        }
        var ins_rooms = view.model.get("rooms");
        if(room_id > 0 && !_.contains(ins_rooms,room_id))
        {
            view.remove();
        }
        this.successLoadInstructors();
    },

    showMsg : function(msj, css, elem)
    {
        elem = elem || this.alert;
        elem.removeClass(function (index, css) {return (css.match (/\balert-\S+/g) || []).join(' ');});
        elem.addClass("alert-" + (css || 'warning'));
        elem.html(msj);
        elem.show();
    },

    saveChangesInstructor : function()
    {
        var unindexed_array = this.modal.find('#form-new-instructor').serializeArray();
        var indexed_array = {};

        $.map(unindexed_array, function(n, i){
            indexed_array[n['name']] = n['value'];
        });
        indexed_array['name'] = indexed_array['firstname'] + ' ' + indexed_array['lastname'];
        var that = this;
        var success = function()
        {
            if(that.modal) that.modal.modal('hide');
            that.searchList.clear();
            that.searchList.clearPrefetchCache();
            that.searchList.initialize(true);
        };
        var error = function(model, xhr, options)
        {
            var msg = Modus.Helper.getMsgXhr(xhr,"Se produjo un error al crear/modificar el docente, intentelo nuevamente.");
            that.showMsg(msg,"danger", that.modal.find('.alert'));
        }
        if(this.editingUser)
        {
            this.editingUser.url = Modus.uriRoot + '/users/instructors/' + this.editingUser.id;
            this.editingUser.save(indexed_array, {
                wait: true,
                success : success, error : error
            });
        }
        else
        {
            var instructor = new Modus.Instructor.Model.Instructor();
            instructor.url = Modus.uriRoot + '/users/instructors';
            instructor.save(indexed_array, { wait: true, success : success, error : error });
        }
    },

    createUserInstructor : function()
    {
        this.editingUser = null;
        this.modal = this.$('#modal-instructor');
        this.modal.find('.alert').hide();
        this.modal.find(".modal-title").text("Agregar Nuevo Docente");
        this.modal.find('#form-new-instructor').trigger("reset");
        this.modal.find('#newUser').removeAttr("disabled");
        this.modal.modal();
    },

    editUserInstructor : function(user)
    {
        this.editingUser = user;
        this.modal = this.$('#modal-instructor');
        this.modal.find('.alert').hide();
        this.modal.find(".modal-title").text("Editar Docente");
        this.modal.find('#form-new-instructor').trigger("reset");

        this.modal.find('#newUser').attr("disabled","disabled");
        this.modal.find('#newUser').val(user.get('username'));
        this.modal.find('#newName').val(user.get('firstname'));
        this.modal.find('#newLastname').val(user.get('lastname'));
        this.modal.find('#newEmail').val(user.get('email'));
        this.modal.find('#newPassword').val('');
        this.modal.modal();
    },

    importInstructors : function(e)
    {
        var that = this;
        var button = $(e.currentTarget);
        var $upload = $(' <input/>').attr({type:'file', accept : "application/csv"});
        $upload.fileupload({
            add: Modus.Helper.fileUploadRestriction,
            url: Modus.uriRoot+'/users/instructors/import/',
            formData : {},
            dataType: 'json',
            done : function (e,data) 
            {
                if(data.result && data.result.success && data.result.success.error)
                {
                    var error = data.result.success.error;
                    if(_.isObject(error) || _.isArray(error)){
                        _.each(error,function(err){
                            alert(err);
                        });
                    }
                    else
                    {
                        alert(error);
                    }
                }
                else if(data.result && data.result.success && data.result.success.success)
                {
                    alert(data.result.success.success);
                }
                that.searchList.clear();
                that.searchList.clearPrefetchCache();
                that.searchList.initialize(true);
            },
            error : function(e, data)
            {
                alert("Se produjo un error al importar. comuníquese con el administrador");
            },
            always : function(e, data)
            {
                button.removeAttr("disabled");
                button.find('.fa-spin').remove();
            },
            progressall: function (e, data)
            {
                if(!button.find('.fa-spin').length)
                {
                    button.attr("disabled", "disabled");
                    button.append('&nbsp;<i class="fa fa-spinner fa-spin"></i>');
                }
            }
        }).click();

        return true;
    },
});