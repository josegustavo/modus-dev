/**
 * ID : Instructor.View.RoomSelect
 * require : School.Model.SchoolList
 */ 
Modus.Instructor.View.RoomSelect = Backbone.View.extend(
{
    tagName:  "div",
    className : "row",
    template: _.template(document.getElementById('tmpl-panel-select-room').innerHTML),

    initialize : function(selected)
    {
        var that = this;
        that.$el.html('<center style="margin-top: 20%;margin-bottom: 20%;"><a><i class="fa fa-circle-o-notch fa-spin"></i> Cargando...</a></center>');
        this.Selected = selected;
        this.Selected.on({"change" : $.proxy(this.fillSchool,this)});
        this.Selected.on({"change" : $.proxy(this.virtualRoute,this)});

        if(!this.Schools || this.Schools.length<=0){
            this.Schools = new Modus.School.Model.SchoolList();
            this.Selected.set('Schools',this.Schools);
            this.Schools.url = Modus.uriRoot+'/schools/listAll';
            this.Schools.fetch({
                success : function()
                {
                        that.render();
                        that.trigger("loadSuccess");
                        that.fillSchool();
                },
                error : function(m, xhr)
                {
                    var msg = Modus.Helper.getMsgXhr(xhr,"Error al obtener Colegios");
                    that.trigger("error", msg);
                }
            });
        }
    },
    render : function()
    {
        this.$el.html($(this.template()));

        this.schoolInput = this.$("#school-input");
        this.gradeInput = this.$("#grade-input");
        this.roomInput = this.$("#room-input");

        this.schoolDDM = this.$("#school-dropdown-menu");
        this.gradeDDM = this.$("#grade-dropdown-menu");
        this.roomDDM = this.$("#room-dropdown-menu");

        return this;
    },

    fillSchool : function()
    {
        var schoolId = this.Selected.get("schoolId");
        var school;
        if(schoolId>0) school = this.Schools.get(schoolId);

        var count = this.Schools.length;
        if(count === 1)
        {
            this.schoolInput.next().find('button').addClass('disabled');
            this.Selected.set({schoolId:this.Schools.at(0).id});
            school = this.Schools.at(0);
        }
        else if(count > 1)
        {
            this.schoolDDM.empty();
            this.Schools.each(
                function(s,index){
                    if(!school || s.id != school.id){
                        var li = $('<li/>');
                        li.html($('<a/>').text(s.get('name')));
                        li.on('click',$.proxy(function(){
                                        this.Selected.set({schoolId:s.id, roomId:0});
                                        this.Selected.trigger("changeSelected");
                                        this.fillRoom();

                                    },this)
                        );
                        this.schoolDDM.append(li);
                    }
                },
            this);
        }
        if( school )
        {
            var school_name = school.get('name');
            this.schoolInput.text(school_name);
            this.fillGrade();
        }
    },

    fillGrade : function()
    {
        var that = this;
        var schoolId = that.Selected.get("schoolId");
        var school;
        if(schoolId>0) school = that.Schools.get(schoolId);

        if(school)
        {
            var onlyOneElement = false;
            var levels = school.getLevels();
            if((levels.length === 1) && (levels.at(0).getGrades().length === 1))
            {
                onlyOneElement = true;
            }
            if( onlyOneElement )
            {
                var level = levels.at(0);
                var grade = level.getGrades().at(0);
                that.Selected.set({levelId : level.id, gradeId : grade.id});
                that.gradeInput.next().find('button').addClass('disabled');
                that.Selected.trigger("changeSelected");
            }
            else
            {
                that.gradeInput.next().find('button').removeClass('disabled');
                that.gradeDDM.empty();
                levels.each(function(level){
                    var li = $('<li/>');
                    li.addClass('pull-left').append('<dt><span class="text">'+level.get("name")+'</span></dt>');
                    level.getGrades().each(function(grade){
                            var a = $('<a/>');
                            a.text(grade.get("name")+'\u00B0 Grado').on('click',
                                function(){
                                    that.Selected.set({levelId : level.id, gradeId : grade.id, roomId:0});
                                    that.Selected.trigger("changeSelected");
                                }
                            );
                            li.append(a);
                    });
                    that.gradeDDM.append(li);
                });
            }
            var levelId = that.Selected.get("levelId");
            var gradeId = that.Selected.get("gradeId");
            if(levelId>0 && gradeId>0)
            {
                var level = levels.get(levelId);
                if(level)
                {
                    var grade = level.getGrades().get(gradeId);
                    if(grade)
                    {
                        that.gradeInput.text(grade.get("name")+'\u00B0 Grado de '+level.get("name"));
                    }
                    else
                    {
                        that.Selected.set({levelId : 0, gradeId : 0, roomId:0});
                    }
                }
                else
                {
                    that.Selected.set({levelId : 0, gradeId : 0, roomId:0});
                }
            }else{
                that.gradeInput.text("");
            }
            that.fillRoom();
        }
    },

    fillRoom : function()
    {
        var schoolId = this.Selected.get("schoolId");
        var school;
        if(schoolId>0) school = this.Schools.get(schoolId);
        if(school)
        {
            this.roomDDM.empty('');
            var levels = school.getLevels();
            var levelId = this.Selected.get("levelId");
            var gradeId = this.Selected.get("gradeId");
            if(levelId>0 && gradeId>0){
                var level = levels.get(levelId),
                grade = level.getGrades().get(gradeId);

                if(grade)
                {
                    this.roomInput.next().find('button').removeClass("disabled");

                    var li = $('<li/>')
                    .html($('<a/>').text('Todas')).on('click',  $.proxy(function(e){
                                this.Selected.set("roomId", 0);
                                this.Selected.trigger("changeSelected");    
                            },this));
                    this.roomDDM.append(li);
                    var rooms = grade.getRooms();
                    if(rooms)
                    {
                        rooms.each(function(room)
                        {
                            var li = $('<li/>');
                            li.html($('<a/>').text('Sección "'+room.get("name")+'"'))
                              .on('click', $.proxy(function(e)
                                {
                                    if(this.Selected.get('roomId') != room.id)
                                    {
                                        this.Selected.set({roomId : room.id});
                                        this.Selected.trigger("changeSelected");
                                    }
                                },this)
                              );
                            this.roomDDM.append(li);
                        },this);

                        var roomId = this.Selected.get("roomId");
                        if(roomId == 0){
                            this.roomInput.text("Todas");
                        }else if(roomId > 0){
                            var room = rooms.get(roomId);
                            this.roomInput.text(room.get("name"));
                        }else{
                            this.roomInput.text("");
                        }
                    }
                    else
                    {
                        this.roomInput.text("No tiene");
                        this.roomInput.next().css("visibility", "hidden");
                    }
                }
            }
            else
            {
                this.Selected.trigger("changeSelected");
                this.roomInput.next().find('button').addClass("disabled");
                this.roomInput.text("");
            }
        }
    },

    virtualRoute : function()
    {
        var url = '/instructor';
        var sel = this.Selected;
        var schoolId = sel.get("schoolId"), levelId = sel.get("levelId"),
                gradeId = sel.get("gradeId"), roomId = sel.get("roomId");
        url += schoolId?'/'+schoolId:'';
        url += schoolId&&levelId&&gradeId?'/'+levelId+'/'+gradeId:'';
        url += schoolId&&levelId&&gradeId&&roomId?'/'+roomId:'';
        Modus.GeneralRouter.navigate(url, { trigger: false,replace:true });
    }

});
