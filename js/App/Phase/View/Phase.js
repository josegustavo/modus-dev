/**
 * ID : Phase.View.Phase
 */
Modus.Phase.View.Phase = Backbone.View.extend(
{
    tagName:  "div",
    className : "col-sm-6",
    template: _.template($('#tmpl-phases-view').html()),

    initialize : function()
    {
        this.model.on("change", this.render, this);
    },

    render : function(){
        this.$el.html($(this.template({model : this.model.toJSON()})));
        return this;
    },

    events : {
        "click .action-delete"  : "clear",
        "click .action-apply" : "applyAllPhases",
        "click .goToEdit" : "goToEdit"
    },

    goToEdit : function()
    {
        this.trigger("goToEdit", this.model);
        return false;
    },

    clear : function()
    {
        if(confirm("¿Está seguro que desea eliminar esta fase?"))
        {
            this.model.url = Modus.uriRoot+'/phases/'+this.model.id;
            this.model.destroy(
                {
                wait: true, 
                error : $.proxy(function(model,response){
                        var resp = response.responseJSON;
                        alert(resp.error?resp.error:'No se pudo eliminar.');
                    },this),
                success : $.proxy(this.remove,this)
                });
        }
    },

    applyAllPhases : function()
    {
        var order = this.model.get('ord');
        if(confirm("¿Está seguro que desea aplicar esta fase como base a todas las fases existentes de orden "+order+"?\n- Si el número elementos de la lista de cotejo ha cambiado, se eliminaran todas las calificaciones asociadas a las fases.\n- Una vez comience el proceso no podrá ser cancelado."))
        {
            var that = this;
            $.ajax({
                dataType: "json",
                url: Modus.uriRoot+'/phases/getListAllPhases/'+order,
                success: function(result)
                {
                    if(result.error)
                    {
                        alert(result.error);
                        return;
                    }
                    var success = result.success;
                    if(success.length > 0)
                    {
                        var modal = $('<div class="modal fade"><div class="modal-dialog"><div class="modal-content">'
                        + '<div class="modal-header"><h4 class="modal-title">Aplicando cambios</h4></div><div class="modal-body">'
                        + '<div class="progress progress-striped active"><div class="progress-bar" style="width: 0%"><span class="status-text">'
                        + '</span></div></div></div></div></div></div>');
                        modal.modal();
                        var progress = modal.find('.progress-bar');
                        var status = modal.find('.status-text');
                        var sending = 0,changed=0, total = success.length;
                        var send = function(id){
                            $.ajax({
                                url : Modus.uriRoot+'/phases/resetPhase',
                                data : {phaseId:that.model.id,phaseDest:id},
                                type : 'POST',
                                async: true,
                                success : function(response) {
                                    changed++;
                                    progress.css('width',((100*(changed))/(total))+'%');
                                    status.text(changed + '/' + total + ' completado');
                                }
                            });
                            sending++; 
                        }
                        var time = 0;
                        _.each(success,function(id){
                            setTimeout(function(){send(id)},time);
                            time += 300;
                        },this);
                    }
                }
            });
        }
    }
});
