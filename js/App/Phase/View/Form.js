/**
 * ID : Phase.View.Form
 * require : Strategy.Model.StrategyList
 * require : Tool.Model.ToolList
 * require : Phase.Model.TemplatePhase
 * require : Phase.Model.RuleList
 */   
Modus.Phase.View.Form = Backbone.View.extend(
{
    tagName     :  "div",
    className   : "tab-pane active",
    template    : _.template($('#tmpl-phases-add').html()),
    Strategies  : null,
    Tools       : null,

    initialize : function(params)
    {
        this.Strategies = new Modus.Strategy.Model.StrategyList();
        this.Tools = new Modus.Tool.Model.ToolList();
        
        this.AppPhase = params.AppPhase;
        if(!this.model && params && params.phaseId)
        {
            this.model = new Modus.Phase.Model.TemplatePhase();
            this.model.url = Modus.uriRoot+'/phases/'+params.phaseId;
            this.model.fetch();
        }
        else if(this.model && this.model.id)
        {
            this.model.url = Modus.uriRoot+'/phases/'+this.model.id;
        }else
        {
            this.model = new Modus.Phase.Model.TemplatePhase();
            this.model.url = Modus.uriRoot+'/phases/';
        }

        if( this.Strategies.length <=0 )
        {
            this.Strategies.url = Modus.uriRoot+'/strategies/list';
            this.Strategies.fetch();
        }
        if( this.Tools.length <= 0 )
        {
            this.Tools.url = Modus.uriRoot+'/tools/list';
            this.Tools.fetch();
        }
    },

    render : function()
    {
        this.$el.html($(this.template({tab:this.tab})));

        this.title          = this.$('#phase-title');
        this.image          = this.$('#phase-image');
        Modus.Helper.activeImageSelector(this.image);
        this.description    = this.$('#phase-description');
        this.strategies     = this.$("#strategy-select");
        this.tools          = this.$("#tool-select");
        this.can_answer     = this.$("#phase-opt-cananswer");
        this.can_mark       = this.$("#phase-opt-canmark");
        this.text_answer    = this.$("#phase-text-answer");
        this.showfinal_others= this.$("#phase-opt-showallothers");
        this.ruleCriterion  = this.$("#rule-criterion");
        this.ruleScore      = this.$("#rule-score");
        this.ruleList       = this.$("#rule-list");

        Modus.Helper.activateSummerNote(this.description);

        this.image.find('.status').removeClass("hidden");

        if(this.model)
        {
            if(this.model.id) this.bind();
            else this.model.once("sync",this.bind,this);
        }

        return this;
    },

    events : 
    {
        "click .btn-save"           : "save",
        "click .btn-cancel"         : "cancel",
        "keyup #strategy-select"    : "showSelectStrategy",
        "keyup #tool-select"        : "showSelectTool",
        "keyup #rule-criterion"     : "addRuleOnEnter",
        "change #rule-score"        : "addRule"
    },

    bind : function()
    {
        var that = this;
        that.title.val(that.model.get('name'));
        if(that.model.get('image_url'))
        {
            that.image.css('background',"url('"+ that.model.get('image_url') +"') no-repeat center center");
            var w = that.model.get('image_width') || 100;
            var h = that.model.get('image_height') || 120;
            var showWidth = Math.round(120*w/h);
            that.image.css({width:showWidth,height:120});
            that.image.attr("image_id", that.model.get('image_id'));
            that.image.find('.status').addClass("hidden").html("");
        }
        else
        {
            that.image.find('.status').removeClass("hidden");
        }
        this.description.code(this.model.get('description'));
        this.text_answer.val(this.model.get('text_answer'));

        (this.model.get('can_answer') == 1)
            ? this.can_answer.prop('checked','checked')
            : this.can_answer.removeAttr('checked');

        (this.model.get('can_mark') == 1)
            ? this.can_mark.prop('checked','checked')
            : this.can_mark.removeAttr('checked');

        (this.model.get('showfinal_others') == 1)
            ? this.showfinal_others.prop('checked','checked')
            : this.showfinal_others.removeAttr('checked');

        ((this.Strategies.length>0) && this.bindStrategies()) || this.Strategies.once("sync",this.bindStrategies,this);
        ((this.Tools.length>0) && this.bindTools()) || this.Tools.once("sync",this.bindTools,this);
        this.bindRules();
    },    

    bindTools : function()
    {
        var tools = this.model.get('tools') || [];
        _.each(tools, function(id){
            var tool = this.Tools.get(id);
            if(tool) this.addTool(tool);
        }, this);
    },

    bindStrategies : function()
    {
        var strategies = this.model.get('strategies') || [];
        _.each(strategies, function(id){
            var strategy = this.Strategies.get(id);
            if(strategy) this.addStrategy(strategy);
        }, this);
    },

    bindRules : function()
    {
        var rules = this.model.get('rules');
        rules.each(function(rule){
            this.addRuleView(rule);
        }, this);
    },

    save : function()
    {
        var that = this;
        if(!this.model.get('rules')) this.model.set('rules',new Modus.Phase.Model.RuleList());
        var totalScore = this.model.get('rules').totalScore();
        if( totalScore!==20)
        {
            if(this.can_mark.is(':checked') || totalScore !== 0)
            {
                alert("El puntaje total de la lista de cotejo debe sumar 20 puntos.\n\nActualmente suma "+ totalScore + " puntos");
                return;
            }
        }
        this.model.save(
            {
                name                : that.title.val(),
                image_id            : that.image.attr('image_id'),
                description         : that.description.code(),
                can_answer          : that.can_answer.is(':checked'),
                can_mark            : that.can_mark.is(':checked'),
                text_answer         : that.text_answer.val(),
                showfinal_others    : that.showfinal_others.is(':checked')
            },
            {
                wait: true,
                success : function(e)
                {
                    that.trigger("save",that.model);
                },
                error : function(model, xhr)
                {
                    that.AppPhase.showMsgXhr(xhr, "No se pudo guardar la Fase");
                }
            });
    },

    showImageStatus : function(html, isError)
    {
        var that = this;
        if(isError)
        {
            that.image.addClass("down");
        }
        else
        {
            that.image.removeClass("down");
        }
        that.image.find('.status').removeClass("hidden").html(html);
    },

    cancel: function(e)
    {
        this.trigger("cancel",this.model);
    },

    showSelectStrategy : function()
    {
        var search = this.strategies.val();
        var container = this.strategies.parent();
        var dropdown = container.find('.dropdown-menu');
        if( search.length>0 ){
            if( !dropdown.length ){
                dropdown = $('<ul/>');
                dropdown.addClass('dropdown-menu strategy-dropdown');
                container.append(dropdown);
            }
            var strategies = this.Strategies.filter(function(strategy) {
                var name = strategy.get("name").toLowerCase();
                var find = search.toLowerCase();
                return (name.indexOf(find) >= 0);
            });
            if( strategies.length ){
                dropdown.empty();
                _.each(strategies, function(strategy, index, list){
                    if( index < 5){
                        var li = $('<li/>')
                        .append($('<a/>').html(strategy.get('name')))
                        .data('strategyId',strategy.get('id'))
                        .click($.proxy(function(){
                            this.addStrategyById(strategy.get('id'));
                        },this));
                        dropdown.append(li);
                    }
                },this);
                container.addClass('open');
            }else{
                container.removeClass('open');
            }
        }else{
            container.removeClass('open');
        }
    },

    addStrategyById : function(id)
    {
        var strategy = this.Strategies.get(id);
        if(strategy)
        {
            if(!this.model.get("strategies")) this.model.set("strategies",[]);
            var selected = this.model.get("strategies")||[];
            if( !_.contains(selected,id) )
            {
                this.model.get("strategies").push(strategy.id);
                this.addStrategy(strategy);
            }
        }
    },

    addStrategy : function(strategy)
    {
        var container = this.strategies.parent().parent();
        var panel = container.parent().find('.panel-selected');
        if( panel.length<=0 )
        {
            var formgroup = $('<div/>');
            formgroup.addClass('form-group');
            var colsm = $('<div/>');
            colsm.addClass('col-sm-8 col-sm-offset-2')
            formgroup.append(colsm);
            var selected = $('<div/>');
            selected.addClass('panel-selected');
            colsm.append(selected);
            container.after(formgroup);
            panel = selected;
        }

        if( strategy )
        {
            var div = $('<div/>');
            var button_close = $('<button/>')
                .attr({type:'button','aria-hidden':"true"})
                .addClass('close pull-right unselect-strategy')
                .html('&times;');
            var title=strategy.get('name');
            var img = strategy.get('image')?
                    '<a class="text-center img-thumbnail"><img src="'+Modus.uriRoot+'/img/strategies/'
                    +strategy.get('image')+'" alt=""/></a>':'';
            var content='<div class="media"><div class="media-body">'
                    +img+strategy.get('description')+'</div></div>';
            div.addClass('panel-content')
                    .append(strategy.get('name'),button_close)
                    .popover({html:true, trigger: "hover", container:'body',
                        title:title,content:content,placement:'right'});

            button_close.click($.proxy(function(){
                    div.popover('destroy');
                    div.remove();
                    this.model.set("strategies",_.without(this.model.get("strategies"),strategy.id));
            },this));
            panel.append(div);
        }
    },

    showSelectTool : function()
    {
        var search = this.tools.val();
        var container = this.tools.parent();
        var dropdown = container.find('.dropdown-menu');
        if( search.length>0 ){
            if( !dropdown.length ){
                dropdown = $('<ul/>');
                dropdown.addClass('dropdown-menu tool-dropdown');
                container.append(dropdown);
            }
            var tools = this.Tools.filter(function(tool) {
                var name = tool.get("name").toLowerCase();
                var find = search.toLowerCase();
                return (name.indexOf(find) >= 0);
            });
            if( tools.length ){
                dropdown.empty();
                _.each(tools, function(tool, index, list)
                {
                    if( index < 5){
                        var li = $('<li/>')
                        .append($('<a/>').html(tool.get('name')))
                        .data('toolId',tool.get('id'))
                        .click($.proxy(function(){
                            this.addToolById(tool.get('id'));
                        },this));
                        dropdown.append(li);
                    }
                },this);
                container.addClass('open');
            }else{
                container.removeClass('open');
            }
        }else{
            container.removeClass('open');
        }
    },

    addToolById : function(id)
    {
        var tool = this.Tools.get(id);
        if(tool)
        {
            if(!this.model.get("tools")) this.model.set("tools",[]);
            var selected = this.model.get("tools")||[];
            if( !_.contains(selected,id) )
            {
                this.model.get("tools").push(tool.id);
                this.addTool(tool);
            }
        }
    },

    addTool : function(tool)
    {
        var id = tool.id;
        var container = this.tools.parent().parent();
        var panel = container.parent().find('.panel-selected');
        if( panel.length<=0 ){
            var formgroup = $('<div/>');
            formgroup.addClass('form-group');
            var colsm = $('<div/>');
            colsm.addClass('col-sm-8 col-sm-offset-2')
            formgroup.append(colsm);
            var selected = $('<div/>');
            selected.addClass('panel-selected');
            colsm.append(selected);
            container.after(formgroup);
            panel = selected;
        }        

        if( tool ){
            var toolId = tool.id;
            var div = $('<div/>');
            var button_close = $('<button/>')
                .attr({type:'button','aria-hidden':"true"})
                .addClass('close pull-right unselect-tool')
                .html('&times;');
            var title=tool.get('name');
            var img = tool.get('image')?
                    '<a class="text-center img-thumbnail"><img src="'+Modus.uriRoot+'/img/tools/'
                    +tool.get('image')+'" alt=""/></a>':'';
            var content='<div class="media"><div class="media-body">'
                    +img+tool.get('description')+'</div></div>';
            div.addClass('panel-content')
                    .append(tool.get('name'),button_close)
                    .popover({html:true, trigger: "hover", container:'body',
                        title:title,content:content,placement:'right'});

            button_close.click($.proxy(function(){
                    div.popover('destroy');
                    div.remove();
                    this.model.set("tools",_.without(this.model.get("tools"),tool.id));
            },this));
            panel.append(div);
        }
    },

    addRuleOnEnter : function(e)
    {
        if (e.keyCode != 13) return;
        this.addRule();
    },

    showFormError : function(el)
    {
        if(!el)return;
        var fg = el.parents('.form-group');
        if(!fg.hasClass('has-error'))
        {
            fg.addClass('has-error');
            el.focus();
            setTimeout(function(){if(fg.hasClass('has-error'))fg.removeClass('has-error');},5000);
        }
    },

    addRule : function(rule)
    {
        if(!this.model.get("rules")) this.model.set("rules",new Modus.Phase.Model.RuleList());
        var _rule = this.ruleCriterion.val(), score = this.ruleScore.val();

        if(!_rule.length || score<=0)
        {
            !_rule.length && this.showFormError(this.ruleCriterion);
            score<=0 && this.showFormError(this.ruleScore);
            return;
        }

        this.ruleCriterion.val('');this.ruleScore.val(0);

        var rule = new Modus.Phase.Model.Rule({rule : _rule, score : score});
        this.model.get("rules").add(rule);
        this.addRuleView(rule);
    },

    addRuleView : function(rule)
    {
        var button_close = $('<button/>')
                .attr({type:'button','aria-hidden':"true"})
                .addClass('close pull-right unselect-rule')
                .html('&times;');
        var div = $('<div/>')
                .addClass('panel-content')
                .append('<span class="badge">'+rule.get('score')+'</span> '+rule.get('rule'),button_close);
        button_close.click($.proxy(function(){
            rule.collection.remove(rule);
            div.remove();
        },this));
        this.ruleList.append(div);
    }
});
