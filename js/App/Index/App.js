/**
 * ID : Index.App
 */
Modus.Index = Modus.Index || { Model : {}, View : {}, Controller : {} };
Modus.Index.App = Backbone.View.extend(
{
    tagName:  "div",
    template : _.template(document.getElementById('tmpl-admin-projects').innerHTML),

    initialize : function(params)
    {
        this.model = new Modus.Index.Model.GroupProjectList();

        this.page = (params && parseInt(params.page)) || 1;
        this.school_id = (params && parseInt(params.school_id)) || 0;
        this.school = (params && params.school) || "todos";
        this.grade_id  = (params && parseInt(params.grade_id)) || 0;
        this.grade  = (params && params.grade) || "todos";
        this.search = (params && params.search) || "";
        
        this.model.on('sync',this.renderProjects,this);            
        this.model.on('reset',this.resetProjects,this);  
    },

    render: function ()
    {
        this.$el.html(this.template());
        this.generalTabs = this.$('.projects-general-tabs');
        this.contentTabs = this.$('.tab-content');

                  
        this.loadProjects();

        return this;
    },

    events :
    {
        "click [data-toggle='tab']" : "changeTab",
        "click [go-to-page]" : "loadPage",
        "click #import-project" : "importProject",
        "click .btn-search-project" : "changeSearchText",
        "keypress #input-search-project" : "onKeyPressInputSearch"
    },

    changeTab : function(e)
    {
        var tabid = $(e.currentTarget).attr('tab-id');
        this.activeTab = tabid;
    },

    resetProjects : function()
    {
        if(this.items && this.items.length)
        {
            _.each(this.items,function(item){
                item.remove();
            });
        }
    },

    renderProjects : function()
    {
        this.activeTab = this.activeTab || 1;
        this.page = this.page || 1;
        if(!this.items)this.items=[];
        this.generalTabs.empty();this.contentTabs.empty();
        if(this.model)
        {
            var that = this, isSelected = false;
            var ulGeneralTabs = $('<ul class="nav navbar-nav"></ul');
            var totalCount = 0;
            this.model.each(function(teamProject)
            {
                var projects = teamProject.get('projects');
                if(projects && projects.length)
                {
                    var li = $('<li class="modus-tab"><a data-toggle="tab" data-target="#project-tab-'+teamProject.id+'" tab-id="'+teamProject.id+'">'+teamProject.get('name')+'</a></li>');
                    ulGeneralTabs.append(li);
                    var content = $('<div id="project-tab-'+teamProject.id+'" class="tab-pane row"></div>');
                    if(teamProject.id == this.activeTab)
                    {
                        li.addClass('active');content.addClass('active');
                        isSelected = true;
                    }

                    this.contentTabs.append(content);
                    projects.each(function(project){
                        var projectItem = new Modus.Index.View.ProjectItem({model:project,appIndex:that});
                        that.items.push(projectItem);
                        projectItem.on('duplicate', that.duplicate, that);
                        projectItem.on('refresh', function(){that.model.fetch({reset: true});}, that);
                        content.append(projectItem.render().el);
                        totalCount++;
                    });
                    content.append('<div class="clearfix"></div>');
                    var paginator = $('<div class="col-sm-12"><ul class="pager"><li class="previous"><a>&larr; Anterior</a></li><li class="next"><a>Siguiente &rarr;</a></li></ul></div>');
                    var page_count = teamProject.get('pages_count');
                    this.page = teamProject.get('page_actual');
                    var previous = paginator.find(".previous");
                    var next = paginator.find(".next");
                    if(this.page == 1)
                    {
                        previous.hide();
                    }
                    else
                    {
                        previous.show();
                    }

                    if(this.page == page_count)
                    {
                        next.hide();
                    }
                    else
                    {
                        next.show();
                    }
                    var next_num = this.page + 1;
                    var prev_num = this.page - 1;
                    previous.html("<a>&larr; Página "+ prev_num +"</a>");
                    previous.attr('go-to-page', prev_num);
                    next.html("<a>Página "+ next_num +" &rarr;</a>").attr('title', 'Página ' + next_num + ' de ' + page_count);
                    next.attr('go-to-page', next_num);

                    content.append(paginator);
                }
            },this);
            this.generalTabs.append(ulGeneralTabs);
            if(totalCount<=0)
            {
                this.contentTabs.html('<div class="body-content error"><h2><i class="fa fa-exclamation-triangle"></i> No se encontraron proyectos. </h2></div>')
            }
            if(!isSelected)
            {
                this.generalTabs.find('li').first().addClass('active');
                this.contentTabs.find('div.tab-pane').first().addClass('active');
                isSelected = true;
            }
            if(Modus.isAdmin || Modus.isCreator || Modus.isCoordinator)
            {
                if(!this.navbarTmpl)this.navbarTmpl = $((_.template($('#tmpl-admin-projects-navbar').html()))());
                this.generalTabs.append(this.navbarTmpl);
                setTimeout($.proxy(this.renderOptionsBar,this),0);
            }
        }
    },


    renderOptionsBar : function()
    {
        if(!this.adminoptions)
        {
            this.adminoptions = new Modus.Index.Model.AbstractModel();
            this.adminoptions.url = Modus.uriRoot + "/schools/grades/own"
            this.adminoptions.fetch({success:$.proxy(this.fillSchoolAndGrades,this)});
        }
        else
        {
            this.fillSchoolAndGrades();
        }
    },

    fillSchoolAndGrades : function()
    {
        this.virtualRoute();
        this.$('#input-search-project').val(this.search);
        if(!this.adminoptions)return;
        var schools = this.adminoptions.get('schools'),
            levels = this.adminoptions.get('levels');
        if(!schools || !schools.length)
        {
            this.navbarTmpl.empty();
            return;
        }

        var schoolList = this.navbarTmpl.find('.school-list');
        var schoolValue = this.navbarTmpl.find('.school-value');
        var gradeList = this.navbarTmpl.find('.grade-list');
        var gradeValue = this.navbarTmpl.find('.grade-value');
        schoolList.empty(); gradeList.empty();
        if(this.school_id>0)
        {
            var liAll = $('<li><a>Todos los colegios</a></li>');
            liAll.on('click', $.proxy(function(){
                this.changeSchool(0);
            },this));
            schoolList.append(liAll);
        }
        else
        {
            schoolValue.html('Todos los colegios <b class="caret"></b>');
        }
        _.each(schools, function(school){
            var li = $('<li><a>'+school.name+'</a></li>');
            if(school.id == this.school_id)
            {
                schoolValue.html(school.name + ' <b class="caret"></b>');
                li.find('a').css('font-weight','bold');
            }
            li.on('click', $.proxy(function(){
                this.changeSchool(school.id);
            },this));
            schoolList.append(li);
        },this);

        if(this.grade_id>0)
        {
            var liAll = $('<li><a>Todos los grados</a></li>');
            liAll.on('click', $.proxy(function(){
                this.changeGrade(0);
            },this));
            gradeList.append(liAll);
        }
        else
        {
            gradeValue.html('Todos los grados <b class="caret"></b>');
        }
        _.each(levels, function(level){
            var li = $('<li/>');
            li.addClass('pull-left').append('<dt><span class="text">'+level.name+'</span></dt>');
            _.each(level.grades, function(grade){

                    var a = $('<a/>');
                    if(grade.id == this.grade_id)
                    {
                        gradeValue.html(grade.name + '\u00B0 Grado de ' + level.name + ' <b class="caret"></b>');
                        a.css('font-weight','bold');
                    }
                    a.text(grade.name+'\u00B0 Grado').on('click',
                        $.proxy(function(){
                            this.changeGrade(grade.id);
                        },this)
                    );
                    li.append(a);
            }, this);
            gradeList.append(li);
        },this);
    },

    changeSchool : function(id)
    {
        this.school = 
        this.school_id = id;
        this.loadProjects();
    },

    changeGrade : function(id)
    {
        this.grade_id = id;
        this.loadProjects();
    },

    onKeyPressInputSearch : function(e)
    {

        if (e.keyCode != 13) return;
        e.preventDefault();
        this.changeSearchText();
    },

    changeSearchText : function()
    {
        this.search = this.$('#input-search-project').val();
        this.loadProjects();
    },

    loadPage : function(e)
    {
        this.page = $(e.currentTarget).attr('go-to-page');
        this.loadProjects();
    },

    loadProjects : function()
    {
        var that = this;
        var tabpanel = this.$('.tab-content');
        var projectListSection = this.$('.project-list-section');
        var width = projectListSection.width()+15;
        var height = projectListSection.height();
        var div_loading = $('<div class="block-loading"><i class="fa fa-spinner fa-spin"></i></div>').css({width : width, height : height});
        tabpanel.append(div_loading);

        this.model.url = Modus.uriRoot+'/projects/list/'+this.page + '/' + this.school_id + '/' + this.grade_id + '/' + this.search;
        that.virtualRoute();
        this.model.fetch({reset: true, 
            error: function(model, xhr){
                var msj = Modus.Helper.getMsgXhr(xhr, "Ocurri\u00F3 un error desconocido al al obtener la lista de proyectos, actualize la página e intente nuevamente.");
                alert(msj);
                div_loading.remove();
            },
            success : function(model, xhr)
            {
                
                div_loading.remove();
            }
        });
    },

    duplicate : function(model)
    {
        var that = this;
        var modal = this.$('#duplicate-modal');
        modal.find('.modal-title').text("Duplicar " + model.get('title'));
        var btn_save = modal.find('.action-save').html("Duplicar").prop( "disabled", false );
        var body = modal.find('.modal-body');

        if( !Modus.Cache.Schools ||  Modus.Cache.Schools.length == 0)
        {
            Modus.Cache.Schools = new Modus.School.Model.SchoolList();
            Modus.Cache.Schools.url = Modus.uriRoot+'/schools/listAll';
            Modus.Cache.Schools.fetch({error : 
                        function(model, xhr){
                            var msg = Modus.Helper.getMsgXhr(xhr, "Ocurrió un error al obtener la lista de colegios, intente recargar la página.");
                            body.html("<center style='color:#d55'><i class='fa fa-exclamation-circle'></i> " + msg + "</center>");
                            btn_save.hide();
                        }});
        }

        var grade_id = model.get('grade_id');
        var rooms_ids = model.get('rooms_ids');
        var createTree = function()
        {
            btn_save.show();
            var schools_html = '<div class="tree"><ul>';
            var schools = Modus.Cache.Schools;
            schools.each(function(school)
            {
                schools_html += '<li><a>'+school.get('name')+'</a><ul>';
                var levels_html = '';
                var levels = school.get('levels');
                levels.each(function(level)
                {
                    var grades_html = '';
                    var grades = level.get('grades');
                    grades.each(function(grade)
                    {
                        grades_html += '<li check="'+ ((grade_id==grade.id)?'yes':'no') +'"><a><strong>'+grade.get('name')+'\u00B0</strong> Grado de '+level.get('name')+'</a><ul><li>';
                        var sections_html = '';
                        var rooms = grade.get('rooms');
                        rooms.each(function(room)
                        {
                            if(!rooms_ids || (rooms_ids.indexOf(room.id)<0))
                            {
                                sections_html += '<label class="checkbox-inline"><input type="checkbox" '+ ((grade_id==grade.id)?'checked="checked"':'') +' value="'+room.id+'"> "'+room.get('name')+'"</label>';
                            }
                        });
                        if(sections_html.length>0)
                        {
                            grades_html += sections_html + '</li></ul></li>';
                        }
                        else
                        {
                            grades_html = '';
                        }
                    });
                    levels_html += grades_html;
                });
                schools_html += levels_html + '</ul></li>';
            });
            schools_html += '</ul></div>';
            var add = $(schools_html).on("click","a", function(e)
            {
                var target = $(e.target).parent();
                if(!target.attr("check") || (target.attr("check") == "no"))
                {
                    target.attr("check","yes").find('input').prop('checked', true);
                    target.find('[check]').attr('check',"yes");
                }
                else
                {
                    target.attr("check","no").find('input').prop('checked', false);
                    target.find('[check]').attr('check',"no");
                }
            });
            body.html(add);
        };
        modal.modal();
        if(Modus.Cache.Schools.length>0)
        {
            createTree();
        }
        else
        {
            body.html("<center><i class='fa fa-circle-o-notch fa-spin'></i> Cargando...</center>");
            Modus.Cache.Schools.once('sync', createTree, this);
        }
        btn_save.unbind( "click" ).on("click", function(evt)
        {
            var original_btn = evt.target.innerHTML;
            evt.target.innerHTML = "<i class='fa fa-circle-o-notch fa-spin'></i> Duplicando...";
            evt.target.setAttribute( "disabled", false );
            var values = [];
            body.find("input:checked").each(function(i,e) {
                values.push(e.getAttribute('value'));
            });

            $.post(Modus.uriRoot + "/projects/duplicate", {id:model.id, values : values})
            .success(function(resp){
                if(resp && resp.success)
                {
                    var s = (resp.success==1?"":"s");
                    alert("Se cre\u00F3 " + resp.success + " proyecto" + s + " duplicado" + s + ".");
                }
            })
            .fail(function(xhr)
            {
                var msj = Modus.Helper.getMsgXhr(xhr, "Ocurri\u00F3 un error desconocido al intentar duplicar el proyecto, actualize la página e intente nuevamente.");
                alert(msj);
                evt.target.innerHTML = original_btn;
                evt.target.removeAttribute("disabled");
            })
            .done(function()
            {
                that.model.fetch({reset:true});
                modal.modal('hide');
            });
        });

    },

    importProject : function(e)
    {
        var button = $(e.currentTarget);
        if(button.find('.fa-spinner').length>0)
        {
            return;
        }
        var original_html = button.html();
        var $upload = $('<input/>').attr({type:'file', accept : "application/zip"});
        var that = this;
        $upload.fileupload({
            add: Modus.Helper.fileUploadRestriction,
            url: Modus.uriRoot+'/projects/import',
            formData : {
            },
            dataType: 'json',
            done: function (e,data) {
                if(data.result && data.result.error)
                    alert(data.result.error);
                else
                    that.model.fetch();
                button.html(original_html);
            },
            fail: function(e,data,c)
            {
                var resp = data.response();
                if(resp && resp.jqXHR && resp.jqXHR.responseJSON && resp.jqXHR.responseJSON.error)
                    alert(resp.jqXHR.responseJSON.error);
                else
                    alert('Ocurri\u00F3 un error al importar, vuelva a intentarlo.');
                button.html(original_html);
            },
            progressall: function (e, data) {
                if(button.find('.fa-spinner').length <= 0)
                {
                    button.html('Importando <span class="text"></span> <i class="fa fa-spinner fa-spin"></i>')
                }
                var calc = Math.round(100*data.loaded/data.total);
                button.find('.text').text((calc<100?calc+'%':'...'))
            }
        });
        $upload.click();
    },

    virtualRoute : function()
    {
        this.page = this.page || 1;
        var school = '';
        var school_id = this.school_id;
        if(school_id > 0)
        {
            var name = "-";
            if(this.adminoptions)
            {
                var schools = this.adminoptions.get('schools');
                _.each(schools, function(s)
                {
                    if(s.id == school_id)
                    {
                        name = Modus.Helper.getSlug(s.name);
                    }
                });
            }
            school = "/"+school_id+"/" + name;
        }
        else if(school_id === 0)
        {
            school = "/0/todos-los-colegios";
        }
        
        var grade = "";
        var grade_id = this.grade_id;
        if(grade_id > 0)
        {
            var name = "-";
            if(this.adminoptions)
            {
                var levels = this.adminoptions.get('levels');
                _.each(levels, function(l)
                {
                    _.each(l.grades, function(g)
                    {
                        if(g.id == grade_id)
                        {
                            name = Modus.Helper.getSlug(g.name+'o ' + l.name);
                        }
                    });
                });
            }
            grade = "/" + grade_id + "/" + name;
        }
        else if(grade_id === 0)
        {
            grade = "/0/todos-los-grados";
        }
        
        var url = '/home';
        
        var page = "/" + this.page;
        if(grade_id <= 0 && this.search.length <= 0)
        {
            grade = "";
            if(school_id <= 0)
            {
                school = "";
                if(this.page === 1)
                {
                    page = "";
                    url = "/?";
                }
            }
        }
        
        url += page + school + grade + (this.search.length>0?"/"+this.search:"");
        
        Modus.GeneralRouter.navigate(url, { trigger: false, replace: true });
    },
});