/**
 * ID : Index.Model.GroupProjectList
 * require : Index.Model.GroupProject
 */
Modus.Index.Model.GroupProjectList = Backbone.Collection.extend(
{
    url : Modus.uriRoot+'/projects/list',
    model : Modus.Index.Model.GroupProject,
    parse : function(response, options)
    {
        var r = response.success || response;
        return r;
    }
});