 /**
  * ID : Tool.App
  * require : Tool.Model.ToolList
  * require : Tool.View.Tool
  * require : Tool.View.Add
  * require : Tool.View.Edit
  */
Modus.Tool.App = Backbone.View.extend(
{
    tagName:  "div",
    className : "tools container",
    template: _.template(document.getElementById('tmpl-tools-tools').innerHTML),

    initialize : function(params)
    {
        if(params.toolId>0)
        {
            this.tab = 'edit';
            this.toolId = params.toolId;
        }
        else
        {
            if(params.tab=='add')
            {
                this.tab = 'add';
            }
            else
            {
                this.tab = 'view';
            }
        }
    },

    render : function()
    {
        this.$el.html($(this.template()));
        this.viewArea = this.$("#view-area");
        this.alert = this.$("#alert-area");
        this.addEditArea = this.$("#add-edit-area");
        this.viewBtn = this.$("#view-btn");
        this.addEditBtn = this.$("#add-edit-btn");

        this.addEditBtn.find("a").text(this.tab=='edit'?"Editar":"Agregar");

        if(this.tab=="view")
        {
            this.goToView();
        }
        else if(this.tab=="add")
        {
            this.goToAdd();
        }else if(this.tab=="edit")
        {
            this.goToEdit();
        }
        return this;
    },

    events : 
    {
        "click #view-btn" : "goToView",
        "click #add-edit-btn" : "goToAddEdit"
    },

    showMsg : function(msj, css)
    {
        if(this.alert)
        {
            var css = css || "warning";
            this.alert.removeClass(function (index, css) {return (css.match (/\balert-\S+/g) || []).join(' ');});
            this.alert.addClass("alert-" + css );
            var title = css=='danger'?"Error!":(css=="success"?"Correcto!":(css=='info'?"Información":"Advertencia!"))
            this.alert.html('<h4><i class="fa fa-'+css+'"></i> '+title+'</h4>' + msj);
            this.alert.show();
        }
        else
        {
            alert(msj);
        }
    },

    showMsgXhr : function(xhr, msg)
    {
        var error = Modus.Helper.getMsgXhr(xhr,msg);
        this.showMsg(error,"danger");
    },

    hideMsg : function()
    {
        this.alert.hide();
    },

    goToView : function()
    {
        var that = this;
        that.virtualRoute("view");
        if(!that.Tools){
            that.Tools = new Modus.Tool.Model.ToolList;
            that.listenTo(that.Tools, 'add', that.addOne);
            that.listenTo(that.Tools, 'reset', that.addAll);
        }
        if(that.Tools.length==0)
        {
            that.viewArea.html('<center><i class="fa fa-circle-o-notch fa-spin"></i> Cargando...</center>');
            that.Tools.fetch(
            {
                success : function(m)
                {
                    if(m.length == 0)
                    {
                        that.showMsg("No se encontraron herramientas.");
                        that.viewArea.html('');
                    }
                },
                error: function(m,xhr)
                {
                    var msg = Modus.Helper.getMsgXhr(xhr,"Error al obtener herramientas");
                    that.viewArea.html('<div style="margin:10em;color:red"><h2 class="text-center"><i class="fa fa-times-circle"></i> Ocurrio un error<br/>'+msg+'</h2></div>')
                }
            });
        }
    },

    goToAddEdit : function()
    {
        if(this.toolId)
        {
            if(this.tab=='edit')return;
            this.goToEdit();
        }
        else
        {
            if(this.tab=='add')return;
            this.goToAdd();
        }
    },

    goToAdd : function()
    {
        this.virtualRoute("add");
        this.alert && this.alert.hide();
        if(this.ToolAdd) this.ToolAdd.remove();

        this.ToolAdd = new Modus.Tool.View.Add();

        this.ToolAdd.on("save", function(model)
        {
            this.showMsg("Se guardó la herramienta <strong>\""+model.get('name')+"\"</strong> correctamente","success");
            if(this.Tools) this.Tools.add(model);
            this.goToView(); 
        }, this);
        this.ToolAdd.on("cancel", this.goToView, this);
        this.ToolAdd.on("showMsgXhr", this.showMsgXhr, this);
        this.addEditArea.html(this.ToolAdd.render().el);
    },

    goToEdit : function(model)
    {
        if(this.ToolEdit) this.ToolEdit.remove();
        this.hideMsg();
        if(model)
        {
            this.ToolEdit = new Modus.Tool.View.Edit({model : model});
            this.toolId = model.id;
        }
        else
        {
            this.ToolEdit = new Modus.Tool.View.Edit({toolId : this.toolId});
        }

        this.ToolEdit.on("save", this.goToView, this);
        this.ToolEdit.on("cancel", this.goToView, this);
        this.ToolEdit.on("showMsgXhr", this.showMsgXhr, this);
        this.virtualRoute("edit");

        this.addEditArea.html(this.ToolEdit.render().el);
    },

    updateOnRemove : function()
    {
        if(this.Tools.length==0)
        {
            this.goToView();
        }
    },

    addOne: function(tool) {
        var view = new Modus.Tool.View.Tool({model: tool});
        view.on("goToEdit",this.goToEdit,this);
        view.on("remove", this.updateOnRemove, this);
        if(this.viewArea.find('.panel-modus').size()==0)
        {
            this.viewArea.empty();
        }

        this.viewArea.append(view.render().el);
    },

    addAll: function() {
        this.Tools.each(this.addOne, this);
    },

    virtualRoute : function(tab)
    {
        this.addEditBtn.hasClass('active') && this.addEditBtn.removeClass('active');
        this.addEditArea.hasClass('active') && this.addEditArea.removeClass('active');

        this.viewBtn.hasClass('active') && this.viewBtn.removeClass('active');
        this.viewArea.hasClass('active') && this.viewArea.removeClass('active');

        this.tab = tab;
        if(this.tab == 'view' )
        {
            this.toolId = null;

            this.addEditBtn.find("a").text("Agregar");

            this.viewBtn.hasClass('active') || this.viewBtn.addClass('active');
            this.viewArea.hasClass('active') || this.viewArea.addClass('active');
        }
        else
        {
            this.addEditBtn.find("a").text(this.tab=='edit'?"Editar":"Agregar");

            this.addEditBtn.hasClass('active') || this.addEditBtn.addClass('active');
            this.addEditArea.hasClass('active') || this.addEditArea.addClass('active');
        }     

        var url = '/tool/'+(this.tab)+(tab=='edit'?'/'+this.toolId:'');
        Modus.GeneralRouter.navigate(url, { trigger: false,replace:true });
    }
});