/**
 * ID : Coordinator.Model.Coordinator
 */
Modus.Coordinator.Model.Coordinator = Backbone.Model.extend(
{
    parse : function(response, options){
        var r = response.success?response.success:response;
        r && r.schools && Modus.School && Modus.School.Model.SchoolList && (r.schools = new Modus.School.Model.SchoolList(r.schools));
        return r;
    },
    getSchools : function()
    {
        return this.get('schools');
    },
});