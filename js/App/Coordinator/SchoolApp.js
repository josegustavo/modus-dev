/**
 * ID : Coordinator.SchoolApp
 * require : Coordinator.Model.Coordinator
 * require : Coordinator.View.School
 */
Modus.Coordinator.SchoolApp = Backbone.View.extend(
{
    tagName:  "div",
    className : "row",
    template: _.template($('#tmpl-admin-coordinator-schools').html()),

    events:
    {
      "click #clear-completed": "clearCompleted",
      "click #toggle-all": "toggleAllComplete"
    },
            
    initialize: function(params) 
    {
        var that = this;
        that.Coordinator = new Modus.Coordinator.Model.Coordinator();
        that.Coordinator.url = Modus.uriRoot+"/coordinators/"+params.coordinatorId+'/schools';
        
        that.$el.html('<center style="margin-top: 10%;"><h1><i class="fa fa-circle-o-notch fa-spin"></i> Cargando...</h1></center>');
        that.Coordinator.fetch({
            success : function()
            {
                that.Schools = that.Coordinator.getSchools();
                that.Schools.url = Modus.uriRoot+"/coordinators/"+that.Coordinator.id+'/schools';
                that.render();
                
                that.listenTo(that.Schools, 'add', that.addOne);
            },
            error : function(m, xhr)
            {
                var msg = Modus.Helper.getMsgXhr(xhr,"Error al obtener Colegios de Coordinador");
                that.$el.html('<div style="margin-top:10%;color:red"><h2 class="text-center"><i class="fa fa-times-circle"></i> Ocurrio un error<br/>'+msg+'</h2></div>')
            }
        });
    },
         
    initializeTypeHead : function()
    {
        var that = this;
        var coordinators = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            limit: 10,
            prefetch: {
              url: Modus.uriRoot + '/schools',
              filter: function(list) {
                return $.map(list.success, function(school) { return { name: school.name, id : school.id }; });
              }
            }
        });
        coordinators.initialize();
        this.input.typeahead({
            hint: true,
            highlight: true,
            minLength: 1
        },
        {
            name: 'coordinators',
            displayKey: 'name',
            source: coordinators.ttAdapter()
        });
        this.input.on("typeahead:selected", function(event, suggest, nameds)
        {
            that.create(suggest);
        });
    },
    
    render: function()
    {
        var that = this;
        if(that.Schools)
        {
            that.$el.html($(that.template({name : that.Coordinator.get('name')})));
            that.inputGroup = that.$('.input-group');
            that.input = that.$("#select-school");
            that.schoolList = that.$(".school-list");
            that.alert = that.$(".alert");
            that.footer = that.$('footer');
            that.main = $('#main');
            
            that.initializeTypeHead();
                
            if(that.Schools.length === 0)
            {
                that.showMsg("No existen Colegios asignados al coordinador.","info");
            }
            else
            {
                that.alert.hide();
                that.addAll();
            }
        }
    },
    
    
    
    addOne : function(school)
    {
        var that = this;
        var view = new Modus.Coordinator.View.School({model: school, AppCoordinatorSchools : this});
        view.on("remove", function()
        {
            if(that.Schools.length == 0)
            {
                that.showMsg("No existen Colegios asignados al coordinador.","info");
            }
        });
        this.schoolList.append(view.render().el);
    },

    addAll : function()
    {
        this.Schools.each(this.addOne, this);
    },

    showMsg : function(msj, css)
    {
        this.alert.removeClass(function (index, css) {return (css.match (/\balert-\S+/g) || []).join(' ');});
        this.alert.addClass("alert-" + (css || 'warning'));
        this.alert.text(msj);
        this.alert.show();
    },
    
    create : function(school)
    {
        var that = this;
        this.Schools.create(school,{wait: true,
            success: function()
            {
                that.input.typeahead('val', "");
                that.alert.hide();
            },
            error : function(m, xhr)
            {
                var msg = Modus.Helper.getMsgXhr(xhr,"Se produjo un error al asignar un Coordinador, intentelo nuevamente.\n ");
                that.showMsg(msg,"danger");
            }
        });
    }
});