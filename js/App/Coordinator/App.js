/**
 * ID : Coordinator.App
 * require : Coordinator.Model.CoordinatorList
 * require : Coordinator.View.Coordinator
 */
Modus.Coordinator.App = Backbone.View.extend(
{
    tagName:  "div",
    className : "row",
    template: _.template(document.getElementById('tmpl-admin-coordinators').innerHTML),

    events: {
        "click .add-coordinator" : "showAddUser",
        "click .btn-save" : "saveUser",
        "click #search-btn" : "search"
    },
            
    initialize: function() 
    {
        var that = this;
        that.Coordinators = new Modus.Coordinator.Model.CoordinatorList();
        that.$el.html('<center style="margin-top: 10%;"><h1><i class="fa fa-circle-o-notch fa-spin"></i> Cargando...</h1></center>');
        
        that.Coordinators.fetch({
            success : function()
            {
                that.render();
                that.listenTo(that.Coordinators, 'add', that.addOne);
            },
            error : function(m, xhr)
            {
                var msg = Modus.Helper.getMsgXhr(xhr,"Error al obtener Coordinadores de Colegios");
                that.$el.html('<div style="margin-top:10%;color:red"><h2 class="text-center"><i class="fa fa-times-circle"></i> Ocurrio un error<br/>'+msg+'</h2></div>')
            }
        });
    },
            
    render: function()
    {
        this.$el.html($(this.template()));
        this.inputGroup = this.$('.input-group');
        this.input = this.$("#new-coordinator");
        this.alert = this.$(".alert");
        this.footer = this.$('footer');
        this.main = $('#main');
        
        if(this.Coordinators.length > 0)
        {
            this.addAll();
            this.alert.hide();
        }
        else
        {
            this.showMsg("No existen coordinadores en el sistema.","info");
        }
        return this;
    },
    
    search : function()
    {
        var search = this.$('#search-value').val().toLowerCase();
        this.$("#coordinators-list").empty();
        var find = [];
        this.Coordinators.map(function(i)
        {
            var username = i.get('username');
            var firstname = i.get('firstname');
            var lastname = i.get('lastname');
            if((username.indexOf(search)>=0) || (firstname.indexOf(search)>=0) || (lastname.indexOf(search)>=0))
            {
                find.push(i);
            }
        });
        find && _.each(find, this.addOne, this);
    },
    
    addOne : function(coordinator)
    {
        var view = new Modus.Coordinator.View.Coordinator({model: coordinator});
        view.on("showEdit", this.showEdit, this);
        this.$("#coordinators-list").append(view.render().el);
    },

    addAll : function()
    {
        this.Coordinators.each(this.addOne, this);
    },

    showMsg : function(msj, css, elem)
    {
        elem = elem || this.alert;
        elem.removeClass(function (index, css) {return (css.match (/\balert-\S+/g) || []).join(' ');});
        elem.addClass("alert-" + (css || 'warning'));
        elem.html(msj);
        elem.show();
    },
    
        
    showAddUser : function()
    {
        this.editingUser = null;
        this.modal = this.$('#modal-coordinator');
        this.modal.find('.alert').hide();
        this.modal.find(".modal-title").text("Agregar Coordinador");
        this.modal.find('#form-new-coordinator').trigger("reset");
        this.modal.modal();
    },

    showEdit : function(user)
    {
        this.editingUser = user;
        this.modal = this.$('#modal-coordinator');
        this.modal.find('.alert').hide();
        this.modal.find("#myModalLabel").text("Editar usuario");
        this.modal.find('#form-new-user').trigger("reset");

        this.modal.find('#newUser').val(user.get('username'));
        this.modal.find('#newName').val(user.get('firstname'));
        this.modal.find('#newLastname').val(user.get('lastname'));
        this.modal.find('#newEmail').val(user.get('email'));
        this.modal.find('#newPassword').val('');
        
        this.modal.modal();
    },

    saveUser : function()
    {
        var unindexed_array = this.modal.find('#form-new-coordinator').serializeArray();
        var indexed_array = {};

        $.map(unindexed_array, function(n, i){
            indexed_array[n['name']] = n['value'];
        });
        indexed_array['name'] = indexed_array['firstname'] + ' ' + indexed_array['lastname'];
        var that = this;
        var success = function()
                {
                    if(that.modal) that.modal.modal('hide');
                    that.alert.hide();
                };
        var error = function(model, xhr, options)
                {
                    var msg = Modus.Helper.getMsgXhr(xhr,"Se produjo un error al crear el coordinador, intentelo nuevamente.");
                    that.showMsg(msg,"danger", that.modal.find('.alert'));
                }
        if(this.editingUser)
        {
            this.editingUser.save(indexed_array, {
                wait: true,
                success : success, error : error
            });
        }
        else
        {
            this.Coordinators.create(indexed_array, {
                wait: true,
                success : success, error : error
            });
        }
    }
});