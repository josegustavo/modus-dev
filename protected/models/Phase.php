<?php

/**
 * This is the model class for table "phase".
 *
 * The followings are the available columns in table 'phase':
 * @property integer $id
 * @property integer $template_phase_id
 * @property integer $ord
 * @property integer $project_id
 * @property string $name
 * @property string $description
 * @property integer $image_id
 * @property integer $can_answer
 * @property integer $can_mark
 * @property integer $showfinal_others
 * @property string $text_answer
 * @property integer $created
 * @property integer $modified
 *
 * The followings are the available model relations:
 * @property Answer[] $answers
 * @property Team[] $teams
 * @property Mark[] $marks
 * @property Project $project
 * @property File $image
 * @property TemplatePhase $templatePhase
 * @property File[] $files
 * @property Strategy[] $strategies
 * @property Tool[] $tools
 * @property Rule[] $rules
 */
class Phase extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'phase';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('project_id, name', 'required'),
			array('template_phase_id, ord, project_id, image_id, can_answer, can_mark, showfinal_others, created, modified', 'numerical', 'integerOnly'=>true),
			array('name, text_answer', 'length', 'max'=>255),
			array('description', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, template_phase_id, ord, project_id, name, description, image_id, can_answer, can_mark, showfinal_others, text_answer, created, modified', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'answers' => array(self::HAS_MANY, 'Answer', 'phase_id'),
			'teams' => array(self::MANY_MANY, 'Team', 'final_mark(phase_id, team_id)'),
			'marks' => array(self::HAS_MANY, 'Mark', 'phase_id'),
			'project' => array(self::BELONGS_TO, 'Project', 'project_id'),
			'image' => array(self::BELONGS_TO, 'File', 'image_id'),
			'templatePhase' => array(self::BELONGS_TO, 'TemplatePhase', 'template_phase_id'),
			'files' => array(self::MANY_MANY, 'File', 'phase_attachment(phase_id, file_id)'),
			'strategies' => array(self::MANY_MANY, 'Strategy', 'phase_strategy(phase_id, strategy_id)'),
			'tools' => array(self::MANY_MANY, 'Tool', 'phase_tool(phase_id, tool_id)'),
			'rules' => array(self::HAS_MANY, 'Rule', 'phase_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'template_phase_id' => 'Template Phase',
			'ord' => 'Ord',
			'project_id' => 'Project',
			'name' => 'Name',
			'description' => 'Description',
			'image_id' => 'Image',
			'can_answer' => 'Can Answer',
			'can_mark' => 'Can Mark',
			'showfinal_others' => 'Showfinal Others',
			'text_answer' => 'Text Answer',
			'created' => 'Created',
			'modified' => 'Modified',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('template_phase_id',$this->template_phase_id);
		$criteria->compare('ord',$this->ord);
		$criteria->compare('project_id',$this->project_id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('image_id',$this->image_id);
		$criteria->compare('can_answer',$this->can_answer);
		$criteria->compare('can_mark',$this->can_mark);
		$criteria->compare('showfinal_others',$this->showfinal_others);
		$criteria->compare('text_answer',$this->text_answer,true);
		$criteria->compare('created',$this->created);
		$criteria->compare('modified',$this->modified);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Phase the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
