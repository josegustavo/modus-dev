<?php

/**
 * This is the model class for table "mark_final".
 *
 * The followings are the available columns in table 'mark_final':
 * @property integer $id
 * @property integer $phase_id
 * @property integer $team_id
 * @property integer $mark
 * @property integer $mark_member
 * @property integer $mark_admin
 * @property integer $modified
 *
 * The followings are the available model relations:
 * @property Phase $phase
 * @property Team $team
 */
class MarkFinal extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'mark_final';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('phase_id, team_id, mark, mark_member, mark_admin, modified', 'required'),
			array('phase_id, team_id, mark, mark_member, mark_admin, modified', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, phase_id, team_id, mark, mark_member, mark_admin, modified', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'phase' => array(self::BELONGS_TO, 'Phase', 'phase_id'),
			'team' => array(self::BELONGS_TO, 'Team', 'team_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'phase_id' => 'Phase',
			'team_id' => 'Team',
			'mark' => 'Mark',
			'mark_member' => 'Mark Member',
			'mark_admin' => 'Mark Admin',
			'modified' => 'Modified',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('phase_id',$this->phase_id);
		$criteria->compare('team_id',$this->team_id);
		$criteria->compare('mark',$this->mark);
		$criteria->compare('mark_member',$this->mark_member);
		$criteria->compare('mark_admin',$this->mark_admin);
		$criteria->compare('modified',$this->modified);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MarkFinal the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
