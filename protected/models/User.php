<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property integer $id
 * @property integer $creator_id
 * @property string $username
 * @property string $password
 * @property string $email
 * @property string $firstname
 * @property string $lastname
 * @property integer $created
 * @property integer $modified
 * @property integer $last_access
 * @property integer $active
 *
 * The followings are the available model relations:
 * @property Answer[] $answers
 * @property Attachment[] $attachments
 * @property Authassignment $authassignment
 * @property File[] $files
 * @property LogAuth[] $logAuths
 * @property LogProject[] $logProjects
 * @property LogSystem[] $logSystems
 * @property Mark[] $marks
 * @property Mark[] $marks1
 * @property Project[] $projects
 * @property Project[] $projects1
 * @property Reanswer[] $reanswers
 * @property School[] $schools
 * @property Room[] $rooms
 * @property Strategy[] $strategies
 * @property Team[] $teams
 * @property TemplatePhase[] $templatePhases
 * @property Tool[] $tools
 * @property User $creator
 * @property User[] $users
 */
class User extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('username, password, email, firstname, lastname', 'required'),
			array('creator_id, created, modified, last_access, active', 'numerical', 'integerOnly'=>true),
			array('username, password, email, firstname, lastname', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, creator_id, username, password, email, firstname, lastname, created, modified, last_access, active', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'answers' => array(self::HAS_MANY, 'Answer', 'author_id'),
			'attachments' => array(self::HAS_MANY, 'Attachment', 'creator_id'),
			'authassignment' => array(self::HAS_ONE, 'Authassignment', 'userid'),
			'files' => array(self::HAS_MANY, 'File', 'creator_id'),
			'logAuths' => array(self::HAS_MANY, 'LogAuth', 'user_id'),
			'logProjects' => array(self::HAS_MANY, 'LogProject', 'user_id'),
			'logSystems' => array(self::HAS_MANY, 'LogSystem', 'user_id'),
			'marks' => array(self::HAS_MANY, 'Mark', 'mark_manager_by'),
			'marks1' => array(self::HAS_MANY, 'Mark', 'mark_member_by'),
			'projects' => array(self::HAS_MANY, 'Project', 'creator_id'),
			'projects1' => array(self::MANY_MANY, 'Project', 'project_manager(manager_id, project_id)'),
			'reanswers' => array(self::HAS_MANY, 'Reanswer', 'author_id'),
			'schools' => array(self::MANY_MANY, 'School', 'school_manager(user_id, school_id)'),
			'rooms' => array(self::MANY_MANY, 'Room', 'sign(user_id, room_id)'),
			'strategies' => array(self::HAS_MANY, 'Strategy', 'creator_id'),
			'teams' => array(self::MANY_MANY, 'Team', 'team_user(user_id, team_id)'),
			'templatePhases' => array(self::HAS_MANY, 'TemplatePhase', 'creator_id'),
			'tools' => array(self::HAS_MANY, 'Tool', 'creator_id'),
			'creator' => array(self::BELONGS_TO, 'User', 'creator_id'),
			'users' => array(self::HAS_MANY, 'User', 'creator_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'creator_id' => 'Creator',
			'username' => 'Username',
			'password' => 'Password',
			'email' => 'Email',
			'firstname' => 'Firstname',
			'lastname' => 'Lastname',
			'created' => 'Created',
			'modified' => 'Modified',
			'last_access' => 'Last Access',
			'active' => 'Active',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('creator_id',$this->creator_id);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('firstname',$this->firstname,true);
		$criteria->compare('lastname',$this->lastname,true);
		$criteria->compare('created',$this->created);
		$criteria->compare('modified',$this->modified);
		$criteria->compare('last_access',$this->last_access);
		$criteria->compare('active',$this->active);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
