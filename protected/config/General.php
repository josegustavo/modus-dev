<?php

return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Modus',
        'defaultController' => 'modus',
        'sourceLanguage' => 'es_pe',
	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
	),
        'modules'=>array(
		//Gii tool
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'okokok',
			//Gii defaults to localhost only
			'ipFilters'=>array('127.0.0.1','::1'),
		),
		
	),
	'components'=>array(
                'db' => array(
                    'class'=>'system.db.CDbConnection',
                    'emulatePrepare' => true,
                    'charset' => 'utf8',
                    'schemaCachingDuration' => YII_DEBUG ? 0 : 86400000, // 1000 days
                    'enableParamLogging' => YII_DEBUG,
                ),
                'cache'=>array(
                    'class'=>'system.caching.CApcCache',
                ),
		'user'=>array(
                        'loginUrl' => '/',
			'allowAutoLogin'=>FALSE,
		),		
		'authManager'=>array(
                    'class'=>'CDbAuthManager',
		    'assignmentTable'=>'authassignment',
		    'itemTable'=>'authitem',
		    'itemChildTable'=>'authitemchild',
                    'connectionID'=>'db',
                ),
		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'modus/error',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),
	),
);