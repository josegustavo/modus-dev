<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
        public $config;
        
        public function filters()
        {
            return array('accessControl');
        }
        
        public function accessRules()
        {
            return array(
                array('allow', 'controllers' => array('modus'), 'actions'=>array('index','login','error','js','css','install'), 'users'=>array('*')),
                array('allow', 'controllers' => array('uploads'), 'actions'=>array('download'), 'users'=>array('*')),
                array('allow', 'controllers' => array('migration'), 'actions'=>array('index'), 'users'=>array('*')),
                array('deny', 'users'=>array('?')),
            );
        }
        
        public $success=null;
        
        public $error=array();
        
        public $debug="";
        
        public function year()
        {
            if(intval(Yii::app()->session['year_id']) <= 0)
            {
                $activeYear = Year::model()->findByAttributes(array('active'=>1));
                if($activeYear)
                {
                    Yii::app()->session['year_id'] = $activeYear->id;
                }
            }
            return Yii::app()->session['year_id'];
        }
        
        private $memo_ini;
        private $memo_end;
        
        protected function beforeAction($action) 
        {
            $this->config = new CConfig();
            $this->memo_ini = memory_get_usage();
            if(!Yii::app()->session['year_id'])
            {
                $activeYear = Year::model()->findByAttributes(array('active'=>1));
                if($activeYear)
                {
                    Yii::app()->session['year_id'] = $activeYear->id;
                }
                //else
                    //throw new CException("No se pudo obtener un ciclo activo");
            }
            
            return parent::beforeAction($action);
        }

        protected function afterAction($action)
        {
            $response = array();
            if(isset($this->success))
            {
                $response['success'] = $this->success;
            }
            if(!empty($this->error))
            {
                $response['error'] = $this->error;
            }
            if(!empty($response))
            {
                $this->memo_end = memory_get_usage();
                if(YII_DEBUG)
                {
                    $response['status']['time'] = round((1000*(Yii::getLogger()->executionTime)),2)." ms";
                    $response['status']['memo'] = round((($this->memo_end-$this->memo_ini)/1048576),2)." MB";
                    $response['status']['peak'] = round(memory_get_peak_usage()/1048576,2) . " MB";
                    $response['status']['size'] = round(strlen(json_encode($response))/1024,2). " kB";
                    $response['status']['debug'] = $this->debug;
                }
                if(isset($response['success']))
                {
                    $this->sendResponse("200", json_encode($response));
                }
                else if(!empty ($response['error']))
                {
                    $this->sendResponse ("400", json_encode($response));
                }
            }
        }
        
        /**
         * Send raw HTTP response
         * @param int $status HTTP status code
         * @param string $body The body of the HTTP response
         * @param string $contentType Header content-type
         * @return HTTP response
         */
        protected function sendResponse($status = 200, $body = '', $contentType = 'application/json')
        {
                // Set the status
                $statusHeader = 'HTTP/1.1 ' . $status . ' ' . $this->getStatusCodeMessage($status);
                header($statusHeader);
                // Set the content type
                header('Content-type: ' . $contentType);

                echo $body;
                Yii::app()->end();
        }

        /**
         * Return the http status message based on integer status code
         * @param int $status HTTP status code
         * @return string status message
         */
        protected function getStatusCodeMessage($status)
        {
            $codes = array(
                        100 => 'Continue',
                        101 => 'Switching Protocols',
                        200 => 'OK',
                        201 => 'Created',
                        202 => 'Accepted',
                        203 => 'Non-Authoritative Information',
                        204 => 'No Content',
                        205 => 'Reset Content',
                        206 => 'Partial Content',
                        300 => 'Multiple Choices',
                        301 => 'Moved Permanently',
                        302 => 'Found',
                        303 => 'See Other',
                        304 => 'Not Modified',
                        305 => 'Use Proxy',
                        306 => '(Unused)',
                        307 => 'Temporary Redirect',
                        400 => 'Bad Request',
                        401 => 'Unauthorized',
                        402 => 'Payment Required',
                        403 => 'Forbidden',
                        404 => 'Not Found',
                        405 => 'Method Not Allowed',
                        406 => 'Not Acceptable',
                        407 => 'Proxy Authentication Required',
                        408 => 'Request Timeout',
                        409 => 'Conflict',
                        410 => 'Gone',
                        411 => 'Length Required',
                        412 => 'Precondition Failed',
                        413 => 'Request Entity Too Large',
                        414 => 'Request-URI Too Long',
                        415 => 'Unsupported Media Type',
                        416 => 'Requested Range Not Satisfiable',
                        417 => 'Expectation Failed',
                        500 => 'Internal Server Error',
                        501 => 'Not Implemented',
                        502 => 'Bad Gateway',
                        503 => 'Service Unavailable',
                        504 => 'Gateway Timeout',
                        505 => 'HTTP Version Not Supported',

            );
            return (isset($codes[$status])) ? $codes[$status] : '';
        }

        /**
         * Gets RestFul data and decodes its JSON request
         * @return mixed
         */
        public function getJsonInput()
        {
                return CJSON::decode(file_get_contents('php://input'));
        }
        /*
        public function init() {
            parent::init();
            Yii::app()->attachEventHandler('onError',array($this, 'handleApiError'));
            Yii::app()->attachEventHandler('onException',array($this, 'handleApiError'));
        }
        */    
        
        public function handleApiError(CEvent $event)
        {
            //$event->handled = true;
            //echo CJSON::encode(array('error'=>$event->exception->getMessage()));
            
        }
        
        function _human_time_diff( $from, $to = '' ) {
            defined('MINUTE_IN_SECONDS') || define( 'MINUTE_IN_SECONDS', 60 );
            defined('HOUR_IN_SECONDS') || define( 'HOUR_IN_SECONDS',   60 * MINUTE_IN_SECONDS );
            defined('DAY_IN_SECONDS') || define( 'DAY_IN_SECONDS',    24 * HOUR_IN_SECONDS   );
            defined('WEEK_IN_SECONDS') || define( 'WEEK_IN_SECONDS',    7 * DAY_IN_SECONDS    );
            defined('YEAR_IN_SECONDS') || define( 'YEAR_IN_SECONDS',  365 * DAY_IN_SECONDS    );
            
          if ( empty( $to ) )
                  $to = time();

          $diff = (int) abs( $to - $from );

          if ( $diff < HOUR_IN_SECONDS ) {
                  $mins = round( $diff / MINUTE_IN_SECONDS );
                  if ( $mins <= 1 )
                          $mins = 1;
                  /* translators: min=minute */
                  $since = sprintf( $mins==1?'%s minuto':'%s minutos', $mins );
          } elseif ( $diff < DAY_IN_SECONDS && $diff >= HOUR_IN_SECONDS ) {
                  $hours = round( $diff / HOUR_IN_SECONDS );
                  if ( $hours <= 1 )
                          $hours = 1;
                  $since = sprintf( $hours==1?'%s hora':'%s horas', $hours );
          } elseif ( $diff < WEEK_IN_SECONDS && $diff >= DAY_IN_SECONDS ) {
                  $days = round( $diff / DAY_IN_SECONDS );
                  if ( $days <= 1 )
                          $days = 1;
                  $since = sprintf( $days==1?'%s día':'%s días', $days );
          } elseif ( $diff < 30 * DAY_IN_SECONDS && $diff >= WEEK_IN_SECONDS ) {
                  $weeks = round( $diff / WEEK_IN_SECONDS );
                  if ( $weeks <= 1 )
                          $weeks = 1;
                  $since = sprintf($weeks==1?'%s semana':'%s semanas', $weeks );
          } elseif ( $diff < YEAR_IN_SECONDS && $diff >= 30 * DAY_IN_SECONDS ) {
                  $months = round( $diff / ( 30 * DAY_IN_SECONDS ) );
                  if ( $months <= 1 )
                          $months = 1;
                  $since = sprintf( $months==1?'%s mes':'%s meses', $months );
          } elseif ( $diff >= YEAR_IN_SECONDS ) {
                  $years = round( $diff / YEAR_IN_SECONDS );
                  if ( $years <= 1 )
                          $years = 1;
                  $since = sprintf($years==1?'%s año':'%s años', $years );
          }

          return $since;
    }
}