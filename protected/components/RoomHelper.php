<?php

class RoomHelper
{
    public static function assignStudent($student_id, $room_id)
    {
        $signed = Sign::model()->findByAttributes(array('year_id' => Yii::app()->controller->year(), 'user_id' => $student_id, 'active' => 1));
        if(!$signed)
        {
            $user = User::model()->with('authassignment')->findByPk($student_id,array('select' => 'username', 'condition' => 'itemname="student"'));
            if($user)
            {
                $time = time();

                $sign = new Sign();
                $sign->attributes = array('user_id' => $student_id, 'room_id' => $room_id,
                    'year_id' => Yii::app()->controller->year(),
                    'created' => $time, 'modified' => $time, 'active' => 1
                );
                if($sign->save())
                {
                    $room = Room::model()->with('level','school')->together()->findByPk($room_id, array('select'=>'name'));
                    $level = $room->level;
                    $school = $room->school;
                    CLog::logSystem("assignStudent", "Se ha asignado el alumno '$user->username' al 'Colegio " . $school->name . " " . $level->name . "° de " . $level->level_name . " sección \"" . $room->name . "\"'", $sign->attributes);

                    return TRUE;
                }
                else
                {
                    throw new Exception("Ocurrió un error al intentar asignar el alumno");
                }
            }
            else
            {
                throw new Exception("El usuario que desea asignar no existe o no es alumno");
            }
        }
        else
        {
            throw new Exception("El alumno ya se encuentra asignado en otra sección");
        }
    }
    
    public static function unassignStudent($student_id, $room_id)
    {
        $year = Yii::app()->controller->year();
        $sign = Sign::model()->findByAttributes(array("user_id" => $student_id, "year_id" => $year, "room_id" => $room_id));
        if($sign)
        {
            $attr = $sign->attributes;
            if($sign->delete())
            {
                $user = User::model()->findByPk($student_id,array('select' => 'username'));
                $room = Room::model()->with('level','school')->findByPk($room_id, array('select'=>'name'));
                $level = $room->level;
                $school = $room->school;
                CLog::logSystem("unassignStudent", "Se ha desasignado el alumno '$user->username' del 'Colegio " . $school->name . " " . $level->name . "° de " . $level->level_name . " sección \"" . $room->name . "\"'", $attr);
                return TRUE;
            }
            else
            {
                throw new Exception("Ocurrió un error al intentar desasignar el alumno.");
            }
        }
        else
        {
            throw new Exception("El alumno que intenta desasignar no se encuentra asignado a la sección.");
        }
        return FALSE;
    }
    
    public static function assignInstructor($instructor_id, $room_id)
    {
        $signed = Sign::model()->findByAttributes(array('year_id' => Yii::app()->controller->year(), 'user_id' => $instructor_id, 'active' => 1, 'room_id' => $room_id));
        if(!$signed)
        {
            $user = User::model()->with('authassignment')->findByPk($instructor_id,array('select' => 'username', 'condition' => 'itemname="instructor"'));
            if($user)
            {
                $time = time();

                $sign = new Sign();
                $sign->attributes = array(
                    'user_id' => $instructor_id, 'room_id' => $room_id,
                    'year_id' => Yii::app()->controller->year(),
                    'created' => $time, 'modified' => $time, 'active' => 1
                );
                if($sign->save())
                {
                    $room = Room::model()->with('level','school')->findByPk($room_id, array('select'=>'name'));
                    $level = $room->level;
                    $school = $room->school;
                    CLog::logSystem("assignInstructor", "Se ha asignado el docente '$user->username' al 'Colegio " . $school->name . " " . $level->name . "° de " . $level->level_name . " sección \"" . $room->name . "\"'", $sign->attributes);
                    return TRUE;
                }
            }
            else
            {
                throw new Exception("El usuario que desea asignar no existe o no es docente.");
            }
        }
        else
        {
            throw new Exception("El docente ya se encuentra asignado a la sección");
        }
        return FALSE;
    }
    
    public static function unassignInstructor($instructor_id, $room_id)
    {
        $year = Yii::app()->controller->year();
        $sign = Sign::model()->findByAttributes(array("user_id" => $instructor_id, "year_id" => $year, "room_id" => $room_id));
        if($sign)
        {
            $user = User::model()->findByPk($instructor_id,array('select' => 'username'));
            $attr = $sign->attributes;
            $sign->delete();

            $room = Room::model()->with('level','school')->findByPk($room_id, array('select'=>'name'));
            $level = $room->level;
            $school = $room->school;

            CLog::logSystem("unassignInstructor", "Se ha desasignado el docente '$user->username' del 'Colegio " . $school->name . " " . $level->name . "° de " . $level->level_name . " sección \"" . $room->name . "\"'", $attr);
            return TRUE;
        }
        else
        {
            throw new Exception("El docente que intenta desasignar no esta asignado a la sección");
        }
        return FALSE;
    }
}