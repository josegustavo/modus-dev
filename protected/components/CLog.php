<?php

Class CLog extends CLogger
{
    
    public static function logProject($action_id, $comment, $diff)
    {
        $project_id = Yii::app()->controller->action->project_id;
        $time = time();
        $user_id = Yii::app()->user->dbid;
        
        $logProject = new LogProject();
        $logProject->setAttributes(array(
            'user_id' => $user_id,
            'project_id' => $project_id,
            'datetime' => $time,
            'controller_action'=> Yii::app()->controller->id.".".Yii::app()->controller->action->id,
            'action' => $action_id,
            'human_comment' => $comment,
            'data' => json_encode($diff)
        ));
        $logProject->save();
    }
    
    public static function logSystem($action_id, $comment, $data)
    {
        $time = time();
        $user_id = Yii::app()->user->dbid;
        
        $logSystem = new LogSystem();
        $logSystem->setAttributes(array(
            'user_id' => $user_id,
            'datetime' => $time,
            'controller_action'=> Yii::app()->controller->id.".".Yii::app()->controller->action->id,
            'action' => $action_id,
            'human_comment' => $comment,
            'data' => json_encode($data)
        ));
        $logSystem->save();
    }
    
}