<?php

class Cache
{
    static $tmpl_folder = '/views/templates/';
    static $tmpl_ext    = '.php';
    static $script_folder = '/js/';
    static $style_folder  = '/css/';
    static $script_ext = '.js';
    static $style_ext = '.css';
    
    public static function getListStyles()
    {
        $css_included = array('bootstrap', 'common', 'fonts','third-party/nprogress');
        $user = Yii::app()->user;
        if( $user->isGuest )
        {
            $css_included [] = 'login';
        }
        else
        {
            $css_included = array_merge($css_included, array('backgrid','summernote','datepicker','custom'));
        }
        return $css_included;
    }
    
    public static function getListScriptsMain()
    {
        $user = Yii::app()->user;
        $isGuest = $user->isGuest;
        $isAdmin = $user->checkAccess("admin");
        $isCreator = $user->checkAccess("creator");
        $isCoordinator = $user->checkAccess("coordinator");
        $isInstructor = $user->checkAccess("instructor");
        
        $js_main = array('jquery','backbone','bootstrap');
        
        if(!$isGuest)
        {
            $js_main []= 'nprogress';
            $js_main []= 'summernote';
            
            $js_main []= 'jquery-ui';
            $js_main []= 'jquery-fileupload';
            
            if( $isInstructor ||  $isCoordinator || $isCreator || $isAdmin)
            {
                $js_main []= 'highcharts';
                $js_main []= 'bootstrap-datetimepicker';
            }
            
            
            if($user->checkAccess("manageUsers"))
            {
                $js_main []= 'backbone-paginator';
                $js_main []= 'backgrid';
                $js_main []= 'backgrid-paginator';
                $js_main []= 'sortable';
            }
            
            if( $isCoordinator || $isCreator || $isAdmin)
            {
                $js_main []= 'typehead';
            }
        }
        
        return array('third-party' => $js_main);
    }
    
    public static function getListScripts()
    {
        $js_included = array();
        $user = Yii::app()->user;
        if( $user->isGuest )
        {
            $js_included []= 'Login';
        }
        else
        {
            $isAdmin = $user->checkAccess("admin");
            $isCreator = $user->checkAccess("creator");
            $isCoordinator = $user->checkAccess("coordinator");
            $isInstructor = $user->checkAccess("instructor");
            $isStudent = $user->checkAccess("student");

            $js_included [] = 'Helper';
            if( $isStudent || $isInstructor ||  $isCoordinator || $isCreator || $isAdmin)
            {
                $js_included['Project'] = array(
                    'Model' => 'Project.ProjectList.Phase.PhaseList.Answer.AnswerList.Qualification.QualificationList.PhaseQualifications.PhaseAnswers.Attachment.AttachmentList.Statistic.StatisticList',
                    'View' => 'SideAnswers.SideMyTeam.ReAnswerContent.ReAnswer.Answer.QualificationModal.Phases.Resume',
                );
            }
            
            if( $isInstructor || $isCoordinator || $isCreator || $isAdmin)
            {
                $js_included['Project']['View'] .= '.SideDashboard.SideMarks.SideAnswers.Assessment.StatisticProgress.StatisticGeneral';
                
                $js_included['ProjectAdmin'] = array(
                    'Model' => array(),
                    'View' => array()
                );
                
                if($user->checkAccess("createProjects") || $user->checkAccess("manageProjectTeams"))
                {
                    $js_included['School'] = array('Model' => 'Room.RoomList.Grade.GradeList.Level.LevelList.School.SchoolList', 'App' => FALSE);
                    $js_included['Student'] = array('Model' => 'Student.StudentList', 'App' => FALSE);
					
					$js_included['ProjectAdmin']['Model'] [0] = 'ProjectEdit.Team.TeamList';
					$js_included['ProjectAdmin']['View'] [0] = 'Team.Teams.AssignTeams';
                }
                if($user->checkAccess("createProjects"))
                {
                    $js_included['Instructor'] = array('Model' => 'Instructor.InstructorList', 'App' => FALSE);
                    $js_included['Tool'] = array('Model' => 'Tool.ToolList', 'App' => FALSE);
                    $js_included['Strategy'] = array('Model' => 'Strategy.StrategyList', 'App' => FALSE);
                    
                    $js_included['ProjectAdmin']['Model'] [1] = 'Type.TypeList.SelectedRoom.PhaseEdit.PhaseListEdit.Phase.PhaseList';
                    $js_included['ProjectAdmin']['View'] [1] = 'RoomSelect.ProjectAdd.PhaseEdit.PhasesEdit';
                }
                
                if($user->checkAccess("viewProjectLog"))
                {
                    $js_included['Project']['Model'] .= '.Log.LogList';
                    $js_included['Project']['View'] .= '.Log';
                }
            }
            
            if($user->checkAccess("manageYears"))
            {
                $js_included['Year'] = array('Model' => 'Year.YearList', 'View' => 'Year');
            }
            
            if($user->checkAccess("manageSchools"))
            {
                $js_included['School'] = array(
                    'Model' => 'Room.RoomList.Grade.GradeList.Level.LevelList.School.SchoolList',
                    'View' => 'Room.Grade.Level.School.Creator.Coordinator',
                    'CoordinatorApp', 'CreatorApp', 'RoomApp'
                );
            }
            
            if($user->checkAccess("manageCoordinators"))
            {
                $js_included['Coordinator'] = array(
                    'Model' => 'Coordinator.CoordinatorList',
                    'View' => 'Coordinator.School',
                    'SchoolApp'
                );
            }
            
            if($user->checkAccess("manageCreators"))
            {
                $js_included['Creator'] = array(
                    'Model' => 'Creator.CreatorList',
                    'View' => 'Creator.School',
                    'SchoolApp'
                );
            }
            
            if($user->checkAccess("manageUsers"))
            {
                $js_included['User'] = array('Model' => 'User', 'Controller' => 'PageableUsers');
            }
            
            if($user->checkAccess("manageTools"))
            {
                $js_included['Tool'] = array(
                    'Model' => 'Tool.ToolList', 
                    'View' => 'Add.Edit.Tool'
                );
            }
            
            if($user->checkAccess("manageStrategies"))
            {
                $js_included['Strategy'] = array(
                    'Model' => 'Strategy.StrategyList', 
                    'View' => 'Add.Edit.Strategy'
                );
            }
            
            if($user->checkAccess("manageTemplatePhases"))
            {
                $js_included['Phase'] = array(
                    'Model' => 'Rule.RuleList.TemplatePhase.TemplatePhaseList', 
                    'View' => 'Phase.Form'
                );
            }
            
            if($user->checkAccess("manageInstructors"))
            {
                $js_included['Instructor'] = array('Model' => 'Selected.Instructor.InstructorList', 'View' => 'RoomSelect.Instructor');
            }
            if($user->checkAccess("manageStudents"))
            {
                $js_included['Student'] = array('Model' => 'Selected.Student.StudentList', 'View' => 'RoomSelect.Student');
            }
            
            $js_included['Index'] = array('Model' => 'GroupProject.GroupProjectList.AbstractModel', 'View' => 'ProjectItem');
            
            $js_included [] = 'GeneralWorkspace.Boot';
        }
        
        return array('App' => $js_included);
    }
    
    public static function getListTemplates()
    {
        $user = Yii::app()->user;
        $templates = array();
        if(!$user || $user->isGuest)
        {
            $templates[] = 'user-login';
            $templates[] = 'user-recover';
        }
        else
        {
            $isAdmin = $user->checkAccess("admin");
            $isCreator = $user->checkAccess("creator");
            $isCoordinator = $user->checkAccess("coordinator");
            $isInstructor = $user->checkAccess("instructor");
            $isStudent = $user->checkAccess("student");

            
            $templates[] = 'projects/app'; 
            
            if( $isInstructor || $isCoordinator || $isCreator || $isAdmin)
            {
                if($user->checkAccess("createProjects"))
                {
                    $templates[] = 'projects/add';
                    $templates[] = 'projects/phases';
                    $templates[] = 'projects/phases-phase';
                }
                if($user->checkAccess("manageProjectTeams"))
                {
                    $templates[] = 'projects/assign-teams';
                    $templates[] = 'projects/assign-teams-cont';
                    $templates[] = 'projects/assign-teams-cont-team';
                }
                if($user->checkAccess("viewProjectLog"))
                {
                    $templates[] = 'projects/logs';
                    $templates[] = 'projects/logs-log';
                }
            }
            if($isStudent || $isInstructor || $isCoordinator || $isCreator || $isAdmin)
            {
                $templates[] = 'admin/projects';
                $templates[] = 'project-item';
                $templates[] = 'projects/resume';
                $templates[] = 'projects/resume-view';
                $templates[] = 'projects/sidebar-myteam';
                $templates[] = 'projects/sidebar-answers';
                $templates[] = 'projects/sidebar-statistics';
                $templates[] = 'projects/sidebar-marks';
                $templates[] = 'projects/statistics';
                $templates[] = 'projects/statistics-phase';
                $templates[] = 'projects/statistics-assessment';
                $templates[] = 'projects/phase-view';
                $templates[] = 'projects/phase-view-content';
                $templates[] = 'projects/phase-view-answer';
                $templates[] = 'projects/phase-view-reanswer';
                $templates[] = 'projects/phase-view-reanswer-content';
                
            }
            
            if($isCoordinator || $isCreator || $isAdmin)
            {
                $templates[] = 'admin/projects-navbar';
            }
            if($isAdmin)
            {
                $templates[] = 'admin/users';        
                $templates[] = 'admin/users-search';        
                $templates[] = 'admin/years';
                $templates[] = 'admin/years-year';
                $templates[] = 'admin/schools';
                $templates[] = 'admin/schools-school';
                $templates[] = 'admin/school';
                $templates[] = 'admin/school-coordinators';
                $templates[] = 'admin/school-coordinator';
                $templates[] = 'admin/school-creators';
                $templates[] = 'admin/school-creator';
                $templates[] = 'admin/coordinator-schools';
                $templates[] = 'admin/coordinator-school';
                $templates[] = 'admin/coordinators';
                $templates[] = 'admin/coordinators-coordinator';
                $templates[] = 'admin/creators';
                $templates[] = 'admin/creators-creator';
                $templates[] = 'admin/creator-schools';
                $templates[] = 'admin/creator-school';
                $templates[] = 'admin/school-level';
                $templates[] = 'admin/school-level-grade';
                $templates[] = 'admin/school-level-grade-room';


                $templates[] = 'tools/tools';
                $templates[] = 'tools/view';
                $templates[] = 'tools/add';

                $templates[] = 'strategies/strategies';
                $templates[] = 'strategies/view';
                $templates[] = 'strategies/add';

                $templates[] = 'phases/phases';
                $templates[] = 'phases/view';
                $templates[] = 'phases/add';
            }
            if($user->checkAccess("manageInstructors"))
            {
                $templates[] = 'panel-select-room';
                $templates[] = 'instructors/instructors';
                $templates[] = 'instructors/panel';
                $templates[] = 'instructors/subpanel';
                $templates[] = 'instructors/content';
            }
            if($user->checkAccess("manageStudents"))
            {
                $templates[] = 'students/students';
                $templates[] = 'students/panel';
                $templates[] = 'students/subpanel';
                $templates[] = 'students/content';
            }
        }
        return $templates;
    }
    
    public static function getMinifiedTemplates()
    {
        $templatesList = self::getListTemplates();
        $template_id = "tmpl_".md5(implode(".", $templatesList));
        $template_cache = Yii::app()->cache->get($template_id);
        $template_date = self::findLastModifiedTemplate($templatesList);
        $regenerate_cache = ($template_cache && ($template_date>$template_cache['date']));

        if(!$template_cache || $regenerate_cache)
        {
            $template_cache = array('date'=>$template_date, 'cache'=>"");
            if(!empty($templatesList))
            {
                foreach ($templatesList as $tmpl)
                {
                    $tmpl_name = str_replace("/", "-", $tmpl);
                    $template_cache['cache'] .= '<script type="text/template" id="tmpl-'.$tmpl_name.'">';
                    $render = Yii::app()->controller->renderFile(Yii::app()->basePath.self::$tmpl_folder . $tmpl . self::$tmpl_ext,array(),true);
                    $render = preg_replace("/(\t|\r|\n)+/", "", $render);$render = preg_replace("/  +/"," ", $render);$render = preg_replace("/\> +\</", "><", $render);
                    $template_cache['cache'] .= $render . '</script>';
                }
            }
            Yii::app()->cache->set($template_id, $template_cache, 0);
        }
        
        return $template_cache['cache'];
    }
    
    public static function getMinifiedScriptsUri()
    {
        $script_list = self::getListScripts();
        
        $script_id = md5(json_encode($script_list));
        $script_date = self::findLastModifiedScript($script_list);
        
        $script_cache = Yii::app()->cache->get("script_cache");
        if(!$script_cache)
        {
            $script_cache = array();
        }
        if(!isset($script_cache[$script_id]))
        {
            $script_cache[$script_id] = $script_list;
            Yii::app()->cache->set("script_cache", $script_cache);
        }
        
        $scriptUrl = "/js/".$script_id."/".$script_date."/app.js";
        return $scriptUrl;
    }
    
    public static function getMinifiedScriptsMainUri()
    {
        $script_list = self::getListScriptsMain();
        $script_id = md5(json_encode($script_list));
        $script_date = self::findLastModifiedScript($script_list);
        
        $script_cache = Yii::app()->cache->get("script_cache");
        if(!$script_cache)
        {
            $script_cache = array();
        }
        if(!isset($script_cache[$script_id]) || (count($script_cache[$script_id]) != count($script_list)) )
        {
            $script_cache[$script_id] = $script_list;
            Yii::app()->cache->set("script_cache", $script_cache);
        }
        
        $scriptUrl = "/js/".$script_id."/".$script_date."/third-party.js";
        return $scriptUrl;
    }
    
    public static function generateMinifiedScript($jscript_id, $type = 'third-party')
    {
        $script_cache = Yii::app()->cache->get("script_cache");
        $script_list = array();
        if(isset($script_cache[$jscript_id]))
        {
            $script_list = $script_cache[$jscript_id];
        }
        else
        {
            if($type == 'third-party')
            {
                $script_list = self::getListScriptsMain();
            }
            else
            {
                $script_list = self::getListScripts();
            }
        }        
        
        $script_date = self::findLastModifiedScript($script_list);
        
        $script_content = ($type == 'third-party')?"":"(function(Modus){";
        if(!empty($script_list))
        {
            $script_content .= self::getContentScriptList($script_list);
        }
        $script_content .= ($type == 'third-party')?"":"})(window.Modus);";
        
        if(!YII_DEBUG)
        {
            if($type != 'third-party')
            {
                $yui = new YUICompressor();
                $yui->setOption("type", "js");
                $yui->addString($script_content);
                $script_content = $yui->compress();
            }
            
            Yii::app()->cache->set($jscript_id, array('time'=>$script_date,'cache'=>$script_content));
        }
        return $script_content;
    }
    
    private static function getContentScriptList($files, $folder = NULL)
    {
        if(!$folder)
        {
            $folder = Yii::app()->basePath . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . "js" . DIRECTORY_SEPARATOR;
        }
        $content = "\n";
        
        if(is_array($files))
        {
            foreach ($files as $key => $value)
            {
                if(is_array($value) && count($value)>0)
                {
                    if(is_string($key) && !is_numeric($key))
                    {
                        $content .= "\nModus['$key'] = Modus['$key'] || {Model:{},View:{},Controller:{}};\n";
                        $content .= self::getContentScriptList($value, $folder.$key.DIRECTORY_SEPARATOR);
                        if(is_dir($folder.$key) && (!array_key_exists("App", $files[$key]) || $files[$key]['App'] !== FALSE))
                        {
                            $content .= self::getContentScriptList("App", $folder.$key.DIRECTORY_SEPARATOR);
                        }
                    }
                    else
                    {
                        $content .= self::getContentScriptList($value, $folder);
                    }
                }
                else if(is_string($value))
                {
                    $k = (!is_string($key)|| is_numeric($key))?"":$key.DIRECTORY_SEPARATOR;
                    $values = explode(".", $value);
                    foreach ($values as $val)
                    {
                        $content .= self::getContentScriptList($val, $folder.$k);
                    }
                }
            }
        }
        else if(is_string($files))
        {
            if(is_dir($folder.$files))
            {
                $content .= self::getContentScriptList("App", $folder.$files.DIRECTORY_SEPARATOR);
            }
            else
            {
                $path = $folder.$files.".js";
                if(file_exists($path))
                {
                    $content .= file_get_contents($path);
                }
                else
                {
                    if($files != 'App')
                    {
                        $content .= "console.log('No se encontró el archivo " . str_replace("\\", "/", $path) . "');";
                    }
                }
            }
        }
        return $content."\n";
    }


    public static function getMinifiedStylesUri()
    {
        $style_list = self::getListStyles();
        $style_id = md5(implode(".", $style_list));
        $style_date = self::findLastModifiedStyle($style_list);
        
        $style_cache = Yii::app()->cache->get("style_cache");
        if(!$style_cache)
        {
            $style_cache = array();
        }
        if(!isset($style_cache[$style_id]) || (count($style_cache[$style_id]) != count($style_list)) )
        {
            $style_cache[$style_id] = $style_list;
            Yii::app()->cache->set("style_cache", $style_cache);
        }
        
        $styleUrl = "/css/".$style_id."/".$style_date.".css";
        return $styleUrl;
    }
    
    public static function generateMinifiedStyles($style_id)
    {
        
        $style_cache = Yii::app()->cache->get("style_cache");
        $style_list = array();
        if(isset($style_cache[$style_id]))
        {
            $style_list = $style_cache[$style_id];
        }
        else
        {
            $style_list = self::getListStyles();
        }
        
        $style_date = self::findLastModifiedStyle($style_list);
        $style_content = "";
        if(!empty($style_list))
        {
            if(YII_DEBUG)
            {
                foreach ($style_list as $file)
                {
                    $path = str_replace("/", DIRECTORY_SEPARATOR, Yii::app()->basePath . DIRECTORY_SEPARATOR . ".." . self::$style_folder .  $file . self::$style_ext);
                    if(file_exists($path))
                    {
                        $style_content .= file_get_contents($path);
                    }
                    else
                    {
                        $style_content .= "/* No se encontró el archivo " . $path . " */\n";
                    }
                }
            }
            else
            {
                $yui = new YUICompressor();
                $yui->setOption("type", "css");
                foreach ($style_list as $file)
                {
                    $path = str_replace("/", DIRECTORY_SEPARATOR, Yii::app()->basePath . DIRECTORY_SEPARATOR . ".." . self::$style_folder .  $file . self::$style_ext);
                    $yui->addFile($path);
                }
                $style_content = $yui->compress();
            }
        }
        if(!YII_DEBUG)
        {
            Yii::app()->cache->set($style_id, array('time'=>$style_date,'cache'=>$style_content));
        }
        return $style_content;
    }
    
    private static function findLastModifiedTemplate($files)
    {
        $templateFolder = Yii::app()->basePath.self::$tmpl_folder;
        $extension = self::$tmpl_ext;
        return self::findLastModifiedFile($templateFolder, $files, $extension);
    }

    private static function findLastModifiedScript($files, $folder = NULL) 
    {
        if(!$folder)
        {
            $folder = Yii::app()->basePath . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . "js" . DIRECTORY_SEPARATOR;
        }
        $newer = 0;
        
        if(is_array($files))
        {
            foreach ($files as $key => $value)
            {
                if(is_array($value) && count($value)>0)
                {
                    if(is_string($key) && !is_numeric($key))
                    {
                        $time = self::findLastModifiedScript($value, $folder.$key.DIRECTORY_SEPARATOR);
                    }
                    else
                    {
                        $time = self::findLastModifiedScript($value, $folder);
                    }
                    if($time>$newer) $newer = $time;
                }
                else if(is_string($value))
                {
                    $k = (!is_string($key)|| is_numeric($key))?"":$key.DIRECTORY_SEPARATOR;
                    $values = explode(".", $value);
                    foreach ($values as $val)
                    {
                        if(strlen($val))
                        {
                            $time = self::findLastModifiedScript($val, $folder.$k);
                            if($time>$newer) $newer = $time;
                        }
                    }
                }
            }
        }
        else if(is_string($files))
        {
            if(is_dir($folder.$files))
            {
                $time = self::findLastModifiedScript("App", $folder.$files.DIRECTORY_SEPARATOR);
            }
            else
            {
                $time = filemtime($folder.$files.".js");
            }
            if($time>$newer) $newer = $time;
        }
        return $newer;
    }
    
    private static function findLastModifiedStyle($files) 
    {
        $styleFolder = Yii::app()->basePath . DIRECTORY_SEPARATOR . ".." . self::$style_folder;
        $extension = self::$style_ext;
        return self::findLastModifiedFile($styleFolder, $files, $extension);
    }
    
    private static function findLastModifiedFile($dir, $files, $ext)
    {
        
        $newer = 0;
        foreach ($files as $file)
        {
            $path = str_replace("/", DIRECTORY_SEPARATOR, $dir . $file . $ext);
            if(file_exists($path))
            {
                $time = filemtime($path);
                if($time>$newer) $newer = $time;
            }
        }
        return $newer;
    }
}