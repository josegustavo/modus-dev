<?php

class ProjectHelper 
{
    
    public static function updateManagers($new_managers_ids = array())
    {
        $project_id = Yii::app()->controller->action->project_id;
        $project_managers_db = ProjectManager::model()->findAllByAttributes(array('project_id' => $project_id), array('select' => 'manager_id'));
        $actual_managers_ids = Functions::list_pluck($project_managers_db, "manager_id");
        
        //Eliminar a los de la lista actual y que no estan en la nueva lista
        $to_delete = array_diff($actual_managers_ids, $new_managers_ids);
        if(count($to_delete))
        {
            ProjectManager::model()->deleteAllByAttributes(array('project_id' => $project_id, 'manager_id' => $to_delete));
            
            $names = Functions::list_pluck(User::model()->findAllByPk($to_delete, array('select' => 'username')), 'username');
            $el = count($names)>1?"los":"el"; $es = count($names)>1?"es":"";
            CLog::logProject("removeProjectManagers","Se quitó $el administrador$es del proyecto \"" . join(", ", $names) . "\" ", array_values($to_delete));
        }

        //Agregar a los que estan en la nueva lista y no estan en la lista actual
        $to_add = array_diff($new_managers_ids, $actual_managers_ids);
        if(count($to_add))
        {
            foreach ($to_add as $o_id)
            {
                $p_o = new ProjectManager();
                $p_o->attributes = array('project_id' => $project_id, 'manager_id' => $o_id);
                $p_o->save();
            }
            
            $names = Functions::list_pluck(User::model()->findAllByPk($to_add, array('select' => 'username')), 'username');
            $el = count($names)>1?"los":"el"; $es = count($names)>1?"es":"";
            CLog::logProject("addProjectManagers","Se agregó $el administrador$es del proyecto \"" . join(", ", $names) . "\" ", array_values($to_add));
        }
    }
    
    public static function updateRooms($new_rooms_ids = array())
    {
        $project_id = Yii::app()->controller->action->project_id;
        $project_rooms_db =  ProjectRoom::model()->findAllByAttributes( array("project_id"=>$project_id), array('select' => 'room_id') );
        $actual_rooms_ids = Functions::list_pluck($project_rooms_db, "room_id");
        
        //Eliminar a los de la lista actual y que no estan en la nueva lista
        $to_delete = array_diff($actual_rooms_ids, $new_rooms_ids);
        if(count($to_delete))
        {
            ProjectRoom::model()->deleteAllByAttributes(array('project_id' => $project_id, 'room_id' => $to_delete));
            
            $rooms_db = Room::model()->with('level','school')->findAllByPk($to_delete, array('select' => 'name'));
            $room = $rooms_db[0];
            $level_db = $room->level;
            $school_db = $room->school;
            $level_name = $level_db->name . "° Grado de " . $level_db->level_name . ", " . $school_db->name;
            $names = Functions::list_pluck($rooms_db, 'name');
            $s = count($names)>1?"s":""; $es = count($names)>1?"es":"";
            CLog::logProject("removeProjectRooms","Se quitó la$s seccion$es \"" . join(", ", $names) . "\" $level_name", array_values($to_delete));
        }

        //Agregar a los que estan en la nueva lista y no estan en la lista actual
        $to_add = array_diff($new_rooms_ids, $actual_rooms_ids);
        if(count($to_add))
        {
            foreach ($to_add as $room_id)
            {
                $ps = new ProjectRoom;
                $ps->room_id = $room_id;
                $ps->project_id = $project_id;
                $ps->save();
            }
            
            $rooms_db = Room::model()->with('level','school')->findAllByPk($to_add, array('select' => 'name'));
            $room = $rooms_db[0];
            $level_db = $room->level;
            $school_db = $room->school;
            $level_name = $level_db->name . "° Grado de " . $level_db->level_name . ", " . $school_db->name;
            $names = Functions::list_pluck($rooms_db, 'name');
            $s = count($names)>1?"s":""; $es = count($names)>1?"es":"";
            CLog::logProject("addProjectRooms","Se agregó la$s seccion$es \"" . join(", ", $names) . "\" $level_name", array_values($to_add));
        }
    }
    
    public static function updatePhaseAttachments($phase)
    {
        $phase_id = $phase['id'];
        $phase_name = $phase['name'];
        
        $new_attachs_ids = isset($phase['attachments'])?$phase['attachments']:array();
        if(count($new_attachs_ids)>0 && is_array($new_attachs_ids[0]) && isset($new_attachs_ids[0]['id']))
        {
            $new_attachs_ids = Functions::list_pluck($new_attachs_ids, "id");
        }
        $phase_attachs_db =  PhaseAttachment::model()->findAllByAttributes( array("phase_id"=>$phase_id), array('select' => 'file_id') );
        $actual_attachs_ids = Functions::list_pluck($phase_attachs_db, "file_id");
        //Eliminar a los de la lista actual y que no estan en la nueva lista
        $to_delete = array_diff($actual_attachs_ids, $new_attachs_ids);
        if(count($to_delete))
        {
            PhaseAttachment::model()->deleteAllByAttributes(array('phase_id' => $phase_id, 'file_id' => $to_delete));
            
            $names = Functions::list_pluck(File::model()->findAllByPk($to_delete, array('select' => 'original_name')), 'original_name');
            $el = count($names)>1?"los":"el"; $s = count($names)>1?"s":"";
            $data = array($phase_id, array_values($to_delete));
            CLog::logProject("removePhaseAttachments","Se quitó $el adjunto$s \"" . join(", ", $names) . "\" de la fase $phase_name", $data);
        }

        //Agregar a los que estan en la nueva lista y no estan en la lista actual
        $to_add = array_diff($new_attachs_ids, $actual_attachs_ids);
        if(count($to_add))
        {
            foreach ($to_add as $file_id)
            {
                $ps = new PhaseAttachment;
                $ps->file_id = $file_id;
                $ps->phase_id = $phase_id;
                $ps->save();
            }
            
            $names = Functions::list_pluck(File::model()->findAllByPk($to_add, array('select' => 'original_name')), 'original_name');
            $el = count($names)>1?"los":"el"; $s = count($names)>1?"s":"";
            $data = array($phase_id, array_values($to_add));
            CLog::logProject("addPhaseAttachments","Se agregó $el adjunto$s \"" . join(", ", $names) . "\" en la fase $phase_name", $data);
        }
    }
    
    public static function updatePhaseStrategies($phase)
    {
        $phase_id = $phase['id'];
        $phase_name = $phase['name'];
        
        $new_strategies_ids = isset($phase['strategies'])?$phase['strategies']:array();
        $phase_strategies_db =  PhaseStrategy::model()->findAllByAttributes( array("phase_id"=>$phase_id), array('select' => 'strategy_id') );
        $actual_strategies_ids = Functions::list_pluck($phase_strategies_db, "strategy_id");
        //Eliminar a los de la lista actual y que no estan en la nueva lista
        $to_delete = array_diff($actual_strategies_ids, $new_strategies_ids);
        if(count($to_delete))
        {
            PhaseStrategy::model()->deleteAllByAttributes(array('phase_id' => $phase_id, 'strategy_id' => $to_delete));
            
            $names = Functions::list_pluck(Strategy::model()->findAllByPk($to_delete, array('select' => 'name')), 'name');
            $la = count($names)>1?"las":"la"; $s = count($names)>1?"s":"";
            $data = array($phase_id, array_values($to_delete));
            CLog::logProject("removePhaseStrategies","Se quitó $la estratégia$s \"" . join(", ", $names) . "\" de la fase $phase_name", $data);
        }

        //Agregar a los que estan en la nueva lista y no estan en la lista actual
        $to_add = array_diff($new_strategies_ids, $actual_strategies_ids);
        if(count($to_add))
        {
            foreach ($to_add as $strategyId)
            {
                $ps = new PhaseStrategy;
                $ps->strategy_id = $strategyId;
                $ps->phase_id = $phase_id;
                $ps->save();
            }
            
            $names = Functions::list_pluck(Strategy::model()->findAllByPk($to_add, array('select' => 'name')), 'name');
            $la = count($names)>1?"las":"la"; $s = count($names)>1?"s":"";
            $data = array($phase_id, array_values($to_add));
            CLog::logProject("addPhaseStrategies","Se agregó $la estratégia$s \"" . join(", ", $names) . "\" en la fase $phase_name", $data);
        }
    }
    public static function updatePhaseTools($phase)
    {
        $phase_id = $phase['id'];
        $phase_name = $phase['name'];

        $new_tools_ids = isset($phase['tools'])?$phase['tools']:array();
        $phase_tools_db =  PhaseTool::model()->findAllByAttributes( array("phase_id"=>$phase_id), array('select' => 'tool_id') );
        $actual_tools_ids = Functions::list_pluck($phase_tools_db, "tool_id");
        //Eliminar a los de la lista actual y que no estan en la nueva lista
        $to_delete = array_diff($actual_tools_ids, $new_tools_ids);
        if(count($to_delete))
        {
            PhaseTool::model()->deleteAllByAttributes(array('phase_id' => $phase_id, 'tool_id' => $to_delete));
            
            $names = Functions::list_pluck(Tool::model()->findAllByPk($to_delete, array('select' => 'name')), 'name');
            $la = count($names)>1?"las":"la"; $s = count($names)>1?"s":"";
            $data = array($phase_id, array_values($to_delete));
            CLog::logProject("removePhaseTools","Se quitó $la herramienta$s \"" . join(", ", $names) . "\" de la fase $phase_name", $data);
        }

        //Agregar a los que estan en la nueva lista y no estan en la lista actual
        $to_add = array_diff($new_tools_ids, $actual_tools_ids);
        if(count($to_add))
        {
            foreach ($to_add as $toolId)
            {
                $ps = new PhaseTool;
                $ps->tool_id = $toolId;
                $ps->phase_id = $phase_id;
                $ps->save();
            }
            
            $names = Functions::list_pluck(Tool::model()->findAllByPk($to_add, array('select' => 'name')), 'name');
            $la = count($names)>1?"las":"la"; $s = count($names)>1?"s":"";
            $data = array($phase_id, array_values($to_add));
            CLog::logProject("addPhaseTools","Se agregó $la herramienta$s \"" . join(", ", $names) . "\" en la fase $phase_name", $data);
        }
    }

    public static function removeAnswer($answer)
    {
        if($answer)
        {
            $data = $answer->attributes;
            $answer_id = $answer->id;
            $phase = $answer->phase;
            $phase_id = $phase->id;
            $phase_name = $phase->name;
            $user = $answer->author;
            $user_name = $user->username;
            
            //Log de eliminación los adjuntos de la respuesta
            if($answer->files)
            {
                foreach ($answer->files as $file)
                {
                    $file_id = $file->id;
                    $original_name = $file->original_name;
                    CLog::logProject("removeAnswerAttachment","Se quitó el adjunto $original_name, de una respuesta de $user_name, en la fase $phase_name", array($phase_id, $answer_id, $file_id) );
                }
            }
            
            //Eliminar las respuesta de las respuestas
            if($answer->reanswers)
            {
                foreach ($answer->reanswers as $reanswer)
                {
                    self::removeReAnswer($reanswer);
                }
            }
            
            if($answer->delete())
            {
                CLog::logProject("removeAnswer","Se quitó una respuesta de $user_name en la fase $phase_name", array($phase_id, $data) );
                return TRUE;
            }
            
        }
        return FALSE;
    }
    
    public static function removeReAnswer($reanswer)
    {
        if($reanswer)
        {
            $reanswer_id = $reanswer->id;
            $data = $reanswer->attributes;
            $answer = $reanswer->answer;
            $answer_id = $answer->id;
            $answer_username = $answer->author->username;
            $reanswer_username = $reanswer->author->username;
            $phase = $answer->phase;
            $phase_id = $phase->id;
            $phase_name = $phase->name;
            
            //Log de eliminación los adjuntos de la respuesta
            if($reanswer->files)
            {
                foreach ($reanswer->files as $file)
                {
                    $file_id = $file->id;
                    $original_name = $file->original_name;
                    CLog::logProject("removeReAnswerAttachment","Se quitó el adjunto $original_name, de una réplica de $reanswer_username, de una respuesta de $answer_username  en la fase $phase_name", array($phase_id, $answer_id, $reanswer_id, $file_id) );
                }
            }
            
            if($reanswer->delete())
            {
                CLog::logProject("removeReAnswer","Se quitó la réplica de $reanswer_username, de una respuesta de $answer_username, en la fase $phase_name", array($phase_id, $answer_id, $data) );
                return TRUE;
            }
        }
        return FALSE;
    }
    
    
    public static function removeTeamUser($team, $user)
    {
        if($team && $user)
        {
            $team_id = $team->id;
            $user_id = $user->id;
            
            $user_name = $user->username;
            $team_name = $team->name;
            
            $answers = Answer::model()->findAllByAttributes(array('team_id' => $team_id, 'author_id' => $user_id));
            //Eliminar las respuestas que el usuarios haya enviado
            foreach ($answers as $answer)
            {
                self::removeAnswer($answer);
            }
            
            $delete = TeamUser::model()->deleteAllByAttributes(array('team_id' => $team_id, 'user_id' => $user_id));
            if($delete)
            {
                CLog::logProject("removeTeamUser", "Se quitó al usuario $user_name del grupo $team_name ", array($team_id, $user_id));
                return TRUE;
            }
        }
        return FALSE;
    }
    
    public static function getAuthorizedProject($project_id)
    {
        $user = Yii::app()->user;
        $project = FALSE;
        if($user->checkAccess("student"))
        {
            $project = Project::model()
                    ->with(array('teams' => array('select' => 'id,ord,name'), 'teams.users' => array('select' => 'id')))
                    ->findByPk($project_id, array(
                        'condition' => 'users.id=:userId',
                        'params' => array(':userId' => $user_id)
                    )
            );
        }
        else if ($user->checkAccess("instructor"))
        {
            $project = Project::model()
                    ->with(array('users' => array('select' => 'id,username,firstname,lastname')))
                    ->findByPk($project_id, array(
                        'condition' => 'users.id=:userId',
                        'params' => array(':userId' => $user_id)
                    )
            );
        }
        else if($user->checkAccess("creator") || Yii::app()->user->checkAccess("coordinator"))
        {
            $project = Project::model()
                    ->with(array('school' => array('select' => 'id,name'), 'school.users' => array('select' => 'id')))
                    ->findByPk($project_id, array(
                        'condition' => 'users.id=:userId',
                        'params' => array(':userId' => $user_id)
                    )
            );
        }
        else if($user->checkAccess("admin"))
        {
            $project = Project::model()->findByPk($project_id);
        }
        return $project;
    }
}