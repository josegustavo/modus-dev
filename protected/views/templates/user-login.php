<div class="row">
<div class="col-md-12 col-sm-12">
<form id="login-recover-form" class="form-horizontal" role="form" method="post">
    <div class="form-group">
        <div class="col-md-4 col-md-offset-0 col-sm-6 col-sm-offset-3"><span class="title pull-right">Iniciar sesión</span></div>
        <div class="clearfix visible-sm"></div>
        <div class="col-md-4 col-md-offset-0 col-sm-6 col-sm-offset-3">
            <input type="user" class="form-control input-lg" id="inputUser" name="user" placeholder="Nombre de usuario">
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
            <input type="password" class="form-control input-lg" id="inputPassword" name="password" placeholder="Contraseña">
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
            <a id="loginButton" class="btn btn-default btn-lg pull-right" role="button">Iniciar sesión</a>
        </div>
    </div>
    <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
        <div class="yellow-line"></div>
    </div>
    <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
        <span>¿Has olvidado tu contraseña? <a class="help-block" modus-link  href="javascript:alert('Si desea recuperar su contraseña, por favor contacte al administrado.')">Recuperar contraseña</a></span>
    </div>
</form>
</div>
</div>