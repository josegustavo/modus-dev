<div class="page-title">

</div>
<div class="form-horizontal">
        <div class="form-group">
                <label class="col-sm-2 control-label">Título</label>
                <div class="col-sm-8">
                        <input type="hidden" id="tool-id" value="">
                        <input type="text" class="form-control" id="tool-title" value="">
                </div>
        </div>
        <div class="form-group">
                <label class="col-sm-2 control-label">Dirección (URL)</label>
                <div class="col-sm-8">
                        <input type="text" class="form-control" id="tool-url" value="">
                </div>
        </div>
        <div class="form-group">
                <label class="col-sm-2 control-label">Imagen</label>
                <div class="col-sm-4">
                        <div class="input-group">
                            <div class="img-thumbnail  middle" id="tool-image" title="Click para seleccionar una imagen">
                                <a id="tool-image-status" class="hidden"><br/>Seleccionar<br/><i class="fa fa-plus"></i><br/><br/></a>
                            </div>
                        </div>
                </div>
        </div>
        <div class="form-group">
                <label class="col-sm-2 control-label">
                Descripción
                <small class="help-block"></small>
                </label>
                <div class="col-sm-8">                            
                                <textarea class="form-control mce" id="tool-description" rows="5"></textarea>
                </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label"></label>
            <div class="col-sm-10">                            
                <ul class="list-inline">
                    <li>
                        <a type="button" rel="button" class="btn btn-secondary btn-save">Guardar&nbsp;<i class="fa fa-chevron-circle-right"></i></a>
                    </li>
                    <li>
                        <a type="button" rel="button" class="btn btn-danger btn-cancel">Cancelar&nbsp;<i class="fa fa-chevron-circle-right"></i></a>
                    </li>
                </ul>
            </div>
        </div>
</div>