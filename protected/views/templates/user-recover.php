<form id="login-recover-form" class="form-horizontal" role="form">
    <div class="form-group">
        <div class="col-sm-3"><span class="title pull-right">Recuperar contraseña</span></div>
        <div class="col-sm-6">
            <input type="user" class="form-control input-lg" id="inputUser" name="user" placeholder="Nombre de usuario">
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-6">
            <input type="password" class="form-control input-lg" id="inputPassword" name="password" placeholder="Contraseña">
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-6">
            <input type="submit" id="loginButton" type="submit" class="btn btn-default btn-lg pull-right" value="Iniciar sesión" />
        </div>
    </div>
    <div class="col-sm-offset-3 col-sm-6">
        <div class="yellow-line"></div>
    </div>
    <div class="col-sm-offset-3 col-sm-6">
        <span>¿Has olvidado tu contraseña? <a class="help-block" modus-link  href="<%=Modus.uriRoot%>/modus/users/recover_password">Recuperar contraseña</a></span>
    </div>
</form>