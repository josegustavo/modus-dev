<div class="row">
    <div class="col-sm-12 page-header">
        <a modus-link  href="<%=Modus.uriRoot%>/project/view/<%=model.slug%>">
            <h2 class="title-modus">
                <img src="<%=model.image?model.image:(Modus.uriCDN + '/img/default_project.png')%>" alt="" class="img-circle">
                <%=" " + model.title%>
            </h2>
        </a>
        <strong class="subtitle">
            
            <%=model.school_name?(model.school_name + ' - '):''%><%=model.level_name?(model.level_name+' - '):''%><%if(model.rooms_ids){%><%=(model.rooms_ids.length>1)?"Secciones ":"Sección "%><%=model.rooms_names%><%}%>
        </strong>
        <ul class='list-inline'>
            <li><a modus-link-back><i class="fa fa-arrow-circle-left"></i> Volver</a></li>
            <%if(model.creator){%>
            <li>
                <a><i class="fa fa-user"></i></a>
                <%if(model.managers && model.managers.length){ %> <a> Administrado por </a>
                    <%model.managers.each(function(manager,i){if(i<3){%>
                    <a title="<%=manager.getFullName()%>"><%=manager.get("name")%><%= i+1 < 3?", ":""%></a>
                    <%}else if(i==4){%>
                    <a title=""> y <%=model.managers.length-3%> profesor<%=model.managers.length-3>1?'es':''%> más.</a>
                    <%}})%>
                    <%} else { %>
                    <a> Creado por <%=model.creator_name%></a>
                    <%}%>
                
            </li>
            <%}%>
            <li><a title="<%=model.modified%>"><i class="fa fa-calendar"></i><span id="modified-span"> Modificado hace <%=model.modified_ago%></span></a></li>
            <li><a title="<%=model.state_date%>"><i class="fa fa-clock-o"></i><span id="starts-ends-span"><%=model.state==0?" Inicia en ":(model.state==1?" Termina en ":" Terminó hace ")%> <%=model.state_ago%></span></a></li>
            <%if(model.managers && model.managers.length>0){%>
            <li><a title="Administrado por <%=_.pluck(model.managers,'name').join(', ')%>"><i class="fa fa-user"></i><span id="managers-span"> Administrado por <%=_.map(model.managers, function(m){return m.name.split(" ")[0];}).join(", ")%></span></a></li>
            <%}%>
            <%if(model.is_owner){%>
            <li><a modus-link href="<%=Modus.uriRoot%>/project/edit/<%=model.id%>/<%=model.slug%>"><i class="fa fa-pencil fa-fw"></i> Modificar</a></li>
            <li><a modus-link href="<%=Modus.uriRoot%>/project/view/<%=model.id%>/<%=model.slug%>/logs"><i class="fa fa-list"></i> Eventos</a></li>
            <%}%>
        </ul>
    </div>
    <div class="col-xs-12 col-sm-3 col-md-2">
        <div class="sidebar hidden-print" role="complementary">
            <ul class="nav sidenav">
                <li id="sidebar-answers"></li>
                <li id="sidebar-myteam"></li>
                <li id="sidebar-dashboard"></li>
                <li id="sidebar-marks"></li>
                <li>
                    <div class="hidden-xs last-block">
                        <a id="collapse-column"><i class="fa fa-caret-square-o-left"></i><span> Mostrar una columna</span></a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <div class="col-xs-12 col-sm-9 col-md-10 generalbar" role="main"></div>
</div>