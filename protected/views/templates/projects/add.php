<div class="col-sm-12 page-title">
    <div class="row">
        <div class="col-sm-3 col-md-2">
            <h2 class="title-modus">Proyecto</h2>
        </div>
    </div>
</div>
<div class="form-horizontal">
    <div class="form-group">
        <label for="project-title" class="col-sm-3 col-md-2 control-label">Título</label>
        <div class="col-sm-9 col-md-10">
            <input type="text" class="form-control bind" bind="title" id="project-title" value="">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 col-md-2 control-label">Colegio</label>
        <div class="col-sm-9 col-md-10">
            <div class="input-group">
                <div class="form-control" id="project-school" value=""></div>
                <div class="input-group-btn">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu pull-right" role="menu" id="project-school-ddm"></ul>
                </div>
            </div>                        
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 col-md-2 control-label">Grado y sección</label>
        <div class="col-sm-5 col-md-5">
            <div class="input-group">
                <div class="form-control" id="project-grade" value="" ></div>
                <div class="input-group-btn">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-caret-down"></i>
                    </button>
                    <ul class="dropdown-menu pull-right" role="menu" id="project-grade-ddm"></ul>
                </div>
            </div>
        </div>
        <div class="col-sm-4 col-md-5">
            <div class="input-group">
                <div class="form-control" id="project-room" value=''></div>
                <div class="input-group-btn">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-caret-down"></i>
                    </button>
                    <ul class="dropdown-menu pull-right" role="menu" id="project-room-ddm"></ul>
                </div>
            </div>
        </div>
    </div>
    <%if(Modus && ( Modus.isAdmin || Modus.isCoordinator || Modus.isCreator)){%>
    <div class="form-group">
        <label class="col-sm-3 col-md-2 control-label">Administradores</label>
        <div class="col-sm-9 col-md-10">
            <div class="input-group" data-toggle="dropdown">
                <div class="form-control" id="project-instructor" value="" style="overflow: auto;" ></div>
                <div class="input-group-btn"  >
                    <button type="button" class="btn btn-default dropdown-toggle ">
                        <i class="fa fa-caret-down"></i>
                    </button>                    
                </div>
            </div>
            <ul class="dropdown-menu pull-right" role="menu" id="project-instructor-ddm"></ul>
        </div>
    </div>
    <%}%>
    <% if(Modus && Modus.config && Modus.config.order_projects_by_type == true){ %>
    <div class="form-group">
        <label class="col-sm-3 col-md-2 control-label">Tipo</label>
        <div class="col-sm-9 col-md-10">
            <div class="input-group" data-toggle="dropdown">
                <div class="form-control" id="project-type" value=""></div>
                <div class="input-group-btn">
                    <button type="button" class="btn btn-default dropdown-toggle ">
                        <i class="fa fa-caret-down"></i>
                    </button>                    
                </div>
            </div>
            <ul class="dropdown-menu pull-right" role="menu" id="project-type-ddm"></ul>
        </div>
    </div>
    <%}%>
    <div class="form-group">
        <label class="col-sm-3 col-md-2 control-label">Fecha de inicio</label>
        <div class="col-sm-4 col-md-3">
            <div class="input-group selected-timepicker" id="project-start-date-parent">
                <input type="text" class="form-control" id="project-start-date" data-format="dd/MM/yyyy" value="">
                <div class="input-group-btn">
                    <button type="button" class="btn btn-default dropdown-toggle add-on">
                        <i class="fa fa-caret-down"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 col-md-2 control-label">Fecha de fin</label>
        <div class="col-sm-4 col-md-3">
            <div class="input-group selected-timepicker" id="project-end-date-parent">
                <input type="text" class="form-control" id="project-end-date" data-format="dd/MM/yyyy" value="">
                <div class="input-group-btn">
                    <button type="button" class="btn btn-default dropdown-toggle add-on">
                        <i class="fa fa-caret-down"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 col-md-2 control-label">Imagen</label>
        <div class="col-sm-4">
            <div class="input-group">
                <div id="project-image"></div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 col-md-2 control-label">
        Descripción
        <small class="help-block"></small>
        </label>
        <div class="col-sm-9 col-md-10">                            
                <textarea class="form-control mce" id="project-description" rows="5"></textarea>
        </div>
    </div>
    <div class="dotted-line"></div>
    <div class="form-group">
        <div class="col-sm-9 col-md-10 col-sm-offset-3 col-md-offset-2">
            <ul class="list-inline">
                    <li>
                        <a type="button" rel="button" class="btn btn-secondary btn-save">Guardar&nbsp;<i class="fa fa-chevron-circle-right"></i></a>
                    </li>
                    <li>
                        <a type="button" rel="button" class="btn btn-secondary btn-next-tab">Siguiente&nbsp;<i class="fa fa-chevron-circle-right"></i></a>
                    </li>
                    <li class="pull-right">
                        <a type="button" rel="button" class="btn btn-danger btn-cancel">Cancelar&nbsp;<i class="fa fa-chevron-circle-right"></i></a>
                    </li>
                </ul>
        </div>
    </div>
</div>