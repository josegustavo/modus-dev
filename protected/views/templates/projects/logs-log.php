<tr>
    <td>
        <span title="hace <%=log.created_ago%>"><%=log.created%></span>
    </td>
    <td>
        <span title="<%=log.user_fullname%>"><%=log.username%></span>
    </td>
    <td>
        <span><%=log.action%></span>
    </td>
    <td>
        <span><%=log.human_comment%></span>
    </td>
    <td class="dropdown">
        <a data-toggle="dropdown"><i class="fa fa-info"></i></a>
        <div class="dropdown-menu">
            <textarea>
                <%=log.data%>
            </textarea>
        </div>
    </td>
</tr>