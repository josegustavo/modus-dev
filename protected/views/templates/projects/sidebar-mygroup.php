<div class="panel-heading groups-heading">
    <h3 class="panel-title side-title">
        <i class="fa fa-users"></i><%=group && (" " + (group.get('name') + " - " + (section.get('name')) )) %> 
        <a class="collapse-btn pull-right collapse-answers" data-toggle="collapse" data-target="#my-group-side-list">
            <i class="fa fa-angle-double-up up "></i>
            <i class="fa fa-angle-double-down down"></i>
        </a>
    </h3>
</div>
<div class="panel-body groups-list  collapse in" id="my-group-side-list">
    <dd class="side-list">
    <%if(group.get('students').length){ group.get('students').each(function(student){%>
        <li><%=student.get('full_name')%></li>
    <%}) }else{%>
        <li>No tiene alumnos</li>
    <%}%>
    </dd>
</div>