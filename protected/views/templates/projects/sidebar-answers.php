<div class="panel-heading teams-heading">
    <h3 class="panel-title side-title">
        <i class="fa fa-bars"></i> Respuestas
        <a class="collapse-btn pull-right collapse-answers" data-toggle="collapse" data-target="#answers-side-list">
            <i class="fa fa-angle-double-up up "></i>
            <i class="fa fa-angle-double-down down"></i>
        </a>
    </h3>
</div>
<div class="panel-body teams-list  collapse in" id="answers-side-list">
    <%_.each(rooms, function(room){
        if(room.teams && room.teams.length>0){%>
        <dl>
            <dt>Sección "<%=room.name%>"</dt>
            <dd class="side-list">
            <%_.each(room.teams, function(team){
                if(team.ord>0){%>
                    <li team-id="<%=team.id%>" class="gotoTeamBtn <%=(selected_team && (selected_team==team.ord))?'active':''%>">
                        <a><i class="fa fa-caret-right"></i>&nbsp;<%=team.name%></a>
                    </li>
                <%}
              })%>
            </dd>
        </dl>
        <%}%>
    <%})%>
</div>