<table class="table table-bordered table-assessment">
        <thead>
            <tr>
                <th rowspan="3"></th>
                <th colspan="<%=(phases.length*3)%>">Nota parcial</th>
                <th rowspan="3" colspan="1">Nota final</th>
            </tr>
            <tr>
                <%_.each(phases, function(phase){%>
                <th colspan="3"><%=phase.name%></th>
                <%})%>
            </tr>
            <tr>
                <%_.each(phases, function(phase){%>
                <th class="small">Nota Alumno</th>
                <th class="small">Nota Profesor</th>
                <th class="small">Promedio</th>
                <%})%>
            </tr>
        </thead>
        <tbody>
            <%_.each(teams, function(team){%>
            <tr>
                <td class="bold" style="border-right: 0;"><%=team.name%></td>
                <%_.each(phases, function(phase){%>
                <td class="mark-student" title="Nota asignada por el grupo"><%=team.marks[phase.id] && team.marks[phase.id][0]%></td>
                <td class="mark-admin" title="Nota asignada por el profesor"><%=team.marks[phase.id] && team.marks[phase.id][1]%></td>
                <td class="mark" title="Promedio"><%=team.marks[phase.id] && team.marks[phase.id][2]%></td>
                <%})%>
                <td class="mark" title="Promedio final"><%=team.final_mark%></td>
            </tr>
            <%})%>
        </tbody>
</table>

<div class="well">
    <h4><a href="<%=url_export%>"><img src="<%=Modus.uriCDN%>/img/xls-121.png" width="24" height="24"/> Descargar</a></h4>
</div>