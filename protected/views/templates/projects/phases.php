<div class="col-sm-12 page-title">
    <div class="row">
        <div class="col-sm-2">
            <h2 class="title-modus">Fases</h2>
        </div>
    </div>
</div>
<div class="form-horizontal">
       
       <div class="panel-group" id="phases-list">

       </div>

       <div class="dotted-line"></div>
        <div class="form-group">
            <div class="col-sm-10 col-sm-offset-2">
                <ul class="list-inline">
                    <li>
                        <a type="button" rel="button" class="btn btn-secondary  btn-save">Guardar&nbsp;<i class="fa fa-chevron-circle-right"></i></a>
                    </li>
                    <li class="pull-right">
                        <a modus-link type="button" rel="button" class="btn btn-danger  btn-cancel" href="/">Cancelar&nbsp;<i class="fa fa-chevron-circle-right"></i></a>
                    </li>
                </ul>
            </div>
        </div>
</div>