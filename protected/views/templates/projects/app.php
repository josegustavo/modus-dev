<ul class="nav nav-tabs general-tabs">
    <li class="modus-tab" id="project-tab">
        <a class="select-tab" data-toggle="tab">Proyecto</a>
    </li>
    <li class="modus-tab" id="phases-and-assignments-tab">
        <a class="select-tab" data-toggle="tab">Fases</a>
    </li>
    <li class="modus-tab" id="assign-teams-tab">
        <a class="select-tab" data-toggle="tab">Asignar grupos</a>
    </li>
</ul>
<div class="tab-content">
    <div class="loading">
        <h2><i class="fa fa-spinner fa-spin"></i> Cargando proyecto...</h2>
    </div>
    <div id="project" class="tab-pane">
        
    </div>
    <div id="phases-and-assignments" class="tab-pane">
        
    </div>
    <div id="assign-teams" class="tab-pane ">
        
    </div>
</div>