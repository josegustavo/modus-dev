<h2 class="accordion-header title-phase collapsed" data-toggle="collapse" data-target="#phase-<%=phase.id%>" data-parent="#phaseslist">
        <%if(phase.image){%>
            <img src="<%=phase.image%>" alt="" class="img-circle">
        <%}%>
        <%=" "+phase.name%>
        <i class="fa fa-chevron-circle-down icon-down"></i>
        <i class="fa fa-chevron-circle-up icon-up"></i>
</h2>
<div class="phase-body collapse out" id="phase-<%=phase.id%>"></div>
