<div class="page-title">
    <h2 class="title-modus">Asignar grupos</h2>
</div>
<div class="form-horizontal">
    
   <div class="form-group">
       <label class="col-sm-2 control-label">Sección </label>
       <div class="col-sm-2">
           <div class="input-group">
                <div class="form-control" id="groups-select-section" value=""></div>
                <div class="input-group-btn">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><i class="fa fa-caret-down"></i></button>
                    <ul class="dropdown-menu pull-right" id="groups-select-section-ddm"></ul>
                </div>
            </div>
       </div>
   </div>
   <div class="form-group" id="group-zone"></div>
   
   <div class="form-group">
        <label class="col-sm-2 control-label"></label>

        <div class="col-sm-10">
            <ul class="list-inline">
                <li><a class="btn-modus" id="end-add-project">Finalizar <i class="fa fa-chevron-circle-right"></i></a></li>
            </ul>       
        </div>
    </div>
</div>