<div class="panel-heading teams-heading">
    <h3 class="panel-title side-title">
        <i class="fa fa-check-square-o"></i> Notas
        <a class="collapse-btn pull-right collapse-score" data-toggle="collapse" data-target="#score-side-list">
            <i class="fa fa-angle-double-up up "></i>
            <i class="fa fa-angle-double-down down"></i>
        </a>
    </h3>
</div>
<div class="panel-body teams-list  collapse in" id="score-side-list">
    <%_.each(rooms, function(room){
        if(room.teams && room.teams.length>0){%>
        <dl >
            <dd class="side-list">
            <li class='gotoTeamBtn' room-id="<%=room.id%>">SECCIÓN "<%=room.name%>"</li>
            </dd>
        </dl>
        <%}%>
    <%})%>
</div>