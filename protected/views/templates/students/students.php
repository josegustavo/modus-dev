        <div class="col-lg-12 col-md-12">
            <h2 class="title-modus"><a modus-link-back title="Volver"><i class="fa fa-arrow-left"></i></a>&nbsp;
            <i class="fa fa-child"></i>&nbsp;Administrar Alumnos</h2>
        </div>
        <div class="col-lg-3 col-md-12 student-panel-left panel-left">

        </div>
        <div class="col-lg-9 col-md-12" >
            <div class="form-group assign-student-form hidden">
                <label class="control-label">Asignar Alumno</label>
                <div>
                    <input id="assign-student-input" type="text" class="form-control typehead assign-student-input" placeholder="Nombre del alumno a asignar" title="Escriba un nombre y presione Enter para agregar">
                </div>
            </div>
            <div class="form-group alert alertassign" style="display: none;">
                
            </div>
            <div>
                <label><span id="who-students"></span></label>
                <div id="panel-students">
                <div class="alert alert-info">Para comenzar seleccione el colegio, grado y sección.</div>
                </div>
            </div>
        </div>