<div class="col-sm-12">
    <h2 class="title-modus"><a modus-link  href="<%=Modus.uriRoot%>/" title="Ir al inicio"><i class="fa fa-arrow-left"></i></a>&nbsp;
    <i class="fa fa-graduation-cap"></i>&nbsp;Administrar Coordinadores</h2>
</div>
<div class="clearfix"></div>
<div class="col-sm-9 col-sm-offset-1 message-area"></div>
<div class="clearfix"></div>
<div class="col-sm-9 col-sm-offset-1">
    <div id="coordinatorsapp" class="panel panel-default">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-sm-6">
                        <a class="link-modus add-coordinator">Agregar Coordinador</a>
                    </div>
                    <div class="col-sm-6">
                        <div class="input-group">
                            <input id="search-value" type="text" class="form-control input-edit-coordinator" placeholder="Buscar coordinador" title="Escriba un nombre y presione Enter para buscar">
                            <span class="input-group-btn">
                                <button id="search-btn" class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <div id="main" class="panel-body table-responsive">
                <div class="alert alert-warning alert-dismissible alert-coordinator" role="alert">
                    &nbsp;
                </div>
                <table  class="table table-hover table-condensed">
                    <thead>
                        <tr>
                          <th>Usuario</th>
                          <th>Nombre y Apellido</th>
                          <th></th>
                        </tr>
                    </thead>
                    <tbody id="coordinators-list"></tbody>
                </table>
            </div>
            <div class="panel-footer">
                <div class="status"><b></b></div>
                <div class="rooms-count"><b></b> </div>
            </div>
    </div>
</div>
<div class="modal fade" id="modal-coordinator" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
          <form role="form" id="form-new-coordinator" autocomplete="off">
            <div class="alert alert-warning alert-dismissible alert-modal" role="alert">
                    &nbsp;
            </div>
            <div class="form-group">
              <label for="newUser">Usuario</label>
              <input type="text" class="form-control" name="username" id="newUser" placeholder="" autocomplete="off">
            </div>
            <div class="form-group">
              <label for="newName">Nombre</label>
              <input type="text" class="form-control" name="firstname" id="newName" placeholder="" autocomplete="off">
            </div>
            <div class="form-group">
              <label for="newLastname">Apellido</label>
              <input type="text" class="form-control" name="lastname" id="newLastname" placeholder="" autocomplete="off">
            </div>
            <div class="form-group">
              <label for="newEmail">Email</label>
              <input type="text" class="form-control" name="email" id="newEmail" placeholder="" autocomplete="off">
            </div>
            <div class="form-group">
              <label for="newPassword">Contraseña</label>
              <input type="password" style="display: none;"/>
              <input type="password" class="form-control" name="password" id="newPassword" placeholder="" autocomplete="off">
            </div>
          </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary btn-save">Guardar</button>
      </div>
    </div>
  </div>
</div>