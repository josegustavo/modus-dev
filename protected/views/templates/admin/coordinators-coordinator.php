<td><label><%=username%></label></td>
<td><label><%=name%></label></td>
<td>
    <div class="actions">
        <div class="pull-right">
            <a type="button" rel="button" class="btn btn-secondary btn-xs btn-modus edit-coordinator" data-toggle="tooltip" title="Editar coordinador"><i class="fa fa-pencil"></i></a>&nbsp;
            <a type="button" rel="button" class="btn btn-secondary btn-xs btn-modus" modus-link  href="<%=Modus.uriRoot%>/coordinator/<%=id%>" data-toggle="tooltip" title="Asignar Colegios">
                <i class="fa fa-university"></i>
                <span class="badge"><%=count_schools%><span>
            </a>&nbsp;
            <a type="button" rel="button" class="btn btn-danger btn-sm  btn-modus delete" data-toggle="tooltip" title="Eliminar coordinador"><i class="fa fa-times"></i></a>
        </div>
    </div>
</td>