<nav class="navbar navbar-default navbar-modus" role="navigation">
  <div class="container-fluid">
    <div class="row">
        <ul class="nav navbar-nav">
            <li><a class="add-user">Agregar usuario <i class="fa fa-plus-circle"></i></a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown"><span  id="role-input" class="value">Todos</span>&nbsp;<b class="caret"></b></a>
                <ul class="dropdown-menu role-list">
                    <li><a>Todos</a></li>
                    <li role="student"><a>Alumno</a></li>
                    <li role="instructor"><a>Docente</a></li>
                    <li role="creator"><a>Creador</a></li>
                    <li role="coordinator"><a>Coordinador</a></li>
                    <li role="admin"><a>Administrador</a></li>
                </ul>
            </li>
        </ul>
        <form class="navbar-form navbar-right" role="search">
            <div class="form-group has-feedback">
                <input type="text" class="form-control" id="search-input" placeholder="Buscar...">
                <span class="form-control-feedback" title="Escriba un criterio y presione 'Enter' para buscar">
                    <i class=" fa fa-search fa-2"></i>
                </span>
            </div>
        </form>
    </div>
  </div>
</nav>
