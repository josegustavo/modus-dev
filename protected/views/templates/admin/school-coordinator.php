<td>
    <label><i class="fa fa-graduation-cap"></i>&nbsp;<%=name%></label>
</td>
<td>
  <div class="actions">
    <div class="pull-right">
        <a type="button" rel="button" class="btn btn-secondary btn-sm btn-modus" modus-link href="<%=Modus.uriRoot%>/coordinator/<%=id%>" data-toggle="tooltip" title="Ver colegios asignados"><i class="fa fa-university"></i></a>&nbsp;
        <a type="button" rel="button" class="btn btn-danger btn-sm  btn-modus delete" data-toggle="tooltip" title="Quitar coordinador asignado"><i class="fa fa-times"></i></a>
    </div>
  </div>
</div>
</td>