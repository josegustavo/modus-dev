<div class="col-sm-12">
    <h2 class="title-modus">
        <a modus-link  href="<%=Modus.uriRoot%>/coordinator" title="Ir a Administrar Coordinadores"><i class="fa fa-arrow-left"></i></a>
        <%=" " + name%> 
        <small>Colegios asignados</small>
    </h2>
</div>
<div class="clearfix"></div>
<div class="col-sm-8 col-sm-offset-2 message-area">
    
</div>
<div class="clearfix"></div>
<div class="col-sm-8 col-sm-offset-2">
        <div id="managersSchoolsApp" class="panel panel-default">
                <div class="panel-heading">
                    <input id="select-school" type="text" class="form-control typehead" placeholder="Nombre del colegio a asignar">
                </div>
                <div id="main" class="panel-body">
                    <div class="alert alert-warning alert-dismissible" role="alert">
                        &nbsp;
                    </div>
                    <div id="school-list">

                    </div>
                </div>
                <div class="panel-footer">
                    <div class="status"><b></b></div>
                    <div class="sections-count"><b></b> </div>
                </div>
        </div>
</div>