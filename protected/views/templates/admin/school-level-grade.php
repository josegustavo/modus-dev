<div class="grade-name input-group-sm">
    <h4><%=name%>° Grado</h4>
</div>
<div class="input-room input-group-sm">
    <input class="form-control input-sm input-new-room" placeholder="Nueva sección..." type="text" value="">
</div>
<div class="table-responsive">
<table class="table table-hover table-condensed">
    <tbody class="room-list"></tbody>
</table>
</div>