<div class="col-lg-12 col-md-12">
    <h2 class="title-modus"><a modus-link-back   title="Volver"><i class="fa fa-arrow-left"></i></a>&nbsp;
    <i class="fa fa-user"></i>&nbsp;Administrar Docentes</h2>
</div>
<div class="col-lg-3 col-md-12 instructor-panel-left">

</div>
<div class="col-lg-9 col-md-12" >
    <div class="form-group assign-instructor-form hidden">
        <label class="control-label">Asignar Docente</label>
        <div>
            <input id="assign-instructor-input" type="text" class="form-control typehead assign-instructor-input" placeholder="Nombre del docente a asignar" title="Escriba un nombre y presione Enter para agregar">
        </div>
    </div>
    <div class="form-group alert alertassign" style="display: none;">

    </div>
    <div>
        <label><span id="who-instructors"></span></label>
        <div id="panel-instructors">
        <div class="alert alert-info">Para ver los Docentes seleccione un Colegio y Grado.</div>
        </div>
    </div>
</div>