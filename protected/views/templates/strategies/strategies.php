<div class="page-title">
        <h2 class="title-modus">
            <a modus-link  href="<%=Modus.uriRoot%>/" title="Ir al inicio">
                 <i class="fa fa-arrow-left"></i> 
            </a>&nbsp;<i class="fa fa-cubes"></i> Estrategias
        </h2>
</div>
<ul class="nav nav-tabs general-tabs">
        <li class="view-btn modus-tab" id="view-btn">
                <a class="select-tab">Ver</a>
        </li>
        <li class="add-btn modus-tab" id="add-edit-btn">
                <a class="select-tab"></a>
        </li>
</ul>
<div class="row tab-content">
        <div id="alert-area" class="alert alert-warning" style="display:none;" role="alert">&nbsp;</div>
        <div class="clearfix"></div>
        <div id="view-area" class="tab-pane row"></div>
        <div id="add-edit-area" class="tab-pane"></div>
</div>
