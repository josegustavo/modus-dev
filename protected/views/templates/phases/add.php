<div class="page-title">

</div>
<div class="form-horizontal">
        <div class="form-group">
                <label class="col-sm-3 col-md-2 control-label">Título</label>
                <div class="col-sm-9 col-md-10">
                        <input type="hidden" id="phase-id" value="">
                        <input type="text" class="form-control" id="phase-title" value="">
                </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 col-md-2 control-label">Imagen<br/>predeterminada</label>
                <div class="col-sm-4">
                        <div class="input-group">
                            <div class="img-thumbnail  middle" id="phase-image" title="Click para seleccionar una imagen">
                                <a id="phase-image-status" class="hidden"><br/>Seleccionar<br/><i class="fa fa-plus"></i><br/><br/></a>
                            </div>
                        </div>
                </div>
        </div>
        <div class="form-group">
                <label class="col-sm-3 col-md-2 control-label">
                Descripción
                <small class="help-block"></small>
                </label>
                <div class="col-sm-9 col-md-10">                            
                                <textarea class="form-control mce" id="phase-description" rows="5"></textarea>
                </div>
        </div>
        <div style="min-height: 50px;">
            <div class="form-group strategy select-dropdown">
                <label class="col-sm-3 col-md-2 control-label">Estrategias</label>
                <div class="col-sm-9 col-md-10">
                    <input type="text" placeholder="Buscar..." class="form-control" id="strategy-select" data-toggle="dropdown">
                </div>
            </div>
        </div>
        <div style="min-height: 50px;">
            <div class="form-group tool select-dropdown">
                <label class="col-sm-3 col-md-2 control-label">Herramientas</label>
                <div class="col-sm-9 col-md-10">
                        <input type="text" placeholder="Buscar..." class="form-control" id="tool-select" data-toggle="dropdown">
                </div>
            </div>
        </div>
        <div class="form-group">
                <label class="col-sm-3 col-md-2 control-label">
                Texto a mostrar
                <small class="help-block"></small>
                </label>
                <div class="col-sm-9 col-md-10"><input type="text" value="respuesta" class="form-control" id="phase-text-answer"></div>
        </div>
        <div class="form-group">
                <label class="col-sm-3 col-md-2 control-label">
                Opciones
                </label>
                <div class="col-sm-9 col-md-10">
                    <div class="checkbox"><label><input type="checkbox" checked="checked" id='phase-opt-cananswer'> Permitir publicar respuestas</label></div>
                    <div class="checkbox"><label><input type="checkbox" checked="checked" id='phase-opt-canmark'> Permitir calificación</label></div>
                    <div class="checkbox"><label><input type="checkbox" id='phase-opt-showallothers'> Mostrar el resultado final en las otras fases</label></div>
                </div>
        </div>
        <div style="min-height: 50px;">
            <div class="form-group rule">
                <label class="col-sm-3 col-md-2 control-label">Lista de Cotejo</label>
                <div class="col-sm-9 col-md-10">
                    
                    <div class="input-group">
                        <div class="input-group-btn">
                          <select class="form-control" style="width: 120px" id="rule-score">
                            <option value="0" disabled selected style='display:none;'>Puntaje</option>  
                            <option value="1">1 punto</option>
                            <option value="2">2 puntos</option>
                            <option value="3">3 puntos</option>
                            <option value="4">4 puntos</option>
                            <option value="5">5 puntos</option>
                            <option value="6">6 puntos</option>
                            <option value="7">7 puntos</option>
                            <option value="8">8 puntos</option>
                            <option value="9">9 puntos</option>
                            <option value="10">10 puntos</option>
                            <option value="11">11 puntos</option>
                            <option value="12">12 puntos</option>
                            <option value="13">13 puntos</option>
                            <option value="14">14 puntos</option>
                            <option value="15">15 puntos</option>
                            <option value="16">16 puntos</option>
                            <option value="17">17 puntos</option>
                            <option value="18">18 puntos</option>
                            <option value="19">19 puntos</option>
                            <option value="20">20 puntos</option>
                          </select>
                        </div>
                        <input type="text" class="form-control" id="rule-criterion" placeholder="Escriba un criterio y presione enter para agregar">
                    </div>
                </div>
            </div>
            <div class="form-group"><div class="col-sm-9 col-md-10 col-sm-offset-2"><div class="panel-selected" id="rule-list"></div></div></div>
        </div>
        
        <div class="form-group">
            <div class="col-sm-10 col-sm-offset-2">
                <ul class="list-inline">
                    <li>
                        <a type="button" rel="button" class="btn btn-secondary  btn-save">Guardar&nbsp;<i class="fa fa-chevron-circle-right"></i></a>
                    </li>
                    <li>
                        <a type="button" rel="button" class="btn btn-danger  btn-cancel">Cancelar&nbsp;<i class="fa fa-chevron-circle-right"></i></a>
                    </li>
                </ul>
            </div>
        </div>
</div>