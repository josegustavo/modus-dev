<div class="panel panel-modus">
        <div class="col-xs-3 panel-left-modus">
                <div class="panel-heading"></div>
                <div class="panel-body">
                        <div class="user-img"><a><img class="img-thumbnail" src="<%=model.image_cdn?model.image_cdn:(Modus.uriCDN+'/img/default_project.png')%>" alt=""/></a></div>
                        <div class="grade-heading">
                            <div><span><%=model.school_name%></span></div>
                            <div><small><%=model.level_name%></small></div>
                            <div><small><%=(model.rooms_names)?(" " + model.rooms_names):""%></small></div>
                        </div>
                </div>
        </div>
        <div class="col-xs-9 panel-right-modus">    						
                <div class="panel-heading"></div>
                <div class="panel-body">
                        <div class="modus-title"><a modus-link href="<%=Modus.uriRoot%>/project/view/<%=model.id%>/<%=model.slug%>" title="<%=model.title%>"><%=model.title%></a></div>
                        <div class="modus-subtitle">
                            <div class="managers">
                                <i class="fa fa-user"></i><%if(model.managers_names){%> Administrado por 
                                    <span title="<%=model.managers_names%>"><%=model.managers_names%> </span>
                                    <%}else{%> Creado por 
                                    <span title="<%=model.creator_name%>"><%=model.creator_name%> </span>
                                    <%}%>
                            </div>
                            <ul class='list-inline'>    
                                <li><span title="<%=model.modified%>"><i class="fa fa-calendar"></i><span id="modified-span"> Modificado hace <%=model.modified_ago%></span></span></li>
                                <li><span title="<%=model.state_date%>"><i class="fa fa-clock-o"></i><span id="starts-ends-span"> <%=model.state==0?" Inicia en ":(model.state==1?" Termina en ":" Terminó hace ")%> <%=model.state_ago%></span></span></li>
                            </ul>                                                                            
                        </div>
                        <div class="modus-content"><%=$(model.description).text()%></div>
                        <div class="modus-footer">
                            <div class="pull-right">
                                <a type="button" rel="button" class="btn btn-secondary btn-modus" modus-link href="<%=Modus.uriRoot%>/project/view/<%=model.id%>/<%=model.slug%>" title="Ver proyecto"><i class="fa fa-eye"></i></a>&nbsp;
                                <%if(model.is_owner){%>
                                <?php if(Yii::app()->user->checkAccess("viewProjectLog")){ ?>
                                    <a type="button" rel="button" class="btn btn-secondary btn-modus" modus-link href="<%=Modus.uriRoot%>/project/view/<%=model.id%>/<%=model.slug%>/logs" title="Ver log"><i class="fa fa-list"></i></a>&nbsp;
                                <?php } ?>
                                    <a type="button" rel="button" class="btn btn-secondary btn-modus" modus-link href="<%=Modus.uriRoot%>/project/edit/<%=model.id%>/<%=model.slug%>"><i class="fa fa-pencil"></i></a>&nbsp;
                                    <a type="button" rel="button" class="btn btn-secondary btn-modus" href="<%=Modus.uriRoot%>/projects/export/<%=model.id%>" title="Exportar"><i class="fa fa-file-archive-o"></i></a>&nbsp;
                                <?php if(Yii::app()->user->checkAccess("createProjects")){ ?>
                                    <a type="button" rel="button" class="btn btn-secondary btn-modus action-duplicate" title="Duplicar proyecto"><i class="fa fa-files-o"></i></a>&nbsp;
                                <?php } ?>
                                <% } %>
                                <%if(model.is_manager){%>
                                    <a type="button" rel="button" class="btn btn-secondary btn-modus" modus-link href="<%=Modus.uriRoot%>/project/edit/<%=model.id+'/'+model.slug%>/assign-teams" title="Modificar grupos"><i class="fa fa-users"></i></a>&nbsp;
                                <% } %>
                                <%if(model.is_owner){%>
                                    <a type="button" rel="button" class="btn btn-danger btn-xs btn-modus action-delete" title="Eliminar proyecto"><i class="fa fa-times"></i></a>
                                <% } %>
                                <%if(!model.is_owner && !model.is_manager){%>
                                    <a type="button" rel="button" class="btn-modus-link see-more" modus-link href="<%=Modus.uriRoot%>/project/view/<%=model.id%>/<%=model.slug%>">Ver más</a>&nbsp;
                                <%}%>
                            </div>
                        </div>
                </div>
        </div>
</div>