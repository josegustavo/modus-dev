<?php

class MigrationController extends Controller
{
    var $microtime = 0;
    var $database;
    public function actionIndex()
    {
        $this->microtime = microtime(true);
        
        defined("DS") || define("DS", DIRECTORY_SEPARATOR);
        set_time_limit(0);
        header("Content-Type: text");
        $this->output("Iniciando migración...\n");
        
        $conn_string_host = preg_match("/host=([^;]*)/", Yii::app()->getDb()->connectionString, $matches_host);
        $conn_string_dbname = preg_match("/dbname=([^;]*)/", Yii::app()->getDb()->connectionString, $matches_dbnme);
        $db_host = $matches_host[1];
        $db_name = $matches_dbnme[1];
        $username = Yii::app()->getDb()->username;
        $password = Yii::app()->getDb()->password;
        $this->database = new Medoo(
                array(
                    'database_type' => 'mysql',
                    'database_name' => $db_name,
                    'server' => $db_host,
                    'username' => $username,
                    'password' => $password,
                )
        );
        
        if(@rename("index.php", "index.php.del"))
        {
            $this->output("Cambió nombre de archivo index.php => index.php.del");
        }
        if(@rename("index_maintenance.php", "index.php"))
        {
            $this->output("Cambió nombre de archivo index_maintenance.php => index.php");
        }
        
        $this->MigrateSign();
        $this->MigrateAttachments();
        $this->MigrateImages();
        
        $this->output("end.");
        
        if(@rename("index.php", "index_maintenance.php"))
        {
            $this->output("Cambió nombre de archivo index.php => index_maintenance.php");
        }
        if(@rename("index.php.del", "index.php"))
        {
            $this->output("Cambió nombre de archivo index.php.del => index.php");
        }
        
        
        Yii::app()->end(0,TRUE);
    }
    
    private function time()
    {
        return sprintf("%.2f ", (microtime(true) - $this->microtime) );
    }
    
    private function output($line)
    {
        echo $this->time();
        if(is_string($line))
        {
            echo  $line;
        }else if(is_array($line))
        {
            print_r($line);
        }
        else
        {
            echo var_dump($line);
        }
        echo "\n";
        ob_flush();flush();
    }
    
    private function outQuery($query, $comment = "")
    {
        if($comment)
        {
            $this->output($comment);
        }
        if($result = $this->database->query($query))
        {
            $this->output($result);
            return $result;
        }
        else
        {
            if(($error = $this->database->error()) && isset($error[2]))
            {
                $this->output("Error: " . $error[2]."\n");
            }
            else
            {
                $this->output("Error:");
                $this->output($this->database->error());
            }
        }
        return false;
    }

    
    private function MigrateAttachments()
    {
        $basePath = Yii::app()->basePath.DS."..".DS; 
        $folder_file = $basePath."file".DS;
        $migrate = array();
        $no_migrate = array();
        //Activar actualización en cascada si se cambia el pk de attachment
        
        //Crear la tabla file
        $this->outQuery(
                 "CREATE TABLE IF NOT EXISTS `file` (`id`  int NOT NULL AUTO_INCREMENT ,`md5`  varchar(255) NOT NULL ,`original_name`  varchar(255) NOT NULL ,`alternative_name`  varchar(255) NULL, `creator_id`  int NOT NULL ,`created`  int NOT NULL ,`properties`  text NULL , PRIMARY KEY (`id`), FOREIGN KEY (`creator_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE) ENGINE=InnoDB DEFAULT CHARSET=utf8;",
                 "Creando la tabla file, si aún no existe");

        $this->output("Migrando attachments");
        $attachments_db = $this->database->select("attachment","*");
        if(!$attachments_db || (count($attachments_db)==0))
        {
            $this->output("No se encontraron attachments");
            return FALSE;
        }
        $this->outQuery("ALTER TABLE `answer_attachment` DROP FOREIGN KEY `answer_attachment_ibfk_1`;
                ALTER TABLE `phase_attachment` DROP FOREIGN KEY `phase_attachment_ibfk_2`;
                ALTER TABLE `answer_attachment` ADD CONSTRAINT `answer_attachment_ibfk_1` FOREIGN KEY (`attachment_id`) REFERENCES `attachment` (`filename`) ON DELETE CASCADE ON UPDATE CASCADE;
                ALTER TABLE `phase_attachment` ADD CONSTRAINT `phase_attachment_ibfk_2` FOREIGN KEY (`attachment_id`) REFERENCES `attachment` (`filename`) ON DELETE CASCADE ON UPDATE CASCADE;
                "
         , "Modificando llaves de las tablas answer_attachment, phase_attachment para que acepte update descendente ");
        

        $attachments = array();
        foreach($attachments_db as $attachment)
        {
                if( !is_numeric($attachment['filename']) )
                {
                        $attachments []= $attachment;
                }
        }

        $total = count($attachments);
        $this->output("Se encontraron $total attachments");
        $success = 0;
        $finfo = finfo_open(FILEINFO_MIME_TYPE); 
        foreach ($attachments as $attachment)
        {
            $filename = $attachment['filename'];
            $original_name = $attachment['originalname'];
            $file = $this->database->get("file", array("id","md5"), array('alternative_name' => $filename) );
            $file_id = isset($file['id'])?$file['id']:FALSE;
            if(!$file_id)
            {
                $creator_id = $attachment['creator_id'];
                $dtime = DateTime::createFromFormat("Y-m-d H:i:s", $attachment['created']);
                $created = $dtime->getTimestamp();
                if($filepath = $this->findPathFileByName($filename, "file"))
                {
                    $md5 = md5_file($filepath);
                    if(!file_exists($folder_file.$md5)) copy($filepath, $folder_file.$md5);
                    $properties = array();
                    $properties['size'] = filesize($folder_file.$md5);
                    if($imagesize = @getimagesize($folder_file.$md5))
                    {
                        //$properties['mime'] = $imagesize['mime'];
                        $properties['width'] = $imagesize[0];
                        $properties['height'] = $imagesize[1];
                        $properties['type'] = $imagesize[2];
                    }
                    $properties['mime'] = finfo_file($finfo, $folder_file.$md5); 
                    $file_id = $this->database->insert("file", array(
                        'md5' => $md5,
                        'original_name' => $original_name,
                        'alternative_name' => $filename,
                        'creator_id' => $creator_id,
                        'created' => $created,
                        'properties' => json_encode($properties)
                    ));
                }
                else
                {
                    $file = $this->database->get("file", "*", array('original_name' => $original_name) );
                    if($file)
                    {
                        $md5 = $file['md5'];
                        $file_id = $this->database->insert("file", array(
                            'md5' => $md5,
                            'original_name' => $file['original_name'],
                            'alternative_name' => $file['alternative_name'],
                            'creator_id' => $file['creator_id'],
                            'created' => $file['created'],
                            'properties' => $file['properties']
                        ));
                    }
                    else
                    {
                        $md5 = "99999999999999999999999999999999";
                        $file_id = $this->database->insert("file", array(
                            'md5' => $md5,
                            'original_name' => $original_name,
                            'alternative_name' => $filename,
                            'creator_id' => $creator_id,
                            'created' => $created,
                            'properties' => "{}"
                        ));
                    }
                }
            }
            else
            {
                $md5 = $file['md5'];
            }
            if($file_id)
            {
                $this->database->update("attachment", array('filename' => $file_id), array('filename' => $filename));
                $percent = round(100*($success/$total), 2);
                $this->output("$percent% Archivo $filename migrado con el id $file_id, md5: $md5");
                $migrate[$filename] = intval($file_id);
                $success++;
            }
            else
            {
                $this->output("Error: No se encontró el archivo $filename");
                $no_migrate []= $filename;
            }
        }
        
        if(!$this->outQuery("ALTER TABLE `answer_attachment` DROP FOREIGN KEY `answer_attachment_ibfk_1`; "
            . "ALTER TABLE `phase_attachment` DROP FOREIGN KEY `phase_attachment_ibfk_2`;"
            . "ALTER TABLE `reanswer_attachment` DROP FOREIGN KEY `reanswer_attachment_ibfk_2`;"
            , "Eliminando llaves de las tablas answer_attachment, phase_attachment, reanswer_attachment"))
        {
            $this->outQuery("ALTER TABLE `answer_attachment` DROP FOREIGN KEY `answer_attachment_file`; "
            . "ALTER TABLE `phase_attachment` DROP FOREIGN KEY `phase_attachment_file`;"
            . "ALTER TABLE `reanswer_attachment` DROP FOREIGN KEY `reanswer_attachment_file`;"
            , "Eliminando llaves de las tablas answer_attachment, phase_attachment, reanswer_attachment");
        }
        
        $this->output("Cambiando columnas de tablas attachments");
        $this->outQuery("ALTER TABLE `answer_attachment` CHANGE COLUMN `attachment_id` `file_id` int(11) NOT NULL AFTER `answer_id`;");
        $this->outQuery("ALTER TABLE `reanswer_attachment` CHANGE COLUMN `attachment_id` `file_id` int(11) NOT NULL AFTER `reanswer_id`;");
        $this->outQuery("ALTER TABLE `phase_attachment` CHANGE COLUMN `attachment_id` `file_id` int(11) NOT NULL AFTER `phase_id`;");

        
        
        $this->output("Agregando llaves a las tablas attachments");
        $this->outQuery("ALTER TABLE `answer_attachment` ADD CONSTRAINT `answer_attachment_file` FOREIGN KEY (`file_id`) REFERENCES `file` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;");
        $this->outQuery("ALTER TABLE `reanswer_attachment` ADD CONSTRAINT `reanswer_attachment_file` FOREIGN KEY (`file_id`) REFERENCES `file` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;");
        $this->outQuery("ALTER TABLE `phase_attachment` ADD CONSTRAINT `phase_attachment_file` FOREIGN KEY (`file_id`) REFERENCES `file` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;");
        
        $this->output("Se prepararon para la migración un total de $total adjuntos");
        $this->output("Se migraron correctamente $success adjuntos");
    }
    
    private function MigrateImages()
    {
        $tables = array('strategy', 'tool', 'default_phase', 'phase','template_phase', 'project');
        $result = array();
        foreach ($tables as $table)
        {
            $this->output("Se procede a migrar la tabla $table");
            $result []= $this->MigrateByTable($table);
        }
        
        $descriptions = array('project' => 'description', 'phase' => 'description' , 'answer' => 'answer', 'reanswer' => 'reanswer','tool' => 'description', 'strategy' => 'description');
        foreach ($descriptions as $table => $column)
        {
            $result []= $this->MigrateDescriptions($table, $column);
        }
        
        return $result;
    }
    
    private function MigrateByTable($table)
    {
        $basePath = Yii::app()->basePath.DS."..".DS; 
        $folder_file = $basePath."file".DS;
        $time = time();
        $migrate = array();
        
        $projects_db = $this->database->select($table,"*");
        if(!$projects_db || (count($projects_db)==0))
        {
            $this->output("No existe nada que migrar en la tabla $table \n");
            return FALSE;
        }
        $projects = array();
        foreach ($projects_db as $p)
        {
            if(isset($p['image']))
            {
                $filename = $p['image'];
                if(strlen($filename)>0 && !is_numeric($filename))
                {
                    $projects[$filename] = isset($p['creator_id'])?$p['creator_id']:4;//admin id
                }
            }
        }
        
        $total = count($projects);
        if(!$total)
        {
            $this->output("No existe nada que migrar en la tabla $table \n");
            
        }
        else
        {
            $this->output("Se encontraron un total de $total elementos de la tabla $table ");
            $success = 0;
            $finfo = finfo_open(FILEINFO_MIME_TYPE); 
            foreach ($projects as $filename => $any_id)
            {
                $file_id = $this->database->get("file", "id", array('alternative_name' => $filename) );
                if(!$file_id)
                {
                    if($filepath = $this->findPathFileByName($filename, "img"))
                    {
                        $md5 = md5_file($filepath);
                        $created = filemtime($filepath);
                        if(!file_exists($folder_file.$md5)) copy($filepath, $folder_file.$md5);

                        $properties = array();
                        $properties['size'] = filesize($folder_file.$md5);
                        if($imagesize = @getimagesize($folder_file.$md5))
                        {
                            //$properties['mime'] = $imagesize['mime'];
                            $properties['width'] = $imagesize[0];
                            $properties['height'] = $imagesize[1];
                            $properties['type'] = $imagesize[2];
                        }
                        $properties['mime'] = finfo_file($finfo, $folder_file.$md5); 
                        $file_id = $this->database->insert("file", array(
                            'md5' => $md5,
                            'original_name' => $filename,
                            'alternative_name' => $filename,
                            'creator_id' => $any_id,
                            'created' => $created,
                            'properties' => json_encode($properties)
                        ));
                    }
                }
                if($file_id>0)
                {
                    $updates = $this->database->update($table, array('image' => $file_id), array('image' => $filename));
                    $migrate[$filename] = intval($file_id);
                    $this->output("Se migró el archivo $filename con id $file_id");
                    $success++;
                }
                else
                {
                    $updates = $this->database->update($table, array('image' => NULL), array('image' => $filename));
                    $this->output("No se encontró el archivo $filename, se colocó null en la imagen");
                }

            }
        }
        $this->outQuery("ALTER TABLE `".$table."` CHANGE COLUMN `image` `image_id` int(11) NULL DEFAULT NULL;"
                . "UPDATE " . $table . " SET image_id = NULL WHERE image_id = 0;"
                . "ALTER TABLE `".$table."` ADD CONSTRAINT `".$table."_image` FOREIGN KEY (`image_id`) REFERENCES `file` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;",
                "Se intenta modificar la tabla $table", "Se intenta modificar la estructura de la tabla $table");
        
        $this->output("Se migró " . count($migrate) . "/$total de la tabla $table");
        
    }
    
    private function MigrateDescriptions($table, $column)
    {
        $basePath = Yii::app()->basePath.DS."..".DS; 
        $folder_file = $basePath."file".DS;
        $migrate = array();
        
        $projects = $this->database->select($table, array($column, 'id'));
        if(!$projects || (count($projects)==0))
        {
            $this->output("No existe descripción que migrar en la tabla $table");
            return FALSE;
        }
        $total = 0;
        $success = 0;
        $this->output("Migrando descripcion de la tabla $table");
        $finfo = finfo_open(FILEINFO_MIME_TYPE); 
        foreach ($projects as $project)
        {
            $project_id = $project['id'];
            $creator_id = isset($project['creator_id'])?$project['creator_id']:4;//admin id
            $description = $project[$column];
            if(strlen($description)>0)
            {
                $found = FALSE;
                $match = array();
                $img_projects_patt = '/src\=\"(\/img\/projects\/(\d+_\d+_\d+_.\..{3,4}))\"/';
                preg_match_all($img_projects_patt, $description, $match);
                
                if($match[1] && $match[2])
                {
                    $srcs = $match[1];
                    $names = $match[2];
                    $total += count($names);
                    foreach ($names as $i => $filename)
                    {
                        $file = $this->database->get("file", "*", array('OR' => array('original_name' => $filename, 'alternative_name' => $filename) ) );
                        if(!$file)
                        {
                            if($filepath = $this->findPathFileByName($filename, "img"))
                            {
                                $md5 = md5_file($filepath);
                                $created = filemtime($filepath);
                                if(!file_exists($folder_file.$md5)) copy($filepath, $folder_file.$md5);
                                $properties = array();
                                $properties['size'] = filesize($folder_file.$md5);
                                if($imagesize = @getimagesize($folder_file.$md5))
                                {
                                    //$properties['mime'] = $imagesize['mime'];
                                    $properties['width'] = $imagesize[0];
                                    $properties['height'] = $imagesize[1];
                                    $properties['type'] = $imagesize[2];
                                }
                                $properties['mime'] = finfo_file($finfo, $folder_file.$md5); 
                                $file_id = $this->database->insert("file", array(
                                    'md5' => $md5,
                                    'original_name' => $filename,
                                    'alternative_name' => $filename,
                                    'creator_id' => $creator_id,
                                    'created' => $created,
                                    'properties' => json_encode($properties)
                                ));
                                $file = array('id' => $file_id,'md5' => $md5, 'original_name' => $filename);
                            }
                        }
                        if($file>0)
                        {
                            $new_name = "/uploads/" . $file['md5'] . "/" . $file['original_name'];
                            if(isset($srcs[$i]))
                            {
                                $description = str_replace($srcs[$i],$new_name,$description);
                                $this->output("Se migró la imagen " . $names[$i] . " con el nuevo ID " . $file['id']);
                                $found = TRUE;
                                $success++;
                            }
                        }
                        else
                        {
                            $this->output("No se encontró el archivo $filename");
                        }
                    }
                }
                if($found)
                {
                    $this->database->update($table, array($column => $description), array('id' => $project_id));
                }
            }
        }
        $this->output("Se migró un total de " . count($migrate) . "/$total en las descripciones de la tabla $table");
    }


    private function findPathFileByName($name, $type="file")
    {
        $type = $type=="file"?"file":"img";
        $basePath = Yii::app()->basePath.DS."..".DS; 
        $folder_file = $basePath."file".DS;
        $folder_img = $basePath."img".DS;
        //Ordenado por mayor probabilidad
        $files_folder = $type=="file"?array(
            $folder_file."answers".DS,
            $folder_file."phases".DS,
        ):array(
            $folder_img."projects".DS,
            $folder_img."answers".DS,
            $folder_img."phases".DS,
            $folder_img."tools".DS,
            $folder_img."strategies".DS
        );
        
        foreach ($files_folder as $folder)
        {
            if(file_exists($folder . $name))
            {
                return $folder.$name;
            }
        }
        return false;;
    }
    
    private function MigrateSign()
    {
        $db = $this->database;
        $time = time();
        $year = $this->database->get("year", array('id','ord'), array('active' => 1));
        if(!$year)
        {
            $this->output("tabla year no existe o no se encontró ciclo activo. se procede a crear");
            $this->outQuery("DROP TABLE IF EXISTS `year`;
                CREATE TABLE `year` (
                  `id` int(11) NOT NULL,
                  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'Nombre a mostrar para el periodo',
                  `ord` int(11) NOT NULL COMMENT 'El orden en que se crearon los periodos',
                  `active` int(1) NOT NULL DEFAULT 0 COMMENT 'Indica si el año es el que se encuentra actualmente activo o no.',
                  PRIMARY KEY (`id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tabla para establecer los periodos que pueden ser anuales o entre un rango de fechas';"
            );
            $year_id = mt_rand(100000,999999);
            $this->database->insert("year", array('id' => $year_id, 'name' => '2014', 'ord' => 1, 'active' => 1));
            $year = array('id' => $year_id);
        }
        $year_id = $year['id'];
        $this->output("El id de year utilizado es: " . $year_id);
        
        $grades = $this->database->get("grade", '*');
        if($grades)
        {
            $this->output("Se procede a migrar la tabla grade");
            $this->outQuery("ALTER TABLE `grade` DROP FOREIGN KEY `grade_ibfk_1`;
                ALTER TABLE `grade`
                CHANGE COLUMN `level_id` `level_num`  int(11) NOT NULL AFTER `id`,
                ADD COLUMN `level_name`  varchar(255) NOT NULL AFTER `id`,
                MODIFY COLUMN `name` varchar(255) NOT NULL AFTER `level_num`,
                ADD COLUMN `active`  int(1) NOT NULL DEFAULT 1 AFTER `name`,
                DROP INDEX `level_id`;"
            );
            $this->output("Migración de level a grade");
            $levels = $this->database->select("level", "*");
            foreach ($levels as $level)
            {
                $this->database->update("grade", array('level_name' => $level['name']), array('level_num' => $level['id']));
            }
            $this->outQuery("DROP TABLE `level`; RENAME TABLE `grade` TO `level`;");
        }
        $sections = $this->database->get("section", '*');
        if($sections)
        {
            $this->output("Se procede a migrar la tabla section");
            $this->outQuery("ALTER TABLE `section` DROP FOREIGN KEY `section_ibfk_1`;
                ALTER TABLE `section` CHANGE COLUMN `grade_id` `level_id`  int(11) NOT NULL AFTER `id`, ADD COLUMN `year_id`  int(11) NOT NULL AFTER `name`, DROP INDEX `grade_id`;
                ALTER TABLE `section` ADD CONSTRAINT `room_level` FOREIGN KEY (`level_id`) REFERENCES `level` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;"
            );
            $this->output("Asignando el presente year a todas las secciones");
            $this->database->update("section", array('year_id' => $year_id));
            $this->output("Agregando la llave foranea de secciones");
            $this->outQuery("ALTER TABLE `section` ADD CONSTRAINT `room_year` FOREIGN KEY (`year_id`) REFERENCES `year` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;");
            $this->output("Cambiando el nombre de la tabla a room");
            $this->outQuery("RENAME TABLE `section` TO `room`;");
        }
        
        if($this->outQuery("RENAME TABLE `section_user` TO `sign`;", "Intentando el cambio del nombre de la tabla section_user por sign"))
        {
            $this->output("Se cambió el nombre de la tabla, se procede a modificar estructura");
            $this->outQuery("ALTER TABLE `sign` DROP FOREIGN KEY `sign_ibfk_1`;
                ALTER TABLE `sign` DROP FOREIGN KEY `sign_ibfk_2`;
                ALTER TABLE `sign`
                CHANGE COLUMN `section_id` `room_id`  int(11) NOT NULL FIRST ,
                DROP PRIMARY KEY,
                DROP INDEX `user_id`;
                ALTER TABLE `sign`
                ADD COLUMN `year_id`  int(11) NOT NULL AFTER `room_id`,
                ADD COLUMN `created`  int(11) NOT NULL AFTER `year_id`,
                ADD COLUMN `modified`  int(11) NOT NULL AFTER `created`,
                ADD COLUMN `active`  int(1) NULL DEFAULT 1 AFTER `modified`;"
            , "Modificando estructura de tabla sign");
            $this->output("Llenando los campos vacios de la tabla sign");
            $this->database->update("sign",  array('created' => $time, 'modified' => $time, 'active' => 1, 'year_id' => $year_id));
            $this->outQuery("
                ALTER TABLE `sign` ADD PRIMARY KEY (`user_id`, `room_id`);
                ALTER TABLE `sign` ADD CONSTRAINT `sign_room` FOREIGN KEY (`room_id`) REFERENCES `room` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE;
                ALTER TABLE `sign` ADD CONSTRAINT `sign_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE;
                ALTER TABLE `sign` ADD CONSTRAINT `sign_year` FOREIGN KEY (`year_id`) REFERENCES `year` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE;
            ", "Agregando las llaves foráneas y las restricciones");    
        }
        
        if($this->outQuery("RENAME TABLE `project_section` TO `project_room`;", "Intentando el cambio de la tabla project_section por project_room"))
        {
            $this->outQuery("ALTER TABLE `project_room` DROP FOREIGN KEY `project_room_ibfk_1`;
                ALTER TABLE `project_room` DROP FOREIGN KEY `project_room_ibfk_2`;
                ALTER TABLE `project_room` DROP INDEX `section_id`;
                ALTER TABLE `project_room` CHANGE COLUMN `section_id` `room_id`  int(11) NOT NULL AFTER `project_id`;
                ALTER TABLE `project_room` ADD CONSTRAINT `project_room_project` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
                ALTER TABLE `project_room` ADD CONSTRAINT `project_room_room` FOREIGN KEY (`room_id`) REFERENCES `room` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;"
            , "Modificando llaves y restricciones de la tabla project_room");
        }
        
        if($this->outQuery("RENAME TABLE `group` TO `team`;", "Intentando el cambio de la tabla group a team"))
        {
            $this->outQuery("ALTER TABLE `team` DROP FOREIGN KEY `team_ibfk_1`;
                ALTER TABLE `team` DROP FOREIGN KEY `team_ibfk_2`;
                ALTER TABLE `team` CHANGE COLUMN `section_id` `room_id`  int(11) NULL DEFAULT NULL AFTER `project_id`;
                ALTER TABLE `team` ADD CONSTRAINT `team_project` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
                ALTER TABLE `team` ADD CONSTRAINT `team_room` FOREIGN KEY (`room_id`) REFERENCES `room` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;"
            , "Cambiando llaves y restricciones a la tabla team");
        }
        
        if($this->outQuery("RENAME TABLE `group_user` TO `team_user`;", "Intentando el cambio de la tabla group_user por team_user"))
        {
            $this->outQuery("
                ALTER TABLE `team_user` DROP FOREIGN KEY `team_user_ibfk_2`;
                ALTER TABLE `team_user` DROP FOREIGN KEY `team_user_ibfk_3`;
                ALTER TABLE `team_user` CHANGE COLUMN `group_id` `team_id`  int(11) NOT NULL FIRST;
                ALTER TABLE `team_user` ADD CONSTRAINT `team_user_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
                ALTER TABLE `team_user` ADD CONSTRAINT `team_user_team` FOREIGN KEY (`team_id`) REFERENCES `team` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
                ALTER TABLE `team_user`
                ADD COLUMN `created`  int(11) NOT NULL DEFAULT 0 AFTER `user_id`,
                ADD COLUMN `modified`  int(11) NOT NULL DEFAULT 0 AFTER `created`;
            "
            , "Cambiando llaves y restricciones a la tabla team_user");
        }
        
        $this->outQuery("RENAME TABLE `type` TO `project_type`;", "Intentando el cambio de la tabla type a project_type");
        
        if($this->outQuery("
            ALTER TABLE `project` DROP FOREIGN KEY `project_ibfk_1`;
            ALTER TABLE `project` DROP FOREIGN KEY `project_ibfk_2`;
            ALTER TABLE `project` DROP FOREIGN KEY `project_ibfk_3`;
            ALTER TABLE `project` DROP FOREIGN KEY `project_ibfk_4`;
            ALTER TABLE `project` DROP COLUMN `deleted`,
            CHANGE COLUMN `grade_id` `level_id`  int(11) NOT NULL AFTER `id`,
            ADD COLUMN `active`  int(1) NOT NULL DEFAULT 1 AFTER `type_id`, 
            ADD COLUMN `year_id`  int(11) NOT NULL AFTER `active`, 
            DROP INDEX `slug`;
        ", "Intentando la modificación de las llaves foráneas de la tabla project"))
        {
            $this->outQuery("ALTER TABLE `project`
                MODIFY COLUMN `level_id`  int(11) NULL AFTER `id`,
                MODIFY COLUMN `school_id`  int(11) NULL AFTER `level_id`;",
                "Modificando para que pueda aceptar school y level nulos");
            $this->output("Llenando columnas adicionales (active,year_id) de la tabla project");
            $this->database->update("project", array('active' => 1, 'year_id' => $year_id));
            $this->outQuery("
                    ALTER TABLE `project` ADD CONSTRAINT `project_type` FOREIGN KEY (`type_id`) REFERENCES `project_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
                    ALTER TABLE `project` ADD CONSTRAINT `project_creator` FOREIGN KEY (`creator_id`) REFERENCES `user` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE;
                    ALTER TABLE `project` ADD CONSTRAINT `project_school` FOREIGN KEY (`school_id`) REFERENCES `school` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
                    ALTER TABLE `project` ADD CONSTRAINT `project_level` FOREIGN KEY (`level_id`) REFERENCES `level` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
                    ALTER TABLE `project` ADD CONSTRAINT `project_year` FOREIGN KEY (`year_id`) REFERENCES `year` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE;
            ", "Agregando nuevas restricciones de la tabla project");
        }
        
        
        if($this->outQuery("ALTER TABLE `answer` DROP FOREIGN KEY `answer_ibfk_1`;
            ALTER TABLE `answer` DROP FOREIGN KEY `answer_ibfk_2`;
            ALTER TABLE `answer` DROP FOREIGN KEY `answer_ibfk_3`;", "Intentando quitar la llave foranea answer_ibfk_1,answer_ibfk_2,answer_ibfk_3 de la tabla answer"))
        {
            $this->outQuery("ALTER TABLE `answer`
                    CHANGE COLUMN `group_id` `team_id`  int(11) NOT NULL AFTER `phase_id`,
                    CHANGE COLUMN `user_id` `author_id`  int(11) NOT NULL AFTER `team_id`,
                    CHANGE COLUMN `is_last_answer` `is_last`  int(1) NOT NULL DEFAULT 0 AFTER `modified`,
                    CHANGE COLUMN `state` `is_published`  int(1) NOT NULL DEFAULT 1 COMMENT '0:draft, 1:published' AFTER `is_last`;

                    ALTER TABLE `answer` ADD CONSTRAINT `answer_author` FOREIGN KEY (`author_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
                    ALTER TABLE `answer` ADD CONSTRAINT `answer_phase` FOREIGN KEY (`phase_id`) REFERENCES `phase` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
                    ALTER TABLE `answer` ADD CONSTRAINT `answer_team` FOREIGN KEY (`team_id`) REFERENCES `team` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
            ", "Modificando la tabla answer");
        }
        
        $this->outQuery("RENAME TABLE `default_phase` TO `template_phase`;", "Intentando el cambio de la tabla default_phase a template_phase");
        
        if($this->outQuery("RENAME TABLE `default_rule` TO `template_rule`;", "Intentando el cambio de la tabla default_rule a template_rule"))
        {
            $this->outQuery("
                ALTER TABLE `template_rule` DROP FOREIGN KEY `template_rule_ibfk_1`;
                ALTER TABLE `template_rule`
                CHANGE COLUMN `default_phase_id` `template_phase_id`  int(11) NOT NULL AFTER `id`;
                ALTER TABLE `template_rule` ADD CONSTRAINT `template_rule_phase` FOREIGN KEY (`template_phase_id`) REFERENCES `template_phase` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;"
            , "Cambiando la tabla template_rule");
        }
        
        if($this->outQuery("RENAME TABLE `default_phase_tool` TO `template_phase_tool`;", "Intentando el cambio de la tabla default_phase_tool a template_phase_tool"))
        {
            $this->outQuery("
                ALTER TABLE `template_phase_tool` DROP FOREIGN KEY `template_phase_tool_ibfk_1`;
                ALTER TABLE `template_phase_tool` DROP FOREIGN KEY `template_phase_tool_ibfk_2`;
                ALTER TABLE `template_phase_tool`
                CHANGE COLUMN `default_phase_id` `template_phase_id`  int(11) NOT NULL FIRST ;
                ALTER TABLE `template_phase_tool` ADD CONSTRAINT `template_phase_tool_phase` FOREIGN KEY (`template_phase_id`) REFERENCES `template_phase` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
                ALTER TABLE `template_phase_tool` ADD CONSTRAINT `template_phase_tool_tool` FOREIGN KEY (`tool_id`) REFERENCES `tool` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
            ", "Modificando la tabla template_phase_toom");
        }
        
        if($this->outQuery("RENAME TABLE `default_phase_strategy` TO `template_phase_strategy`;", "Intentando el cambio de la tabla default_phase_strategy a template_phase_strategy"))
        {
            $this->outQuery("
                ALTER TABLE `template_phase_strategy` DROP FOREIGN KEY `template_phase_strategy_ibfk_1`;
                ALTER TABLE `template_phase_strategy` DROP FOREIGN KEY `template_phase_strategy_ibfk_2`;
                ALTER TABLE `template_phase_strategy`
                CHANGE COLUMN `default_phase_id` `template_phase_id`  int(11) NOT NULL FIRST ;
                ALTER TABLE `template_phase_strategy` ADD CONSTRAINT `template_phase_strategy_phase` FOREIGN KEY (`template_phase_id`) REFERENCES `template_phase` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
                ALTER TABLE `template_phase_strategy` ADD CONSTRAINT `template_phase_strategy_tool` FOREIGN KEY (`strategy_id`) REFERENCES `strategy` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
            ", "Modificando la tabla template_phase_strategy");
        }
        

        if($this->outQuery("RENAME TABLE `final_qualification` TO `final_mark`;", "Intentando el cambio de la tabla final_qualification a final_mark"))
        {
            $this->outQuery("
                ALTER TABLE `final_mark` DROP FOREIGN KEY `final_mark_ibfk_1`;
                ALTER TABLE `final_mark` DROP FOREIGN KEY `final_mark_ibfk_2`;
                ALTER TABLE `final_mark`
                CHANGE COLUMN `group_id` `team_id`  int(11) NOT NULL AFTER `phase_id`,
                CHANGE COLUMN `mark_student` `mark_member`  int(11) NOT NULL DEFAULT 0 AFTER `mark`,
                CHANGE COLUMN `mark_admin` `mark_manager`  int(11) NOT NULL DEFAULT 0 AFTER `mark_member`;
                ALTER TABLE `final_mark` ADD CONSTRAINT `final_mark_team` FOREIGN KEY (`team_id`) REFERENCES `team` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
                ALTER TABLE `final_mark` ADD CONSTRAINT `final_mark_phase` FOREIGN KEY (`phase_id`) REFERENCES `phase` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
            ", "Modificando la tabla final_mark");   
        }
        
        if($this->outQuery("RENAME TABLE `qualification` TO `mark`;", "Intentando el cambio de la tabla qualification a mark"))
        {
            $this->outQuery("
                ALTER TABLE `mark` DROP FOREIGN KEY `mark_ibfk_1`;
                ALTER TABLE `mark` DROP FOREIGN KEY `mark_ibfk_2`;
                ALTER TABLE `mark` DROP FOREIGN KEY `mark_ibfk_3`;
                ALTER TABLE `mark` DROP FOREIGN KEY `mark_ibfk_4`;
                ALTER TABLE `mark` DROP FOREIGN KEY `mark_ibfk_5`;
            
                ALTER TABLE `mark`
                CHANGE COLUMN `group_id` `team_id`  int(11) NOT NULL AFTER `id`,
                CHANGE COLUMN `mark_student_by` `mark_member_by`  int(11) NULL DEFAULT NULL AFTER `rule_id`,
                CHANGE COLUMN `mark_student` `mark_member`  int(11) NULL DEFAULT NULL AFTER `mark_member_by`,
                CHANGE COLUMN `mark_student_date` `mark_member_date`  int(11) NULL DEFAULT NULL AFTER `mark_member`,
                CHANGE COLUMN `mark_student_comment` `mark_member_comment`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER `mark_member_date`,
                CHANGE COLUMN `mark_admin_by` `mark_manager_by`  int(11) NULL DEFAULT NULL AFTER `mark_member_comment`,
                CHANGE COLUMN `mark_admin` `mark_manager`  int(11) NULL DEFAULT NULL AFTER `mark_manager_by`,
                CHANGE COLUMN `mark_admin_date` `mark_manager_date`  int(11) NULL DEFAULT NULL AFTER `mark_manager`,
                CHANGE COLUMN `mark_admin_comment` `mark_manager_comment`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER `mark_manager_date`;
            
                ALTER TABLE `mark` ADD CONSTRAINT `mark_phase` FOREIGN KEY (`phase_id`) REFERENCES `phase` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
                ALTER TABLE `mark` ADD CONSTRAINT `mark_rule` FOREIGN KEY (`rule_id`) REFERENCES `rule` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
                ALTER TABLE `mark` ADD CONSTRAINT `mark_team` FOREIGN KEY (`team_id`) REFERENCES `team` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
                ALTER TABLE `mark` ADD CONSTRAINT `mark_member` FOREIGN KEY (`mark_member_by`) REFERENCES `user` (`id`) ON DELETE SET NULL ON UPDATE RESTRICT;
                ALTER TABLE `mark` ADD CONSTRAINT `mark_manager` FOREIGN KEY (`mark_manager_by`) REFERENCES `user` (`id`) ON DELETE SET NULL ON UPDATE RESTRICT;"
            , "Modificando la tabla mark");
        }
        
        $this->outQuery("ALTER TABLE `phase` DROP FOREIGN KEY `phase_ibfk_4`;
            ALTER TABLE `phase`
            CHANGE COLUMN `default_phase_id` `template_phase_id`  int(11) NULL DEFAULT NULL AFTER `id`,
            DROP INDEX `default_phase_id`;
            ALTER TABLE `phase` ADD CONSTRAINT `phase_template_phase` FOREIGN KEY (`template_phase_id`) REFERENCES `template_phase` (`id`) ON DELETE SET NULL ON UPDATE SET NULL;"
        , "Intentando el cambio de la tabla phase");
        
        $this->outQuery("RENAME TABLE `phase_stretegy` TO `phase_strategy`;", "Corrigiendo el nombre de la tabla phase_stretegy a phase_strategy");
        $this->outQuery("
                RENAME TABLE `project_owner` TO `project_manager`;
                ALTER TABLE `project_manager` DROP FOREIGN KEY `project_manager_ibfk_2`;
                ALTER TABLE `project_manager`
                CHANGE COLUMN `owner_id` `manager_id`  int(11) NOT NULL AFTER `project_id`;
                ALTER TABLE `project_manager` ADD CONSTRAINT `project_manager_ibfk_2` FOREIGN KEY (`manager_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;                
        ", "Modificando la tabla project_owner");
        
        if($this->outQuery("CREATE TABLE `school_manager` (
            `user_id` int(11) NOT NULL,
            `school_id` int(11) NOT NULL,
            PRIMARY KEY (`user_id`,`school_id`),
            KEY `user_id_school_id` (`user_id`,`school_id`) USING BTREE,
            KEY `school_id` (`school_id`),
            CONSTRAINT `school_manager_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
            CONSTRAINT `school_manager_ibfk_2` FOREIGN KEY (`school_id`) REFERENCES `school` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ", "Intentando crear la nueva tabla school_manager"))
        {
            $this->output("Migrar a los usuarios managers");
            
            $schools = School::model()->findAll(array('select' => 'id'));
            $schools_ids = Functions::list_pluck($schools, "id");
            
            $admins = User::model()->with('authassignment')->findAll("itemname='creator' OR itemname='coordinator'", array('select' => 'id'));
            $admins_ids = Functions::list_pluck($admins, "id");
            
            $insert_admins = array();
            foreach ($schools_ids as $school_id)
            {
                foreach ($admins_ids as $user_id)
                {
                    $insert_admins [] = array('user_id' => $user_id, 'school_id' => $school_id);
                }
            }
            $db->insert("school_manager", $insert_admins);
        }
        
        if($this->outQuery("ALTER TABLE `authassignment` DROP FOREIGN KEY `authassignment_ibfk_1`;", "Intentando modificar authassignment"))
        {
            $this->outQuery("
            ALTER TABLE `user`
            CHANGE COLUMN `name` `firstname`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL AFTER `email`;
            
            ALTER TABLE `user`
            ADD COLUMN `active`  int(1) NOT NULL DEFAULT 1 AFTER `last_access`;
            
            ALTER TABLE `authassignment` ADD PRIMARY KEY (`userid`);
            DROP TABLE IF EXISTS `authitemchild`; DROP TABLE IF EXISTS `authitem`;
            
            DROP TABLE IF EXISTS `authitem`;
            CREATE TABLE `authitem` (
              `name` varchar(64) NOT NULL,
              `type` int(11) NOT NULL,
              `description` text,
              `bizrule` text,
              `data` text,
              PRIMARY KEY (`name`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
            INSERT INTO `authitem` VALUES ('admin', '2', 'Tiene acceso a todo.', null, null);
            INSERT INTO `authitem` VALUES ('canGetOwnSchoolAndGrades', '0', 'Puede obtener la lista de colegios y grados a los cuales pertenece', null, null);
            INSERT INTO `authitem` VALUES ('changeActiveYear', '0', 'Cambiar el Periodo seleccionado', null, null);
            INSERT INTO `authitem` VALUES ('coordinator', '2', 'Tiene acceso a ver todos los proyectos y ver la participación de los docentes y alumnos.', null, null);
            INSERT INTO `authitem` VALUES ('createProjects', '0', 'Tiene acceso a crear proyectos', null, null);
            INSERT INTO `authitem` VALUES ('creator', '2', 'Tiene acceso a ingresar contenido a las fases. No puede crear un proyecto, el proyecto solo lo crea el administrador.', null, null);
            INSERT INTO `authitem` VALUES ('instructor', '2', 'Docentes', null, null);
            INSERT INTO `authitem` VALUES ('manageCoordinators', '1', 'Administrar Coordinadores', null, null);
            INSERT INTO `authitem` VALUES ('manageCreators', '1', 'Administrar Creadores', null, null);
            INSERT INTO `authitem` VALUES ('manageInstructors', '1', 'Administrar Docentes', null, null);
            INSERT INTO `authitem` VALUES ('manageOwnSchoolProjects', '1', 'Administrar los proyectos de su colegio asignado', null, null);
            INSERT INTO `authitem` VALUES ('manageProjectTeams', '1', 'Administrar Equipos de Proyecto', null, null);
            INSERT INTO `authitem` VALUES ('manageSchoolCoordinators', '1', 'Administrar a Coordinadores de Colegios', null, null);
            INSERT INTO `authitem` VALUES ('manageSchoolCreators', '1', 'Administrar a Creadores de Colegios', null, null);
            INSERT INTO `authitem` VALUES ('manageSchools', '1', 'Administrar colegios', null, null);
            INSERT INTO `authitem` VALUES ('manageStrategies', '1', 'Administrar Estratégias', null, null);
            INSERT INTO `authitem` VALUES ('manageStudents', '1', 'Administrar Alumnos', null, null);
            INSERT INTO `authitem` VALUES ('manageTemplatePhases', '1', 'Administrar Plantillas de Fases', null, null);
            INSERT INTO `authitem` VALUES ('manageTools', '1', 'Administrar Herramientas', null, null);
            INSERT INTO `authitem` VALUES ('manageUsers', '1', 'Administrar Usuarios', null, null);
            INSERT INTO `authitem` VALUES ('manageYears', '1', 'Administrar años', null, null);
            INSERT INTO `authitem` VALUES ('student', '2', 'Estudiantes', null, null);
            INSERT INTO `authitem` VALUES ('viewActiveYear', '0', 'Ver el Periodo seleccionado', null, null);
            INSERT INTO `authitem` VALUES ('viewAllCreators', '0', 'Ver todos los Usuarios Creadores', null, null);
            INSERT INTO `authitem` VALUES ('viewAllManagers', '0', 'Ver todos los Usuarios Coordinadores', null, null);
            INSERT INTO `authitem` VALUES ('viewOwnSchoolProjects', '0', 'Ver Todos los Proyectos, su contenido y respuestas', null, null);
            INSERT INTO `authitem` VALUES ('viewProjectLog', '0', 'Ver los log de los proyectos', null, null);
            INSERT INTO `authitem` VALUES ('viewStrategies', '0', 'Ver todas las estratégias', null, null);
            INSERT INTO `authitem` VALUES ('viewTools', '0', 'Ver todas las herramientas', null, null);
            
            DROP TABLE IF EXISTS `authitemchild`;
            CREATE TABLE `authitemchild` (
              `parent` varchar(64) NOT NULL,
              `child` varchar(64) NOT NULL,
              PRIMARY KEY (`parent`,`child`),
              KEY `child` (`child`),
              CONSTRAINT `authitemchild_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `authitem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
              CONSTRAINT `authitemchild_ibfk_2` FOREIGN KEY (`child`) REFERENCES `authitem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

            INSERT INTO `authitemchild` VALUES ('admin', 'canGetOwnSchoolAndGrades');
            INSERT INTO `authitemchild` VALUES ('coordinator', 'canGetOwnSchoolAndGrades');
            INSERT INTO `authitemchild` VALUES ('creator', 'canGetOwnSchoolAndGrades');
            INSERT INTO `authitemchild` VALUES ('manageYears', 'changeActiveYear');
            INSERT INTO `authitemchild` VALUES ('admin', 'createProjects');
            INSERT INTO `authitemchild` VALUES ('creator', 'createProjects');
            INSERT INTO `authitemchild` VALUES ('admin', 'manageCoordinators');
            INSERT INTO `authitemchild` VALUES ('admin', 'manageCreators');
            INSERT INTO `authitemchild` VALUES ('admin', 'manageInstructors');
            INSERT INTO `authitemchild` VALUES ('creator', 'manageOwnSchoolProjects');
            INSERT INTO `authitemchild` VALUES ('admin', 'manageProjectTeams');
            INSERT INTO `authitemchild` VALUES ('creator', 'manageProjectTeams');
            INSERT INTO `authitemchild` VALUES ('instructor', 'manageProjectTeams');
            INSERT INTO `authitemchild` VALUES ('admin', 'manageSchoolCoordinators');
            INSERT INTO `authitemchild` VALUES ('admin', 'manageSchoolCreators');
            INSERT INTO `authitemchild` VALUES ('admin', 'manageSchools');
            INSERT INTO `authitemchild` VALUES ('admin', 'manageStrategies');
            INSERT INTO `authitemchild` VALUES ('admin', 'manageStudents');
            INSERT INTO `authitemchild` VALUES ('admin', 'manageTemplatePhases');
            INSERT INTO `authitemchild` VALUES ('admin', 'manageTools');
            INSERT INTO `authitemchild` VALUES ('admin', 'manageUsers');
            INSERT INTO `authitemchild` VALUES ('admin', 'manageYears');
            INSERT INTO `authitemchild` VALUES ('admin', 'viewProjectLog');
            INSERT INTO `authitemchild` VALUES ('changeActiveYear', 'viewActiveYear');
            INSERT INTO `authitemchild` VALUES ('manageCreators', 'viewAllCreators');
            INSERT INTO `authitemchild` VALUES ('manageCoordinators', 'viewAllManagers');
            INSERT INTO `authitemchild` VALUES ('manageSchoolCoordinators', 'viewAllManagers');
            INSERT INTO `authitemchild` VALUES ('coordinator', 'viewOwnSchoolProjects');
            INSERT INTO `authitemchild` VALUES ('manageOwnSchoolProjects', 'viewOwnSchoolProjects');
            INSERT INTO `authitemchild` VALUES ('creator', 'viewStrategies');
            INSERT INTO `authitemchild` VALUES ('creator', 'viewTools');
        
            ALTER TABLE `authassignment` ADD CONSTRAINT `authassignment_authitem` FOREIGN KEY (`itemname`) REFERENCES `authitem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
            
            
        ", "Modificando tablas de controles de acceso");
        }
        
        
        $this->outQuery("CREATE TABLE IF NOT EXISTS `log_project` (
                `id` int(11) NOT NULL AUTO_INCREMENT,
                `user_id` int(11) DEFAULT NULL,
                `project_id` int(11) DEFAULT NULL,
                `datetime` int(11) NOT NULL,
                `controller_action` varchar(255) NOT NULL,
                `action` varchar(255) NOT NULL,
                `human_comment` text,
                `data` text,
                PRIMARY KEY (`id`),
                KEY `log_project` (`project_id`),
                KEY `log_user` (`user_id`),
                CONSTRAINT `log_project_project` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
                CONSTRAINT `log_project_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
            ) ENGINE=InnoDB AUTO_INCREMENT=0  DEFAULT CHARSET=utf8;",
                "Intentando crear la tabla log_project"
            );
        
            $this->outQuery("CREATE TABLE IF NOT EXISTS `config` (
                `key` varchar(255) NOT NULL,
                `value` text NOT NULL,
                PRIMARY KEY (`key`)
              ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

              INSERT INTO `config` (`key`, `value`) VALUES
              ('file_extensions_allowed', 'bmp,cdr,doc,docx,gdoc,gif,gz,jpe,jpeg,jpg,odg,odp,ods,odt,pdf,png,ppt,pptx,ps,pub,rar,rtf,svg,txt,vsd,xls,xlsx,xps,zip'),
              ('image_extensions_allowed', 'bmp,gif,jpe,jpeg,jpg,png'),
              ('order_projects_by_type', '0');", 
            "Creando la tabla config e insertando valores default");
            
            $this->outQuery("
                    CREATE TABLE IF NOT EXISTS `log_system` (
                      `id` int(11) NOT NULL AUTO_INCREMENT,
                      `user_id` int(11) DEFAULT NULL,
                      `datetime` int(11) NOT NULL,
                      `controller_action` varchar(255) NOT NULL,
                      `action` varchar(255) NOT NULL,
                      `human_comment` text,
                      `data` text,
                      PRIMARY KEY (`id`),
                      KEY `log_user` (`user_id`),
                      CONSTRAINT `log_system_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
                    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
            ", "Creando la tabla log_system");
            
            $this->outQuery("
                    CREATE TABLE IF NOT EXISTS `log_auth` (
                      `id` int(11) NOT NULL AUTO_INCREMENT,
                      `user_id` int(11) DEFAULT NULL,
                      `username` varchar(255) DEFAULT NULL,
                      `datetime` int(11) NOT NULL,
                      `ip` varchar(255) NOT NULL,
                      `event` varchar(32) NOT NULL,
                      `type` varchar(32) DEFAULT NULL,
                      `url` varchar(255) NOT NULL,
                      `request_headers` text NOT NULL,
                      PRIMARY KEY (`id`),
                      KEY `user_id` (`user_id`),
                      CONSTRAINT `log_auth_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
                    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
            ", "Creando la tabla log_auth");
    }
}