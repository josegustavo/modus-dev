<?php

class UpdateAction extends CAction
{
    public function run()
    {
        if($post = Yii::app()->request->getPost("model"))
        {
            $attributes = json_decode($post,true);
            try
            {
                $strategy = Strategy::model()->findByPk($attributes['id']); 
                $old_strategy = $strategy->attributes;
                
                $strategy->name = $attributes['name'];
                $strategy->description = $attributes['description'];
                if(isset($attributes['image_id']))
                {
                    $strategy->image_id = $attributes['image_id'];
                }
                
                $diff = array_diff_assoc($strategy->attributes, $old_strategy);
                $ffid = array_diff_assoc($old_strategy, $strategy->attributes);
                if($diff || $ffid)
                {
                    $strategy->modified = time();
                    if(!$strategy->save())
                    {
                        $this->controller->error = "No se pudo guardar la estratégia.";
                    }
                    else
                    {
                        
                        $count_diff = count($diff);
                        $s = $count_diff>1?"s":"";
                        CLog::logSystem("updateStrategy", "Se modificó $count_diff atributo$s de la herramienta", array('old' => $ffid, 'new' => $diff));
                        
                        $this->controller->actionRead($strategy->id);
                    }
                }
                else
                {
                    $this->controller->success = TRUE;
                }
            }
            catch (Exception $e)
            {
                $this->controller->error = "No se pudo actualizar.";
                if(YII_DEBUG)
                {
                    $this->controller->debug = $e->getMessage();
                }
            }
        }
        else
        {
            $this->controller->error = "Ocurrio un error al enviar el formulario";
        }
    }
}