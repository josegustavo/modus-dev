<?php

class DeleteAction extends CAction
{
    public function run($id=NULL)
    {
        try
        {
            $model = Strategy::model()->findByPk($id);
            $attr = $model->attributes;
            $name = $model->name;
            $model->delete();
            CLog::logSystem("deleteStrategy", "se eliminó la estratégia $name.", $attr);
            $this->controller->success = true;
        }
        catch(Exception $e) 
        {
            $this->controller->error = 'Esta estrategia no se puede eliminar.';
            if(YII_DEBUG)
            {
                $this->controller->debug = $e->getMessage();
            }
        }
    }
}