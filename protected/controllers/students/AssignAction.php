<?php

class AssignAction extends CAction
{
        
    function run($school_id=NULL, $grade_id=NULL,$room_id,$student_id)
    {
        try
        {
            $assign = RoomHelper::assignStudent($student_id, $room_id);
            if($assign === TRUE)
            {
                $this->controller->success = true;
            }
            else
            {
                $this->controller->error = "Ocurrió un error desconocido al intentar asignar.";
            }
        }
        catch(Exception $ex)
        {
            $this->controller->error = $ex->getMessage();
        }
    }
    
}