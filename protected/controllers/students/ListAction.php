<?php

class ListAction extends CAction
{
    
    function run($school_id=NULL, $grade_id=NULL,$room_id=NULL)
    {
        
        $result = array();
        if($school_id>0 && $grade_id>0 && $room_id>0)
        {
            $students = User::model()->with('rooms','authassignment')->together()
                    ->findAll(
                    array(
                        'select' => array('id','username','email','firstname','lastname'),
                        'condition' => "room_id=:roomId AND rooms.year_id=:yearId AND itemname='student'",
                        'params' => array(':roomId' => $room_id, ':yearId' => $this->controller->year() ),
                        'order' => 'firstname asc'
                    )
            );
            $i = 1;
            foreach($students as $user)
            {
                $result[] = array('id' => $user->id, 'name' => $user->firstname." ".$user->lastname, 'username' => $user->username, 'firstname' => $user->firstname, 'lastname' => $user->lastname, 'email' => $user->email, 'password' => '**********');
                $i++;
            }
            $this->controller->success = $result;
        }
        else
        {
            $this->controller->error = 'Se necesita la sección';
        }
    }
    
}