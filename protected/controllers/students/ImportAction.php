<?php

class ImportAction extends CAction
{
    
    public function run()
    {
        $post = Yii::app()->request->getPost("model");
        if($post)
        {
            $roomId = $post['roomId'];
            $ids = $post['ids'];
            $errors = array();
            $count = 0;
            $total = count($ids);
            foreach ($ids as $id)
            {
                $this->controller->error = NULL;
                $this->controller->success = NULL;
                Yii::import("application.controllers.students.AssignAction");
                AssignAction::run(NULL,NULL, $roomId, $id);
                if($this->controller->error)
                {
                   $errors[] = $this->controller->error; 
                }
                else if($this->controller->success)
                {
                    $count++;
                }
            }
            $this->controller->success = array();
            if($errors)
            {
                $this->controller->success['error'] = $errors;
            }
            if($count>0)
            {
                $this->controller->success['msg'] = "Se asignaron $count/$total alumnos a la sección.";
            }
        }
        else
        {
            $this->controller->error = "No se enviaron correctamente los datos, vuelva a intentarlo.";
        }
    }
    
}