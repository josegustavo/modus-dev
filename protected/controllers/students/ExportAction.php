<?php

class ExportAction extends CAction
{
    function run($room_id)
    {
        if(Yii::app()->user->checkAccess("admin"))
        {
            $room = Room::model()->with( array('users' => array('order' => 'firstname ASC')) )->findByPk($room_id);
            if(!$room) return;
            
            $level = $room->level;
            
            $school = $room->school;
            
            $users = $room->users;
            $arr = array();
            foreach ($users as $user)
            {
                if($user->authassignment->itemname == 'student')
                {
                    $arr_line = array($user->firstname,$user->lastname,$user->username,"**********",$user->email);
                    $arr[] = implode(",", $arr_line);
                }
            }
            ob_get_clean();
        
            header('Cache-Control: no-cache, no-store, must-revalidate');
            header('Pragma: no-cache');
            header('Expires: 0');
            header('Content-Type: application/csv;charset=UTF-8');
            header('Content-Disposition: attachment;filename="'.$school->name." - " . $level->name . "° Grado " . $level->level_name . " - Sección " . $room->name.'.csv"');
            echo implode("\n", $arr);
            Yii::app()->end();
        }
    }
}