<?php

class EnableAction extends CAction
{
    public function run($id)
    {
        try
        {
            if(UserHelper::enableUser($id))
            {
                $this->controller->success = true;
            }
            else
            {
                $this->controller->error = "Este usuario no se puede habilitar.";
            }
        }
        catch(Exception $e) 
        {
            $this->controller->error = 'Este usuario no se pudo habilitar.';
        }
    }
}