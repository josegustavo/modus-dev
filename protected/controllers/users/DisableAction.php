<?php

class DisableAction extends CAction
{
    public function run($id)
    {
        if(Yii::app()->user->dbid == $id)
        {
            $this->controller->error = "No puede deshabilitar su propio usuario";
            return;
        }
        try
        {
            if(UserHelper::disableUser($id))
            {
                $this->controller->success = true;
            }
            else
            {
                $this->controller->error = "Este usuario no se puede deshabilitar.";
            }
        }
        catch(Exception $e) 
        {
            $this->controller->error = 'Este usuario no se pudo deshabilitar.';
        }
    }
}