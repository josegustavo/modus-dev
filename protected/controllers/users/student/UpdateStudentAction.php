<?php

class UpdateStudentAction extends CAction
{
    
    public function run($id)
    {
        $model = Yii::app()->request->getPost("model");
        if($model)
        {    
            $decode = json_decode($model,true);
            try
            {
                $user = UserHelper::validateUser($decode);
                if(UserHelper::upsertUser($user))
                {
                    $this->controller->success = array('password' => '');
                }
            }
            catch (Exception $ex)
            {
                $this->controller->error = $ex->getMessage();
            }
        }
        else
        {
            $this->controller->error = "No se envió correctamente el formulario, vuelva a intentarlo";
        }
    }
}