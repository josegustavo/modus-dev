<?php

class UpdateAction extends CAction
{
    public function run($id)
    {
        $post = Yii::app()->request->getPost("model");
        $model = json_decode($post, TRUE);
            
        try
        {
            $user = UserHelper::validateUser($model);
            if((Yii::app()->user->dbid == $user['id']) && ($user['username'] != Yii::app()->user->id))
            {
                $this->controller->error = "No puede modificar su propio nombre de usuario.";
            }
            else
            {
                
                $role = isset($model['role'])?$model['role']:'coordinator';
                $transaction =  Yii::app()->db->beginTransaction();
                if($user_db = UserHelper::upsertUser($user, $role))
                {
                    $transaction->commit();
                    
                    $this->controller->success = array('id' => $user_db->id);
                }
                else
                {
                    $transaction->rollback();
                    $this->controller->error = "Ocurrió un error al guardar usuario, recargue la pagina y vuelva a intentarlo.";
                }
            }
        }
        catch (Exception $ex)
        {
            $this->controller->error = $ex->getMessage();
        }
    }
}