<?php

class CreateInstructorAction extends CAction
{
    
    function run()
    {
        $result = array();
        $model = Yii::app()->request->getPost("model");
        if($model)
        {
            try
            {
                $decode = json_decode($model,true);

                $user = UserHelper::validateUser($decode);
                $role = "instructor";

                $userdb = UserHelper::upsertUser($user, $role);
                if($userdb)
                {
                    $result['id'] = $userdb->id;
                    $result['name'] = $userdb->firstname . " " . $userdb->lastname;
                    $result['count_schools'] = 0;
                    $this->controller->success = $result;
                }
            }
            catch (Exception $ex)
            {
                $this->controller->error = $ex->getMessage();
            }
        }
        else
        {
            $this->controller->error = "No se envió correctamente el formulario, vuelva a intentarlo";
        }
    }
}