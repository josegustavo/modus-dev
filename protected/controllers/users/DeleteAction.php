<?php

class DeleteAction extends CAction
{
    public function run($id)
    {
        if(Yii::app()->user->dbid == $id)
        {
            $this->controller->error = "No puede eliminar su propio usuario";
            return;
        }
        try
        {
            if(UserHelper::deleteUser($id))
            {
                $this->controller->success = true;
            }
            else
            {
                $this->controller->error = "Este usuario no se puede eliminar.";
            }
        }
        catch(Exception $e) 
        {
            $this->controller->error = 'Este usuario no se pudo eliminar. '.$e->getMessage();
        }
    }
}