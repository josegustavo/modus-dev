<?php

class YearsController extends Controller
{
    public function actionList($id=0)
    {
        $select = array();
        $select = Year::model()->findAll(array('order' => 'name ASC') );
        
        if(is_array($select))
        {
            foreach ($select as $s)
            {
                $result[] = array('id' => $s->id, 'name' => $s->name, 'active' => $s->active);
            }
        }
        else
        {
            $result = array('id' => $select->id, 'name' => $select->name, 'active' => $select->active);
        }
        
        $this->success = $result;
    }
    
    function actionCreate()
    {
        $post = Yii::app()->request->getPost("model");
        if($post)
        {
            $post = json_decode($post, true);
            $year = new Year();
            $year->name = $post['name'];
            $year->id = mt_rand(100000,999999);
            $year->active = 0;
            $year->ord = $this->_getNextOrder();
            if($year->save())
            {
                $this->success = array('id' => $year->id, 'name' => $year->name, 'active' => $year->active);
            }
            else
            {
                $this->error = "Ocurrio un error al crear un periodo.";
                if(YII_DEBUG)
                {
                    $this->debug = $year->getErrors();
                }
            }
        }
        else
        {
            $this->error = "No se envió correctamente el formulario, vuelva a intentarlo.";
        }
        
    }
    
    function actionDelete($id=null)
    {
        try
        {
            $model = Year::model()->findByPk($id);
            $model->delete();
            $this->success = true;
        }
        catch(Exception $e)
        {
            $this->error = 'Este periodo no se puede eliminar.';
        }
    }
    
    function actionUpdate()
    {
        $result = array();
        if(!empty($_POST) && !empty($_POST['model']))
        {
            $attributes = json_decode($_POST['model'],true);
            try
            {
                $year = Year::model()->findByPk($attributes['id']); 
                $year->name = $attributes['name'];
                if($attributes['active'] == 1)
                {
                    Year::model()->updateAll(array('active' => 0));
                }
                $year->active = $attributes['active'];
                $year->save();
            
                $result['id'] = $year->id;
                $this->success = $result;
            }
            catch (Exception $ex)
            {
                $this->error = "No se pudo actualizar:\n";
                if(YII_DEBUG)
                {
                    $this->debug = $ex->getMessage();
                }
            }
        }
        
    }
    
    function _getNextOrder()
    {
        $order = 1;
        $years = Year::model()->findAll(array('order'=>'ord','select'=>'ord'));
        if($years) foreach ($years as $y)
        {
            if($y['ord']==$order)$order++;
        }
        return $order;
    }
}