<?php

class DeleteAnswerAction extends CAction
{
    
    function run($project_id, $phase_id, $team_id, $answer_id)
    {
        $user_id = Yii::app()->user->dbid;
        if($user_id)
        {
            $answer = Answer::model()->findByPk($answer_id);
            if(!$answer)
            {
                $this->controller->error = "No se encuentra la respuesta o ya se ha eliminado";
            }
            else
            {
                $project_db = Project::model()->findByPk($project_id,array('select' => 'creator_id'));
                $is_owner = ($user_id == $project_db->creator_id) || Yii::app()->user->checkAccess("admin");
                $is_manager = $is_owner || (ProjectManager::model()->countByAttributes(array('manager_id'=>$user_id,'project_id'=>$project_id))?TRUE:FALSE);
            
                if($is_owner || $is_manager)
                {
                    if($answer->delete()){
                        $this->controller->success = true;
                    }else{
                        $this->controller->error = "Ocurrio un error al eliminar";
                    }
                }
                else
                    {
                    $this->controller->error = "No tiene permisos para eliminar la respuesta";
                }
            }
        }
    }
}