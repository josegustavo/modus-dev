<?php

class ReadAction extends CAction
{
    
    /**
     * Prepara información de un proyecto para su edición
     * @param type $id
     */
    public function run($id)
    {
        $project_db = Project::model()->findByPk($id); 
        if($project_db)
        {
            
            if($project_db->year_id == $this->controller->year())
            {
                $user_id = Yii::app()->user->dbid;

                $is_owner = ($user_id == $project_db->creator_id) || Yii::app()->user->checkAccess("admin");
                $is_principal_manager = false;
                if($project_db->school_id > 0 && Yii::app()->user->checkAccess("manageOwnSchoolProjects"))
                {
                    $is_principal_manager = SchoolManager::model()->countByAttributes(array('school_id' => $project_db->school_id, 'user_id' => $user_id)) > 0;
                }
                $is_manager = $is_owner || (ProjectManager::model()->countByAttributes(array('manager_id'=>$user_id,'project_id'=>$project_db->id))?TRUE:FALSE);
                if($is_owner || $is_principal_manager)
                {

                    $project = array(
                        'id' => $project_db->id,
                        'title' => $project_db->title,
                        'slug' => $project_db->slug,
                        'description' => FileHelper::changeImageSrc($project_db->description),
                        'start_date' => $project_db->start_date,
                        'end_date' => $project_db->end_date,
                        'image_id' => $project_db->image_id
                    );

                    if($image_db = $project_db->image)
                    {
                        $image = array();
                        $image['id'] = $image_db->id;
                        $image['cdn_url'] = $this->controller->config->uriCDN . "/uploads/" . $image_db->md5 . "/" . $image_db->original_name;
                        if(isset($image_db['properties']))
                        {
                            $prop = json_decode($image_db['properties'], TRUE);
                            if(isset($prop['width']) && isset($prop['height']))
                            {
                                $image['width'] = $prop['width'];
                                $image['height'] = $prop['height'];
                            }
                        }
                        $project['image'] = $image;
                    }

                    if($project_db->school_id)
                    {
                        $project['school_id'] = $project_db->school_id;
                    }

                    if($project_db->level)
                    {
                        $level = $project_db->level;
                        $project['level_id'] = $level->level_num;
                        $project['grade_id'] = $level->id;
                    }

                    if($project_db->type_id)
                    {
                        $type = $project_db->type;
                        $project['type'] = array('id' => $type->id, 'name' => $type->name);
                    }

                    $phases = array();
                    foreach($project_db->phases as $phase_db)
                    {
                        $phase = array('id' => $phase_db->id, 'ord' => $phase_db->ord, 'name' => $phase_db->name, 'description' => FileHelper::changeImageSrc($phase_db->description) );
                        if($image_db = $phase_db->image)
                        {
                            $phase['image'] = $this->_parseImage($image_db);
                            $phase['image_id'] = $phase['image']['id'];
                        }

                        $phase['strategies'] = Functions::list_pluck($phase_db->strategies, "id");
                        $phase['tools'] = Functions::list_pluck($phase_db->tools, "id");;

                        $attachments = array();
                        foreach($phase_db->files as $a)
                        {
                            $attachments[] = array('id' => $a->id, 'full_url' => $this->controller->config->uriCDN . "/uploads/" . $a->md5 . "/" . $a->original_name, 'original_name' => $a->original_name);
                        }
                        $phase['attachments'] = $attachments;

                        $phases[] = $phase;
                    }
                    $project['phases'] = $phases;

                    if($users = $project_db->users)
                    {
                        $project_managers_ids = Functions::list_pluck($users, "id");
                        $project['instructors_ids'] = $project_managers_ids;
                    }



                    if($project_db->creator)
                    {
                        //$creator = $project_db->creator;
                        //$project['creator'] = array('id' => $creator->id, 'username' => $creator->username, 'name' => $creator->firstname . " " . $creator->lastname);
                    }



                    if($rooms_db = $project_db->rooms)
                    {
                        $project['rooms_ids'] = array();
                        foreach ($rooms_db as $room_db)
                        {
                            $project['rooms_ids'][] = $room_db->id;
                        }
                    }
                    
                    if($project_db->teams)
                    {
                        $project['has_teams'] = true;
                    }
                    else
                    {
                        $project['has_teams'] = false;
                    }

                    $this->controller->success = $project;
                }
                else if($is_manager)
                {
                    $this->controller->success = array('only_assign_teams' => TRUE, 'slug' => $project_db->slug, 'id' => $project_db->id);
                }
                else
                {
                    $this->controller->error = "No tiene permiso para ver este proyecto.";
                }
            }
            else
            {
                $this->controller->error = "Este proyecto no existe en el presente año.";
            }
        }
        else
        {
            $this->controller->error = "No se encontro el proyecto";
        }
    }
    
    function _parseImage($image_db)
    {
        $image = array();
        $image['id'] = $image_db->id;
        $image['cdn_url'] = $this->controller->config->uriCDN . "/uploads/" . $image_db->md5 . "/" . $image_db->original_name;
        if(isset($image_db['properties']))
        {
            $prop = json_decode($image_db['properties'], TRUE);
            if(isset($prop['width']) && isset($prop['height']))
            {
                $image['width'] = $prop['width'];
                $image['height'] = $prop['height'];
            }
        }
        return $image;
    }
  
    function _setState(&$project)
    {
        $isAdmin = Yii::app()->user->checkAccess("admin");
        $isCreator = Yii::app()->user->checkAccess("creator");
        $user_id = Yii::app()->user->dbid;
        
        $project['is_manager'] = ProjectManager::model()->countByAttributes(array('manager_id'=>$user_id,'project_id'=>$project['id']))>0?TRUE:FALSE;
        $project['is_owner'] = ($project['creator_id'] == $user_id) || $isAdmin || $isCreator;
        $created = $project['created'];
        $project['created'] = date( 'd/m/Y H:i:s', $created );
        $project['created_ago'] = $this->_human_time_diff($created);
        $modified = $project['modified'];
        $project['modified'] = date( 'd/m/Y H:i:s', $modified );
        $project['modified_ago'] = $this->_human_time_diff($modified);
        
        $now = time();
        if( $now < $project['start_date'] ){
            $project['state'] = 0;
            $project['state_ago'] = $this->_human_time_diff($now,$project['start_date']);
            $project['state_date'] = date( 'd/m/Y h:i:sa', $project['start_date']);
        }else if( $now < $project['end_date'] ){
            $project['state'] = 1;
            $project['state_ago'] = $this->_human_time_diff($now,$project['end_date']);
            $project['state_date'] = date( 'd/m/Y h:i:sa', $project['end_date']);
        }
        else
        {
            $project['state'] = 2;
            $project['state_ago'] = $this->_human_time_diff($project['end_date'],$now);
            $project['state_date'] = date( 'd/m/Y h:i:sa', $project['end_date']);
        }
    }
}