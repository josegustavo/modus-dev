<?php

class DeleteAction extends CAction
{
    function run()
    {
        $user_id = Yii::app()->user->dbid;
        
        $pk = $_POST['id'];if(!$pk)return;
        
        $force = (isset($_POST['force']) && $_POST['force'])?true:false;
        $project = Project::model()->findByPk($pk, array('select' => array('id', 'creator_id', 'school_id')));
        if(!$project)
        {
            $this->controller->error = "No se encuentra el proyecto o ya se ha eliminado.";
        }
        else
        {
            $is_principal_manager = false;
            if($project->school_id > 0 && Yii::app()->user->checkAccess("manageOwnSchoolProjects"))
            {
                $is_principal_manager = SchoolManager::model()->countByAttributes(array('school_id' => $project->school_id, 'user_id' => $user_id)) > 0;
            }
            if( $is_principal_manager || ($project->creator_id == $user_id) ||  Yii::app()->user->checkAccess("admin")  )
            {
                $select_phase = array(
                    'project_id','id','name','description','image_id','modified', 'ord'
                );
                $phases = Phase::model()->findAllByAttributes(array('project_id' => $project['id']), array('select' => $select_phase, 'order' => 'ord ASC'));
                $phases_ids = Functions::list_pluck($phases, "id");
                $answers = Answer::model()->countByAttributes(array('phase_id'=>$phases_ids));
                $marks = Mark::model()->countByAttributes(array('phase_id'=>$phases_ids));
                if( ($answers>0 || $marks >0) && !$force)
                {
                    $this->controller->error = "El proyecto tiene ";
                    if($answers > 0)
                    {
                        $this->controller->error .= $answers . " respuesta" . ($answers>1?"s":"") ;
                        if($marks > 0)
                        {
                            $this->controller->error .= " y ";
                        }
                    }
                    if($marks >0 )
                    {
                        $this->controller->error .= $marks . " calificacion" . ($marks>1?"es":"") ;
                    }
                }
                else
                {
                    Answer::model()->deleteAllByAttributes(array('phase_id'=>$phases_ids));
                    Mark::model()->deleteAllByAttributes(array('phase_id'=>$phases_ids));
                    FinalMark::model()->deleteAllByAttributes(array('phase_id'=>$phases_ids));
                    if($project->delete()){
                        $this->controller->success = true;
                    }else{
                        $this->controller->error = "No se puede eliminar";
                    }
                }
            }
            else
            {
                $this->controller->error = "No tiene permisos para eliminar el proyecto";
            }
        }
    }
}