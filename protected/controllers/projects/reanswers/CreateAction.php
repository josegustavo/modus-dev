<?php

class CreateAction extends CAction
{
    
    public $project_id = 0;
            
    function run($project_id, $phase_id, $team_id, $answer_id)
    {
        $user = Yii::app()->user;
        $user_id = $user->dbid;
        
        $post = Yii::app()->request->getPost("model");
        if($post)
        {
            $model = json_decode($post,true);

            $project_db = Project::model()->findByPk($project_id,array('select' => 'creator_id,school_id'));
            $this->project_id = $project_id;
            
            $school_id = $project_db->school_id;
            
            $is_admin = $user->checkAccess("admin") || (SchoolManager::model()->countByAttributes(array('school_id' => $school_id, 'user_id' => $user_id))>0);
            
            $is_owner = $is_admin || ($user_id == $project_db->creator_id);
            $is_manager = $is_owner || (ProjectManager::model()->countByAttributes(array('manager_id'=>$user_id,'project_id'=>$project_id))?TRUE:FALSE);
            if($is_manager)
            {
                $answer = Answer::model()->with(array('author' => array('select' => 'username')))->findByPk($answer_id, array('select' => 'id'));
                if($answer)
                {
                    $transaction = Yii::app()->db->beginTransaction();
                    
                    $reanswer = new Reanswer();
                    $reanswer->author_id      = $user_id;
                    $reanswer->answer_id      = $answer_id;
                    $reanswer->created        = time();
                    $reanswer->reanswer       = FileHelper::repareCDN($model['reanswer']);

                    if($reanswer->save())
                    {
                        $attributes = $reanswer->attributes;
                        $user = User::model()->findByPk($user_id, array('select' => 'username'));
                        $user_name = $user->username;
                        $student_name = $answer->author->username;
                        $phase = Phase::model()->findByPk($phase_id, array('select' => 'name'));
                        $phase_name = $phase->name;
                        CLog::logProject("createReAnswer","Se agregó una réplica de $user_name, a la respuesta de $student_name, en la fase $phase_name", array($phase_id, $answer_id, $attributes) );
                    
                        foreach ($model['attachments'] as $attachment)
                        {
                            $a = new ReanswerAttachment;
                            $a->file_id = $attachment['id'];
                            $a->reanswer_id = $reanswer->id;
                            $a->save();
                        }
                        $transaction->commit();
                        $this->controller->success = $this->controller->_parseReAnswer($reanswer);
                    }
                    else
                    {
                        $transaction->rollback();
                        $this->controller->error = $reanswer->getErrors();
                    }
                }
                else
                {
                    $this->controller->error = "La respuesta se ha eliminado o no existe.";
                }
            }
            else
            {
                $this->controller->error = "No tiene permisos para enviar una publicación.";
            }
        }
        else
        {
            $this->controller->error = "No se envió correctamente el formulario, intente recargar la página.";
        }
    }
}