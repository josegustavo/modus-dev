<?php

class DeleteAction extends CAction
{
    
    public $project_id = 0;
    
    function run($project_id, $phase_id, $team_id, $answer_id,$reanswer_id)
    {
        $is_admin = Yii::app()->user->checkAccess("admin");
        $is_instructor = Yii::app()->user->checkAccess("instructor");
        if(!$is_admin && !$is_instructor) return;
        
        $user_id = Yii::app()->user->dbid;
        if($user_id)
        {
            $reanswer = Reanswer::model()->findByPk($reanswer_id);
            if(!$reanswer)
            {
                $this->controller->error = "No se encuentra la respuesta o ya se ha eliminado";
            }
            else
            {
                $project_db = Project::model()->findByPk($project_id,array('select' => 'creator_id'));
                $this->project_id = $project_id;
                
                $is_owner = ($user_id == $project_db->creator_id) || Yii::app()->user->checkAccess("admin");
                $is_manager = $is_owner || (ProjectManager::model()->countByAttributes(array('manager_id'=>$user_id,'project_id'=>$project_id))?TRUE:FALSE);
            
                if($is_owner || $is_manager)
                {
                    if( ProjectHelper::removeReAnswer($reanswer) )
                    {
                        $this->controller->success = true;
                    }
                    else
                    {
                        $this->controller->error = "Ocurrio un error al eliminar";
                    }
                }
                else
                {
                    $this->controller->error = "No tiene permisos para eliminar la respuesta";
                }
            }
        }
    }
}