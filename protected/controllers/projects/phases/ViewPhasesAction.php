<?php

class ViewPhasesAction extends CAction
{
    
    function run($project_id)
    {
        $uriCDN = $this->controller->config->uriCDN;
        
        $phases_db = Phase::model()->findAllByAttributes(array('project_id'=>$project_id),array('order'=>'t.ord ASC'));
        
        $phases = array();
        $showfinal_others = "";
        foreach ($phases_db as $phase)
        {
            $add = array();
            $add['id'] = $phase->id;
            $add['name'] = $phase->name;
            $add['description'] = FileHelper::changeImageSrc($phase->description);
            $add['ord'] = intval($phase->ord);
            
            if($image = $phase->image)
            {
                $add['image'] = $uriCDN. "/uploads/" . $image->md5 . "/" . $image->original_name;
            }
            
            
            $rules_db = $phase->rules;
            
            
            $add['can_mark'] = intval($phase['can_mark']);
            $add['can_answer'] = intval($phase['can_answer']);
            $add['text_answer'] = $phase['text_answer'];
            
            if(strlen($showfinal_others))
                $add['showfinal_text'] = $showfinal_others;
            
            $rules = array();
            foreach($rules_db as $rule)
            {
                $newRule = array();
                $newRule['id'] = $rule->id;
                $newRule['ord'] = intval($rule->ord);
                $newRule['rule'] = $rule->rule;
                $newRule['score'] = intval($rule->score);
                $rules []= $newRule;
            }
            $add['rules'] = $rules;
            
            $strategies = array();$tools = array();$attachments = array();
            foreach($phase->strategies as $s)
            {
                $strategy = array('name' => $s->name, 'description' => $s->description);
                if($image = $s->image)
                {
                    $strategy['image'] = $uriCDN. "/uploads/" . $image->md5 . "/" . $image->original_name;
                }
                $strategies[] = $strategy;
            }
            foreach($phase->tools as $s)
            {
                $tool = array('name' => $s->name, 'description' => $s->description, 'url' => $s->url);
                if($image = $s->image)
                {
                    $tool['image'] = $uriCDN. "/uploads/" . $image->md5 . "/" . $image->original_name;
                }
                $tools[] = $tool;
            }
            foreach($phase->files as $f)
            {
                $attach = array(
                    'url' => $uriCDN. "/uploads/" . $f->md5 . "/" . $f->original_name,
                    'original_name' =>  $f->original_name,
                    'type' => FileHelper::getFileType($f->original_name, $f->md5)
                );
                $attachments[] = $attach;
            }
            
            $add['strategies'] = $strategies;
            $add['tools'] = $tools;
            $add['attachments'] = $attachments;
            
            $phases[] = $add;
        }

        $this->controller->success = $phases;
    }
}