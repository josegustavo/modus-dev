<?php

class ExportAction extends CAction
{
    public function run($id)
    {
        $user_id = Yii::app()->user->dbid;
        $project_db = Project::model()->with('phases')->findByAttributes( array( 'id' => $id )); 
        if($project_db)
        {
            
            $regex = "/\d+_\d+_\d+_\w\.\w{3,4}/";
            
            $project['title'] = $project_db->title;
            $project['description'] = $project_db->description;
            
            
            
            $filesList = array();
            preg_match_all($regex, $project['description'], $filesList);
            $imagesAttach = $filesList[0];
            
            if($image = $project_db->image)
            {
                $project['image'] = $image->md5;
                $project['image_name'] = $image->original_name;
            }
            $project['start_date'] = $project_db->start_date;
            $project['end_date'] = $project_db->end_date;
            $phases = array();
            
            foreach($project_db->phases as $p => $phase)
            {
                $phases[$p] = array();
                $attachments = array();
                foreach($phase->files as $s => $attachment)
                {
                    $attachments[$s]['filename'] = $attachment->md5;
                    $attachments[$s]['originalname'] = $attachment->original_name;
                }
                $phases[$p]['attachments'] = $attachments;

                $phases[$p]['template_phase_id'] = $phase->template_phase_id;
                $phases[$p]['ord'] = $phase->ord;
                $phases[$p]['name'] = $phase->name;
                $phases[$p]['description'] = $phase->description;
                
                $phasesFileList = array();
                preg_match_all($regex, $phase->description, $phasesFileList);
                $imagesAttach = array_merge($imagesAttach,$phasesFileList[0]);
                
                if($image = $phase->image)
                {
                    $phases[$p]['image'] = $image->md5;
                    $phases[$p]['image_name'] = $image->original_name;
                }
                
                $phases[$p]['can_answer'] = $phase->can_answer;
                $phases[$p]['can_mark'] = $phase->can_mark;
                $phases[$p]['showfinal_others'] = $phase->showfinal_others;
                $phases[$p]['text_answer'] = $phase->text_answer;
                $phases[$p]['strategies'] = Functions::list_pluck($phase->strategies, "name");
                $phases[$p]['tools'] = Functions::list_pluck($phase->tools, "name");;
                
            }
            $project['phases'] = $phases;
            
            
            $project['grade'] = array();
            if( ($school = $project_db->school) && ($level = $project_db->level) )
            {
                $project['school'] = $school->name;
                $project['grade']['name'] = $level->name;
                $project['grade']['level'] = $level->level_name;
                $project['grade']['rooms'] = Functions::list_pluck($project_db->rooms, "name");
            }
            
            
            if($project_db->type_id)
                $project['type'] = $project_db->type->name;

            $imagesAttach = array_unique($imagesAttach);
            
            $fileTempPath = Yii::app()->getRuntimePath().DIRECTORY_SEPARATOR;
            $dirUploadsPath = Yii::app()->basePath.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'file'.DIRECTORY_SEPARATOR;
            
            //Crear el archivo con la información del proyecto
            $filename = $project_db->slug."_".time();
            $filePathJson = $fileTempPath.$filename.".mp";
            file_put_contents($filePathJson, json_encode($project));
            
            //Zipear todos los adjuntos multimedia
            $fileZipAllPath = $fileTempPath.$filename.".zip";
            $zip = new ZipArchive;
            if ($zip->open($fileZipAllPath,ZIPARCHIVE::CREATE) === TRUE)
            {
                //Agregar el archivo con la información del proyecto
                $zip->addFile($filePathJson, $filename.".mp");
                if(isset($project['image']) && (strlen($project['image']) > 0) && file_exists($dirUploadsPath.$project['image']))
                    $zip->addFile($dirUploadsPath.$project['image'], $project['image']);
                
                foreach ($imagesAttach as $imageAttach)
                {
                    if(file_exists($dirUploadsPath.$imageAttach))
                        $zip->addFile($dirUploadsPath.$imageAttach,$imageAttach);
                }
                foreach($project_db->phases as $phase)
                {
                    if($phase->image && strlen($phase->image->md5)>0 && file_exists($dirUploadsPath.$phase->image->md5))
                        $zip->addFile($dirUploadsPath.$phase->image->md5,$phase->image->md5);
                    foreach($phase->files as $attachment)
                    {
                        if($attachment->md5 && file_exists($dirUploadsPath.$attachment->md5))
                            $zip->addFile($dirUploadsPath.$attachment->md5,$attachment->md5);
                    }
                }
                $zip->close();
                unlink($filePathJson);
            }
            
            if(file_exists($fileZipAllPath))
            {
                ob_get_clean();

                header('Cache-Control: max-age=9999999,public,');
                header('Pragma:cache');
                header('Date:Tue, 24 Jan 1989 14:04:14 GMT');
                header('Last-Modified, 24 Jan 1989 14:04:14 GMT');
                header('ETag:"'.time().'"');
                header('Keep-Alive:timeout=5, max=90');
                header('Content-Type: application/zip, application/octet-stream');
                header('Content-Disposition: attachment; filename="'.$project_db->title.'.zip"');
                header("Content-Transfer-Encoding: binary");
                header("Content-Length: ".filesize($fileZipAllPath));
                readfile($fileZipAllPath);
                
                unlink($fileZipAllPath);
                Yii::app()->end();
            }else
            {
                throw new Exception("No se pudo exportar el proyecto");
            }
        }
        else
        {
            throw new Exception("No se encontro el proyecto");
        }
    }
}
