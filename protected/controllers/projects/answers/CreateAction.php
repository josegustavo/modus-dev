<?php

class CreateAction extends CAction
{
    
    public $project_id = 0;
    
    public function run($project_id, $phase_id, $team_id)
    {
        $user_id = Yii::app()->user->dbid;
        $is_student = Yii::app()->user->checkAccess("student");
        if( !$is_student )
        {
            $this->controller->error = "Sólo un alumno puede enviar respuestas.";
            return;
        }
        
        $post = Yii::app()->request->getPost("model");
        if(!$post || !$post['model'])
        {
            $this->controller->error = "No se envió correctamente el formulario.";
            return;
        }
        
        //Verificar si los datos enviados son consistentes
        $project = Project::model()->with(array(
                    'phases' => array('select'=>'id'),
                    'teams' => array('select' => 'id'),
                    'teams.users' => array('select' => 'id','first_name','last_name')
                ))->together()
                ->findByPk($project_id, array(
                    'condition' => 'phases.id=:phaseId AND teams.id=:teamId AND users.id=:userId', 
                    'params' => array(':phaseId' => $phase_id, ':teamId' => $team_id, ':userId' => $user_id),
                    'select' => array('id','start_date','end_date')
                ));
        if(!$project)
        {
            $this->controller->error = "Los datos enviados no coinciden.";
            return;
        }
        
        $model = json_decode($post,true);
        if(!isset($model['answer']) || (strlen($model['answer']) <= 0) )
        {
            $this->controller->error = "Debe escribir algún contenido.";
            return;
        }
        
        $this->project_id = $project->id;
        
        //Verificar si el proyecto ya empezó y aún  no termina
        $time = time();
        if( $time < $project->start_date)
        {
            $this->controller->error = "No se pudo enviar ya que el proyecto no empieza.";
            return;
        }
        if( $time > $project->end_date)
        {
            $this->controller->error = "No se pudo enviar ya que el proyecto ha finalizado.";
            return;
        }
        
        $transaction = Yii::app()->db->beginTransaction();
        
        $newAnswer = NULL;
        if(isset($model['id']) && is_numeric($model['id']))
        {
            $newAnswer = Answer::model()->findByPk($model['id']);
        }
        if(!$newAnswer)
        {
            $newAnswer = new Answer;
        }
        //Preparar para insertar a la base de datos
        
        if(!$newAnswer->id)
        {
            $newAnswer->author_id      = $user_id;
            $newAnswer->phase_id       = $phase_id;
            $newAnswer->team_id        = $team_id;
            $newAnswer->created        = $time; 
        }
        $newAnswer->answer         = FileHelper::repareCDN($model['answer']);
        $newAnswer->is_published   = (isset($model['is_published']) && $model['is_published'] == TRUE)?1:0;
        $newAnswer->is_last        = (isset($model['is_last']) && $model['is_last'] == TRUE)?1:0;
        $newAnswer->modified       = $time;
        
        
        //Consultar si se puede responder
        //Consultar si puede enviar una respuesta final
        $canAnswer = true;
        $canLastAnswer = true;
        $answers =  Answer::model()
                    ->findAll(array(
                            'condition'=>'phase_id=:phase_id AND team_id=:team_id AND is_published=1',
                            'params'=>array(':phase_id'=>$phase_id, ':team_id'=>$team_id),
                            'order' => 't.modified ASC'
                            ));
        foreach ($answers as $answer)
        {
            if($answer->is_last == 1 && $answer->is_published == 1)
            {
                $canAnswer = false;
                $canLastAnswer = false;
            }
        }
        //Si se intenta enviar una respuesta final
        $users_whithout_answer = array();
        if($canAnswer == TRUE)
        {
            $answers_authors_ids = array_map(function($ans){
                if($ans['is_published'] == 1)
                {
                    return $ans['author_id'];
                }
            }, $answers);
            $team_users = Team::model()->with('users')->findByPk($team_id);
            foreach ($team_users->users as $team_user)
            {
                if(!in_array($team_user->id, $answers_authors_ids))
                {
                    $canLastAnswer = false;
                    $users_whithout_answer[] = $team_user->id;
                }
            }
        }
        else
        {
            $canLastAnswer = false;
        }
        
        if( ($newAnswer->is_last == 0 && $canAnswer == TRUE) || ($newAnswer->is_last == 1 && $canLastAnswer == TRUE))
        {
            if($newAnswer->save())
            {
                //Enviar al log, si se ha publicado
                if($newAnswer->is_published)
                {
                    $attributes = $newAnswer->attributes;
                    $user = User::model()->findByPk($user_id, array('select' => 'username'));
                    $user_name = $user->username;
                    $phase = Phase::model()->findByPk($phase_id, array('select' => 'name'));
                    $phase_name = $phase->name;
                    CLog::logProject("createAnswer","Se agregó una respuesta de $user_name, en la fase $phase_name", array($phase_id, $attributes) );
                }
                
                if(isset($model['attachment_id']))
                {
                    if(isset($model['id']))
                    {
                        AnswerAttachment::model()->deleteAllByAttributes(array('answer_id' => $newAnswer->id));
                    }
                    $attachment = new AnswerAttachment;
                    $attachment->answer_id = $newAnswer->id;
                    $attachment->file_id = $model['attachment_id'];
                    $attachment->save();
                    
                }
                
                $last_time = isset($model['last_time'])?$model['last_time']:0;
                $answers_db = Answer::model()
                        ->findAll(array(
                                'condition'=>'phase_id=:phase_id AND team_id=:team_id AND is_published=1 AND t.modified > :modified',
                                'params'=>array(':phase_id'=>$phase_id, ':team_id'=>$team_id, 'modified'=>$last_time),
                                'order' => 't.modified ASC'
                                ));
                $result = array();
                $result_answers = array();
                foreach ($answers_db as $k => $a)
                {
                    $result_answers[] = $this->controller->_parseAnswer($a, FALSE);
                }
                $result['answers'] = $result_answers;
                $result['options'] = array();
                $result['options']['last_time'] = $time;
                $result['options']['can_answer'] = $canAnswer;
                $result['options']['can_last_answer'] = $canLastAnswer;
                //Si fue la última respuesta enviada y no fue borrador, ya no se pueden publicar más respuestas
                if($newAnswer->is_last == 1 && $newAnswer->is_published == 1)
                {
                    $result['options']['can_answer'] = FALSE;
                    $result['options']['can_last_answer'] = FALSE;
                }
                //Si no fue un borrador y sólo faltaba que el mismo alumno envíe su respuesta
                else if($newAnswer->is_published == 1 && count($users_whithout_answer) == 1 && $users_whithout_answer[0] == $user_id)
                {
                    $result['options']['can_answer'] = TRUE;
                    $result['options']['can_last_answer'] = TRUE;
                }
                $result['id'] = $newAnswer->id;

                $transaction->commit();
                $this->controller->success = $result;
            }
            else
            {
                $transaction->rollback();
            }
        }
        else
        {
            if($newAnswer->is_last == 0 && $canAnswer == FALSE)
            {
                $this->controller->error = "Ya se ha enviado una respuesta final y no se puede enviar otra respuesta.";
            }
            else if( $newAnswer->is_last == 1 && $canLastAnswer == FALSE )
            {
                $this->controller->error = "Todos los miembros tiene que publicar su respuesta, para poder publicar la respuesta final.";
            }
            else
            {
                $this->controller->error = "No se pudo publicar su respuesta, recargue la página y reintente.";
            }
            return;
        }
    }
}