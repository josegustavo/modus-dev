<?php

class UpdateAction extends CAction
{
    
    public function run()
    {
        $is_student = Yii::app()->user->checkAccess("student");
        $user_id = Yii::app()->user->dbid;
        $result = array();
        if( !$is_student ) return;
        
        if(!empty($_POST) && !empty($_POST['model'])){
            $model = json_decode($_POST['model'],true);
            $model['is_last']=(isset($model['is_last']) && $model['is_last'])?1:0;

            if(isset($model['id']))
            {
                $answer_id = $model['id'];
                $answer = Answer::model()->findByPk($answer_id);
                $answer->answer = $model['answer'];
                $answer->modified = time();
                $answer->is_last = $model['is_last']?1:0;
                $answer->state = $model['state'];

                $this->_saveAnswer($answer,$model);
            }
        }
    }
    
    function _saveAnswer($answer,$model)
    {
        $answers_opts = $this->getAnswersOptions($answer->phase_id, $answer->team_id);
                    
        $canAnswer = $answers_opts && $answers_opts['options'] && $answers_opts['options']['can_answer'];
        $canLastAnswer = $answers_opts && $answers_opts['options'] && $answers_opts['options']['can_last_answer'];

        $answer->is_last = ($canLastAnswer && $answer->is_last)?1:0;
        
        if($canAnswer)
        {
            if($answer->save())
            {
                AnswerAttachment::model()->deleteAllByAttributes(array('answer_id' => $answer->id));
                if(isset($model['attachment']) && isset($model['attachment']['filename'])){
                    $attachment = new AnswerAttachment;
                    $attachment->answer_id = $answer->id;
                    $attachment->attachment_id = $model['attachment']['filename'];
                    $attachment->save();
                }

                $originalOptions = $this->getAnswersOptions($answer->phase_id, $answer->team_id );
                $result = $this->getAnswersOptions($answer->phase_id, $answer->team_id, (isset($model['last_time'])?$model['last_time']:0) );
                $result['options'] = $originalOptions['options'];
                $result['id'] = $answer->id;

                $this->controller->success = $result;
            }
            else
            {
                $this->controller->error = "OcurriÃ³ un error al guardar, " . json_encode($answer->getErrors());
            }
        }
        else
        {
            $this->controller->error = "Ya se finalizÃ³ esta fase.";
        }
    }
}