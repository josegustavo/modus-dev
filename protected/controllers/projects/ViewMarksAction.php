<?php

class ViewMarksAction extends CAction
{
    
    function run($project_id, $phase_id, $team_id)
    {
        if($phase_id>0) 
        {
            $options = $this->controller->getQualificationsOptions($phase_id,$team_id);
            if($options && count($options))
            {
                $this->controller->success = $options;
            }
        }
    }
}