<?php

class ViewAction extends CAction
{
    /**
     * Prepara un proyecto para su vista
     * @param type $project_id
     */
    public function run($project_id)
    {
        $result = array();
        $user = Yii::app()->user;
        $user_id = $user->dbid;
        
        $project = FALSE;
        
        if($user->checkAccess("student"))
        {
            $project = Project::model()
                    ->with(array('teams' => array('select' => 'id,ord,name'), 'teams.users' => array('select' => 'id')))
                    ->findByPk($project_id, array(
                        'condition' => 'users.id=:userId',
                        'params' => array(':userId' => $user_id)
                    )
            );
        }
        else if ($user->checkAccess("instructor"))
        {
            $project = Project::model()
                    ->with(array('users' => array('select' => 'id,username,firstname,lastname')))
                    ->findByPk($project_id, array(
                        'condition' => 'users.id=:userId',
                        'params' => array(':userId' => $user_id)
                    )
            );
        }
        else if($user->checkAccess("creator") || Yii::app()->user->checkAccess("coordinator"))
        {
            $project = Project::model()
                    ->with(array('school' => array('select' => 'id,name'), 'school.users' => array('select' => 'id')))
                    ->findByPk($project_id, array(
                        'condition' => 'users.id=:userId',
                        'params' => array(':userId' => $user_id)
                    )
            );
        }
        else if($user->checkAccess("admin"))
        {
            $project = Project::model()->findByPk($project_id);
        }
        
        if($project)
        {
            $result = $this->parseProject($project);
            
            $my_team = array();
            $team_db = Team::model()->with('users')->findByAttributes(array('project_id'=>$project_id), "users.id=:userId", array(':userId' => $user_id));
            if($team_db)
            {
                $my_team['id'] = $team_db->id;
                $my_team['room_id'] = $team_db->room_id;
                $my_team['ord'] = $team_db->ord;
                $my_team['name'] = $team_db->name;

                $students = User::model()->with('teams')->findAll('team_id=:teamId', array(':teamId' => $team_db->id));
                if($students)
                {
                    $my_team['students'] = array();
                    foreach($students as $k => $u)
                    {
                        $my_team['students'][] = array('username' => $u->username, 'name' => $u->firstname . " " . $u->lastname);
                    }
                }
            }
            if(count($my_team))
            {
                $result['my_team'] = $my_team;
            }
            
            if(isset($result['rooms']))
            {
                foreach ($result['rooms'] as $s => $room)
                {
                    if(isset($result['my_team']) && ($result['my_team']['room_id'] == $room['id'])){
                        $result['my_team']['room'] = $room;
                    }
                }
            }
            $this->controller->success = $result;
        }
        else
        {
            $this->controller->error = "No se encontro el proyecto o no tiene permisos para acceder.";
        }
    }
    
    function parseProject($project_db)
    {
        $user_id = Yii::app()->user->dbid;
        $project = array(
            'id' => $project_db->id,
            'title' => $project_db->title,
            'slug' => $project_db->slug,
            'description' => FileHelper::changeImageSrc($project_db->description),
            'start_date' => date('d/m/Y h:i:sa', $project_db->start_date),
            'end_date' => date('d/m/Y h:i:sa', $project_db->end_date),
            'created' => date('d/m/Y h:i:sa', $project_db['created']),
            'created_ago' => $this->controller->_human_time_diff($project_db['created']),
            'modified' => date('d/m/Y h:i:sa', $project_db['modified']),
            'modified_ago' => $this->controller->_human_time_diff($project_db['modified'])
        );

        if($image_db = $project_db->image)
        {
            $project['image'] = $this->controller->config->uriCDN . "/uploads/" . $image_db->md5 . "/" . $image_db->original_name;
        }

        
        if($level = $project_db['level'])
        {
            $project['level_name'] = $level->name."° Grado de ".$level->level_name;
            $project['grade_id'] = $level->id;
        }

        $project_school_id = 0;
        if($school = $project_db['school'])
        {
            $project['school_name'] = $school->name;
            $project_school_id = $school->id;
        }

        if($rooms = $project_db['rooms'])
        {
            $r = Functions::list_pluck($rooms, "name");
            $project['rooms_names'] = join(", ", $r);
            $project['rooms_ids'] = Functions::list_pluck($rooms, "id");
            $project['rooms'] = array();
            foreach ($rooms as $room)
            {
                $project['rooms'][] = array('id' => $room->id, 'name' => $room->name);
            }
        }

        if($project_db->type_id)
        {
            $type = $project_db->type;
            $project['type'] = array('id' => $type->id, 'name' => $type->name);
        }
        
        $managers_ids = array();
        if($managers = $project_db->users)
        {
            $project['managers'] = array();
            foreach ($managers as $manager)
            {
                $project['managers'][] = array('username' => $manager->username, 'name' => $manager->firstname . " " . $manager->lastname);
                $managers_ids[] = $manager->id;
            }
        }
        else if($creator = $project_db->creator)
        {
            $project['creator_name'] = $creator->firstname . " " . $creator->lastname;
        }
        
        $is_principal_manager = false;
        if($project_school_id>0 && Yii::app()->user->checkAccess("viewOwnSchoolProjects"))
        {
            $is_principal_manager = SchoolManager::model()->countByAttributes(array('school_id' => $project_school_id, 'user_id' => $user_id)) > 0;
        }
        
        $project['is_owner'] = ($user_id == $project_db->creator_id) || $is_principal_manager || Yii::app()->user->checkAccess("admin");
        $project['is_manager'] = $project['is_owner'] || in_array($user_id, $managers_ids);
        
        $now = time();
        if( $now < $project_db['start_date'] )
        {
            $project['state'] = 0;
            $project['state_ago'] = $this->controller->_human_time_diff($now,$project_db['start_date']);
            $project['state_date'] = date( 'd/m/Y h:i:sa', $project_db['start_date']);
        }else if( $now < $project_db['end_date'] ){
            $project['state'] = 1;
            $project['state_ago'] = $this->controller->_human_time_diff($now,$project_db['end_date']);
            $project['state_date'] = date( 'd/m/Y h:i:sa', $project_db['end_date']);
        }else{
            $project['state'] = 2;
            $project['state_ago'] = $this->controller->_human_time_diff($project_db['end_date'],$now);
            $project['state_date'] = date( 'd/m/Y h:i:sa', $project_db['end_date']);
        }
        
        $project['can_view_all_answer'] = $project['is_manager'] || ($project['state'] == 2);
        
        if($project['can_view_all_answer'] == true)
        {
            if(isset($project['rooms'])) foreach ($project['rooms'] as $k => $room)
            {
                $room_id = $room['id'];
                $project['rooms'][$k]['teams'] = $this->getTeams($room_id, $project['id']);
            }
        }
        
        return $project;
    }
    
    
    private function getTeams($room_id, $project_id)
    {
        $teams = array();
        $teams_db = Team::model()->with('users')->together()->findAllByAttributes(array('room_id' => $room_id, 'project_id' => $project_id), array('select' => array('id', 'ord', 'name')));
        foreach ($teams_db as $t)
        {
            $users = array();
            $users_db = $t->users;
            foreach ($users_db as $u)
            {
                $users []= array('username' => $u->username, 'name' => $u->firstname . " " . $u->lastname);
            }
            $teams []= array('id' => $t->id, 'ord' => $t->ord, 'name' => $t->name, 'students' => $users);
        }
        return $teams;
    }
}