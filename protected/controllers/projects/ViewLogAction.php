<?php

class ViewLogAction extends CAction
{
    /**
     * Prepara un log proyecto para su vista
     * @param type $project_id
     */
    public function run($project_id)
    {
        $result = array();
        $user = Yii::app()->user;
        $user_id = $user->dbid;
        
        $project = FALSE;
        
        if($user->checkAccess("viewProjectLog"))
        {
            $project = ProjectHelper::getAuthorizedProject($project_id);
        }
        else
        {
            $this->controller->error = "No tiene permiso para ve logs de proyectos.";
        }
        
        if($project)
        {
            $logs = LogProject::model()
                    ->with(array('user' => array('select' => 'username,firstname,lastname')))
                    ->findAllByAttributes(
                    array('project_id' => $project_id),
                    array(
                        'order' => 'datetime ASC',
                        'select' => 'datetime,action,human_comment,data'
                        )
                    );
            
            $result = array();
            
            if($logs)
            {
                foreach ($logs as $log)
                {
                    $add = array_filter($log->attributes);
                    $add['created'] = date('d/m/Y h:i:sa', $add['datetime']);
                    $add['created_ago'] = $this->controller->_human_time_diff($add['datetime']);
                    
                    if($user = $log->user)
                    {
                        $add['username'] = $user->username;
                        $add['user_fullname'] = $user->firstname . " " . $user->lastname;
                        
                    }
                    
                    $result []= $add;
                }
            }
            
            $this->controller->success = $result;
        }
        else
        {
            $this->controller->error = "No se encontro el proyecto o no tiene permisos para acceder.";
        }
    }
}