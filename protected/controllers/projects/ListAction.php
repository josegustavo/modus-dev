<?php

class ListAction extends CAction
{
    
    public function run($page=1,$school_id=0,$level_id=0, $search_text="")
    {
        $this->controller->success = (Yii::app()->params['order_projects_by_type'])
                ?$this->loadProjectsByType(4, $page,$school_id,$level_id, $search_text)
                :$this->loadProjectsByAssigned(4, $page,$school_id,$level_id, $search_text);
    }
    
    function loadProjectsByAssigned_Admin($limit=10,$page=1,$school_id=0,$level_id=0, $search_text="")
    {
        $total_pages_assigned = 0;
        $total_pages_latest = 0;
        $assignedProjects = array();
        $latestProjects = array();
        $userId = Yii::app()->user->dbid;
        
        /**
         * Buscar proyectos propios
         */
        //Preparar el filtro para buscar proyectos
        $condition = "t.year_id = :yearId AND t.creator_id = :userId ";
        
        $params = array(':userId' => $userId, ':yearId' => $this->controller->year() );
        if($school_id>0)
        {
            $condition .= ' AND t.school_id = :schoolId';
            $params[':schoolId'] = $school_id;
        }
        if($level_id>0)
        {
            $condition .= ' AND level_id = :levelId';
            $params[':levelId'] = $level_id;
        }
        
        if(strlen($search_text) > 0 )
        {
            $condition .= " AND (t.title LIKE :searchText OR t.description LIKE :searchText) ";
            $params[':searchText'] = "%$search_text%";
        }

        //Buscar todos los proyectos pertenecientes al usuario
        $projects_owner = Project::model()->findAll(array('condition' => $condition,'params' => $params, 'select'=>'id'));
        $projects_manager_ids = Functions::list_pluck($projects_owner, "id");
        
        //Salvar el numero total de páginas encontradas
        $total_pages_assigned = ceil(count($projects_manager_ids) / $limit);
        
        //Verificar si la página solicitada esta fuera de rango
        $page_assigned = min(intval($page),$total_pages_assigned);
        
        if(count($projects_manager_ids))
        {
            //Cargar los proyectos de acuerdo al límite y número de página
            $assignedProjects_db = Project::model()->findAllByPk(
                    $projects_manager_ids, 
                    array('order'=>'t.modified DESC','limit'=>$limit,'offset'=>($page_assigned-1)*$limit)
            );
            
            foreach($assignedProjects_db as $p)
            {
                $assignedProjects[] = $this->parseSimpleProject($p);
            }
        }
        
        /**
         * Buscar Otros Proyectos
         */
        $condition = "t.creator_id != :userId AND t.year_id = :yearId";
        $params = array(':userId' => $userId, ':yearId' => $this->controller->year() );
        if($school_id>0)
        {
            $condition .= ' AND t.school_id = :schoolId';
            $params[':schoolId'] = $school_id;
        }
        if($level_id>0)
        {
            $condition .= ' AND level_id = :levelId';
            $params[':levelId'] = $level_id;
        }
        if(strlen($search_text) > 0 )
        {
            $condition .= " AND (t.title LIKE :searchText OR t.description LIKE :searchText) ";
            $params[':searchText'] = "%$search_text%";
        }
        //Contar el total de otros Proyectos
        $total_pages_latest = ceil(Project::model()->count(array('condition' => $condition,'params' => $params)) / $limit);
        $page_latest = min(intval($page), $total_pages_latest);
        $latestProjects_db = Project::model()->findAll(
                array('condition' => $condition,'params' => $params, 'order'=>'t.modified DESC', 'limit' => $limit, 'offset' => ($page_latest-1)*$limit)
        );
        foreach($latestProjects_db as $p)
        {
            $latestProjects[] = $this->parseSimpleProject($p);
        }
        $result = array(
            array('id'=>1, 'name' => 'Mis Proyectos', 'projects' => $assignedProjects,'projects_count' => count($assignedProjects), 'pages_count' => $total_pages_assigned,'page_actual' => $page_assigned),
            array('id'=>2, 'name' => 'Otros Proyectos', 'projects' => $latestProjects,'projects_count' => count($latestProjects), 'pages_count' => $total_pages_latest,'page_actual' => $page_latest)
        );
        return $result;
    }
    
    function loadProjectsByAssigned_Coordinator($limit=10,$page=1,$school_id=0,$level_id=0, $search_text="")
    {
        $result = array();
        
        $total_pages_assigned = 0;
        $total_pages_latest = 0;
        $assignedProjects = array();
        $latestProjects = array();
        $userId = Yii::app()->user->dbid;
        
        /**
         * Buscar proyectos propios
         */
        //Preparar el filtro para buscar proyectos
        $condition = "t.year_id = :yearId AND t.creator_id = :userId ";
        
        $params = array(':userId' => $userId, ':yearId' => $this->controller->year() );
        if($school_id>0)
        {
            $condition .= ' AND t.school_id = :schoolId';
            $params[':schoolId'] = $school_id;
        }
        if($level_id>0)
        {
            $condition .= ' AND level_id = :levelId';
            $params[':levelId'] = $level_id;
        }
        
        if(strlen($search_text) > 0 )
        {
            $condition .= " AND (t.title LIKE :searchText OR t.description LIKE :searchText) ";
            $params[':searchText'] = "%$search_text%";
        }

        //Buscar todos los proyectos pertenecientes al usuario
        $projects_owner = Project::model()->findAll(array('condition' => $condition,'params' => $params, 'select'=>'id'));
        $projects_manager_ids = Functions::list_pluck($projects_owner, "id");
        
        //Salvar el numero total de páginas encontradas
        $total_pages_assigned = ceil(count($projects_manager_ids) / $limit);
        
        //Verificar si la página solicitada esta fuera de rango
        $page_assigned = min(intval($page),$total_pages_assigned);
        
        if(count($projects_manager_ids))
        {
            //Cargar los proyectos de acuerdo al límite y número de página
            $assignedProjects_db = Project::model()->findAllByPk(
                    $projects_manager_ids, 
                    array('order'=>'t.modified DESC','limit'=>$limit,'offset'=>($page_assigned-1)*$limit)
            );
            
            foreach($assignedProjects_db as $p)
            {
                $assignedProjects[] = $this->parseSimpleProject($p);
            }
        }
        $result [] = array('id'=>1, 'name' => 'Mis Proyectos', 'projects' => $assignedProjects,'projects_count' => count($assignedProjects), 'pages_count' => $total_pages_assigned,'page_actual' => $page_assigned);
        
        /**
         * Buscar Otros Proyectos
         */
        $schools_ids = Functions::list_pluck(SchoolManager::model()->findAllByAttributes(array('user_id' => $userId)), 'school_id');
        if($schools_ids)
        {
            $condition = "t.creator_id != :userId AND t.school_id IN (". implode(", ", $schools_ids).")  AND t.year_id = :yearId";
            $params = array(':userId' => $userId, ':yearId' => $this->controller->year() );
            if($school_id>0)
            {
                $condition .= ' AND t.school_id = :schoolId';
                $params[':schoolId'] = $school_id;
            }
            if($level_id>0)
            {
                $condition .= ' AND level_id = :levelId';
                $params[':levelId'] = $level_id;
            }
            if(strlen($search_text) > 0 )
            {
                $condition .= " AND (t.title LIKE :searchText OR t.description LIKE :searchText) ";
                $params[':searchText'] = "%$search_text%";
            }
            //Contar el total de otros Proyectos
            $total_pages_latest = ceil(Project::model()->count(array('condition' => $condition,'params' => $params)) / $limit);
            $page_latest = min(intval($page), $total_pages_latest);
            $latestProjects_db = Project::model()->findAll(
                    array('condition' => $condition,'params' => $params, 'order'=>'t.modified DESC', 'limit' => $limit, 'offset' => ($page_latest-1)*$limit)
            );
            foreach($latestProjects_db as $p)
            {
                $latestProjects[] = $this->parseSimpleProject($p);
            }
            $result [] = array('id'=>2, 'name' => 'Otros Proyectos', 'projects' => $latestProjects,'projects_count' => count($latestProjects), 'pages_count' => $total_pages_latest,'page_actual' => $page_latest);
        }
        return $result;
    }
    
    
    function loadProjectsByAssigned_Student($limit=10,$page=1,$school_id=0,$level_id=0, $search_text="")
    {
        $total_pages_assigned = 0;
        $total_pages_latest = 0;
        $assignedProjects = array();
        $latestProjects = array();
        $userId = Yii::app()->user->dbid;
        
        /**
         * Buscar proyectos propios
         */
        //Buscar todos los proyectos pertenecientes al usuario
        $condition = "t.year_id = :yearId AND users.id = :userId ";
        $params = array(':userId' => $userId, ':yearId' => $this->controller->year() );
        $projects_owner = Project::model()
                ->with(array('teams' => array('select' => 'id'), 'teams.users' => array('select' => 'user.id')))->together()
                ->findAll(array('condition' => $condition,'params' => $params, 'select'=>'id'));
        $projects_owner_ids = Functions::list_pluck($projects_owner, "id");
        //Salvar el numero total de páginas encontradas
        $total_pages_assigned = ceil(count($projects_owner_ids) / $limit);
        
        //Verificar si la página solicitada esta fuera de rango
        $page_assigned = min(intval($page),$total_pages_assigned);
        
        if(count($projects_owner_ids))
        {
            //Cargar los proyectos de acuerdo al límite y número de página
            $assignedProjects_db = Project::model()->findAllByPk(
                    $projects_owner_ids, 
                    array('order'=>'t.modified DESC','limit'=>$limit,'offset'=>($page_assigned-1)*$limit)
            );
            
            foreach($assignedProjects_db as $p)
            {
                $assignedProjects[] = $this->parseSimpleProject($p);
            }
        }
        $result = array(
            array('id'=>1, 'name' => 'Mis Proyectos', 'projects' => $assignedProjects,'projects_count' => count($assignedProjects), 'pages_count' => $total_pages_assigned,'page_actual' => $page_assigned),
        );
        
        /**
         * Buscar Otros Proyectos del mismo grado
         */
        /*
        $sign = Sign::model()->with(array('room' => array('select' => 'level_id')))->together()->findByAttributes(array('user_id' => $userId, 'year_id' => $this->controller->year()), array('select' => 'user_id'));
        if($sign && $sign->room)
        {
            $level_id = $sign->room->level_id;
            $condition = "t.level_id =:levelId  AND t.year_id = :yearId ";
            if($projects_owner_ids)
            {
                $condition = "t.id NOT IN (". implode(", ", $projects_owner_ids).") ";
            }
            $params = array('levelId' => $level_id, ':yearId' => $this->controller->year());
            //Contar el total de otros Proyectos
            $total_pages_latest = ceil(Project::model()->count(array('condition' => $condition,'params' => $params)) / $limit);
            $page_latest = min(intval($page), $total_pages_latest);
            $latestProjects_db = Project::model()->findAll(
                    array('condition' => $condition,'params' => $params, 'order'=>'t.modified DESC', 'limit' => $limit, 'offset' => ($page_latest-1)*$limit)
            );
            foreach($latestProjects_db as $p)
            {
                $latestProjects[] = $this->parseSimpleProject($p);
            }
            $result []= array('id'=>2, 'name' => 'Otros Proyectos', 'projects' => $latestProjects,'projects_count' => count($latestProjects), 'pages_count' => $total_pages_latest,'page_actual' => $page_latest);
        }
        */
        return $result;
    }
    
    function loadProjectsByAssigned_Instructor($limit=10,$page=1,$school_id=0,$level_id=0, $search_text="")
    {
        $result = array();
        
        $total_pages_assigned = 0;
        $total_pages_latest = 0;
        $assignedProjects = array();
        $latestProjects = array();
        $userId = Yii::app()->user->dbid;
        
        /**
         * Buscar proyectos propios
         */
        //Buscar todos los proyectos pertenecientes al usuario
        $condition = "t.year_id = :yearId AND users.id = :userId ";
        $params = array(':userId' => $userId, ':yearId' => $this->controller->year() );
        $projects_owner = Project::model()
                ->with(array('users' => array('select' => 'id')))->together()
                ->findAll(array('condition' => $condition, 'params' => $params, 'select'=>'id'));
        $projects_owner_ids = Functions::list_pluck($projects_owner, "id");
        //Salvar el numero total de páginas encontradas
        $total_pages_assigned = ceil(count($projects_owner_ids) / $limit);
        
        //Verificar si la página solicitada esta fuera de rango
        $page_assigned = min(intval($page),$total_pages_assigned);
        
        if(count($projects_owner_ids))
        {
            //Cargar los proyectos de acuerdo al límite y número de página
            $assignedProjects_db = Project::model()->findAllByPk(
                    $projects_owner_ids, 
                    array('order'=>'t.modified DESC','limit'=>$limit,'offset'=>($page_assigned-1)*$limit)
            );
            
            foreach($assignedProjects_db as $p)
            {
                $assignedProjects[] = $this->parseSimpleProject($p);
            }
        }
        $result [] = array('id'=>1, 'name' => 'Mis Proyectos', 'projects' => $assignedProjects,'projects_count' => count($assignedProjects), 'pages_count' => $total_pages_assigned,'page_actual' => $page_assigned);
        
        /**
         * Buscar Otros Proyectos del mismo grado
         */
        /*
        $sign = Sign::model()->with(array('room' => array('select' => 'level_id')))->together()->findAllByAttributes(array('user_id' => $userId, 'year_id' => $this->controller->year()), array('select' => 'user_id'));
        
        if($sign)
        {
            $rooms = Functions::list_pluck($sign, "room_id");
            $levels = Level::model()->with(array('rooms' => array('select' => 'id') ))->findAll('rooms.id IN (' . implode(", ", $rooms) . ')');
            $levels_ids = Functions::list_pluck($levels, "id");
            
            $condition = "t.level_id IN (". implode(", ", $levels_ids).") ";
            if($projects_owner_ids)
            {
                $condition = "t.id NOT IN (". implode(", ", $projects_owner_ids).") ";
            }
            $params = array();
            //Contar el total de otros Proyectos
            $total_pages_latest = ceil(Project::model()->count(array('condition' => $condition,'params' => $params)) / $limit);
            $page_latest = min(intval($page), $total_pages_latest);
            $latestProjects_db = Project::model()->findAll(
                    array('condition' => $condition,'params' => $params, 'order'=>'t.modified DESC', 'limit' => $limit, 'offset' => ($page_latest-1)*$limit)
            );
            foreach($latestProjects_db as $p)
            {
                $latestProjects[] = $this->parseSimpleProject($p);
            }
            $result [] = array('id'=>2, 'name' => 'Otros Proyectos', 'projects' => $latestProjects,'projects_count' => count($latestProjects), 'pages_count' => $total_pages_latest,'page_actual' => $page_latest);
        }
        */
        return $result;
    }
    
    function loadProjectsByAssigned($limit=10,$page=1,$school_id=0,$level_id=0, $search_text = "")
    {
        $user = Yii::app()->user;
        if($user->checkAccess("admin"))
        {
            return $this->loadProjectsByAssigned_Admin($limit,$page,$school_id,$level_id, $search_text);
        }
        else if($user->checkAccess("creator") || Yii::app()->user->checkAccess("coordinator"))
        {
            return $this->loadProjectsByAssigned_Coordinator($limit,$page,$school_id,$level_id, $search_text);
        }
        else if($user->checkAccess("instructor"))
        {
            return $this->loadProjectsByAssigned_Instructor($limit,$page,$school_id,$level_id, $search_text);
        }
        else if($user->checkAccess("student"))
        {
            return $this->loadProjectsByAssigned_Student($limit,$page,$school_id,$level_id, $search_text);
        }
    }

    
    function loadProjectsByType_Admin($limit=10,$page=1,$school_id=0,$level_id=0, $search_text="")
    {
        $result = array();
        
        $types_db = ProjectType::model()->findAll(); $max = 1;
        foreach($types_db as $t)
        {
            $id = intval($t['id']);
            if($id>$max) $max = $id; 
            $result[$t['id']] = array('id'=>$id, 'name' => $t['name'], 'projects' => array());
        }
        
        $total_pages = 0; $page_n = $page;
        
        foreach($types_db as $t)
        {
            $page_n = $page;
            $t_id = $t['id'];

            $attr = array('type_id' => $t['id'], 'year_id' => $this->controller->year());
            if($school_id>0)$attr['school_id'] = $school_id;
            if($level_id>0)$attr['level_id'] = $level_id;

            $count_projects = Project::model()->countByAttributes($attr);
            $total_pages = ceil($count_projects / $limit);
            if($page_n > $total_pages) $page_n = $total_pages;
            $projects_db = Project::model()->findAllByAttributes($attr, array('order'=>'t.modified DESC', 'limit' => $limit, 'offset' => ($page_n-1)*$limit) );
            foreach($projects_db as $p)
                $result[$t_id]['projects'] []= $this->parseSimpleProject($p);

            $result[$t_id]['page_actual'] = $page_n;
            $result[$t_id]['pages_count'] = $total_pages;
            $result[$t_id]['projects_count'] = count($projects_db);
            $result[$t_id]['projects_total'] = $count_projects;
        }

        $condition = "`type_id` IS NULL AND year_id=:yearId ";
        $params = array(':yearId' => $this->controller->year());
        if($school_id>0)
        {
            $condition .= ' AND t.school_id = :schoolId';
            $params[':schoolId'] = $school_id;
        }
        if($level_id>0)
        {
            $condition .= ' AND t.level_id = :levelId';
            $params[':levelId'] = $level_id;
        }
        $_crit = new CDbCriteria(); 
        $_crit->condition = $condition; $_crit->params = $params;
        $count_others_projects = Project::model()->count($_crit);
        $total_pages = ceil($count_others_projects / $limit);
        $page_n = $page;
        if($total_pages>0)
        {
            $result['otros'] = array('id'=>$max+1, 'name' => 'Otros', 'projects' => array());

            if($page_n > $total_pages) $page_n = $total_pages;
            $result['otros']['page_actual'] = $page_n;
            $result['otros']['pages_count'] = $total_pages;


            $_oCrit = new CDbCriteria(); 
            $_oCrit->condition = $condition; $_oCrit->params = $params;
            $_oCrit->order = 't.modified DESC'; $_oCrit->limit = $limit; $_oCrit->offset = ($page_n-1)*$limit;
            $withOutType = Project::model()->findAll($_oCrit);
            $result['otros']['projects_count'] = count($withOutType);
            $result['otros']['projects_total'] = $count_others_projects;
            foreach($withOutType as $p)
                $result['otros']['projects'] []= $this->parseSimpleProject($p);
        }
        return array_values($result);
    }
    
    function loadProjectsByType_Coordinator($limit=10,$page=1,$school_id=0,$level_id=0, $search_text)
    {
        $result = array();
        $userId = Yii::app()->user->dbid;
        
        $types_db = ProjectType::model()->findAll(); $max = 1;
        foreach($types_db as $t)
        {
            $id = intval($t['id']);
            if($id>$max) $max = $id; 
            $result[$t['id']] = array('id'=>$id, 'name' => $t['name'], 'projects' => array());
        }
        
        $schools_ids = Functions::list_pluck(SchoolManager::model()->findAllByAttributes(array('user_id' => $userId)), 'school_id');
        
            $total_pages = 0; $page_n = $page;
            foreach($types_db as $t)
            {
                $page_n = $page;
                $t_id = $t['id'];

                $condition = "type_id=:typeId AND year_id=:yearId AND ( ";
                if($schools_ids)
                {
                    $condition .= "(school_id IN (". implode(", ", $schools_ids)."))  OR ";
                }
                $condition .=  "(creator_id=:userId) )";
                $params = array('typeId' => $t_id,':yearId' => $this->controller->year(), ':userId' => $userId);
                if($school_id>0)
                {
                    $condition .= ' AND t.school_id = :schoolId';
                    $params[':schoolId'] = $school_id;
                }
                if($level_id>0)
                {
                    $condition .= ' AND level_id = :levelId';
                    $params[':levelId'] = $level_id;
                }
                if(strlen($search_text) > 0 )
                {
                    $condition .= " AND (t.title LIKE :searchText OR t.description LIKE :searchText) ";
                    $params[':searchText'] = "%$search_text%";
                }
                $count_projects = Project::model()->count( array('condition' => $condition, 'params' => $params));
                $total_pages = ceil($count_projects / $limit);
                if($page_n > $total_pages) $page_n = $total_pages;
                $projects_db = Project::model()->findAll( array('condition' => $condition, 'params' => $params, 'order'=>'t.modified DESC', 'limit' => $limit, 'offset' => ($page_n-1)*$limit) );
                foreach($projects_db as $p)
                {
                    $result[$t_id]['projects'] []= $this->parseSimpleProject($p);
                }
                $result[$t_id]['page_actual'] = $page_n;
                $result[$t_id]['pages_count'] = $total_pages;
                $result[$t_id]['projects_count'] = count($projects_db);
                $result[$t_id]['projects_total'] = $count_projects;
            }

            $condition = "`type_id` IS NULL AND year_id=:yearId AND ( ";
            if($schools_ids)
            {
                $condition .= "(school_id IN (". implode(", ", $schools_ids)."))  OR ";
            }
            $condition .=  "(creator_id=:userId) )";
            $params = array(':yearId' => $this->controller->year(), ':userId' => $userId);
            if($school_id>0)
            {
                $condition .= ' AND t.school_id = :schoolId';
                $params[':schoolId'] = $school_id;
            }
            if($level_id>0)
            {
                $condition .= ' AND t.level_id = :levelId';
                $params[':levelId'] = $level_id;
            }
            if(strlen($search_text) > 0 )
            {
                $condition .= " AND (t.title LIKE :searchText OR t.description LIKE :searchText) ";
                $params[':searchText'] = "%$search_text%";
            }
            $_crit = new CDbCriteria(); 
            $_crit->condition = $condition; $_crit->params = $params;
            $count_others_projects = Project::model()->count($_crit);
            $total_pages = ceil($count_others_projects / $limit);
            $page_n = $page;
            if($total_pages>0)
            {
                $result['otros'] = array('id'=>$max+1, 'name' => 'Otros', 'projects' => array());

                if($page_n > $total_pages) $page_n = $total_pages;
                $result['otros']['page_actual'] = $page_n;
                $result['otros']['pages_count'] = $total_pages;


                $_oCrit = new CDbCriteria(); 
                $_oCrit->condition = $condition; $_oCrit->params = $params;
                $_oCrit->order = 't.modified DESC'; $_oCrit->limit = $limit; $_oCrit->offset = ($page_n-1)*$limit;
                $withOutType = Project::model()->findAll($_oCrit);
                $result['otros']['projects_count'] = count($withOutType);
                $result['otros']['projects_total'] = $count_others_projects;
                foreach($withOutType as $p)
                    $result['otros']['projects'] []= $this->parseSimpleProject($p);
            }
        
        return array_values($result);
    }
    
    function loadProjectsByType_Instructor($limit=10,$page=1,$school_id=0,$level_id=0, $search_text)
    {
        $result = array();
        $userId = Yii::app()->user->dbid;
        
        $types_db = ProjectType::model()->findAll(); $max = 1;
        foreach($types_db as $t)
        {
            $id = intval($t['id']);
            if($id>$max) $max = $id; 
            $result[$t['id']] = array('id'=>$id, 'name' => $t['name'], 'projects' => array());
        }
        
        $condition = "t.year_id = :yearId AND users.id = :userId ";
        $params = array(':userId' => $userId, ':yearId' => $this->controller->year() );
        $projects_owner = Project::model()
                ->with(array('users' => array('select' => 'id')))->together()
                ->findAll(array('condition' => $condition,'params' => $params, 'select'=>'id'));
        $project_ids = Functions::list_pluck($projects_owner, "id");
        
        foreach($types_db as $t)
        {
            $page_n = $page;
            $t_id = $t['id'];
            $count_projects = Project::model()->countByAttributes(array('id' => $project_ids,'type_id'=>$t['id']));
            $total_pages = ceil($count_projects / $limit);
            if($page_n > $total_pages) $page_n = $total_pages;
            $projects_db = Project::model()->findAllByAttributes(array('id' => $project_ids,'type_id'=>$t_id), array('order'=>'t.modified DESC', 'limit' => $limit, 'offset' => ($page_n-1)*$limit) );
            foreach($projects_db as $p)
                $result[$t_id]['projects'] []= $this->parseSimpleProject($p);

            $result[$t_id]['page_actual'] = $page_n;
            $result[$t_id]['pages_count'] = $total_pages;
            $result[$t_id]['projects_count'] = count($projects_db);
            $result[$t_id]['projects_total'] = $count_projects;
        }
        if($project_ids)
        {
            $_crit = new CDbCriteria(); $_crit->condition = '`type_id` IS NULL AND t.id IN ('.  implode(",", $project_ids).')';
            $count_others_projects = Project::model()->count($_crit);
            $total_pages = ceil($count_others_projects / $limit);
            $page_n = $page;
            if($total_pages>0)
            {
                $result['otros'] = array('id'=>$max+1, 'name' => 'Otros', 'projects' => array());

                if($page_n > $total_pages) $page_n = $total_pages;
                $result['otros']['page_actual'] = $page_n;
                $result['otros']['pages_count'] = $total_pages;

                $_oCrit = new CDbCriteria(); $_oCrit->condition = '`type_id` IS NULL AND t.id IN ('.  implode(",", $project_ids).')'; 
                $_oCrit->order = 't.modified DESC'; $_oCrit->limit = $limit; $_oCrit->offset = ($page_n-1)*$limit;
                $withOutType = Project::model()->findAll($_oCrit);
                $result['otros']['projects_count'] = count($withOutType);
                $result['otros']['projects_total'] = $count_others_projects;
                foreach($withOutType as $p)
                    $result['otros']['projects'] []= $this->parseSimpleProject($p);
            }
        }
        return array_values($result);
    }
    
    function loadProjectsByType_Student($limit=10,$page=1,$school_id=0,$level_id=0, $search_text)
    {
        $result = array();
        $userId = Yii::app()->user->dbid;
        
        $types_db = ProjectType::model()->findAll(); $max = 1;
        foreach($types_db as $t)
        {
            $id = intval($t['id']);
            if($id>$max) $max = $id; 
            $result[$t['id']] = array('id'=>$id, 'name' => $t['name'], 'projects' => array());
        }
        
        $condition = "t.year_id = :yearId AND users.id = :userId ";
        $params = array(':userId' => $userId, ':yearId' => $this->controller->year() );
        $projects_owner = Project::model()
                ->with(array('teams' => array('select' => 'id'), 'teams.users' => array('select' => 'user.id')))->together()
                ->findAll(array('condition' => $condition,'params' => $params, 'select'=>'id'));
        $project_ids = Functions::list_pluck($projects_owner, "id");
        
        foreach($types_db as $t)
        {
            $page_n = $page;
            $t_id = $t['id'];
            $count_projects = Project::model()->countByAttributes(array('id' => $project_ids,'type_id'=>$t['id']));
            $total_pages = ceil($count_projects / $limit);
            if($page_n > $total_pages) $page_n = $total_pages;
            $projects_db = Project::model()->findAllByAttributes(array('id' => $project_ids,'type_id'=>$t_id), array('order'=>'t.modified DESC', 'limit' => $limit, 'offset' => ($page_n-1)*$limit) );
            foreach($projects_db as $p)
                $result[$t_id]['projects'] []= $this->parseSimpleProject($p);

            $result[$t_id]['page_actual'] = $page_n;
            $result[$t_id]['pages_count'] = $total_pages;
            $result[$t_id]['projects_count'] = count($projects_db);
            $result[$t_id]['projects_total'] = $count_projects;
        }
        if($project_ids)
        {
            $_crit = new CDbCriteria(); $_crit->condition = '`type_id` IS NULL AND t.id IN ('.  implode(",", $project_ids).')';
            $count_others_projects = Project::model()->count($_crit);
            $total_pages = ceil($count_others_projects / $limit);
            $page_n = $page;
            if($total_pages>0)
            {
                $result['otros'] = array('id'=>$max+1, 'name' => 'Otros', 'projects' => array());

                if($page_n > $total_pages) $page_n = $total_pages;
                $result['otros']['page_actual'] = $page_n;
                $result['otros']['pages_count'] = $total_pages;

                $_oCrit = new CDbCriteria(); $_oCrit->condition = '`type_id` IS NULL AND t.id IN ('.  implode(",", $project_ids).')'; 
                $_oCrit->order = 't.modified DESC'; $_oCrit->limit = $limit; $_oCrit->offset = ($page_n-1)*$limit;
                $withOutType = Project::model()->findAll($_oCrit);
                $result['otros']['projects_count'] = count($withOutType);
                $result['otros']['projects_total'] = $count_others_projects;
                foreach($withOutType as $p)
                    $result['otros']['projects'] []= $this->parseSimpleProject($p);
            }
        }
        return array_values($result);
    }
    
    function loadProjectsByType($limit=10,$page=1,$school_id=0,$level_id=0, $search_text)
    {
        
        $user = Yii::app()->user;
        
        if($user->checkAccess("admin") )
        {
            return $this->loadProjectsByType_Admin($limit,$page,$school_id,$level_id, $search_text);
        }
        else if($user->checkAccess("creator") || Yii::app()->user->checkAccess("coordinator"))
        {
            return $this->loadProjectsByType_Coordinator($limit,$page,$school_id,$level_id, $search_text);
        }
        else if($user->checkAccess("instructor"))
        {
            return $this->loadProjectsByType_Instructor($limit,$page,$school_id,$level_id, $search_text);
        }
        else if($user->checkAccess("student"))
        {
            return $this->loadProjectsByType_Student($limit,$page,$school_id,$level_id, $search_text);
        }
        return;
        
        if(Yii::app()->user->checkAccess("creator") || Yii::app()->user->checkAccess("coordinator"))
        {
            
        }
        else if(Yii::app()->user->checkAccess("instructor"))
        {
            $userId = Yii::app()->user->dbid;
            $school_id = User::model()->with('rooms.school')->together()->findByAttributes(array('id' => $userId))->rooms[0]->school->id;
            
            $projects_admin = ProjectManager::model()->findAllByAttributes(array('manager_id'=>$userId),array('select'=>'project_id'));
            $projects_admin_ids = Functions::list_pluck($projects_admin, "project_id");
            
            $projects_owner = Project::model()->findAllByAttributes(array('creator_id'=>$userId),array('select'=>'id'));
            $projects_manager_ids = Functions::list_pluck($projects_owner, "id");
            
            $user_levelsId = $this->controller->_getLevelIdByUserId($userId);
            $projects_level = Project::model()->findAllByAttributes(array('level_id'=>$user_levelsId,'school_id'=>$school_id), array('select'=>'id', 'order'=>'t.modified DESC'));
            $projects_level_ids = Functions::list_pluck($projects_level, "id");
            
            $my_projects_ids = array_merge($projects_admin_ids,$projects_manager_ids,$projects_level_ids);

            foreach($types_db as $t)
            {
                $page_n = $page;
                $t_id = $t['id'];
                $count_projects = Project::model()->countByAttributes(array('id' => $my_projects_ids,'type_id'=>$t['id']));
                $total_pages = ceil($count_projects / $limit);
                if($page_n > $total_pages) $page_n = $total_pages;
                $projects_db = Project::model()->with('level.level','school','creator')->together()->findAllByAttributes(array('id' => $my_projects_ids,'type_id'=>$t_id), array('order'=>'t.modified DESC', 'limit' => $limit, 'offset' => ($page_n-1)*$limit) );
                foreach($projects_db as $p)
                    $result[$t_id]['projects'] []= $this->parseSimpleProject($p);

                $result[$t_id]['page_actual'] = $page_n;
                $result[$t_id]['pages_count'] = $total_pages;
                $result[$t_id]['projects_count'] = count($projects_db);
                $result[$t_id]['projects_total'] = $count_projects;
            }
            
            $_crit = new CDbCriteria(); $_crit->condition = '`type_id` IS NULL';
            if(count($my_projects_ids))  $_crit->condition .= ' AND id IN ('.  implode(",", $my_projects_ids).')';
            $count_others_projects = Project::model()->count($_crit);
            $total_pages = ceil($count_others_projects / $limit);
            $page_n = $page;
            if($total_pages>0)
            {
                $result['otros'] = array('id'=>$max+1, 'name' => 'Otros', 'projects' => array());
                
                if($page_n > $total_pages) $page_n = $total_pages;
                $result['otros']['page_actual'] = $page_n;
                $result['otros']['pages_count'] = $total_pages;

                $_oCrit = new CDbCriteria(); $_oCrit->condition = '`type_id` IS NULL';
                if(count($my_projects_ids))  $_oCrit->condition .= ' AND t.id IN ('.  implode(",", $my_projects_ids).')'; 
                $_oCrit->order = 't.modified DESC'; $_oCrit->limit = $limit; $_oCrit->offset = ($page_n-1)*$limit;
                $withOutType = Project::model()->with('level.level','school','creator')->together()->findAll($_oCrit);
                $result['otros']['projects_count'] = count($withOutType);
                $result['otros']['projects_total'] = $count_others_projects;
                foreach($withOutType as $p)
                    $result['otros']['projects'] []= $this->parseSimpleProject($p);
            }
        }
        else if(Yii::app()->user->checkAccess("student"))
        {
            
        }
        
        return array_values($result);
    }
    
    function _getRoomByProject($project)
    {
        $result = array();
        $rooms = $project->rooms;
        foreach($rooms as $s)
        {
            if($s->level_id == $project->level_id)
            {
                $result[] = array('id'=> $s->id,'name'=>$s->name);
            }
        }
        return $result;
    }
    
    function parseSimpleProject($p)
    {
        $user_id = Yii::app()->user->dbid;
        $result = array();
        $result['id'] = $p['id'];
        $result['title'] = $p['title'];
        $result['slug'] = $p['slug'];
        $result['description'] = FileHelper::changeImageSrc($p['description']);
        
        $result['created'] = date('d/m/Y h:i:sa', $p['created']);
        $result['created_ago'] = $this->controller->_human_time_diff($p['created']);
        $result['modified'] = date('d/m/Y h:i:sa', $p['modified']);
        $result['modified_ago'] = $this->controller->_human_time_diff($p['modified']);
        $result['start_date'] = date('d/m/Y h:i:sa', $p['start_date']);
        $result['end_date'] = date('d/m/Y h:i:sa', $p['end_date']);
        
        if($image_db = $p->image)
        {
            $result['image_cdn'] = $this->controller->config->uriCDN . "/uploads/" . $image_db->md5 . "/" . $image_db->original_name;
        }
            
        if($level = $p['level'])
        {
            $result['level_name'] = $level->name."° Grado de ".$level->level_name;
            $result['grade_id'] = $level->id;
        }
        
        $project_school_id = 0;
        if($school = $p['school'])
        {
            $result['school_name'] = $school->name;
            $project_school_id = $school->id;
        }
        
        if($rooms = $p['rooms'])
        {
            $r = Functions::list_pluck($rooms, "name");
            $result['rooms_names'] = join(", ", $r);
            $result['rooms_ids'] = Functions::list_pluck($rooms, "id");
        }
        
        $result['creator_name'] = $p['creator']->firstname . " " . $p['creator']->lastname;
        
        if($managers = $p['users'])
        {
            $names = array();
            foreach($p['users'] as $u)
            {
                $names[] = $u->firstname . " " . $u->lastname;
            }
            $result['managers_names'] = join(", ", $names);
        }
        
            
        $now = time();
        if( $now < $p['start_date'] )
        {
            $result['state'] = 0;
            $result['state_ago'] = $this->controller->_human_time_diff($now,$p['start_date']);
            $result['state_date'] = date( 'd/m/Y h:i:sa', $p['start_date']);
        }
        else if( $now < $p['end_date'] )
        {
            $result['state'] = 1;
            $result['state_ago'] = $this->controller->_human_time_diff($now,$p['end_date']);
            $result['state_date'] = date( 'd/m/Y h:i:sa', $p['end_date']);
        }
        else
        {
            $result['state'] = 2;
            $result['state_ago'] = $this->controller->_human_time_diff($p['end_date'],$now);
            $result['state_date'] = date( 'd/m/Y h:i:sa', $p['end_date']);
        }

        $is_principal_manager = false;
        if($project_school_id>0 && Yii::app()->user->checkAccess("manageOwnSchoolProjects"))
        {
            $is_principal_manager = SchoolManager::model()->countByAttributes(array('school_id' => $project_school_id, 'user_id' => $user_id)) > 0;
        }
        
        $result['is_owner'] = ($user_id == $p['creator_id']) || $is_principal_manager || Yii::app()->user->checkAccess("admin");
        
        $result['is_manager'] = $result['is_owner'] || (ProjectManager::model()->countByAttributes(array('manager_id'=>$user_id,'project_id'=> $p['id']))?TRUE:FALSE);
        
        return $result;
    }
}