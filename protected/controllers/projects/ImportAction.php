<?php

class ImportAction extends CAction
{
    public $project_id = 0;
    
    public function run()
    {
        if(!Yii::app()->user->checkAccess("admin") && !Yii::app()->user->checkAccess("creator")  && !Yii::app()->user->checkAccess("coordinator"))
        {
            $this->controller->error = "No tiene permisos para realizar esta acción";
        }
        else if(isset($_FILES['files']) && ($file = $_FILES['files']))
        {
            try
            {

                $tmp_name = reset($file['tmp_name']);
                $type = reset($file['type']);
                $filename = reset($file['name']);
                $pathinfo = pathinfo($filename);
                
                $isZip = false;
                $accepted_types = array('application/zip', 'application/x-zip-compressed', 'multipart/x-zip', 'application/x-compressed','application/octet-stream');
                foreach($accepted_types as $mime_type) {
                        if($mime_type == $type) {
                                $isZip = true;
                                break;
                        } 
                }
                $continue = strtolower($pathinfo['extension']) == 'zip' ? true : false;
                if(!$isZip || !$continue) {
                    $this->controller->error = ("El archivo que ha subido no es un .zip.");
                }
                else
                {
                    
                    $new_name = implode("_",array(rand(1000,9999), time()));
                    $new_file_name = $new_name . '_z';
                    $dirpath = Yii::app()->getRuntimePath().DIRECTORY_SEPARATOR.$new_file_name.DIRECTORY_SEPARATOR;
                    $file_path = $dirpath . $new_file_name;
                    if (is_dir($dirpath))  $this->removeDirectory($dirpath);
                    mkdir($dirpath, 0776);
                    $zip = new ZipArchive();
                    if ($zip->open($tmp_name) === true)
                    {
                            $zip->extractTo($dirpath);
                            $zip->close();
                            unlink($tmp_name);
                    }
                    $fileModusProject = "";
                    foreach (glob($dirpath."*.mp") as $filename)
                        $fileModusProject = $filename;
                    
                    if(file_exists($fileModusProject))
                    {
                        $size = filesize($fileModusProject);
                        $fH = fopen($fileModusProject,"r");   // File handle
                        $data = fread($fH,$size);  // Read data from file handle
                        fclose($fH); 
                        $content = json_decode($data,true);
                        $this->import($content, $dirpath);
                        $this->controller->success = true;
                    }
                    else
                    {
                        $this->controller->error = ("El archivo subido no es válido.");
                    }
                    if(file_exists($dirpath))
                        $this->removeDirectory($dirpath);
                }
            }
            catch(Exception $ex)
            {
                $this->controller->error = "Ocurrio un error desconocido, al llevar a cabo la operación de importación. intentelo nuevamente.";
                $this->controller->debug = $ex->getMessage();
            }
        }
        else
        {
            $this->controller->error = "No se ha subido el archivo, o es demasiado grande.";
        }
    }
    
    private function parseContent($text, $dirpath)
    {
        $match = array();
        $img_projects_patt = '/src\=\"(\/img\/projects\/(\d+_\d+_\d+_.\..{3,4}))\"/';
        preg_match_all($img_projects_patt, $text, $match);
        $total = array();
        if($match[1] && $match[2])
        {
            $srcs = $match[1];
            $names = $match[2];
            foreach ($names as $i => $name)
            {
                $image = FileHelper::upload($dirpath.$name, "", "image", $name);
                if($image)
                {
                    $new_name = "/uploads/" . $image->md5 . "/" . $image->original_name;
                    if(isset($srcs[$i]))
                    {
                        $text = str_replace($srcs[$i],$new_name,$text);
                    }
                }
            }
        }
        /*
        $match_uploads = array();
        $img_uploads_patt  = '/src\=\"(\/uploads\/(\d+_\d+_\d+_.\..{3,4}))\"/';
        preg_match_all($img_uploads_patt, $text, $match_uploads);
         * 
         */
        return $text;
    }
    
    private function import($model, $dirpath)
    {
        $user = Yii::app()->user;
        $transaction = Yii::app()->db->beginTransaction();
        $now = time();
        
        $project = new Project;
        $project->title         = $model['title'];
        
        $project->start_date    = $model['start_date'];
        $project->end_date      = $model['end_date'];
        
        
        if( isset($model['school']) && ($schoolId = $this->getSchoolId($model['school'])))
        {
            if( $user->checkAccess('admin') || (SchoolManager::model()->countByAttributes(array('user_id' => $user->dbid, 'school_id' => $schoolId)) > 0) )
            {
                $project->school_id = $schoolId;
            }
        }
        if($project->school_id)
        {
            $grade = NULL;
            if($grade = $model['grade'])
            {
                $grade_name = $grade['name'];
                $level_name = $grade['level'];
                $project->level_id = $this->getLevelId($grade_name, $level_name);
            }
        }
        
        if(isset($model['type']))
        {
            $type = $model['type'];
            if(is_array($type) && array_key_exists("id", $type))
            {
                $project->type_id = $model['type']['id'];
            }
            else if(is_string($type))
            {
                $type_db = ProjectType::model()->findByAttributes(array('name' => $model['type']));
                if($type_db)
                {
                    $project->type_id = $type_db->id;
                }
            }
        }
            
        
        $project->creator_id = Yii::app()->user->dbid;
        $project->created = $now;
        $project->modified = $now;
        
        $project->year_id = $this->controller->year();
        $project->active = 1;
        $project->slug = Functions::create_slug($model['title']);
        
        $project->description   = $this->parseContent($model['description'],$dirpath);
        

        //Importar imagen
        if(isset($model['image']))
        {
            $name = (isset($project['image_name'])?$project['image_name']:"");
            $image_db = FileHelper::upload($dirpath . $model['image'], $name, "image", $model['image']);
            if($image_db)
            {
                $project->image_id = $image_db->id;
            }
        }
        
        $save = $project->save();
        if($save)
        {
            $this->project_id = $project->id;
            $name = $project->title;
            CLog::logProject("importProject", "Se creó el proyecto $name", $project->attributes);
            
            //Guardar Secciones
            if($project->school_id && ($grade = $model['grade']))
            {
                $rooms = isset($grade['sections'])?$grade['sections']:(isset($grade['rooms'])?$grade['rooms']:array());
                $this->addRooms($project->id, $project->school_id, $project->level_id, $rooms);
            }

            //Guardar Fases
            $phases_ids = array();
            $phases = $model['phases'];
            foreach ($phases as $phase)
            {
                $p = new Phase;
                
                $p->ord                 = isset($phase['ord'])?$phase['ord']:$order;
                $p->project_id          = $project->id;
                $p->name                = isset($phase['name'])?$phase['name']:'';
                $p->description         = isset($phase['description'])?  $this->parseContent($phase['description'],$dirpath):'';
                
                
                if(isset($phase['image']))
                {
                    $image_name = isset($phase['image'])?$phase['image']:"";
                    $phase_image = FileHelper::upload($dirpath . $phase['image'], $image_name, "image",$phase['image']);
                    $p->image_id = $phase_image->id;
                }
                
                
                
                
                if(isset($phase['options']))
                {
                    $options = unserialize($phase['options']);
                    $p->can_answer  = isset($options['can_answer']) && $options['can_answer']?1:0;
                    $p->can_mark    = isset($options['can_mark'])&&$options['can_mark']?1:0;
                    $p->text_answer = isset($options['text_answer'])?$options['text_answer']:"";
                    $p->showfinal_others  = isset($options['showfinal_others'])&&$options['showfinal_others']?1:0;
                }
                else
                {
                    $p->can_answer  = isset($phase['can_answer']) && $phase['can_answer']?1:0;
                    $p->can_mark    = isset($phase['can_mark'])&&$phase['can_mark']?1:0;
                    $p->text_answer = isset($phase['text_answer'])?$phase['text_answer']:"";
                    $p->showfinal_others  = isset($phase['showfinal_others'])&&$phase['showfinal_others']?1:0;
                }
                
                $p->created             = $now;
                $p->modified            = $now;
                
                $template_phase_id = isset($phase['template_phase_id'])?$phase['template_phase_id']:(isset($phase['default_phase_id'])?$phase['default_phase_id']:NULL);
                if($template_phase_id && TemplatePhase::model()->findByPk($template_phase_id))
                {
                    $p->template_phase_id = $template_phase_id;
                }
                
                if($p->save())
                {
                    $phase['id'] = $p->id;
                    $phase_name = $p->name;
                    CLog::logProject("importPhase", "Se creó la fase $phase_name", $p->attributes);
                
                    $phases_ids[] = array("id"=>$p->id,"ord"=>$p->ord);
                    
                    //Guardar adjuntos
                    $aPhase = array('id' => $phase['id'], 'name' => $phase_name);
                    $attachments = isset($phase['attachments'])?$phase['attachments']:array();
                    if($attachments)
                    {
                        $to_add = array();
                        foreach ($attachments as $attachment)
                        {
                            $filename = isset($attachment['filename'])?$attachment['filename']:$attachment['md5'];
                            $file_db = FileHelper::upload($dirpath . $filename, $attachment['originalname'], "file");
                            if($file_db)
                            {
                                $to_add []= $file_db->id;
                            }
                        }
                        $aPhase['attachments'] = $to_add;
                        ProjectHelper::updatePhaseAttachments($aPhase);
                    }

                    //Guardar Herramientas y Estrategias
                    $strategies = isset($phase['strategies'])?$phase['strategies']:array();
                    if($strategies)
                    {
                        $strategies_ids  = array();
                        foreach ($strategies as $strategy_name)
                        {
                            $strategy = Strategy::model()->findByAttributes(array('name'=>$strategy_name));
                            if($strategy)
                            {
                                $strategies_ids []= $strategy->id;;
                            }
                        }
                        $aPhase['strategies'] = $strategies_ids;
                        ProjectHelper::updatePhaseStrategies($aPhase);
                    }

                    $tools = isset($phase['tools'])?$phase['tools']:array();
                    if($tools)
                    {
                        $tools_ids = array();
                        foreach ($tools as $tool_name)
                        {
                            $tool = Tool::model()->findByAttributes(array('name'=>$tool_name));
                            if($tool)
                            {
                                $tools_ids []= $tool->id;
                            }
                        }
                        $aPhase['tools'] = $tools_ids;
                        ProjectHelper::updatePhaseTools($aPhase);
                    }

                    //Guardar criterios de evaluacion
                    
                    if($p->template_phase_id)
                    {
                        $rules_ids  = array();
                        $templateRules = TemplateRule::model()->findAllByAttributes(array('template_phase_id'=> $p->template_phase_id),array('order'=>"ord"));
                        if($templateRules)
                        {
                            foreach($templateRules as $templateRule)
                            {
                                $rule = new Rule();
                                $rule->phase_id = $p->id;
                                $rule->ord      = $templateRule->ord;
                                $rule->score    = $templateRule->score;
                                $rule->rule     = $templateRule->rule;

                                $rule->save();
                                $rules_ids[] = $rule->id;
                            }
                            $count = count($templateRules);
                            $s = $count>1?"s":"";
                            CLog::logProject("createPhaseRules", "Se agregó $count criterio$s de calificación", $rules_ids);
                        }
                    }
                }else{
                    $this->controller->error = true;
                }
            }
        }
        else
        {
            $errors = $project->getErrors();
            throw  new Exception(print_r($errors, true));
        }
        
        if(!$this->controller->error && $project->id && $project->slug)
        {
            $transaction->commit();
            return TRUE;
        }
        else
        {
            $transaction->rollback();
            return FALSE;
        }
    }
    
    private function addRooms($project_id, $school_id, $level_id, $rooms )
    {
        $rooms_ids = array();
        foreach ($rooms as $room_name)
        {
            $room = Room::model()->findByAttributes(array('name' => $room_name, 'school_id' => $school_id, 'level_id' => $level_id, 'year_id' => $this->controller->year()), array('select' => 'id'));
            if($room)
            {
                $rooms_ids []= $room->id;
            }
        }
        ProjectHelper::updateRooms($rooms_ids);
    }


    private function getSchoolId($school_name)
    {
        $school = School::model()->findByAttributes(array('name' => $school_name));
        if(!$school)
        {
            $school = new School();
            $school->name = $school_name;
            $school->save();
        }
        $school_id = $school->id;
        return $school_id;
    }
    
    private function getLevelId($grade_name,$level_name)
    {
        $grade = Level::model()->findByAttributes(array('name' => $grade_name, 'level_name' => $level_name));
        $grade_id = $grade->id;
        return $grade_id;
    }
    
    private function removeDirectory($path)
    {
        $path = rtrim( strval( $path ), '/' ) ;

        $d = dir( $path );

        if( ! $d )
            return false;

        while ( false !== ($current = $d->read()) )
        {
            if( $current === '.' || $current === '..')
                continue;

            $file = $d->path . '/' . $current;

            if( is_dir($file) )
                $this->removeDirectory($file);

            if( is_file($file) )
                unlink($file);
        }

        rmdir( $d->path );
        $d->close();
        return true;
    }
}