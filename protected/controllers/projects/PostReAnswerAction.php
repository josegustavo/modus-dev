<?php

class PostReAnswerAction extends CAction
{
    function run($project_id, $phase_id, $team_id, $answer_id)
    {
        $user_id = Yii::app()->user->dbid;
        
        $post = Yii::app()->request->getPost("model");
        if($post)
        {
            $model = json_decode($post,true);

            $project_db = Project::model()->findByPk($project_id,array('select' => 'creator_id'));
            $is_owner = ($user_id == $project_db->creator_id) || Yii::app()->user->checkAccess("admin");
            $is_manager = $is_owner || (ProjectManager::model()->countByAttributes(array('manager_id'=>$user_id,'project_id'=>$project_id))?TRUE:FALSE);
        
            if($is_owner || $is_manager)
            {
                $reanswer = new Reanswer();
                $reanswer->author_id      = $user_id;
                $reanswer->answer_id      = $answer_id;
                $reanswer->created        = time();
                $reanswer->reanswer       = FileHelper::repareCDN($model['reanswer']);

                if($reanswer->save())
                {
                    foreach ($model['attachments'] as $attachment)
                    {
                        $a = new ReanswerAttachment;
                        $a->file_id = $attachment['id'];
                        $a->reanswer_id = $reanswer->id;
                        $a->save();
                    }
                    $this->controller->success = $this->controller->_parseReAnswer($reanswer);
                }
                else
                {
                    $this->controller->error = $reanswer->getErrors();
                }
            }
            else
            {
                $this->controller->error = "No tiene permisos para publicar.";
            }
        }
        else
        {
            $this->controller->error = "No se envió correctamente el formulario, intente recargar la página.";
        }
    }
}