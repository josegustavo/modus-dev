<?php

class StudentsController extends Controller
{
    
    public function accessRules()
    {
        return array_merge(parent::accessRules(), array(
            array('allow', 'actions' => array('listAllNoSigned','list','assign','unassign','import','export'),'roles' => array('manageStudents')),
            array('deny', 'users'=>array('*')),
        ));
    }
    
           
    public function actions()
    {
        return array(
            'assign'            =>'application.controllers.students.AssignAction',
            'export'            =>'application.controllers.students.ExportAction',
            'import'            =>'application.controllers.students.ImportAction',
            'list'              =>'application.controllers.students.ListAction',
            'listAllNoSigned'   =>'application.controllers.students.ListAllNoSignedAction',
            'unassign'          =>'application.controllers.students.UnassignAction',
            'upload'            =>'application.controllers.students.UploadAction',
            
        );
    }
    
    
    
}
