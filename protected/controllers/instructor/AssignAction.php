<?php

class AssignAction extends CAction
{
        
    function run($school_id, $grade_id, $user_id)
    {
        $post = Yii::app()->request->getPost("model");
        if(!$post)
        {
            $this->controller->error = "No se enviaron correctamente los datos, vuelva a intentarlo.";
            return;
        }
        
        $decode = json_decode($post, TRUE);
        
        
        if(isset($decode['rooms']))
        {
            $year = $this->controller->year();
            $allRooms = Room::model()->findAllByAttributes(array('year_id' => $year, 'level_id' => $grade_id, 'school_id' => $school_id), array('select' => 'id'));
            $all_rooms_ids = Functions::list_pluck($allRooms, "id");
            
            $signeds = Sign::model()->findAllByAttributes(array('room_id' => $all_rooms_ids, 'year_id' => $year, 'user_id' => $user_id, 'active' => 1), array('select' => 'room_id'));
            
            $old_rooms = Functions::list_pluck($signeds, "room_id");
            $new_rooms = $decode['rooms'];
            $to_delete = array_diff($old_rooms, $new_rooms);
            
            $this->controller->error = NULL;
            
            if(count($to_delete))
            {
                foreach($to_delete as $room_id)
                {
                    try
                    {
                        RoomHelper::unassignInstructor($user_id, $room_id);
                    }
                    catch (Exception $ex)
                    {
                        if(!is_array($this->controller->error)) $this->controller->error = array();
                        $this->controller->error []= $ex->getMessage();
                    }
                }
            }
            
            $to_add = array_diff($new_rooms, $old_rooms);
            if(count($to_add))
            {
                foreach($to_add as $room_id)
                {
                    try
                    {
                        RoomHelper::assignInstructor($user_id, $room_id);
                    }
                    catch (Exception $ex)
                    {
                        if(!is_array($this->controller->error)) $this->controller->error = array();
                        $this->controller->error []= $ex->getMessage();
                    }
                }
            }
            if(!$this->controller->error)
            {
                $this->controller->success = true;
            }
        }
        else
        {
            $this->controller->error  = "No se conoce las secciones que se desea asignar.";
        }

    }
    
}