<?php

class ReadSchoolsAction extends CAction
{
    public function run($id)
    {
        $user = User::model()->findByPk($id);
        $schools = $user->schools;
        $result = array('id' => $user->id, 'name' => $user->firstname . " " . $user->lastname, 'schools' => array());
        
        foreach ($schools as $school)
        {
            $result['schools'][] = array('id' => $school->id, 'name' => $school->name);
        }
        
        $this->controller->success = $result;
    }
}