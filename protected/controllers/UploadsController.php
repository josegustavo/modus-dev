<?php

class UploadsController extends Controller
{
    
    
    public function actionUpload($type = "file")
    {
        $type = $type=="file"?"file":"image";
        
        if(isset($_FILES['files']) && $_FILES['files'])
        {
            try 
            {
                $files = $_FILES['files'];
                $tmp_name = is_array($files['tmp_name'])?reset($files['tmp_name']):$files['tmp_name'];
                if($tmp_name)
                {
                    $original_name = is_array($files['name'])?reset($files['name']):$files['name'];
                    $pathinfo = pathinfo($original_name);
                    $extension = strtolower($pathinfo['extension']);
                    
                    $file_extension_allowed = $this->config->fileExtAllowed;
                    $image_extension_allowed = $this->config->imageExtAllowed;
                    if( ($type=="file" && in_array($extension, $file_extension_allowed)) || ($type=="image" && in_array($extension, $image_extension_allowed)) )
                    {
                        $file_db = FileHelper::upload($tmp_name, $original_name, $type);
                        $md5 = $file_db->md5;
                        if($file_db->properties)
                        {
                            $prop = json_decode($file_db->properties, true);
                            if(array_key_exists('size', $prop))
                            {
                                $result['size'] = $prop['size'];           
                            }
                            if(array_key_exists('width', $prop))
                            {
                                $result['width'] = $prop['width'];           
                            }
                            if(array_key_exists('height', $prop))
                            {
                                $result['height'] = $prop['height'];
                            }
                        }
                        $result['id'] = $file_db->id;
                        $result['original_name'] = $file_db->original_name;
                        $result['full_url'] =  $this->config->uriCDN. "/uploads/" . $md5 . "/" . $original_name;
                        
                        $this->success = $result;
                    }
                    else
                    {
                        $this->error = "No se permite el tipo de archivo \"".$extension."\".";
                    }
                }
                else
                {
                    $this->error = "No subió ningún archivo";
                }
            }
            catch(Exception $ex)
            {
                $this->error = "Ocurrio un error al subir el archivo ";
                if(YII_DEBUG)
                {
                    $this->debug = $ex->getMessage();
                }
            }
        }
        else
        {
            $this->error = "No se permite el archivo";
        }
    }
    
    public function actionDownload($id="",$name="")
    {
        
        @ini_set('error_reporting', E_ALL & ~ E_NOTICE);
        //- turn off compression on the server
        @apache_setenv('no-gzip', 1);
        @ini_set('zlib.output_compression', 'Off');
        
        if(key_exists('HTTP_IF_NONE_MATCH', $_SERVER))
        {
            header("HTTP/1.1 304 Not Modified"); 
            Yii::app()->end();
        }
        
        if(!$id || !$name) 
        {
            throw new CHttpException(404,"El nombre del archivo no es válido.");
        }

        $file_db = File::model()->findByAttributes(array("md5" => $id, 'original_name' => $name));
        if(!$file_db)
        {
            throw new CHttpException(404,"El archivo $name no existe.");
        }
        
        // set the mime type based on extension, add yours if needed.
        $ctype_default = "application/octet-stream";
        if($properties = $file_db->properties)
        {
            $prop = json_decode($properties, TRUE);
            if(isset($prop['mime']))
            {
                $ctype_default = $prop['mime'];
            }
        }
        
        $file_path  = $name;
        $path_parts = pathinfo($file_path);
        $file_name  = $path_parts['basename'];
        $file_ext   = $path_parts['extension'];
        $file_path  = Yii::app()->basePath.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'file'.DIRECTORY_SEPARATOR .  $id;

        // allow a file to be streamed instead of sent as an attachment
        $is_attachment = isset($_REQUEST['stream']) ? false : true;

        // make sure the file exists
        if (is_file($file_path))
        {
            $file_size  = filesize($file_path);
            $file = @fopen($file_path,"rb");
            if ($file)
            {
                header('Cache-Control: max-age=9999999,public,');
                header('Pragma:cache');
                header('Date:Tue, 24 Jan 1989 14:04:14 GMT');
                header('Last-Modified, 24 Jan 1989 14:04:14 GMT');
                header('ETag:"'.$id.'"');
                header("Content-Disposition: attachment; filename=\"$file_name\"");

                // set appropriate headers for attachment or streamed file
                //if ($is_attachment)
                //        header("Content-Disposition: attachment; filename=\"$file_name\"");
                //else
                        header('Content-Disposition: inline;');

                
                
                header("Content-Type: " . $ctype_default);

                    //check if http_range is sent by browser (or download manager)
                if(isset($_SERVER['HTTP_RANGE']))
                {
                    list($size_unit, $range_orig) = explode('=', $_SERVER['HTTP_RANGE'], 2);
                    if ($size_unit == 'bytes')
                    {
                        //multiple ranges could be specified at the same time, but for simplicity only serve the first range
                        //http://tools.ietf.org/id/draft-ietf-http-range-retrieval-00.txt
                        list($range, $extra_ranges) = explode(',', $range_orig, 2);
                    }
                    else
                    {
                        $range = '';
                        header('HTTP/1.1 416 Requested Range Not Satisfiable');
                        exit;
                    }
                }
                else
                {
                    $range = '';
                }

                //figure out download piece from range (if set)
                list($seek_start, $seek_end) = explode('-', $range, 2);

                //set start and end based on range (if set), else set defaults
                //also check for invalid ranges.
                $seek_end   = (empty($seek_end)) ? ($file_size - 1) : min(abs(intval($seek_end)),($file_size - 1));
                $seek_start = (empty($seek_start) || $seek_end < abs(intval($seek_start))) ? 0 : max(abs(intval($seek_start)),0);

                //Only send partial content header if downloading a piece of the file (IE workaround)
                if ($seek_start > 0 || $seek_end < ($file_size - 1))
                {
                        header('HTTP/1.1 206 Partial Content');
                        header('Content-Range: bytes '.$seek_start.'-'.$seek_end.'/'.$file_size);
                        header('Content-Length: '.($seek_end - $seek_start + 1));
                }
                else
                  header("Content-Length: $file_size");

                header('Accept-Ranges: bytes');

                set_time_limit(0);
                fseek($file, $seek_start);

                while(!feof($file)) 
                {
                        print(@fread($file, 1024*8));
                        ob_flush();
                        flush();
                        if (connection_status()!=0) 
                        {
                                @fclose($file);
                                exit;
                        }			
                }

                // file save was a success
                @fclose($file);
                exit;
            }
            else 
            {
                // file couldn't be opened
                throw new CHttpException(500,"El archivo $name no se puede descargar en estos momentos.");
            }
        }
        else
        {
            // file does not exist
            throw new CHttpException(404,"El archivo $name no existe.");
        }
    }

}