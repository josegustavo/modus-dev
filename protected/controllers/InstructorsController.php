<?php

class InstructorsController extends Controller
{
    
    public function accessRules()
    {
        return array_merge(parent::accessRules(), array(
            array('allow', 'actions' => array('list','assign','unassign'),'roles' => array('manageInstructors')),
            array('deny', 'users'=>array('*')),
        ));
    }
    
    public function actions()
    {
        return array(
            'list'=>'application.controllers.instructor.ListAction',
            'assign'=>'application.controllers.instructor.AssignAction',
            'unassign'=>'application.controllers.instructor.UnassignAction',
        );
    }
    
}
