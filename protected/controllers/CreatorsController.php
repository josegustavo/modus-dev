<?php

class CreatorsController extends Controller
{
    
    public function accessRules()
    {
        return array_merge(parent::accessRules(), array(
            array('allow', 'actions' => array('readSchools'),'roles' => array('manageSchoolCreators')),
            array('deny', 'users'=>array('*')),
        ));
    }
    
    public function actions()
    {
        return array(
            'readSchools' => 'application.controllers.creator.ReadSchoolsAction'
        );
    }
}

