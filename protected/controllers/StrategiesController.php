<?php

class StrategiesController extends Controller
{
    public function accessRules()
    {
        return array_merge(parent::accessRules(), array(
            array('allow', 'actions' => array('list','read','create','delete','update'),'roles' => array('manageStrategies')),
            array('allow', 'actions' => array('list'),'roles' => array('viewStrategies')),
            array('deny', 'users'=>array('*')),
        ));
    }
    
    public function actions()
    {
        return array(
            'create'=>'application.controllers.strategies.CreateAction',
            'update'=>'application.controllers.strategies.UpdateAction',
            'delete'=>'application.controllers.strategies.DeleteAction',
        );
    }
    
    public function actionList()
    {
        $strategies = $this->_getStrategies();
        $this->success = $strategies;
    }

    public function actionRead($id=null)
    {
        $result = array();
        $strategies = $this->_getStrategies($id);
        $this->success = reset($strategies);
    }
    
    

  
    private function _getStrategies($strategy_id=NULL)
    {
        $strategies_db = (is_numeric($strategy_id) && $strategy_id>0)?
                            Strategy::model()->findAllByPk($strategy_id):
                            Strategy::model()->findAll();
        
        $strategies = array();
        foreach($strategies_db as $k => $s_db)
        {
            
            
            $strategy = array('id' => $s_db->id, 
                'name' => $s_db->name, 
                'description' => $s_db->description, 
                'modified' => date( 'd-m-Y H:i:s', $s_db->modified ),
                'modified_ago' => $this->_human_time_diff($s_db->modified));
            if($image = $s_db->image)
            {
                $strategy['image_id'] = $image->id;
                $strategy['image_url'] = $this->config->uriCDN . "/uploads/" . $image->md5 . "/" . $image->original_name;
                if(isset($image['properties']))
                {
                    $prop = json_decode($image['properties'], TRUE);
                    if(isset($prop['width']) && isset($prop['height']))
                    {
                        $strategy['image_width'] = $prop['width'];
                        $strategy['image_height'] = $prop['height'];
                    }
                }
            }
            $strategies[] = $strategy;
        }

        return $strategies;
    }
    
}
