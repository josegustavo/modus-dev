<?php

class CoordinatorsController extends Controller
{
    
    public function accessRules()
    {
        return array_merge(parent::accessRules(), array(
            array('allow', 'actions' => array('readSchools'),'roles' => array('manageSchoolCoordinators')),
            array('deny', 'users'=>array('*')),
        ));
    }
    
    public function actions()
    {
        return array(
            'readSchools' => 'application.controllers.coordinator.ReadSchoolsAction'
        );
    }
    
}

