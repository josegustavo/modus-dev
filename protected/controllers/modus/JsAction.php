<?php

class JsAction extends CAction
{
    
    public function run($id, $time, $type)
    {
        ob_get_clean();
        
        header('Cache-Control: max-age=9999999,public,');
        header('Pragma:cache');
        header('Date:Tue, 24 Jan 1989 14:04:14 GMT');
        header('Last-Modified, 24 Jan 1989 14:04:14 GMT');
        header('ETag:"'.$time.'"');
        header('Keep-Alive:timeout=5, max=90');
        header('Access-Control-Allow-Origin: *');
        header('Content-Type: text/javascript');
            
        if(key_exists('HTTP_IF_NONE_MATCH', $_SERVER))
        {
            header("HTTP/1.1 304 Not Modified"); 
        }
        else
        {
            $script_cache = Yii::app()->cache->get($id);
            if(YII_DEBUG)
            {
                echo Cache::generateMinifiedScript($id, $type);
            }
            else if($script_cache && isset($script_cache['time']) && $script_cache['time']==$time) 
            {
                echo $script_cache['cache'];
            }
            else
            {
                echo Cache::generateMinifiedScript($id, $type);
            }
        }
        Yii::app()->end();
    }
}