<?php

class ModusController extends Controller
{
    public $templates = array();
    public $js_included = array();
    public $css_included = array();
    public $js_main = array();
    
    public $year;
    public $years;
    
    public function actions()
    {
        return array(
            'js'=>'application.controllers.modus.JsAction',
        );
    }
    
    
    public function actionIndex()
    {
        
        $user = Yii::app()->user;
        
        $this->templates = Cache::getMinifiedTemplates();
        $this->css_included = Cache::getMinifiedStylesUri();
        $this->js_main = Cache::getMinifiedScriptsMainUri();
        $this->js_included = Cache::getMinifiedScriptsUri();
        $this->years = array();
        
        
        if($user->checkAccess("viewActiveYear") && $this->year())
        {
            $year_db = Year::model()->findByPk($this->year());
            if($year_db)
            {
                $this->year = $year_db->attributes;
            }
            if($user->checkAccess("changeActiveYear")) 
            {
                $years_db = Year::model()->findAll(array(
                        "order" => "ord ASC",
                        "limit" => 10
                    ));
                foreach ($years_db as $year_db)
                {
                    $this->years[] = array('id' => $year_db->id, 'name' => $year_db->name);
                }
            }
        }
        $this->render('index');
    }
    
    /**
     * This is the action to handle external exceptions.
     */
    public function actionError()
    {
        $this->css_included = Cache::getMinifiedStylesUri();
        if ($error = Yii::app()->errorHandler->error) 
        {
            if (Yii::app()->request->isAjaxRequest)
            {
                $this->error = $error['message'];
            }
            else
            {
                $this->render('error', $error);
            }
        }
    }
    
    public function actionYear($id)
    {
        $user = Yii::app()->user;
        if($user->checkAccess("changeActiveYear")) 
        {
            $year_db = Year::model()->findByPk($id);
            if($year_db)
            {
                Yii::app()->session['year_id'] = $year_db->id;
            }
        }
        $url = Yii::app()->request->getParam("url");
        $this->redirect($url);
    }
    
    /**
    * Displays the login page
    */
    public function actionLogin()
    {
        if(Yii::app()->request->isAjaxRequest)
        {
            if(isset($_POST['username']) && isset($_POST['password']))
            {
                $username = $_POST['username'];
                $password = $_POST['password'];
                $identity = new UserIdentity($username,$password);
                
                if($identity->authenticate())
                {
                    Yii::app()->user->login($identity);
                    $this->success = true;
                }
                else
                {
                    if($identity->errorMessage)
                    {
                        $this->error = $identity->errorMessage;
                    }
                    else
                    {
                        $this->error = "Nombre de usuario o contraseña incorrecto";
                    }
                }
            }
        }
        else
        {
            $this->error = "Solicitud no válida.";
        }
    }
    
    
    public function actionLogout()
    {
            Yii::app()->user->logout();
            $this->redirect(Yii::app()->homeUrl);
    }
   
    public function actionCss($id, $time)
    {
        ob_get_clean();
        
        header('Cache-Control: max-age=9999999,public,');
        header('Pragma:cache');
        header('Date:Tue, 24 Jan 1989 14:04:14 GMT');
        header('Last-Modified, 24 Jan 1989 14:04:14 GMT');
        header('ETag:"'.$time.'"');
        header('Keep-Alive:timeout=5, max=90');
        header('Access-Control-Allow-Origin: *');
        header('Content-Type: text/css');
            
        if(key_exists('HTTP_IF_NONE_MATCH', $_SERVER))
        {
            header("HTTP/1.1 304 Not Modified"); 
        }
        else
        {
            $style_cache = Yii::app()->cache->get($id);
            
            if($style_cache && isset($style_cache['time']) && $style_cache['time']==$time) 
            {
                echo $style_cache['cache'];
            }
            else
            {
                echo Cache::generateMinifiedStyles($id);
            }
        }
        Yii::app()->end();
    }
}